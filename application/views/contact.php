<?php
$this->load->view('includes/header_new');
?>
<!-- Contact Area section -->
<section class="contact-area-02">
<div class="container">
    <div class="row">
        <div class="col-sm-5 contact-info">
            <div class="col-sm-12 contact-title">
                <h2>Contact Info</h2>		
                <!-- <p class="content-sub_p">Welcome to our Website. We are glad to have you around.</p> -->
            </div>
            <div class="col-sm-12 contact-box">
                <div class="row">
                    <div class="col-sm-12 col-md-6 single-address-box">
                        <div class="single-address">
                            <i class="fa fa-phone"></i>
                            <h4>Phone</h4>
                            <p>+968 9355 5657</p>
                        </div>
                    </div>  
                    <div class="col-sm-12 col-md-6  single-address-box">
                        <div class="single-address">
                            <i class="fa fa-envelope"></i>
                            <h4>Email</h4>
                            <p>info@ilamaelearing.com</p>
                        </div>
                    </div> 
                    <div class="col-sm-12 col-md-12 single-address-box">
                        <div class="single-address">
                            <i class="fa fa-map-marker"></i>
                            <h4>Location:</h4>
                            <p>PWRA-163, Kakkanad West, Kochi-30, Kerala, India</p>
                        </div>
                    </div> 
                    <div class="col-sm-12 single-address-box">
                        <ul class="list-unstyled">
                            <li><a target="_blank" href="https://www.facebook.com/groups/729752991192338/"><i class="fa fa-facebook teacher-icon"></i></a></li>
                            <li><a target="_blank" href="https://twitter.com/ElearningIlama"><i class="fa fa-twitter teacher-icon"></i></a></li>
                            <!-- <li><a target="_blank" href=""><i class="fa fa-google-plus teacher-icon"></i></a></li> -->
                            <li><a target="_blank" href="https://www.linkedin.com/in/ilama-elearing-8321861b5/"><i class="fa fa-linkedin teacher-icon"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/ilamaelearning/"><i class="fa fa-instagram teacher-icon"></i></a></li>
                        </ul>	 
                    </div> 
                </div>
            </div>                          	                        
        </div>	

        <div class="col-sm-6  col-sm-offset-1 contact-form">
            <div class="row">
                <div class="col-sm-12 contact-title-btm">
                    <h2>Speak with an Expert</h2>		
                    <p class="content-sub_p">Please submit your information below to be contacted with more information about our software and services.</p>
                </div>
            </div>
            <div class="input-contact-form mgt10">
                <div id="contact">
                <div id="mail-status"></div>                       
                <?php echo form_open_multipart('',array('id' => "contactform", 'name' => "contactform")); ?>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Name" name="name" id="name">
                                <div class="text-red" id="name_error"></div>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <input type="email" class="form-control" placeholder="Email" name="email" id="email">
                                <div class="text-red" id="email_error"></div>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Subject" name="subject" id="subject">
                                <div class="text-red" id="subject_error"></div>
                            </div>
                        </div>	
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="6" placeholder="Message" name="comments" id="comments"></textarea>
                                <div class="text-red" id="comments_error"></div>
                            </div>
                        </div>	
                        <div class="col-sm-12">                                    
                            <div class="full-width">
                                <input value="Submit" type="submit" name="submit" id="submit" onclick="sendMail()">
                            </div>
                        </div>	
                    </div>
                </form>
                </div>
                
                
            </div>
        </div>																
    </div>
</div>
</section>
<!-- ./ End Contact Area section -->

<?php
$this->load->view('includes/footer_new');
?>
<script>
function sendMail() {
    var valid;
    valid = validateContact();
    var csrf = $('[name="csrf_test_name"]').val();
    
    if (valid) {
        jQuery.ajax({
            url: "<?php echo base_url(); ?>contact/send_mail",
            data: 'name=' + $("#name").val() + '&email=' + $("#email").val() + '&subject=' + $("#subject").val() + '&message=' + $("#comments").val() +"&csrf_test_name="+csrf,
            type: "POST",
            success: function (data) {
                $("#mail-status").html(data);
                $("#name").val('');
                $("#email").val('');
                $("#subject").val('');
                $("#comments").val('');
                $("#name_error").html("");
                $("#email_error").html("");
                $("#subject_error").html("");
                $("#comments_error").html("");
            },
            error: function () { }
        });
    }
}

function validateContact() {
    var valid = true;

    if (!$("#name").val()) {
        $("#name_error").html("Enter valid name");
        valid = false;
    }

    if (!$("#email").val()) {
        $("#email_error").html("Enter email id");
        valid = false;
    }

    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!filter.test($("#email").val())) {
        $("#email_error").html("Enter valid email id");
        valid = false;
    }
    if (!$("#subject").val()) {
        $("#subject_error").html("Enter valid subject");
        valid = false;
    }
    if (!$("#comments").val()) {
        $("#comments_error").html("Enter valid Message");
        valid = false;
    }

    return valid;
}

</script>
