<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
     <!-- Main content -->
    <div class="content">

        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                <img src="<?php echo HTTP_IMAGES_PATH;?>slider/1.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="<?php echo HTTP_IMAGES_PATH;?>slider/2.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                <img src="<?php echo HTTP_IMAGES_PATH;?>slider/3.jpg"  class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
            </div>
        </div>

        <!-- Course Categories -->
        <div class="row ptb30 justify-content-center">
                <?php if($categories){
                    foreach($categories as $cat){ 
                            $each = $this->Common_model->get_row('courses',array('category_id' => $cat['id']),'icon_name');        
                ?>
                            <div class="col-sm-12 col-lg-3 col-md-6 pb10">
                                <div class="home-cat-caption">
                                    <?php if($each != '' && file_exists('uploads/courses/small/'.$each)) { ?>
                                        <img class="home-cat img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/<?php echo $each;?>">
                                    <?php } 
                                    else{ ?>
                                        <img class="home-cat img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/default.jpg">
                                    <?php } ?>
                                    <div class="shadow">
                                        <h5><a  class="course-view-link" href="#"><?php echo $cat['name'];?></a></h5>
                                    </div>
                                </div>
                            </div>
                <?php
                    }

                } ?>
        </div>
        <!-- Course categories end -->

        <!-- How it works -->
        <div class="row ptb10 justify-content-center">
            <div class="col-sm-6">
                <video class="home-video" controls>
                    <source src="<?php echo HTTP_ASSETS_PATH;?>videos/home_how_it_works.mp4" type="video/mp4">
                </video>
            </div>
            <div class="col-sm-6">
                <video class="home-video" controls>
                    <source src="<?php echo HTTP_ASSETS_PATH;?>videos/home_how_it_works.mp4" type="video/mp4">
                </video>
            </div>
        </div>
        <!-- How it works end -->

        <!-- Course section -->
        <div class="row ptb30 mlr15 pdlr15 justify-content-left">
                <h5 class="ft25"><?php echo strtoupper($category_title);?></h5>
        </div>
        <div class="row mt-15 pdlr15">
                <?php if(!empty($course_arr)){
                    foreach($course_arr as $each){ 
                ?>
                            <div class="col-sm-12 col-lg-4 col-md-6 pb20">
                                <div class="home-cat-caption mx-auto">
                                    <?php if($each['icon_name'] != '' && file_exists('uploads/courses/small/'.$each['icon_name'])) { ?>
                                        <img class="home-cat img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/<?php echo $each['icon_name'];?>">
                                    <?php } 
                                    else{ ?>
                                        <img class="home-cat img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/default.jpg">
                                    <?php } ?>
                                    <div class="shadow">
                                        <h5><a target="_blank" class="course-view-link" href="<?php echo base_url();?>courses/view_course?id=<?php echo urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>"><?php echo $each['title'];?></a></h5>
                                    </div>
                                </div>
                            </div>
                <?php
                    }

                } ?>
        </div>
        <!-- Course section ends -->

        <!-- Demo section -->
        <div class="row bg-dark mlr3">
                <div class="col-sm-12 col-md-6 col-lg-8">
                    <video class="demo-videos-main" controls>
                        <source src="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $demos[0]['demo_video'];?>">
                    </video>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4 mt30">
                    <div class="row justify-content-center"><h2 class="demo-head">DEMOS</h2></div>
                    <?php for($d = 1; $d < sizeof($demos); $d++){ 
                            if($d %2 == 1) { ?>
                                <div class="row sub-demos-align">
                            <?php } ?>
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <video class="sub-demo" controls>
                                            <source src="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $demos[$d]['demo_video'];?>">
                                        </video>
                                    </div>
                            <?php if($d %2 == 0){ ?>
                                </div>
                            <?php } ?>
                    <?php } ?>
                </div>
        </div>
        <!-- Demo section ends -->

    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
