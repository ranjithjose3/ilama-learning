<?php
$this->load->view('includes/header_home');
?>

<section class="slider-content-bottom">
	<div class="container">
		<div class="row sider-btm-row-inner">
			<div class="col-sm-4 content_body slide_cont_box_01">
				<div class="slider-btm-box btm-item_01">
					<img src="<?php echo HTTP_IMAGES_PATH;?>index/slide-bottom-01.png" alt="" class="btm-item-icon">
					<h3>Professional Teachers</h3>
					<p>Courses are lead by subject matter experts having excellent track records in their career</p>
					<a href="<?php echo base_url();?>tutors">read more<i class="fa fa-long-arrow-right slider-b-btn-icon"></i></a>
				</div>
			</div>

			<div class="col-sm-4 content_body slide_cont_box_02">
				<div class="slider-btm-box btm-item_02">
					<img src="<?php echo HTTP_IMAGES_PATH;?>index/slide-bottom-02.png" alt="" class="btm-item-icon">
					<h3>Learn Anywhere Online</h3>
					<p>Students can join attend classes from any where with a computer having internet connection from any part of the world</p>
					<a href="<?php echo base_url();?>how_it_works">read more<i class="fa fa-long-arrow-right slider-b-btn-icon"></i></a>
				</div>
			</div>

			<div class="col-sm-4 content_body slide_cont_box_03">
				<div class="slider-btm-box btm-item_03">
					<img src="<?php echo HTTP_IMAGES_PATH;?>index/slide-bottom-03.png" alt="" class="btm-item-icon">
					<h3>Certification Programs</h3>
					<p>We conduct certification courses for distance education or non-regular admission courses</p>
					<a href="<?php echo base_url();?>certification_programs">read more<i class="fa fa-long-arrow-right slider-b-btn-icon"></i></a>
				</div>
			</div>

		</div>
	</div>
</section>
<!-- Start Welcome Area section -->
<section class="Welcome-area">
	<div class="container">	
		<div class="row">
			<div class="col-sm-6 Welcome-area-text">
				<div class="row">
					<div class="col-sm-12 section-header-box">
						<div class="section-header section-header-l">
							<h2 class="welcome-header"><span>welcome to</span> <br>iLAMA eLearning</h2>
						</div><!-- ends: .section-header -->
					</div>
				</div>
				<p>iLAMA eLearning is a technology-based education system. iLAMA eLearning system enables students to learn lessons much easier by providing multiple learning methods,  tools and accessories. iLAMA platform connects students and teachers through online live classes. Every iLAMA online class is assisted by a team of subject matter experts and creative visualizers. iLAMA students can participate in online forums and clear doubts pertaining to the respective topics. Students are free to access a lot of supporting learning videos to clear ambiguities if any, by using the Interconnected Self Learning Platform. This enables students to learn their lessons meticulously. iLAMA eLearning enable students to test their acquired knowledge using interactive testing tools. All these four elements provide a promising pathway towards perfect learning.</p>	

				<a href="<?php echo base_url();?>about_us" class="read_more-btn fa fa-long-arrow-right">read more</a>		
			</div><!-- Ends: . -->
			
			<div class="col-sm-6 welcome-img">
				<img src="<?php echo HTTP_IMAGES_PATH;?>index/welcome.png" alt="" class="img-responsive">
			</div><!-- Ends: . -->					
		</div>
	</div>
</section><!-- Ends: . -->
<!-- ./ End Welcome Area section -->

<!--Start Courses Area Section-->
<section class="Courses-area courses">
	<div class="container courses-03">	
		<div class="row">
			<div class="col-sm-12 section-header-box">
				<div class="section-header">
					<h2>Major Courses</h2>
				</div><!-- ends: .section-header -->
			</div>
		</div>

		
            <?php
            if($courses){
                $i=0;
        foreach($courses as $each){
			    $duration_uppcase = strtoupper($each['duration']);
				if(strpos($duration_uppcase, 'MONTHS') !== false) {
					$duration_exp = explode('MONTHS',$duration_uppcase);
					$duration = str_pad(trim($duration_exp[0]), 2, '0', STR_PAD_LEFT);
					$duration_type = 'Months';
				}
				else{
					$date1=date_create($each['start_date']);
					$date2=date_create($each['end_date']);
					$diff=date_diff($date1,$date2);
					$duration = $diff->format("%a");
					$duration_type = 'Days';
				}
                if($i==0){
                    echo '<div class="row courses-r-margin-bottom home-course-list">';
                }
                echo '<div class="col-sm-4 single-courses-box each-cat-course">
                    <div class="single-courses">
                        <a  href="'.base_url().'courses/view_course?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY)).'">
                            <div class="courses-img">';
                            if($each['icon_name'] != '' && file_exists('uploads/courses/medium/'.$each['icon_name'])) 
                                echo '<img src="'.HTTP_UPLOADS_PATH.'courses/medium/'.$each['icon_name'].'" alt="" class="img-responsive">';
                            else
                                echo '<img class="img-responsive" src="'.HTTP_UPLOADS_PATH.'courses/medium/default.jpg">';
                            echo '</div>
                        </a>
                        <div class="courses-price">
                            <ul class="list-unstyled">
                                <li class="courses-teacher"><span class="duration">'.$duration.'</span> <span class="c-author">'.$duration_type.'</span>
                                <div class="duration-base"></div> 
                                </li>
                                <li class="price-red">
                                      <span>₹ '.$each['fee'].'</span>
                                      <div class="base"></div>
                                </li>								
                            </ul>
                        </div>
                        <div class="courses-content">						
                            <h3><a href="'.base_url().'courses/view_course?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY)).'">'.$each['title'].'</a></h3>	
                            <p>'.substr(strip_tags($each['details']),0,50).'</p>
                        </div>
                    </div>
                </div>';
                $i++;
                if($i>=3){
                    echo '</div>';
                    $i=0;
                }
               
                
            }
            if($i>=3 && $i>0){
                echo '</div>';
                $i=0;
            }
        }
        ?>

		


	</div>
</section><!-- Ends: . -->	
<!-- ./ End Courses Area section -->
<!-- Start Video Area Section -->

<section class="video-main-area video">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 video-play-btn">
				<a onclick="open_knowitbetter_modal()" class="video-iframe modal-link"><i class="fa fa-play"></i></a>
			</div>

			<div class="video-content">
				<h2>iLAMA eLearning- Know it better</h2>
			</div><!-- ends: .section-header -->				
		</div>
	</div>
</section>
<!-- ./ End Video Area section -->

<!-- Start News Area section -->
<section class="news-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 section-header-box">
				<div class="section-header">
					<h2>Demo Classes</h2>
				</div><!-- ends: .section-header -->
			</div>
		</div>

		<div class="row">
		<?php echo form_open_multipart(''); ?>
			<?php if($demo_icons){
					foreach($demo_icons as $icon_index => $icon){
						if(isset($demos[$icon_index]) && $demos[$icon_index]['demo_video'] != '' && file_exists('uploads/tutors/demo/'.$demos[$icon_index]['demo_video'])){
							$video_href = HTTP_UPLOADS_PATH."tutors/demo/".$demos[$icon_index]['demo_video'];
						}
						else{
							$video_href = "#";
						}
						if(file_exists('assets/images/demo/'.$icon)){
							$image_href = HTTP_IMAGES_PATH."demo/".$icon;
						}
						else{
							$image_href = HTTP_IMAGES_PATH."demo/default.jpg";
						}
						?>
						<div class="col-sm-4 news-single">
							<div class="news-single-box">
								<div class="news-img-box">
					<a  class="modal-link" <?php if($video_href != "#") { ?> onclick="open_demo_modal('<?php echo $video_href;?>')" <?php } ?>><img src="<?php echo $image_href;?>" alt="" class="img-responsive"></a>
								</div>
							</div>
						</div>
					<?php }
			} ?>
		<?php echo form_close();?>	
		</div>
	</div>
</section>
<section class="instraction-area">
	<div class="container">
		<div class="row inspiration-full-box">
			<div class="col-md-9 section-header-l">
				<h2>Like to become an instructor to work with us</h2>
				<!-- <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</p> -->
			</div>

			<div class="col-md-3">
				<div class="instraction-btn">
					<a href="<?php echo base_url();?>work_with_us" class="">get started now</a>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- ./ End News Area section -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog reply-modal" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">DEMO VIDEO</h4>
			</div>
			<div class="modal-body" id="ViewModalBody1">
			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
	<div class="modal-dialog reply-modal" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel2">KNOW IT BETTER</h4>
			</div>
			<div class="modal-body" id="ViewModalBody2">
				<div class="row">
					<div class="col-sm-12 video-play-btn">
						<video id="video2" class="home-video" controls style="width:100%;padding:50px;">
							<source src="<?php echo HTTP_ASSETS_PATH;?>videos/home_how_it_works.mp4" type="video/mp4">
						</video>
					</div>
		
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>

<?php
$this->load->view('includes/footer_home');
?>
<!-- Revolution Extensions -->
<script>
	function open_demo_modal(video_href){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody1" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>home/demo_video_modal",
            data:"video_href="+video_href+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody1").html(data);
                $( "#myModal" ).modal();
                
            }
        }); 
    }
	$('#myModal').on('shown.bs.modal', function () {
		$('#video1')[0].play();
	})
	$('#myModal').on('hidden.bs.modal', function () {
		$('#video1')[0].pause();
	})

	function open_knowitbetter_modal(){
        $( "#myModal2" ).modal();
    }

	$('#myModal2').on('shown.bs.modal', function () {
		$('#video2')[0].play();
	})
	$('#myModal2').on('hidden.bs.modal', function () {
		$('#video2')[0].pause();
	})
</script>
