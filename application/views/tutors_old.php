<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content inner-pages">
        <div class="container-fluid">
           <h2 class="featured-title">FEATURED TUTORS</h2>
           <?php if($tutors){ ?>
                    <div class="row mt25 pdlr15">
                        <!-- <div class="col-sm-12"> -->
                        <?php foreach($tutors as $each){ ?>

                                    <div class="col-md-3 pdb40">
                                        <?php if($each['profile_pic'] != '' && file_exists('uploads/tutors/small/'.$each['profile_pic'])) { ?>
                                            <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/<?php echo $each['profile_pic'];?>">
                                        <?php } 
                                        else{ ?>
                                            <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/default.png">
                                        <?php } ?>
                                        <div class="caption-tutor shadow">
                                            <h5><?php echo $each['name_title'].' '.strtoupper($each['name']);?></h5>
                                            <h5 class="tutor-degree"><?php echo $each['degrees'];?></h5>
                                            <h5 class="tutor-degree"><?php 
                                                $active_courses = $this->Common_model->get_tutor_active_course($each['course_assigned']);
                                                $act_course = substr($active_courses,0,35);
                                                echo 'Subject: ';
                                                if(strlen(trim($active_courses))<=35)
                                                {
                                                    echo $active_courses; 
                                                }
                                                else
                                                {
                                                    echo $act_course.'..';
                                                }
                                            ?></h5>
                                            <h5 class="tutor-degree"><a target="_blank" class="linkgreen" href="<?php echo base_url().'tutors/view_profile?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>">VIEW PROFILE &nbsp;<i class="fa fa-caret-square-o-right black-text"></i></a></h5>
                                            <h5 class="tutor-degree">
                                                <?php if($each['demo_video'] != '' && file_exists('uploads/tutors/demo/'.$each['demo_video'])){ ?>
                                                        <a target="_blank" class="linkdemo" href="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $each['demo_video'];?>">VIEW DEMO &nbsp;<i class="fa fa-caret-square-o-right black-text"></i></a>
                                                <?php }
                                                else{ ?>
                                                        <a target="_blank" class="linkdemo" href="#">VIEW DEMO &nbsp;<i class="fa fa-caret-square-o-right black-text"></i></a>
                                                <?php } ?>
                                            </h5>
                                        </div>
                                    </div>

                        <?php
                        } ?>
                                <!-- </div> -->
                    </div>

            <?php } ?>
            <br/>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
