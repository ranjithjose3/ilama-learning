<?php
$this->load->view('includes/header_new');
?>
<section class="page" >
    <div class="container privacy-policy">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-heading"><?=$page_title?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">	
                <p class="privpolpara">
                    ilamaeleraning.com offers its registered customers/students the facility to cancel the service based on the following conditions: All the courses we offered at www.ilamaeleraning.com are considered as packaged products. Fee for each course are payable in parts or in full as mentioned in course payment schedule. 
                </p>
                <ul class="privpollist parasublist">
                    <li>Condition1- You are eligible for complete refund for any course you have joined before the starting date of the course. The refund amount will be exempted from the registration fee and financial charges born by the financial agencies. </li>
                    <li>Condition2- You can also claim for refund of fee in between the course period. The refund amount will be calculated based on how many classes has been completed at the time of claiming for refund. </li>
                    <li>Condition3 -You are not eligible for refund of course fee once the course period is over. </li>
                    <li>Condition4 - To complete your return, we require a receipt or proof of purchase you will received by your mail at the time of payment.</li>
                </ul>
            </div>
        </div>	
    </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
