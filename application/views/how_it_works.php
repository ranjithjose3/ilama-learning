<?php
$this->load->view('includes/header_new');
?>
<section>
<div class="container howitworks">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-heading"><?=$page_title?></h1>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 video-play-btn">
        <video class="home-video" controls style="width:100%;padding:50px">
                    <source src="<?php echo HTTP_ASSETS_PATH;?>videos/home_how_it_works.mp4" type="video/mp4">
                </video>
        </div>
			
    </div>
</div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
