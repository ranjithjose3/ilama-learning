<?php
$this->load->view('includes/header_new');
?>
<section class="page" >
    <div class="container privacy-policy">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-heading"><?=$page_title?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">	
                <p class="privpolpara">
                    Fee for each course varies based on the following factors.
                    <ul class="privpollist paralist">
                        <li>Course duration – Fee for courses having long duration is higher than that of short-term courses.</li>
                        <li>Specialization- Course fee may varies based on the specialty of the course.</li>
                        <li>Student participation- The course fee may varies based on the students joined for the courses. Obviously, the course fee may be less for courses having a greater number of students.</li>
                        <li>Teacher preferences- Course fee may vary based on the credentials of the teacher.</li>
                        <li>Promotional factors- Some times course fee may be based on promotional aspects.</li>
                        <li>Training method- Some course may have special training aids such as animation and live experiments. Fee for such courses may higher than that of lecture classes.</li>
                        <li>Course fee may vary to courses for distant education with administration service for university procedures.</li> 
                        <li>Course fee may revise time to time depending up on the demand for the course.</li>
                    </ul>
                    <br/>
                </p>
            </div>
        </div>	
    </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
