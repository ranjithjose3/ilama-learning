<?php
$this->load->view('includes/header_new');
?>
<section class="page" >
    <div class="container ourvision">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-heading"><?=$page_title?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">	
                <p>Our vision is to create a new pathway for easy learning by developing a simplified yet powerful learning system for everyone in demand by combining the proven methods of conventional teaching and the potential of advanced communication and media technologies.<br/><br/>Creating a system which can clear every single doubt of a student is the utmost priority for us. Opening the world of knowledge to everyone in need is the driving force of iLAMA eLearning.</p>
            </div>
        </div>
    </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
