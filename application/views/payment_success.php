<?php
$this->load->view('includes/header_new');


    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

    $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right payments">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>

                <div class="single-courses_v">
                    <div class="single-courses-area">
                        <div class="sidebar-content">
                        
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="courses-features">
                                        <div class="features-hading">
                                            <h3>PAYMENT DETAILS</h3>
                                        </div>
                                        <div class="features-text">
                                        <?php 
                                        if($group_id == '4'){
                                        ?>
                                                <div class="row mt10 pdlr15">
                                                
                                                    <h4>Transaction Success!!</h4>
                                                    <?php 
                                                    $student_id = $this->Common_model->get_row('students_enrolled',array('id' => $trans_details->enrollment_id),'student_id');
                                                    $student_details = $this->Common_model->get_row('students',array('id' => $student_id),'');?>
                                                    <table class="table table-striped">
                                                        <tbody>
                                                            <tr>
                                                                <td>Transaction Id</td>
                                                                <td>:</td>
                                                                <td><?php echo $trans_details->transaction_id;?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Transaction Date</td>
                                                                <td>:</td>
                                                                <td><?php echo date('d/m/Y h:i:s A', strtotime($trans_details->transaction_date));?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Transaction Amount</td>
                                                                <td>:</td>
                                                                <td><?php echo '₹ '.$trans_details->transaction_amount;?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Name</td>
                                                                <td>:</td>
                                                                <td><?php echo $student_details->name;?></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="<?php echo base_url();?>payments" name="reset_add_emp" id="re-submit-emp" class="btn btn-warning"><i class="fa fa-mail-reply"></i> Back</a>
                                                    </div>
                                                </div>
                                        <?php 
                                        } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>

            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>