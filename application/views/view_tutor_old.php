<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content inner-pages">
        <div class="container">
           <div class="row mt25 pdlr25">
               <h2><?php echo strtoupper($details->name_title).' '.strtoupper($details->name);?></h2>
           </div>

           <div class="row">
               <div class="col-sm-3">
                    <div class="col-md-12 justify-content-center pb10">
                        <?php if($details->profile_pic != '' && file_exists('uploads/tutors/medium/'.$details->profile_pic)) { ?>
                            <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/<?php echo $details->profile_pic;?>">
                        <?php } 
                        else{ ?>
                            <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/default.jpg">
                        <?php } ?>
                    </div>

                    <div class="col-md-12 justify-content-left pb10">
                        <h2 class="view-tutor-degrees">Degrees</h2>
                        <p>
                            <?php if($details->degrees != ''){
                                        $degrees_arr = explode(',',$details->degrees);
                                        foreach($degrees_arr as $each){ ?>
                                            <i class="fa fa-graduation-cap viewtut-degcourseicon"></i><?php echo trim($each);?><br/>
                            <?php
                                        }
                            } ?>
                        </p>
                    </div>

                    <div class="col-md-12 justify-content-left pb10">
                        <h2 class="view-tutor-degrees">Subjects</h2>
                        <p>
                            <?php 
                                     $active_courses = $this->Common_model->get_tutor_active_course($details->course_assigned);
                                    if($active_courses){
                                        $active_course_arr = explode(',',$active_courses);
                                        foreach($active_course_arr as $each){ ?>
                                            <i class="fa fa-book viewtut-degcourseicon"></i><?php echo trim($each);?><br/>
                            <?php
                                        }
                            } ?>
                        </p>
                    </div>

                    <div class="col-md-12 justify-content-left pb10">
                        <?php if($details->demo_video != '' && file_exists('uploads/tutors/demo/'.$details->demo_video)){ ?>
                                <a target="_blank" class="btn btn-dark" href="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $details->demo_video;?>">View Demo &nbsp;<i class="fa fa-arrow-right"></i></a>
                        <?php }
                        else{ ?>
                                <a target="_blank" class="btn btn-dark" href="#">View Demo &nbsp;<i class="fa fa-arrow-right"></i></a>
                        <?php } ?>
                    </div>

               </div>

               <div class="col-sm-9">
                   <p><?php echo $details->profile;?></p>
               </div>
           </div>

           <br/>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
