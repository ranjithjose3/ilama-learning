<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
           <div class="row no-margin">
               <div class="col-sm-2">
                    <?php $this->load->view('includes/sidebar');?>
                </div>
                <div class="col-sm-10">
                    <?php if($message != '') { ?>
                        <div class="row mt10 pdlr15">  
                            <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                <?php echo urldecode($message);?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php 
                        if($group_id == '4'){
                            if($active_course != '') {
                                $course_details = $this->Common_model->get_row('courses',array('id' => $active_course->course_id),'');
                                $course_name = 'PAYMENT DETAILS OF COURSE - '.strtoupper($course_details->title);
                                $course_fee = 'TOTAL FEE: Rs/- '.$course_details->fee;

                                $fee_structure = $this->Common_model->get_all_rows('fee_structure',array('course_id' => $active_course->course_id),array('due_date' => 'ASC'));

                                $students_enrolled = $this->Common_model->get_row('students_enrolled',array('student_id' => $id, 'course_id' => $active_course->course_id),'');
                                if($students_enrolled->payment_amount != '')
                                    $paid_arr = explode(',',$students_enrolled->payment_amount);
                                else
                                    $paid_arr = array();
                                $paid_fee = $students_enrolled->paid_fee;
                                $balance_fee = $students_enrolled->balance_fee;
                            }
                            else{
                                $course_name = 'NO ACTIVE COURSE';
                                $fee_structure = $paid_arr = array();
                                $course_fee = $paid_fee = $balance_fee = '';
                            }
                        ?>
                                <div class="row mt10 pdlr15">
                                    <div class="card widthfull">
                                        <div class="card-header bg-secondary whitetext">
                                            <?php echo $course_name;?>
                                        </div>
                                        <div class="card-body">
                                            <?php if($active_course != '') { ?>
                                                <h5 class="card-title"><?php echo $course_fee;?></h5>
                                                <p>
                                                    <?php if($fee_structure){ ?>
                                                        <table class="table table-striped">
                                                            <thead>
                                                                <tr>
                                                                <th scope="col">TERM</th>
                                                                <th scope="col">AMOUNT</th>
                                                                <th scope="col">DUE DATE</th>
                                                                <th scope="col">PAYMENT STATUS</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <?php 
                                                                    $term = 1;
                                                                    $index = 0;
                                                                    foreach($fee_structure as $each) { ?>
                                                                    <tr>
                                                                        <th scope="row">PART - <?php echo $term++;?></th>
                                                                        <td>Rs/- <?php echo $each['amount'];?></td>
                                                                        <td><?php echo date('d/m/Y',strtotime($each['due_date']));?></td>
                                                                        <td><?php 
                                                                                if(isset($paid_arr[$index++]))
                                                                                    echo 'PAID';
                                                                                else
                                                                                    echo 'PENDING';

                                                                            ?>
                                                                        </td>
                                                                    </tr>
                                                                <?php } ?>
                                                            </tbody>
                                                        </table>
                                                        <h5>TOTAL PAID: Rs/-<?php echo $paid_fee;?><br/><br/>TOTAL PENDING: Rs/-<?php echo $balance_fee;?></h5>
                                                    <?php } ?>
                                                </p>
                                            <?php } 
                                                else{
                                                    echo '<h5 class="card-title">No details found!!</h5>';
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                        } 
                        else{ 
                            if($active_course != '') {
                                $active_course_arr = explode(',',$active_course);
                            }
                            else{
                                $active_course_arr = $staff_payments = array();
                            }
                        ?>
                                <div class="row mt10 pdlr15">
                                    <div class="card widthfull">
                                        <div class="card-header bg-secondary whitetext">
                                            PAYMENT DETAILS
                                        </div>
                                        <div class="card-body">
                                            <?php if(!empty($active_course_arr)) { ?>
                                                   
                                                <p>
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                            <th scope="col">#</th>
                                                            <th scope="col">COURSE</th>
                                                            <th scope="col">AMOUNT</th>
                                                            <th scope="col">RELEASED</th>
                                                            <th scope="col">BALANCE</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php 
                                                                $term = 1;
                                                                $total_amount = $total_released = $total_balance = 0;
                                                                foreach($active_course_arr as $each) {
                                                                    
                                                                    $staff_payments = $this->Common_model->get_row('staff_payment',array('tutor_id'=> $id,'course_id' => $each),'');
                                                                ?>
                                                                <tr>
                                                                    <th scope="row"><?php echo $term++;?></th>
                                                                    <td><?php echo $this->Common_model->get_row('courses',array('id' => $each),'title');?></td>
                                                                    <td><?php 
                                                                        if($staff_payments){
                                                                            echo 'Rs/-'.$staff_payments->total;
                                                                            $total_amount += $staff_payments->total;
                                                                        }
                                                                        else
                                                                            echo '-';
                                                                        ?>
                                                                    </td>
                                                                    <td><?php 
                                                                        if($staff_payments){
                                                                            echo 'Rs/-'.$staff_payments->total_released;
                                                                            $total_released += $staff_payments->total_released;
                                                                        }
                                                                        else
                                                                            echo '-';
                                                                        ?>
                                                                    </td>
                                                                    <td><?php 
                                                                        if($staff_payments){
                                                                            echo 'Rs/-'.$staff_payments->balance;
                                                                            $total_balance += $staff_payments->balance;
                                                                        }
                                                                        else
                                                                            echo '-';
                                                                        ?>
                                                                    </td>
                                                                </tr>
                                                            <?php } ?>
                                                        </tbody>
                                                    </table>
                                                    <h5>TOTAL AMOUNT: Rs/-<?php echo $total_amount;?><br/><br/>TOTAL RELEASED: Rs/-<?php echo $total_released;?><br/><br/>TOTAL BALANCE: Rs/-<?php echo $total_balance;?><br/><br/></h5>
                                                </p>
                                            <?php } 
                                                else{
                                                    echo '<h5 class="card-title">No Active subjects!!</h5>';
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                        } ?>
                </div>
          </div>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
