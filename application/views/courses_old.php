<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content inner-pages">
        <div class="container-fluid">
           <h2 class="featured-title">FEATURED COURSES</h2>
           <?php if($course_categories){
                    foreach($course_categories as $each_cat){ 
                        $courses_catwise = $this->Common_model->get_all_rows('courses',array('category_id' => $each_cat['id'],'display_from <= '=> date('Y-m-d'), 'end_date >= ' => date('Y-m-d'),'status' => 'A'),array('end_date' => 'DESC'));
                        if($courses_catwise){ ?>
                            <div class="row mt25">
                                <div class="col-sm-12">
                                    <h5 class="featured-subtitle"><?php echo strtoupper($each_cat['name']);?></h5>
                                </div>
                            </div>
                            <div class="row pdlr15">
                                <!-- <div class="col-sm-12"> -->
                        <?php foreach($courses_catwise as $each){ ?>

                                    <div class="col-md-3 pdb40">
                                        <?php if($each['icon_name'] != '' && file_exists('uploads/courses/small/'.$each['icon_name'])) { ?>
                                            <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/<?php echo $each['icon_name'];?>">
                                        <?php } 
                                        else{ ?>
                                            <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/default.jpg">
                                        <?php } ?>
                                        <div class="caption shadow">
                                            <h5><a target="_blank" class="course-view-link" href="<?php echo base_url();?>courses/view_course?id=<?php echo urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>"><?php echo $each['title'];?></a></h5>
                                        </div>
                                    </div>

                        <?php
                        } ?>
                                <!-- </div> -->
                        </div>

                        <?php 
                        }
             
                    }

            } ?>
            <br/>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
