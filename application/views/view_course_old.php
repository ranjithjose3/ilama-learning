<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper ashbg mt-25">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content inner-pages">
        <div class="container">
           <div class="viewcourse-header">
               <div class="row mt25 pdlr25 justify-content-center">
                    <div class="col-sm-4">
                            <?php if($course_details->icon_name != '' && file_exists('uploads/courses/small/'.$course_details->icon_name)) { ?>
                                <img class="viewcourse-imgthumb img-thumbnail" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/<?php echo $course_details->icon_name;?>">
                            <?php } 
                            else{ ?>
                                <img class="viewcourse-imgthumb" src="<?php echo HTTP_UPLOADS_PATH;?>courses/small/default.jpg">
                            <?php } ?>
                    </div>
                    <div class="col-sm-8">
                          <h2 class="viewcourse-title"><?php echo $course_details->title;?></h2>
                          <h5 class="ft25">Course Duration: <?php echo $course_details->duration;?></h5>  
                    </div>
               </div>
           </div>

           <div class="viewcourse-body">
                <div class="row pdlr25">
                    <div class="col-sm-9">
                            <h5>COURSE OVERVIEW</h5>
                            <p><?php echo $course_details->details;?></p>
                            <br/>
                            <p>
                                <?php if($course_details->course_pdf != '' && file_exists('uploads/courses/pdf/'.$course_details->course_pdf)) { ?>
                                        <a class="btn btn-dark ft12" target="_blank" href="<?php echo HTTP_UPLOADS_PATH;?>courses/pdf/<?php echo $course_details->course_pdf;?>">VIEW&nbsp;&nbsp;COMPLETE&nbsp;&nbsp;TOPIC&nbsp;&nbsp;DETAILS&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                <?php } 
                                else{ ?>
                                        <a class="btn btn-dark ft12" target="_blank" href="#">VIEW COMPLETE TOPIC DETAILS&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                <?php } ?>
                            </p>
                    </div>

                    <div class="col-sm-3">
                            <h5>TEACHERS</h5>
                            <div class="row">
                                <?php 
                                    if(!empty($tutors_assigned)){
                                        foreach($tutors_assigned as $each){ 
                                            ?>
                                            <div class="col-md-12 justify-content-center pdb40">
                                                <?php if($each['profile_pic'] != '' && file_exists('uploads/tutors/small/'.$each['profile_pic'])) { ?>
                                                    <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/<?php echo $each['profile_pic'];?>">
                                                <?php } 
                                                else{ ?>
                                                    <img class="widthfull" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/default.png">
                                                <?php } ?>
                                                <div class="caption-tutor shadow">
                                                    <h5><?php echo $each['name_title'].' '.strtoupper($each['name']);?></h5>
                                                    <h5 class="tutor-degree"><?php echo $each['degrees'];?></h5>
                                                    <h5 class="tutor-degree"><?php 
                                                        $active_courses = $this->Common_model->get_tutor_active_course($each['course_assigned']);
                                                        $act_course = substr($active_courses,0,30);
                                                        echo 'Subject: ';
                                                        if(strlen(trim($active_courses))<=30)
                                                        {
                                                            echo $active_courses; 
                                                        }
                                                        else
                                                        {
                                                            echo $act_course.'..';
                                                        }
                                                    ?></h5>
                                                    <h5 class="tutor-degree"><a target="_blank" class="linkgreen" href="<?php echo base_url().'tutors/view_profile?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>">VIEW PROFILE &nbsp;<i class="fa fa-caret-square-o-right black-text"></i></a></h5>
                                                    <h5 class="tutor-degree">
                                                        <?php if($each['demo_video'] != '' && file_exists('uploads/tutors/demo/'.$each['demo_video'])){ ?>
                                                                <a target="_blank" class="linkdemo" href="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $each['demo_video'];?>">VIEW DEMO &nbsp;<i class="fa fa-caret-square-o-right black-text"></i></a>
                                                        <?php }
                                                        else{ ?>
                                                                <a target="_blank" class="linkdemo" href="#">VIEW DEMO &nbsp;<i class="fa fa-caret-square-o-right black-text"></i></a>
                                                        <?php } ?>
                                                    </h5>
                                                </div>
                                            </div>

                                <?php
                                        }
                                    }
                                ?>
                            </div>
                    </div>
                </div>
            </div>

           <div class="viewcourse-feeheader">
               <div class="row">
                   <div class="col-sm-9">
                        <p>
                            <h5 class="coursefee">Course Fee: Rs/- <?php echo $course_details->fee;?></h5>
                            <h5 class="align-center">
                                <?php if($this->session->userdata('group_id') != '' && $this->session->userdata('group_id') == '4'){
                                    $user_id = $this->session->userdata('user_id');
                                    $student_id = $this->Common_model->get_row('students',array('user_id' => $user_id),'id');
                                    $active_course = $this->Common_model->get_active_enrolled_course($student_id);
                                    if($active_course == ''){ ?>
                                        <a class="btn btn-warning" href="<?php echo base_url();?>courses/register_course?id=<?php echo $encoded_id;?>" onclick="return confirm('Are you sure you want to register for this course? You can only enroll in one course at a time.')">Register Now</a>
                                    <?php }
                                    else{ ?>
                                        <a class="btn btn-warning" href="#" onclick="return alert('You already have an active course. You can only enroll in one course at a time.')">Register Now</a>
                                <?php
                                    }
                                }
                                else{ ?>
                                        <a class="btn btn-warning" href="<?php echo base_url();?>login?pageurl=courses/view_course?id=<?php echo $encoded_id;?>">Register Now</a>
                                <?php
                                } ?>
                            </h5>
                        </p>
                   </div>
                   <div class="col-sm-3 justify-content-center">
                        <p>
                            <h2 class="feestructure">FEE STRUCTURE</h2>
                            <?php if(!empty($fee_arr)){
                                    $i = 1;
                                    foreach($fee_arr as $each){ ?>
                                        <h5 class="fee-part">Part-<?php echo $i++;?> - Rs/-<?php echo $each['amount'];?></h5>
                            <?php
                                    }
                            }?>
                        </p>
                   </div>
               </div>
           </div>
           <br/>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
