<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container-fluid sidebar-padding">
           <div class="row no-margin">
               <div class="col-sm-2">
                    <?php $this->load->view('includes/sidebar');?>
                </div>
                <div class="col-sm-10">


                	<div class="card t mt-2">
  <div class="card-header">
   <h5> Add Forum Thread </h5>
  </div>
  <div class="card-body">
    
                			<?php if ($assigned_courses) {?>
<h5>Course</h5>
                				<form name="thread_add" action="<?=base_url()?>forums/thread_save" method="POST">
                				<div class="form-group">
								 
								    <select id="courseList" class="form-control" name="id">
								      <option value="<?=urlencode(base64_encode('-1_'.ENCRYPTION_KEY))?>"> --Please Select Course -- </option>

								      <?php 


								      foreach($assigned_courses as $row_ac){ 

								      	if($selected_course==$row_ac['id']){$selected='selected';}else{$selected='';}

								      	echo ' <option '.$selected.' value="'.urlencode(base64_encode($row_ac['id'].'_'.ENCRYPTION_KEY)).'">'.$row_ac['title'].'</option>';
								      }

								      ?>
								     
								    </select>
								  </div>

<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
<h5>Title</h5>
<div class="form-group">

<input type="text" name="thread_title" style="width: 100%">
</div>


<h5>Description</h5>
<div class="form-group">
<textarea name="thread_description" style="width: 100%" rows="4" cols="50">
	
</textarea>








                		<?php
                			}

                			?>
   </div>             			

  </div>
  <div class="card-footer text-muted">
   <input type="submit" value="Submit" name="submit" class="btn btn-primary">


    </form>
  </div>
</div>



                	<div class="row">
                		
                			          		


                		

                		</div>
                	</div>

                	</div>



                </div>
          </div>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>


<style type="text/css">
	
	table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
  padding: 10px;
}


</style>