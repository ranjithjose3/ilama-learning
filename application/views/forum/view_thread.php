<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 
    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right">

                <div class="post-1 forum-view">
                    <div class="post_1_area">
                        <div class="row mglr0">
                            <div class="col-sm-12 post_left-side">
                            <?php if($message != '') { ?>
                                <div class="row mglr0">  
                                    <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                        <?php echo urldecode($message);?>
                                    </div>
                                </div>
                            <?php } ?>
                                <div class="row">
                                    <div class="col-sm-12 mgb-25">
                                        <h1 class="page-heading no-transform">
                                            <table>
                                                <tr>
                                                    <td class="threadnamecol"><?php echo $thread_details->title;?></td>
                                                    <td><a class="btn btn-warning" href="<?php echo base_url();?>forums/threads/<?php echo base64_encode($thread_details->course_id.'_'.ENCRYPTION_KEY);?>">THREADS</a></td>
                                                </tr>
                                            </table>
                                        </h1>
                                    </div>
                                    <?php 
                                            $thread_comments = $this->Common_model->get_all_rows('forum_thread_comments',array('thread_id' => $thread_details->id, 'deleted' => '0000-00-00 00:00:00', 'status' => 'A'));
                                            if($thread_comments)
                                                $comment_count = sizeof($thread_comments);
                                            else
                                                $comment_count = 0;
                                    ?>
                                    <div class="row mglr0">
                                        <div class="col-sm-1">
                                            <div class="description-side-left">
                                                <div class="author-img">
                                                    <?php $created_by_details = $this->Common_model->get_row('users',array('id' => $thread_details->created_by),'');
                                                    if($created_by_details->group_id == '3'){
                                                        $creator_det = $this->Common_model->get_row('tutors',array('user_id' => $created_by_details->id),'');
                                                        $creator_name = $creator_det->name;
                                                        $creator_image = $creator_det->profile_pic;
                                                        $creator_folder = 'tutors';
                                                    }
                                                    elseif($created_by_details->group_id == '4'){
                                                        $creator_det = $this->Common_model->get_row('students',array('user_id' => $created_by_details->id),'');
                                                        $creator_name = $creator_det->name;
                                                        $creator_image = $creator_det->profile_pic;
                                                        $creator_folder = 'students';
                                                    }
                                                    else{
                                                        
                                                        $creator_name = $created_by_details->name;
                                                        $creator_image = '';
                                                        $creator_folder = '';
                                                    }
                                                    if($creator_image != '' && file_exists('uploads/'.$creator_folder.'/very_small/'.$creator_image)){
                                                    ?>
                                                        <img src="<?php echo HTTP_UPLOADS_PATH.$creator_folder;?>/very_small/<?php echo $creator_image;?>" alt="" class="img-circle">
                                                    <?php } 
                                                    else{ ?>
                                                        <img src="<?php echo HTTP_UPLOADS_PATH;?>default.jpg" alt="" class="img-circle">
                                                    <?php } ?>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-11">
                                            <h5 class="creator-name"><?php echo $creator_name;?></h5>
                                            <h5 class="created-on"><?php echo date('M d, Y',strtotime($thread_details->created_on)); ?></h5>
                                       
                                          <div class="col-sm-12 pdlr0">
                                                <div class="description-text-right">
                                                    <p class="txt-justify"><?php 
                                                    $description = $thread_details->description;
                                                    echo str_replace("\n","<br />",$description);?></p>
                                                </div>
                                            </div>
                                            
                                            <?php if($thread_details->image != ''){ ?>
                                                <div class="col-sm-12 pdlr0">
                                                    <div class="post-img-box">
                                                        <?php if($thread_details->image != '' && file_exists('uploads/forums/medium/'.$thread_details->image)){ ?>
                                                                <img src="<?php echo HTTP_UPLOADS_PATH;?>forums/medium/<?php echo $thread_details->image;?>" alt="" class="img-responsive">
                                                        <?php }
                                                            else { ?>
                                                                    <img src="<?php echo HTTP_UPLOADS_PATH;?>forums/medium/default.jpg" alt="" class="img-responsive">
                                                        <?php    } ?>
                                                    </div>
                                                </div>
                                            <?php } ?>

                                            <div class="col-sm-12 pdlr0">
                                                <h5 class="comments-count"><?php echo $comment_count.' Comments';?></h5>
                                            </div>
                                        
                                            <div class="col-sm-12 pdlr0">
                                                <div class="leave-comment-box">
                                                    <div class="comment-respond">
                                                        <div class="comment-reply-title">
                                                            <h3>Leave a Comment</h3>
                                                        </div>
                                                        <div class="comment-form">
                                                        <?php echo form_open_multipart(''); ?>
                                                                <div class="row">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="hidden" id="thread_id" name="thread_id" value="<?php echo $thread_details->id;?>">
                                                                            <textarea class="form-control" rows="8" placeholder="Type Your Comments *" name="comment" id="comment"></textarea>
                                                                            <div class="text-red" id="comment_error" ></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-sm-12">                             <div class="col-sm-6 pdl0">
                                                                            <div class="form-group custom-file custom-file-position">
                                                                                <input type="file" class="custom-file-input" id="comment_image" name="comment_image">
                                                                                <label class="custom-file-label" for="comment_image">Comment Image (jpg or png) </label>
                                                                            </div>
                                                                            <div class="text-red" id="comment_image_error"></div>
                                                                        </div>  
                                                                        <div class="col-sm-6 full-width pdr0">
                                                                            <input class="pull-right mgt5" value="Submit" type="submit" onclick="return comment_save()">
                                                                        </div>
                                                                    </div>	
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12 comments">							
                                                <div class="row">
                                                    <h3>Comments</h3>
                                                    <div id="comment_div">
                                                        <?php 
                                                        if($thread_comments) { 
                                                            $main_comments = $this->Common_model->get_all_rows('forum_thread_comments',array('thread_id' => $thread_details->id,'parent_id' => '0','deleted' => '0000-00-00 00:00:00', 'status' => 'A'),array('commented_on' => 'ASC'));

                                                            foreach($main_comments as $each){ 
                                                                $sub_comments = $this->Common_model->get_all_rows('forum_thread_comments',array('thread_id' => $thread_details->id,'parent_id' => $each['id'],'deleted' => '0000-00-00 00:00:00', 'status' => 'A'),array('commented_on' => 'ASC'));

                                                                $commented_on_details = $this->Common_model->get_row('users',array('id' => $each['commented_by']),'');
                                                                if($commented_on_details->group_id == '3'){
                                                                    $commentor_det = $this->Common_model->get_row('tutors',array('user_id' => $commented_on_details->id),'');
                                                                    $commentor_name = $commentor_det->name;
                                                                    $commentor_image = $commentor_det->profile_pic;
                                                                    $commentor_folder = 'tutors';
                                                                }
                                                                elseif($commented_on_details->group_id == '4'){
                                                                    $commentor_det = $this->Common_model->get_row('students',array('user_id' => $commented_on_details->id),'');
                                                                    $commentor_name = $commentor_det->name;
                                                                    $commentor_image = $commentor_det->profile_pic;
                                                                    $commentor_folder = 'students';
                                                                }
                                                                else{
                                                                    
                                                                    $commentor_name = $commented_on_details->name;
                                                                    $commentor_image = '';
                                                                    $commentor_folder = '';
                                                                }
                                                            ?>
                                                            <div class="col-sm-12 comment-single-item">
                                                                <div class="col-sm-1 img-box">
                                                                    <?php 
                                                                    if($commentor_image != '' && file_exists('uploads/'.$commentor_folder.'/very_small/'.$commentor_image)){ ?>
                                                                        <img src="<?php echo HTTP_UPLOADS_PATH.$commentor_folder;?>/very_small/<?php echo $commentor_image;?>" alt="" class="img-circle">
                                                                    <?php } else{ ?>
                                                                        <img src="<?php echo HTTP_UPLOADS_PATH;?>default.jpg" alt="" class="img-circle">
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="col-sm-11 comment-left-bar">
                                                                    <div class="comment-text">
                                                                        <ul class="list-unstyled comment-author-box">
                                                                            <li class="nameli"> <span class="name"><?php echo $commentor_name;?></span> <span>
                                                                            <?php echo date('M d, Y',strtotime($each['commented_on']));?>
                                                                            </span></li>
                                                                        </ul>
                                                                        <p>“<?php echo str_replace("\n","<br/>",$each['comment']);
                                                                        if($each['image'] != '' && file_exists('uploads/forums/comments/'.$each['image'])){ ?>
                                                                            <br/><a class="txt-black" target="blank" href="<?php echo HTTP_UPLOADS_PATH;?>forums/comments/<?php echo $each['image'];?>">Click here to view the image</a>
                                                                        <?php } ?></p>
                                                                        <ul class="list-unstyled comment-author-box">
                                                                            <?php if($this->session->userdata('user_id') == $each['commented_by']){ ?>
                                                                                <li class="reply"><a href="#" onclick="edit(<?php echo "'".$each['id']."'";?>)" >Edit</a></li>
                                                                                
                                                                            <?php } 
                                                                            if(($groupid == '4' && $this->session->userdata('user_id') == $each['commented_by'] && $sub_comments == '') || ($groupid == '3' && ($commented_on_details->group_id == '4' || ($this->session->userdata('user_id') == $each['commented_by'] && $sub_comments == '')))){ 
                                                                            ?>
                                                                                <li class="reply"><a href="#" onclick="del_comment(<?php echo "'".$each['id']."'";?>)" >Delete</a></li>
                                                                            <?php 
                                                                            }?>
                                                                            <li class="reply"><a href="#" onclick="reply(<?php echo "'".$each['id']."'";?>)" >Reply</a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <?php
                                                            if($sub_comments){
                                                                foreach($sub_comments as $sub){
                                                                    $commented_subon_details = $this->Common_model->get_row('users',array('id' => $sub['commented_by']),'');
                                                                    if($commented_subon_details->group_id == '3'){
                                                                        $commentor_subdet = $this->Common_model->get_row('tutors',array('user_id' => $commented_subon_details->id),'');
                                                                        $commentor_subname = $commentor_subdet->name;
                                                                        $commentor_subimage = $commentor_subdet->profile_pic;
                                                                        $commentor_subfolder = 'tutors';
                                                                    }
                                                                    elseif($commented_subon_details->group_id == '4'){
                                                                        $commentor_subdet = $this->Common_model->get_row('students',array('user_id' => $commented_subon_details->id),'');
                                                                        $commentor_subname = $commentor_subdet->name;
                                                                        $commentor_subimage = $commentor_subdet->profile_pic;
                                                                        $commentor_subfolder = 'students';
                                                                    }
                                                                    else{
                                                                        
                                                                        $commentor_subname = $commented_subon_details->name;
                                                                        $commentor_subimage = '';
                                                                        $commentor_subfolder = '';
                                                                    }
                                                                    ?>

                                                                    <div class="col-sm-12 comment-single-item reply_text">
                                                                        <div class="row">
                                                                            <div class="col-sm-1 img-box">
                                                                                <?php if($commentor_subimage != '' && file_exists('uploads/'.$commentor_subfolder.'/very_small/'.$commentor_subimage)){ ?>
                                                                                    <img src="<?php echo HTTP_UPLOADS_PATH.$commentor_subfolder;?>/very_small/<?php echo $commentor_subimage;?>" alt="" class="img-circle">
                                                                                <?php } else{ ?>
                                                                                    <img src="<?php echo HTTP_UPLOADS_PATH;?>default.jpg" alt="" class="img-circle">
                                                                                <?php } ?>
                                                                            </div>
                                                                            <div class="col-sm-11 comment-left-bar">
                                                                                <div class="comment-text">
                                                                                    <ul class="list-unstyled comment-author-box">
                                                                                    <li class="nameli"> <span class="name"><?php echo $commentor_subname;?></span> <span>
                                                                                        <?php echo date('M d, Y',strtotime($sub['commented_on']));?> 
                                                                                        </span></li>
                                                                                    </ul>
                                                                                    <p>“<?php echo str_replace("\n","<br/>",$sub['comment']);
                                                                                        if($sub['image'] != '' && file_exists('uploads/forums/comments/'.$sub['image'])){ ?>
                                                                                            <br/><a class="txt-black" target="blank" href="<?php echo HTTP_UPLOADS_PATH;?>forums/comments/<?php echo $sub['image'];?>">Click here to view the image</a>
                                                                                        <?php } ?></p>
                                                                                        <ul class="list-unstyled comment-author-box">
                                                                                        <?php 
                                                                                        if($this->session->userdata('user_id') == $sub['commented_by']){ ?>
                                                                                            <li class="reply"><a href="#" onclick="edit(<?php echo "'".$sub['id']."'";?>)" >Edit</a></li>
                                                                                        <?php } 
                                                                                        if(($groupid == '4' && $this->session->userdata('user_id') == $sub['commented_by']) || ($groupid == '3' && ($commented_subon_details->group_id == '4' || $this->session->userdata('user_id') == $sub['commented_by']))){?> 
                                                                                            <li class="reply"><a href="#" onclick="del_comment(<?php echo "'".$sub['id']."'";?>)" >Delete</a></li>
                                                                                        <?php }
                                                                                        ?>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                            <?php  }
                                                            }
                                                            
                                                            ?>
                                                        <?php }
                                                        } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>	
                            </div>

                        </div>
                    </div>
                </div>

            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<div class="modal fade" id="ViewModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog reply-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">LEAVE A REPLY</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="ViewModalBody1">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" onclick="validate_reply()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ViewModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog reply-modal" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">EDIT YOUR COMMENT</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="ViewModalBody2">
            
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" onclick="validate_edit()">Save</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<?php
$this->load->view('includes/footer_new');
?>
<script>

    function reply(comment_id){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody1" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>forums/reply_modal",
            data:"comment_id="+comment_id+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody1").html(data);
                $( "#ViewModal1" ).modal();
                
            }
        }); 
    }

    function edit(comment_id){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody2" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>forums/edit_modal",
            data:"comment_id="+comment_id+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody2").html(data);
                $( "#ViewModal2" ).modal();
                
            }
        }); 
    }

    function del_comment(comment_id){
        var csrf = $('[name="csrf_test_name"]').val();
        if (confirm('Are you sure you want to delete this comment?')) {
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>forums/delete_comment",
                data:"comment_id="+comment_id+"&csrf_test_name="+csrf,
                success:function(data){
                    location.href = "<?php echo base_url(); ?>forums/view_thread?id="+data+"&msg=Comment has been deleted&action=alert-success";
                }
            }); 
        }
    }

    function validate_reply(){
        var parent_comment_id = document.getElementById('parent_comment_id').value;
        var thread_id = document.getElementById('thread_id').value;
        var enc_thread_id = document.getElementById('enc_thread_id').value;
        var comment = document.getElementById('reply_comment').value;
        var image_file = $('#reply_comment_image')[0].files[0];
        var csrf = $('[name="csrf_test_name"]').val();
        var error = 0;
        if(comment == ''){
            $("#reply_comment_error").html('Please enter a comment');
            error++;
        }
        else{
            $("#reply_comment_error").html('');
        }

        if(image_file == undefined){
            $("#reply_comment_image_error").html('');
        }
        else{
            var image_type = image_file['type'];
            if(image_type == 'image/jpeg' || image_type == 'image/png'){
                $("#reply_comment_image_error").html('');
            }
            else{
                $("#reply_comment_image_error").html('Please upload a jpeg or png file');
                error++;
                return false;
            }
        }
        
        if(error > 0){
            return false;
        }
       if(error == 0){
            $("#reply_comment_error").html('');
            $("#reply_comment_image_error").html('');
            var formData = new FormData();
            formData.append('comment_image', image_file);
            formData.append('parent_comment_id', parent_comment_id);
            formData.append('comment', comment);
            formData.append('thread_id', thread_id);
            formData.append('csrf_test_name', csrf);
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>forums/reply_comment_save",
                data:formData,
                processData: false,  
                contentType: false,
                success:function(data){
                    if(data == '1'){
                        location.href = "<?php echo base_url(); ?>forums/view_thread?id="+enc_thread_id+"&msg=Reply Comment has been added&action=alert-success";
                        $( "#ViewModal1" ).modal('hide');
                    }
                }
            });
        }
    }

    function validate_edit(){
        var comment_id = document.getElementById('comment_id').value;
        var thread_id = document.getElementById('thread_id').value;
        var enc_thread_id = document.getElementById('enc_thread_id').value;
        var comment = document.getElementById('edit_comment').value;
        var image_file = $('#edit_comment_image')[0].files[0];
        var csrf = $('[name="csrf_test_name"]').val();
        var error = 0;
        if(comment == ''){
            $("#edit_comment_error").html('Please enter a comment');
            error++;
        }
        else{
            $("#edit_comment_error").html('');
        }

        if(image_file == undefined){
            $("#edit_comment_image_error").html('');
        }
        else{
            var image_type = image_file['type'];
            if(image_type == 'image/jpeg' || image_type == 'image/png'){
                $("#edit_comment_image_error").html('');
            }
            else{
                $("#edit_comment_image_error").html('Please upload a jpeg or png file');
                error++;
                return false;
            }
        }
        
        if(error > 0){
            return false;
        }
       if(error == 0){
            $("#edit_comment_error").html('');
            $("#edit_comment_image_error").html('');
            var formData = new FormData();
            formData.append('comment_image', image_file);
            formData.append('comment_id', comment_id);
            formData.append('comment', comment);
            formData.append('thread_id', thread_id);
            formData.append('csrf_test_name', csrf);
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>forums/edit_comment_save",
                data:formData,
                processData: false,  
                contentType: false,
                success:function(data){
                    if(data == '1'){
                        location.href = "<?php echo base_url(); ?>forums/view_thread?id="+enc_thread_id+"&msg=Comment has been edited&action=alert-success";
                        $( "#ViewModal2" ).modal('hide');
                    }
                }
            });
        }
    }


    function comment_save(){
        var thread_id = document.getElementById('thread_id').value;
        var comment = document.getElementById('comment').value;
        var image_file = $('#comment_image')[0].files[0];
        var csrf = $('[name="csrf_test_name"]').val();
        var error = 0;
        if(comment == ''){
            $("#comment_error").html('Please enter a comment');
            error++;
        }
        else{
            $("#comment_error").html('');
        }

        if(image_file == undefined){
            $("#comment_image_error").html('');
        }
        else{
            var image_type = image_file['type'];
            if(image_type == 'image/jpeg' || image_type == 'image/png'){
                $("#comment_image_error").html('');
            }
            else{
                $("#comment_image_error").html('Please upload a jpeg or png file');
                error++;
                return false;
            }
        }
        
        if(error > 0){
            return false;
        }
       if(error == 0){
            $("#comment_error").html('');
            $("#comment_image_error").html('');
            var formData = new FormData();
            formData.append('comment_image', image_file);
            formData.append('thread_id', thread_id);
            formData.append('comment', comment);
            formData.append('csrf_test_name', csrf);
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>forums/comment_save",
                data:formData,
                processData: false,  
                contentType: false,
                success:function(data){
                        $("#comment_div").html(data);
                        $("#comment").val('');
                        return false;
                }
            });
            return false;
        }
    }
</script>

