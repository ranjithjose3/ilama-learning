<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 
    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right">
                <?php if($message != '') { ?>
                    <div class="row mglr0">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>
                <div class="blog_1" id="forum_div">
                    <div class="blog-area forum-list">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h1 class="page-heading">Threads of Course - <?php echo $course_name;?></h1>
                                </div>
                            </div>
                            <div class="row">
                                <?php echo form_open_multipart('forums/thread_add',array('class' => "learnpro-register-form")); ?>
                                    <div class="col-md-12 txt-left">
                                        <input type="hidden" name="course_id" value="<?php echo $course_id;?>">
                                        <input type="submit" class="btn btn-info" value="Add New Thread">
                                        <hr class="thread-hr">
                                    </div>
                                <?php echo form_close();?>
                            </div>
                            <br/>
                            <div class="row mglr0">
                                <div class="col-sm-12 bolg_side-left">
                                    <?php if($threads){
                                        foreach($threads as $each){ 
                                            $thread_comments = $this->Common_model->get_all_rows('forum_thread_comments',array('thread_id' => $each['id'], 'deleted' => '0000-00-00 00:00:00', 'status' => 'A'));
                                            if($thread_comments)
                                                $comment_count = sizeof($thread_comments);
                                            else
                                                $comment_count = 0;
                                            ?>
                                            <div class="col-sm-12 single-item-box">
                                                <div class="single-item">
                                                    <div class="single-text-box">
                                                        <h4><a href="#"><?php echo $each['title'];?></a></h4>
                                                        <div class="button-divs">
                                                            <div class="blog-btn-box bg-view">
                                                                <a href="<?php echo base_url();?>forums/view_thread?id=<?php echo urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>" >View</a>
                                                            </div>
                                                            <?php if($each['created_by'] == $this->session->userdata('user_id')) { ?>
                                                                <div class="blog-btn-box bg-edit">
                                                                    <a href="<?php echo base_url();?>forums/thread_add?id=<?php echo urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>">Edit</a>
                                                                </div>
                                                            <?php }
                                                            $created_group_id = $this->Common_model->get_row('users',array('id' => $each['created_by']),'group_id');
                                                            if(($groupid == '4' && $each['created_by'] == $this->session->userdata('user_id') && $thread_comments == '') || ($groupid == '3' && ( $created_group_id == '4' || ($each['created_by'] == $this->session->userdata('user_id') && $thread_comments == '')))) { ?>
                                                                <div class="blog-btn-box bg-delete">
                                                                    <a href="<?php echo base_url();?>forums/delete_thread?id=<?php echo urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to delete this thread? ')">Delete</a>
                                                                </div>
                                                            <?php } ?>
                                                           
                                                        </div>
                                                        <ul class="list-unstyled">
                                                            <li><a href="#">By <?php echo $this->Common_model->get_row('users',array('id' => $each['created_by']),'name');?></a></li>
                                                            <li><a href="#"><?php echo date('d M, Y',strtotime($each['created_on']));?></a></li>
                                                            <li><a href="#"><?php echo date('h:i:s A',strtotime($each['created_on']));?></a></li>
                                                            <li><a href="#"><?php echo $comment_count;?> comments</a></li>
                                                        </ul>
                                                        <hr class="thread-hr">
                                                    </div>
                                                </div>
                                            </div>
                                    
                                        <?php }
                                    }
                                    else{ ?>
                                            <div class="col-sm-12 single-item-box">
                                                <div class="single-item">
                                                    <div class="single-text-box">
                                                        <h4>No threads found!!</h4>
                                                        <hr class="thread-hr">
                                                    </div>
                                                </div>
                                            </div>
                                    <?php } ?>
                                </div>
                    
                            </div>
                            <div class="row mglr0">
                                <div class="col-md-12 txt-left">
                                    <a type="button" class="btn btn-info" href="<?php echo base_url();?>forums/forum_courses">Forums</a>
                                </div>
                            </div>
					</div>
                </div>
            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
<script>
    function get_details(){
        var course_id = document.getElementById('course_id').value;
        var csrf = $('[name="csrf_test_name"]').val();
        if(course_id != ''){
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>forums/threads",
                data:"course_id="+course_id+"&csrf_test_name="+csrf+"&mode=ajax",
                success:function(data){
                        $("#forum_div").html(data);
                        document.getElementById('add_new_thread').style.display= "block";
                }
            });
        }
        else{
                $("#forum_div").html('');
                document.getElementById('add_new_thread').style.display= "none";
        }
    }
</script>

