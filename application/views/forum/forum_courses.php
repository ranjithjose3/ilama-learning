<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

    $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right act-forum-courses">
                <?php if($message != '') { ?>
                    <div class="row mglr0">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>
                <?php if($groupid == '3'){ ?>
                        <div class="row courses-instuctor mgt0">
                            <div class="col-sm-12">
                                <h3 class="courses-title pdt0">ACTIVE FORUM COURSES</h3>
                                <?php if($active_courses){
                                    $active_course_arr = explode(',',$active_courses);
                                    $i = 1;
                                    foreach($active_course_arr as $each){
                                        $course_details = $this->Common_model->get_row('courses',array('id' => $each),'');
                                        $course_category = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');
                                        if($i%2 == 1){    ?>
                                            <div class="row item-margin">
                                        <?php } ?>
                                                <div class="col-sm-6 instractor-single">
                                                    <div class="instractor-courses-single mgt50">
                                                        <div class="instractor-courses-text">
                                                            <div class="instractor-parson">
                                                                
                                                                <p><a href=""><?php echo strtoupper($course_category);?></p>
                                                            </div>
                                                            <div class="text-bottom">
                                                                <h3><a href="<?php echo base_url();?>forums/threads/<?php echo urlencode(base64_encode($each.'_'.ENCRYPTION_KEY));?>"><?php echo $course_details->title;?></a></h3>
                                                            </div>
                                                        </div>
                                                    </div>                                  
                                                </div>
                                    <?php if($i%2 == 0){ ?>
                                            </div>                        
                                    <?php  } 
                                    $i++; 
                                    }
                                } ?>
                            
                            </div>
                        </div>
                    
                <?php } 
                else {
                ?>
                        <?php if($active_course != '') {
                            $course_details = $this->Common_model->get_row('courses',array('id' => $active_course->course_id),'');
                            $course_name = $course_details->title;
                            $course_category = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');
                            $actcourse = strtoupper($course_name); 
                            if($active_course->status == 'R'){
                            ?>
                             <div class="row mglr0">  
                                <div class="alert alert-danger alert-dismissable widthfull">
                                    Forum links are available only for Admitted students. You have registered for the course and waiting for Admin approval.
                                </div>
                            </div>
                            <?php }
                            if($active_course->status == 'I'){
                                ?>
                                 <div class="row mglr0">  
                                    <div class="alert alert-danger alert-dismissable widthfull">
                                        Forum links are available only for Admitted students. You have been suspended from the course.
                                    </div>
                                </div>
                                <?php } ?>
                           <div class="row courses-instuctor mgt0">
                                <div class="col-sm-12">
                                    <h3 class="courses-title pdt0">ACTIVE FORUM COURSES</h3>
                                    <div class="row item-margin">
                                        <div class="col-sm-6 instractor-single">
                                            <div class="instractor-courses-single mgt50">
                                                <div class="instractor-courses-text">
                                                    <div class="instractor-parson">
                                                        <p><a href=""><?php echo strtoupper($course_category);?></a></p>
                                                    </div>
                                                    <div class="text-bottom">
                                                    <h3><a <?php if($active_course->status == 'A') { ?> href="<?php echo base_url();?>forums/threads/<?php echo urlencode(base64_encode($course_details->id.'_'.ENCRYPTION_KEY));?>" <?php } else { ?> href="#" <?php } ?>><?php echo $actcourse;?></a></h3>
                                                    </div>
                                                    <div class="price">
                                                        <ul class="list-unstyled">
                                                            <li>&nbsp;</li>
                                                            <li><i class="fa fa-bookmark"></i><?php echo $status_arr[$active_course->status];?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                            else{ ?>
                                <div class="row courses-instuctor mgt0">
                                <div class="col-sm-12">
                                    <h3 class="courses-title pdt0">ACTIVE FORUM COURSES - NIL</h3>
                                </div>
                            </div>
                            <?php }
                        ?>
                
                <?php } ?>  
            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
