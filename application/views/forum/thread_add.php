<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 
    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right forum-thread-add">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-heading">Add / Edit Forum Thread</h1>
                    </div>
                </div>
                  <?php echo form_open_multipart('forums/thread_save',array('class' => "learnpro-register-form")); ?>
                      <div class="row">
                          <div class="col-sm-12 col-md-6 col-lg-6">
                              <div class="form-group">
                                  <input type="hidden" name="id" value="<?php echo $id;?>">
                                  <select class="form-control" id="course_id" name="course_id">
                                      <option value="<?php echo $courses->id;?>"><?php echo $courses->title;?></option>
                                  </select>
                              </div>
                          </div>
                          <div class="col-sm-12 col-md-6 col-lg-6">
                            <div class="form-group custom-file ">
                                <div class="text-red-f"><?php echo $file_error;?></div>
                                <?php if($thread_image != '' && file_exists('uploads/forums/medium/'.$thread_image)){ ?>
                                  <div class="text-red-f"><a class="txt-black" target="_blank" href="<?php echo HTTP_UPLOADS_PATH;?>forums/medium/<?php echo $thread_image;?>"><?php echo $thread_image;?></a></div>
                                <?php } ?>
                                <input type="file" class="custom-file-input" id="thread_image" name="thread_image">
                                <label class="custom-file-label" for="thread_image">Thread Image (jpg or png) <br/><span>Min widthxheight: 760 X 350px<span></label>
                            </div>
                          </div>
                          
                      </div>

                      <div class="row">
                          <div class="col-sm-12 col-md-12 col-lg-12">
                              <div class="form-group">
                                  <input type="text" autocomplete="off" class="form-control" placeholder="Thread Title *" name="title" value="<?php echo $title;?>" autofocus>
                                  <?php echo form_error('title');?>
                              </div>
                          </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="10" placeholder="Description" name="description" id="description"><?php echo $description;?></textarea>     <?php echo form_error('description');?>

                            </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-4 txt-left">
                            <div class="form-group register-btn">
                                <button class="btn btn-primary btn-lg" type="submit">  Save Thread </button>
                              
                            </div> 
                        </div>
                      </div>
                  <?php echo form_close();?>
            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>


