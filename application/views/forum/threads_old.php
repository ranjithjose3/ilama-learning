<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<?php   
if(isset($msg)){
    $message = $msg;
}
else
$message ='';       
?>

<!-- Main content -->
<div class="content">
    <div class="container-fluid sidebar-padding">
        <div class="row no-margin">
            <div class="col-sm-2">
                <?php $this->load->view('includes/sidebar');?>
            </div>
            <div class="col-sm-10">

                <div class="card t mt-1">
                    
                    <div class="card-body">
    
                        <div class="row">
                        
                                            
                            <div class="col-md-8 ">

                                <?php if($assigned_courses){?>

                                    <form name="thread_crs" action="" method="GET">

                                        <div class="form-group row">
                                            <label for="staticEmail" class="col-sm-2 col-form-label">Course</label>
                                            <div class="col-sm-10">
                                                <select id="courseList" class="form-control" name="id">
                                                    <option value="<?=urlencode(base64_encode('-1_'.ENCRYPTION_KEY))?>"> --Please Select Course -- </option>

                                                    <?php 


                                                    foreach($assigned_courses as $row_ac){ 

                                                        if($selected_course==$row_ac['id']){$selected='selected';}else{$selected='';}

                                                        echo ' <option '.$selected.' value="'.urlencode(base64_encode($row_ac['id'].'_'.ENCRYPTION_KEY)).'">'.$row_ac['title'].'</option>';
                                                    }

                                                    ?>
                                     
                                                </select>
                                            </div>
                                        </div>
                                    </form>

                                    <?php
                                }

                                ?>
                            
                            </div>                                     
                        </div>
                        <div class="row">                                                                   
                            <div class="col-md-10  ">


                                <?php if($forum_threads){?>
                                    <div class="row">
                                        <div class="    col-12">   
                                    <div class="    float-left"> <h5 class=" float-left">Threads </h5>   </div>   
                                    <div class="    float-right"> <a class="btn btn-secondary btn-sm float-right" href="<?=base_url()?>forums/thread_add">Add New</a>   </div>            
                                    </div>
                                </div>
                                    <div class="row">
                                    <table>
                                        <tr>
                                            <th>#</th>
                                            <th>Thread Title</th>
                                            <th>Action</th>
                                
                                        </tr>

                                        <?php 
                                        $i=1;
                                        foreach($forum_threads as $row_ft){ 
                                            echo '<tr>
                                            <td>'.$i++.'</td><td>'.$row_ft['title'].'</td><td>
                                            <a title="View Thread" href="'.base_url().'forums/forum_thread?id='.urlencode(base64_encode($row_ft['id'].'_'.ENCRYPTION_KEY)).'">
                                            <i class="fa fa-eye"></i>

                                            </a>



                                            </td></tr>';
                                        }

                                        ?>
                                    </table>
                                       </div>
                                    

                                <?php
                                }

                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- /.container-->
    </div>
</div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
<script type="text/javascript">
    $("#courseList").change(function() {
            this.form.submit();
        });


</script>

<style type="text/css">
    
    table {
        border-collapse: collapse;
    }

    table, th, td {
        border: 1px solid black;
        padding: 10px;
    }


</style>