<?php 
		$this->load->view('email_template/email_header',array('email_title' => 'PAYMENT RECEIVED'));
?>
			<tr>
				<td class="email-body" width="100%">
					<table class="email-body_inner" align="left"  cellpadding="0" cellspacing="0">
					<!-- Body content -->
					<tr class="black-text-row">
						<td>
						<h2>DEAR <?php echo $name;?>,</h2>
						<p>We have received your Part <?php echo $part_no;?> installment ( <?php echo $amount;?> ) of Course <?php echo $course.'.';?><br/><br/>Details of payment is as follows:<br/><br/>
						Payment Type: <?php echo $payment_type;?><br/>
						Payment Id: <?php echo $payment_id;?><br/>
						Amount: <?php echo $amount;?><br/>
						Payment Date: <?php echo $trans_date;?><br/>
						</p>
						
						</td>
					</tr>
					</table>
				</td>
			</tr>
<?php 
		$this->load->view('email_template/email_footer');
?>
							