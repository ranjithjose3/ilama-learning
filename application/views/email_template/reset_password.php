<?php 
		$this->load->view('email_template/email_header',array('email_title' => 'RESET PASSWORD'));
?>
			<tr>
				<td class="email-body" width="100%">
					<table class="email-body_inner" align="left" cellpadding="0" cellspacing="0">
					<!-- Body content -->
					<tr>
						<td>
						<h1>Dear <?php echo $name;?>,</h1>
						<p>We have received a forgot password request for the username: <b><?php echo $username;?></b>. Please click on the button to reset your password.</p>
						<!-- Action -->
						<table class="body-action" align="left" cellpadding="0" cellspacing="0">
							<tr>
								<td align="left">
									<div>
									<a target="_blank" href="<?php echo base_url();?>login/reset_password/<?php echo urlencode(base64_encode($uid.'_'.ENCRYPTION_KEY));?>" class="button button--blue">Reset Password</a>
									</div>
								</td>
							</tr>
						</table>
						
						<!-- Sub copy -->
						<table class="body-sub">
							<tr>
							<td>
								<p class="sub">If you’re having trouble clicking the button, copy and paste the URL below into your web browser.
								</p>
								<p class="sub"><a target="_blank" style="word-break: break-all;" href="<<?php echo base_url();?>login/reset_password/<?php echo urlencode(base64_encode($uid.'_'.ENCRYPTION_KEY));?>"><?php echo base_url().'login/reset_password/'.urlencode(base64_encode($uid.'_'.ENCRYPTION_KEY));?></a><br/></p>
							</td>
							</tr>
						</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
<?php 
		$this->load->view('email_template/email_footer');
?>
							