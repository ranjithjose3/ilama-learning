<?php 
		$this->load->view('email_template/email_header',array('email_title' => 'VERIFY EMAIL ADDRESS'));
?>
			<tr>
				<td class="email-body" width="100%">
					<table class="email-body_inner" align="left"  cellpadding="0" cellspacing="0">
					<!-- Body content -->
					<tr>
						<td>
						<h1>Dear <?php echo $name;?>,</h1>
						<p class="textblack">Thanks for signing up with iLAMA eLearning for the course <?php echo $course;?>! <br/>Your username is: <b><?php echo $username;?></b>.
						<br/><br/>Please click on the button provided below to complete your email verification and complete your registration process.</p>
						<!-- Action -->
						<table class="body-action" align="left" width="100%" cellpadding="0" cellspacing="0">
							<tr>
							<td align="left">
								<div>
								<a target="_blank" href="<?php echo base_url();?>registration/verify_email/<?php echo urlencode(base64_encode($uid.'_'.ENCRYPTION_KEY));?>/<?php echo urlencode(base64_encode($course_id.'_'.ENCRYPTION_KEY));?>" class="button button--blue">Verify Email</a>
								</div>
							</td>
							</tr>
						</table>
						<p class="textblack">Please click on the button provided below to complete your payment.</p>
						<table class="body-action" align="left" width="100%" cellpadding="0" cellspacing="0">
							<tr>
							<td align="left">
								<div>
								<a target="_blank" href="<?php echo base_url();?>payments" class="button button--blue">Make Payment</a>
								</div>
							</td>
							</tr>
						</table>

						<!-- Sub copy -->
						<table class="body-sub">
							<tr>
							<td>
								<p class="sub">If you’re having trouble clicking the button, copy and paste the URL below into your web browser.
								</p>
								<p class="sub"><a target="_blank" style="word-break: break-all;" href="<?php echo base_url();?>registration/verify_email/<?php echo urlencode(base64_encode($uid.'_'.ENCRYPTION_KEY));?>"><?php echo base_url().'registration/verify_email/'.urlencode(base64_encode($uid.'_'.ENCRYPTION_KEY));?></a><br/><br/></p>
							</td>
							</tr>
						</table>
						</td>
					</tr>
					</table>
				</td>
			</tr>
<?php 
		$this->load->view('email_template/email_footer');
?>
							