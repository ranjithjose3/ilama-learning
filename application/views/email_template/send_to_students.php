<?php 
		$this->load->view('email_template/email_header',array('email_title' => 'CLASS SCHEDULE'));
?>
			<tr>
				<td class="email-body" width="100%">
					<table class="email-body_inner" align="left" cellpadding="0" cellspacing="0">
					<!-- Body content -->
					<tr class="black-text-row">
						<td>
						<h2>DEAR <?php echo $name;?>,</h2>
						<p>PLEASE MAKE SURE TO ATTEND THE WEBINAR ON<br/><?php echo $start_date;?><br/><?php echo $time;?> IST<br/><br/><?php echo $course.' BY '.$tutor_title.' '.strtoupper($tutor);?><br/><br/>ON <?php echo $topic;?><br/><u><a target="_blank" href="<?php echo $webinar_link;?>" class="black-text" >PLEASE FOLLOW THE FOLLOWING WEBINAR LINK TO ENSURE YOUR PARTICIPATION</a></u><br/><br/><?php echo $message;?><br/><br/><u><a href="<?php echo base_url();?>technical_guidelines" class="black-text" target="_blank">TECHNICAL GUIDELINES LINK </a></u> <br/></p>
						</td>
					</tr>
					</table>
				</td>
			</tr>
<?php 
		$this->load->view('email_template/email_footer');
?>
							