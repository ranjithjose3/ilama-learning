<style>
	.email-footer {
		width: 100%;
		margin: 0 auto;
		padding: 0;
		text-align: left;
		background: #fff;
	}
	.email-footer p {
		color: #000;
	}
	.content-cell {
		padding: 35px;
	}
	p.sub {
		font-size: 12px;
	}
	@media only screen and (max-width: 600px) {
		.email-footer {
				width: 100% !important;
		}
	}
</style>
							<tr>
								<td>
									<table class="email-footer" align="left" width="100%" cellpadding="0" cellspacing="0" >
										<tr>
											<td>
												<p class="textblack"><br/>Thanks,<br>iLAMA eLearning Team</p><br/>
											</td>
										</tr>
										<tr>
											<td>
												<a target="_blank" href="<?php echo base_url();?>"><img src="<?php echo HTTP_ASSETS_PATH;?>images/emailfooter.jpg" style="margin-left:-5px;margin-top:-20px;"></a>
											</td>
										</tr>
									</table>
								</td>
							</tr>
					</table>
				</td>
			</tr>
		</table>
	</body>
</html>