<?php 
		$this->load->view('email_template/email_header',array('email_title' => 'PAYMENT REMINDER'));
?>
			<tr>
				<td class="email-body" width="100%">
					<table class="email-body_inner" align="left"  cellpadding="0" cellspacing="0">
					<!-- Body content -->
					<tr class="black-text-row">
						<td>
						<h2>DEAR <?php echo $name;?>,</h2>
						<p>This is to remind you that the Part <?php echo $part_no;?> installment ( <?php echo $amount;?> ) of Course <?php echo $course;?> is due on <?php echo $due_date;?>. <br/><br/>Please make the payment at your earliest convenience for unhindered classes.<br/></p>
						
						</td>
					</tr>
					</table>
				</td>
			</tr>
<?php 
		$this->load->view('email_template/email_footer');
?>
							