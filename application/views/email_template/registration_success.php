<?php 
		$this->load->view('email_template/email_header',array('email_title' => 'REGISTRATION SUCCESSFULL'));
?>
			<tr>
				<td class="email-body" width="100%">
					<table class="email-body_inner" align="left" cellpadding="0" cellspacing="0">
					<!-- Body content -->
					<tr>
						<td>
						<h1>Dear <?php echo $name;?>,</h1>
						<p>Your email has been verified and successfully completed the registration process for Course <?php echo $course;?>.<br/><br/>You can login with your username : <b><?php echo $username;?></b> and the password provided by you during registration.<br/><br/>
						You can access the forums and class schedules only after admin approval.<br/><br/>
						</p>
						</td>
					</tr>
					</table>
				</td>
			</tr>
<?php 
		$this->load->view('email_template/email_footer');
?>
							