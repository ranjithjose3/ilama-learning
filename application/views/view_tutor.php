<?php
$this->load->view('includes/header_new');
?>
<section class="teacher-prolile-01">
	<div class="container">
		<div class="row">
			<div class="col-sm-3 t-profile-left">
				<div class="teacher-contact">
                <?php if($details->profile_pic != '' && file_exists('uploads/tutors/medium/'.$details->profile_pic)) { ?>                 
                    <img class="img-responsive" alt="" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/<?php echo $details->profile_pic;?>">
                        <?php } 
                        else{ ?>
                            <img class="img-responsive" alt="" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/default.jpg">
						<?php } ?>
						<div class="view-demo-p">
							<?php if($details->demo_video != '' && file_exists('uploads/tutors/demo/'.$details->demo_video)){ ?>
								<button class="view-demo-btn"  onclick="open_demo_modal()">View Demo Video <i class="fa fa-caret-square-o-right"></i></button>

								<!-- Demo Modal -->
								<div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
									<div class="modal-dialog reply-modal" role="document">
										<div class="modal-content">
											<div class="modal-header">
												<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
												<h4 class="modal-title" id="myModalLabel2">DEMO VIDEO</h4>
											</div>
											<div class="modal-body" id="ViewModalBody2">
												<div class="row">
													<div class="col-sm-12 video-play-btn">
														<video id="video2" class="home-video" controls style="width:100%;padding:50px;">
															<source src="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $details->demo_video;?>">
														</video>
													</div>
										
												</div>
											</div>
											<div class="modal-footer">
												<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- Demo Modal -->
							<?php }
								else{ ?>
									<button class="view-demo-btn"  onclick="">View Demo Video <i class="fa fa-caret-square-o-right"></i></button>
								<?php } ?>
								</div>
				</div>
			</div>
			<div class="col-sm-9 t-profile-right">
				<div class="row all-corsess-wapper">
					<div class="col-sm-12">
						<div class="all-courses">
							<h3><?php echo strtoupper($details->name_title).' '.strtoupper($details->name);?></h3>
							<div class="profile__courses__inner">
                                <ul class="profile__courses__list list-unstyled">
                                    <li><i class="fa fa-graduation-cap"></i>Degree:</li>
                                    <li><i class="fa fa-bookmark"></i>My Courses:</li>
                                    <!-- <li><i class="fa fa-caret-square-o-right"></i>Demo Video:</li> -->
                                </ul>
                                <ul class="profile__courses__right list-unstyled">
                                    <li><?php echo $details->degrees;?></li>
                                    <li>
                                        <?php 
                                         $active_courses = $this->Common_model->get_tutor_active_course($details->course_assigned);
                                         if($active_courses!='')
                                            echo $active_courses;
                                         ?></li>
                                </ul>
                            </div>
                            <p><?php echo $details->profile;?></p>
						</div>
					</div>
				</div>

                <div class="row courses-instuctor">
                	<div class="col-sm-12">
                        <h3 class="courses-title">Courses By <?php echo strtoupper($details->name);?></h3>
                        <?php 
                        $course_handled_array = explode(',',$details->course_assigned);
                        $all_handled_courses = $this->Common_model->get_all_rows('courses',array(),array(),'',array('id' =>$course_handled_array));
                        if($all_handled_courses){
                            $i=0;
                       foreach($all_handled_courses as $each){
                           $students = explode(',',$each['students_enrolled']);
                		    if($i==0){
                                echo '<div class="row item-margin">';
                            }
                			echo '<div class="col-sm-6 instractor-single">
								<div class="instractor-courses-single">
									<div class="img-box">';
                                    if($each['icon_name'] != '' && file_exists('uploads/courses/medium/'.$each['icon_name'])) {
                                    	echo '<img src="images/teachars/latest-01.jpg" alt="" class="img-responsive">';
                                    }
                                    else{
                                        echo '<img class="img-responsive" src="'.HTTP_UPLOADS_PATH.'courses/medium/default.jpg">';
                                    }
									echo '</div>
									<div class="instractor-courses-text">';
										echo '<div class="text-bottom" style="min-height:150px;">
											<h3><a href="'.base_url().'courses/view_course?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY)).'">'.$each['title'].'</a></h3>
											<p>'.substr(strip_tags($each['details']),0,50).'</p>
										</div>
									</div>
									<div class="price">
										<ul class="list-unstyled">
											<li><i class="fa fa-user"></i>'.sizeof($students).' Students</li>
											<li>&nbsp;</li>
										</ul>
									</div>
								</div>                  				
                			</div>';
                            $i++;
                		    if($i>=2){
                                echo '</div>';
                                $i=0;
                            }
                            
                        }
                        if($i<2 && $i>0){
                            echo '</div>';
                            $i=0;
                        }
                        } 
                        ?>
                	
                	</div>
                </div>
			</div>															
		</div>
	</div>
</section>

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer_new');
?>
<script>
	function open_demo_modal(){
        $( "#myModal2" ).modal();
    }

	$('#myModal2').on('shown.bs.modal', function () {
		$('#video2')[0].play();
	})
	$('#myModal2').on('hidden.bs.modal', function () {
		$('#video2')[0].pause();
	})
</script>
