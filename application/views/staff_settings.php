<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

    $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right settings">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="courses-features">
                            <div class="features-text">
                                <div class="row mt10 pdlr15">
                                    <div class="panel widthfull">
                                        <div class="panel-header">
                                            CHANGE PASSWORD
                                        </div>
                                        <div class="panel-body">
                                                <?php echo form_open_multipart('settings/staff_password_save'); ?>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                                                <label for="password">Password<span class="text-red"> *</span></label>
                                                                <input autocomplete="off" type="password" class="form-control" id="password" placeholder="Enter Current Password" name="password" value="<?php echo $password;?>">
                                                                <?php echo form_error('password');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="password">New Password<span class="text-red"> *</span></label>
                                                                <input autocomplete="off"  type="password" class="form-control" id="new_password" placeholder="Enter New Password" name="new_password" value="<?php echo $new_password;?>">
                                                                <?php echo form_error('new_password');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="password">Confirm Password<span class="text-red"> *</span></label>
                                                                <input autocomplete="off"  type="password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password" value="<?php echo $confirm_password;?>">
                                                                <?php echo form_error('confirm_password');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>&nbsp;</label><br />
                                                                <input type="submit" class="btn btn-reg" name="submit" value="Change Password" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php echo form_close();?>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="row mt10 pdlr15">
                                    <div class="panel widthfull">
                                        <div class="panel-header">
                                            CHANGE USER DETAILS
                                        </div>
                                        <div class="panel-body">
                                            <?php echo form_open_multipart('settings/staff_details_save'); ?>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                                            <input type="hidden" name="id" value="<?php echo $id;?>">
                                                            <label for="password">Email<span class="text-red"> *</span></label>
                                                            <input autocomplete="off" type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email;?>">
                                                            <?php echo form_error('email');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="password">Contact No<span class="text-red"> *</span></label>
                                                            <input autocomplete="off" type="text" class="form-control" id="mobile" placeholder="Enter Contact No." name="mobile" value="<?php echo $mobile;?>">
                                                            <?php echo form_error('mobile');?>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="password">Address<span class="text-red"> *</span></label>
                                                            <textarea autocomplete="off" class="form-control" id="address" placeholder="Enter Address" name="address"><?php echo $address;?></textarea>
                                                            <?php echo form_error('address');?>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="row justify-content-end">
                                                    <div class="col-md-4">
                                                        <div class="form-group float-right">
                                                            <label>&nbsp;</label><br />
                                                            <input type="submit" class="btn btn-reg" name="submit" value="Change Details" >
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php echo form_close();?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
