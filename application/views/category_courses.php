<?php
$this->load->view('includes/header_new');
?>
<section class="courses-03" id="courses">
    <div class="container">	

   <?php
  // print_r($courses);
  if($courses){
    $i=0;
    foreach($courses as $each){
    
        $duration_uppcase = strtoupper($each['duration']);
        if(strpos($duration_uppcase, 'MONTHS') !== false) {
            $duration_exp = explode('MONTHS',$duration_uppcase);
            $duration = str_pad(trim($duration_exp[0]), 2, '0', STR_PAD_LEFT);
            $duration_type = 'Months';
        }
        else{
            $date1=date_create($each['start_date']);
            $date2=date_create($each['end_date']);
            $diff=date_diff($date1,$date2);
            $duration = $diff->format("%a");
            $duration_type = 'Days';
        }
    if($i==0){
        echo '<div class="row courses-r-margin-bottom home-course-list">';
    }
    echo '<div class="col-sm-4 single-courses-box each-cat-course">
        <div class="single-courses">
            <a  href="'.base_url().'courses/view_course?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY)).'">
                <div class="courses-img">';
                if($each['icon_name'] != '' && file_exists('uploads/courses/medium/'.$each['icon_name'])) 
                    echo '<img src="'.HTTP_UPLOADS_PATH.'courses/medium/'.$each['icon_name'].'" alt="" class="img-responsive">';
                else
                    echo '<img class="img-responsive" src="'.HTTP_UPLOADS_PATH.'courses/medium/default.jpg">';
                echo '</div>
            </a>
            <div class="courses-price">
                <ul class="list-unstyled">
                    <li class="courses-teacher"><span class="duration">'.$duration.'</span> <span class="c-author">'.$duration_type.'</span>
                    <div class="duration-base"></div> 
                    </li>
                    <li class="price-red">
                          <span>₹ '.$each['fee'].'</span>
                          <div class="base"></div>
                    </li>								
                </ul>
            </div>
            <div class="courses-content">						
                <h3><a href="'.base_url().'courses/view_course?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY)).'">'.$each['title'].'</a></h3>	
                <p>'.substr(strip_tags($each['details']),0,50).'</p>
            </div>
        </div>
    </div>';
    $i++;
    if($i>=3){
        echo '</div>';
        $i=0;
    }
   
    
}
if($i>=3 && $i>0){
    echo '</div>';
    $i=0;
}
}
    ?>
        </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
