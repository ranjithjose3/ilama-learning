<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
           <div class="row no-margin">
               <div class="col-sm-2">
                    <?php $this->load->view('includes/sidebar');?>
                </div>
                <div class="col-sm-10">
                    <?php if($message != '') { ?>
                        <div class="row mt10 pdlr15">  
                            <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                <?php echo urldecode($message);?>
                            </div>
                        </div>
                    <?php } ?>

                   
                    <div class="row mt10 pdlr15">
                        <div class="card widthfull">
                            <div class="card-header bg-secondary whitetext">
                                CHANGE PASSWORD
                            </div>
                            <div class="card-body">
                                    <?php echo form_open_multipart('settings/password_save'); ?>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                                    <label for="password">Password<span class="text-red"> *</span></label>
                                                    <input type="password" class="form-control" id="password" placeholder="Enter Current Password" name="password" value="<?php echo $password;?>">
                                                    <?php echo form_error('password');?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="password">New Password<span class="text-red"> *</span></label>
                                                    <input type="password" class="form-control" id="new_password" placeholder="Enter New Password" name="new_password" value="<?php echo $new_password;?>">
                                                    <?php echo form_error('new_password');?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label for="password">Confirm Password<span class="text-red"> *</span></label>
                                                    <input type="password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password" value="<?php echo $confirm_password;?>">
                                                    <?php echo form_error('confirm_password');?>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <label>&nbsp;</label><br />
                                                    <input type="submit" class="btn btn-info" name="submit" value="Change Password" >
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                            </div>
                        </div>
                    </div>

                    <div class="row mt10 pdlr15">
                        <div class="card widthfull">
                            <div class="card-header bg-secondary whitetext">
                                CHANGE USER DETAILS
                            </div>
                            <div class="card-body">
                                    <?php echo form_open_multipart('settings/student_details_save'); ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                                    <input type="hidden" name="id" value="<?php echo $id;?>">
                                                    <label for="password">Email<span class="text-red"> *</span></label>
                                                    <input type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email;?>">
                                                    <?php echo form_error('email');?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="password">Contact No<span class="text-red"> *</span></label>
                                                    <input type="text" class="form-control" id="mobile" placeholder="Enter Contact No." name="mobile" value="<?php echo $mobile;?>">
                                                    <?php echo form_error('mobile');?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="password">Address<span class="text-red"> *</span></label>
                                                    <textarea class="form-control" id="address" placeholder="Enter Address" name="address"><?php echo $address;?></textarea>
                                                    <?php echo form_error('address');?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="password">Parent Email</label>
                                                    <input type="email" class="form-control" id="parent_email" placeholder="Enter Parent email" name="parent_email" value="<?php echo $p_email;?>">
                                                    <?php echo form_error('parent_email');?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="password">Parent Contact No</label>
                                                    <input type="text" class="form-control" id="parent_mobile" placeholder="Enter Parent Contact No." name="parent_mobile" value="<?php echo $p_mobile;?>">
                                                    <?php echo form_error('parent_mobile');?>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label>&nbsp;</label><br />
                                                    <input type="submit" class="btn btn-info" name="submit" value="Change Details" >
                                                </div>
                                            </div>
                                        </div>
                                    <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                       
                </div>
          </div>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
