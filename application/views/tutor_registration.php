<?php
$this->load->view('includes/header_new');
?>
<!-- Teachers Area section -->
<section class="become-teachers-01">
<div class="container">
    
    <div class="row">
        <div class="col-sm-12 text-center become-title">
            <h2>Work With Us</h2>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur</p> -->
            <span class="line"></span>
        </div>
        <div class="col-sm-8 col-sm-offset-2 text-center become-title-text">
            <p>iLAMA eLearning offers extensive opportunities for teachers who have dynamic presentation skills on their topics. iLAMA eLearning is a live classroom platform connecting teachers and students. Those who are interested should be ready to work in unison with the course development team. An expert group of creative designers and visual media experts are ready to assist you in developing the graphical teaching aids for your online classes.</p>
        </div>
    </div>		
    <div class="row">
        <div class="col-sm-12">
            <img src="<?php echo HTTP_IMAGES_PATH;?>workwithus/tutor_reg.jpg" alt="" class="img-responsive">
        </div>												
    </div>
    <div class="row">
        <div class="col-sm-12 text-center become-title become-teacher-title">
            <h2>How to become a teacher</h2>
            <!-- <p>Lorem ipsum dolor sit amet, consectetur</p> -->
            <span class="line"></span>
        </div>
        <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-5">
                    <div class="contact-form text-center">
                        <h3>Register to become a teacher</h3>
                        <!-- <p>You have to <a href="">login</a> to fill out this form</p> -->
                        <div id="mail-status"></div>      
                        <?php echo form_open_multipart(''); ?>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" id="name" name="name" class="form-control"  placeholder="Your Name">
                                        <div class="text-red" id="name_error"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="email" id="email" name="email" class="form-control" placeholder="Your E-mail">
                                        <div class="text-red" id="email_error"></div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="phone" name="phone" placeholder="Your Phone Number">
                                        <div class="text-red" id="phone_error"></div>
                                    </div>
                                </div>		
                                <div class="col-sm-12">                                    
                                    <div class="full-width submit-btn">
                                        <input value="Submit" type="button" onclick="sendMail()">
                                    </div>
                                </div>	
                            </div>
                        <?php echo form_close();?>						
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="tab-wapper">
                        <ul class="nav nav-tabs">
                              <li  class="active"><a href="#become_a_teacher"  data-toggle="tab"><i class="fa fa-user-plus"></i>Be a teacher @iLAMA</a></li>
                              <li ><a href="#roles" data-toggle="tab"><i class="fa fa-list-ol"></i>Teacher Roles</a></li>
                              <!-- <li ><a href="#courses"  data-toggle="tab"><i class="fa fa-book"></i>Start with Courses</a></li> -->
                        </ul>

                        <div class="tab-content">
                              <div class="tab-pane fade in active" id="become_a_teacher">
                                  <p>iLAMA eLearning offers a wide range of academic programmes which include curricula based school subjects from 1 to12 in different modes like State syllabus, CBSE and ICSE;  classes for students appearing with private registration or through NIOS;  degree classes for students registered under distant learning programmes; coaching classes for various entrance examinations(medical/para- medical/agriculture/engineering); special training for competitive examinations for various government and bank jobs; special coaching  for a variety of skill development programmes  and technical training programmes of miscellaneous nature like music, yoga etc.</p>
                                  <p>If you have commendable expertise/excellence in any of the above areas, you are welcome to be a part of  iLAMA eLearning as an approved teacher or mentor. </p>
                              </div>
                              <div class="tab-pane fade" id="roles">
                                  <p>The primary role of a teacher is more or less the same that of regular teaching personnel both online and offline mode. Classes can be scheduled discussing with the concerned Course Coordinators. The second responsibility of a teacher is the development of academic content required for the specific topics of various subjects by supporting/enriching the dedicated visual content development team.</p>
                                  <p>Since iLAMA web platform enables students to interact with teachers for clearing doubts pertaining to  the lessons/concepts, a teacher should be willing for the purpose on demand. Conducting class tests and timely evaluation of papers will also be the responsibility of a teacher.
                                  </p>
                              </div>
                              <?php /* <div class="tab-pane fade" id="courses">
                                  <p>We are Educatios, creat your passion and inspiration. And hope success will come for your dream.  Please send email and get latest news. And hope success will come for your dream. Please send email and get latest news Please send email and get latest news. And hope success will come for your dream. Please send email and get latest news</p>

                                  <p>We are Educatios, creat your passion and inspiration. And hope success will come for your dream. Please send email and get latest news. And hope success will come for your dream. Please send email and get latest news</p>
                              </div> */?>
                        </div>
                      </div>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
<script>
function sendMail() {
    var valid;
    valid = validateContact();
    var csrf = $('[name="csrf_test_name"]').val();
    
    if (valid) {
        jQuery.ajax({
            url: "<?php echo base_url(); ?>work_with_us/tutor_enquiry",
            data: 'name=' + $("#name").val() + '&email=' + $("#email").val() + '&phone=' + $("#phone").val() + '&csrf_test_name='+csrf,
            type: "POST",
            success: function (data) {
                $("#mail-status").html(data);
                $("#name").val('');
                $("#email").val('');
                $("#phone").val('');
                $("#name_error").html("");
                $("#email_error").html("");
                $("#phone_error").html("");
            },
            error: function () { }
        });
    }
}

function validateContact() {
    var valid = true;

    if (!$("#name").val()) {
        $("#name_error").html("Enter valid name");
        valid = false;
    }

    if (!$("#email").val()) {
        $("#email_error").html("Enter email id");
        valid = false;
    }

    var filter = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (!filter.test($("#email").val())) {
        $("#email_error").html("Enter valid email id");
        valid = false;
    }
    if (!$("#phone").val()) {
        $("#phone_error").html("Enter valid phone number");
        valid = false;
    }
    return valid;
}

</script>
