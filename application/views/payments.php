<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

    $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right payments">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>

                <div class="single-courses_v">
                    <div class="single-courses-area">
                        <div class="sidebar-content">

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="courses-features">
                                        <div class="features-hading">
                                            <h3>PAYMENT DETAILS
                                            <?php if($group_id == '4'){
                                                    if($active_course != '') {
                                                        $coursetitle = $this->Common_model->get_row('courses',array('id' => $active_course->course_id),'title');
                                                        echo ' - '.strtoupper($coursetitle);
                                                    }
                                                }
                                            ?>
                                            </h3>
                                        </div>
                                        <div class="features-text">
                                        <?php 
                                        if($group_id == '4'){
                                            if($active_course != '') {
                                                $course_details = $this->Common_model->get_row('courses',array('id' => $active_course->course_id),'');
                                                $course_name = 'PAYMENT DETAILS OF COURSE - '.strtoupper($course_details->title);
                                                $course_fee = $course_details->fee;

                                                $fee_structure = $this->Common_model->get_all_rows('fee_structure',array('course_id' => $active_course->course_id),array('due_date' => 'ASC'));

                                                $students_enrolled = $this->Common_model->get_row('students_enrolled',array('student_id' => $id, 'course_id' => $active_course->course_id),'');
                                                if($students_enrolled->payment_amount != '')
                                                    $paid_arr = explode(',',$students_enrolled->payment_amount);
                                                else
                                                    $paid_arr = array();
                                                $paid_fee = $students_enrolled->paid_fee;
                                                $balance_fee = $students_enrolled->balance_fee;
                                            }
                                            else{
                                                $course_name = 'NO ACTIVE COURSE';
                                                $fee_structure = $paid_arr = array();
                                                $course_fee = $paid_fee = $balance_fee = '';
                                            }
                                        ?>
                                                <div class="row mt10 pdlr15">
                                                
                                                    <?php if($active_course != '') { ?>
                                                       
                                                            <?php if($fee_structure){ ?>
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                        <th scope="col">TERM</th>
                                                                        <th scope="col">AMOUNT</th>
                                                                        <th scope="col">DUE DATE</th>
                                                                        <th scope="col">PAYMENT STATUS</th>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                        <?php 
                                                                            $term = 1;
                                                                            $index = 0;
                                                                            foreach($fee_structure as $each) { ?>
                                                                            <tr>
                                                                                <th scope="row">PART - <?php echo $term++;?></th>
                                                                                <td>Rs/- <?php echo $each['amount'];?></td>
                                                                                <td><?php echo date('d/m/Y',strtotime($each['due_date']));?></td>
                                                                                <td><?php 
                                                                                        if(isset($paid_arr[$index++]))
                                                                                            echo 'PAID';
                                                                                        else{
                                                                                            echo 'PENDING &nbsp;
                                                                                            <a class="btn btn-payment" href="'.base_url().'payments/confirm_payment?cid='.urlencode(base64_encode($active_course->course_id.'_'.ENCRYPTION_KEY)).'&fid='.urlencode(base64_encode($each["id"].'_'.ENCRYPTION_KEY)).'&tid='.urlencode(base64_encode('PART - '.($term - 1).'_'.ENCRYPTION_KEY)).'">Make Payment</a>';
                                                                                            
                                                                                        }

                                                                                    ?>
                                                                                </td>
                                                                            </tr>
                                                                        <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            <?php } ?>
                                                            <div class="row button-divs txtctr">
                                                                <button class="total-button bg-view">&nbsp;&nbsp;&nbsp;&nbsp;TOTAL FEE:&nbsp;&nbsp;&nbsp;&nbsp;<br/><?php echo 'Rs/-'.$course_fee;?></button>
                                                                <button class="total-button bg-edit">&nbsp;&nbsp;TOTAL PAID:&nbsp;&nbsp;<br/><?php echo 'Rs/-'.$paid_fee;?></button>
                                                                <button class="total-button bg-balance">TOTAL PENDING: <br/><?php echo 'Rs/-'.$balance_fee;?></button>
                                                                  
                                                            </div>
                                                    <?php } 
                                                        else{
                                                            echo '<h5 class="card-title">No Active courses!!</h5>';
                                                    } ?>
                                                
                                                
                                                </div>
                                        <?php 
                                        } 
                                        else{ 
                                            if($active_course != '') {
                                                $active_course_arr = explode(',',$active_course);
                                            }
                                            else{
                                                $active_course_arr = $staff_payments = array();
                                            }
                                        ?>
                                                <div class="row mt10 pdlr15">
                                                    <?php if(!empty($active_course_arr)) { ?>
                                                            <table class="table table-striped">
                                                                <thead>
                                                                    <tr>
                                                                    <th scope="col">#</th>
                                                                    <th scope="col">COURSE</th>
                                                                    <th scope="col">AMOUNT</th>
                                                                    <th scope="col">RELEASED</th>
                                                                    <th scope="col">BALANCE</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php 
                                                                        $term = 1;
                                                                        $total_amount = $total_released = $total_balance = 0;
                                                                        foreach($active_course_arr as $each) {
                                                                            
                                                                            $staff_payments = $this->Common_model->get_row('staff_payment',array('tutor_id'=> $id,'course_id' => $each),'');
                                                                        ?>
                                                                        <tr>
                                                                            <th scope="row"><?php echo $term++;?></th>
                                                                            <td><?php echo $this->Common_model->get_row('courses',array('id' => $each),'title');?></td>
                                                                            <td><?php 
                                                                                if($staff_payments){
                                                                                    echo 'Rs/-'.$staff_payments->total;
                                                                                    $total_amount += $staff_payments->total;
                                                                                }
                                                                                else
                                                                                    echo '-';
                                                                                ?>
                                                                            </td>
                                                                            <td><?php 
                                                                                if($staff_payments){
                                                                                    echo 'Rs/-'.$staff_payments->total_released;
                                                                                    $total_released += $staff_payments->total_released;
                                                                                }
                                                                                else
                                                                                    echo '-';
                                                                                ?>
                                                                            </td>
                                                                            <td><?php 
                                                                                if($staff_payments){
                                                                                    echo 'Rs/-'.$staff_payments->balance;
                                                                                    $total_balance += $staff_payments->balance;
                                                                                }
                                                                                else
                                                                                    echo '-';
                                                                                ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                </tbody>
                                                            </table>
                                                            <div class="row button-divs txtctr">
                                                                <button class="total-button bg-view">TOTAL AMOUNT: <br/><?php echo 'Rs/-'.$total_amount;?></button>
                                                                <button class="total-button bg-edit">TOTAL RELEASED: <br/><?php echo 'Rs/-'.$total_released;?></button>
                                                                <button class="total-button bg-balance">TOTAL BALANCE: <br/><?php echo 'Rs/-'.$total_balance;?></button>
                                                            </div>
                                                    <?php } 
                                                        else{
                                                            echo '<h5 class="card-title">No Active courses!!</h5>';
                                                    } ?>
                                                </div>
                                        <?php 
                                        } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>
            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
