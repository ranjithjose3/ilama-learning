<?php
$this->load->view('includes/header_new');
?>
<div class="section-paddings gallery-images event-01">
<div class="container gallery">
    <div class="row">
        <div class="col-sm-12">
            <h1 class="page-heading"><?=$page_title?></h1>
        </div>
    </div>
    <div class="row gallery_img_wrapper">
        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="single-gallery">
                <figure>
                    <img src="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_1.jpg" alt="">
                    <figcaption>
                        <a href="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_1.jpg" title=""><i class="fa fa-eye"></i></a>
                    </figcaption>
                </figure>
            </div>
        </div> <!-- end single-gallery -->

        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="single-gallery">
                <figure>
                    <img src="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_2.jpg" alt="">
                    <figcaption>
                        <a href="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_2.jpg" title=""><i class="fa fa-eye"></i></a>
                    </figcaption>
                </figure>
            </div>
        </div> <!-- end single-gallery -->

        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="single-gallery">
                <figure>
                    <img src="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_3.jpg" alt="">
                    <figcaption>
                        <a href="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_3.jpg" title=""><i class="fa fa-eye"></i></a>
                    </figcaption>
                </figure>
            </div>
        </div> <!-- end single-gallery -->

        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="single-gallery">
                <figure>
                    <img src="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_4.jpg" alt="">
                    <figcaption>
                        <a href="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_4.jpg" title=""><i class="fa fa-eye"></i></a>
                    </figcaption>
                </figure>
            </div>
        </div> <!-- end single-gallery -->

        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="single-gallery">
                <figure>
                    <img src="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_5.jpg" alt="">
                    <figcaption>
                        <a href="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_5.jpg" title=""><i class="fa fa-eye"></i></a>
                    </figcaption>
                </figure>
            </div>
        </div> <!-- end single-gallery -->

        <div class="col-12 col-sm-6 col-md-6 col-lg-4">
            <div class="single-gallery">
                <figure>
                    <img src="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_6.jpg" alt="">
                    <figcaption>
                        <a href="<?php echo HTTP_IMAGES_PATH;?>index-04/gallery_6.jpg" title=""><i class="fa fa-eye"></i></a>
                    </figcaption>
                </figure>
            </div>
        </div> <!-- end single-gallery -->
    </div>
</div>
</div> <!-- incredible place end here -->


<?php
$this->load->view('includes/footer_new');
?>
