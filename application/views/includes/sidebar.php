<div class="sidenav">
  <?php 
    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); ?>

    <div class="row">
        <div class="col-md-12">
            <?php if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/small/'.$profile_pic)){ ?>
                    <img class="fullwidth img-dashboard" src="<?php echo HTTP_UPLOADS_PATH.$table_array[$groupid].'/small/'.$profile_pic;?>">
            <?php }
            else{ ?>
                    <img class="fullwidth img-dashboard" src="<?php echo HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.png';?>">
            <?php } ?>
        </div>
    </div>
    <div class="row mt25">
        <div class="col-md-12">
            <a class="side-href" href="<?=base_url()?>forums/forum_courses"><i class="fa fa-comment"></i>Forums</a>
            <a class="side-href" href="#about"><i class="fa fa-file-text"></i>Documents</a>
            <a class="side-href  <?php if(isset($side_menu) && $side_menu == 'payments') echo 'active';?>" href="<?=base_url()?>payments"><i class="fa fa-inr"></i>Payments</a>
            <a class="side-href <?php if(isset($side_menu) && $side_menu == 'settings') echo 'active';?>" href="<?=base_url()?>settings"><i class="fa fa-gear"></i>Settings</a>
        </div>
    </div>
</div>