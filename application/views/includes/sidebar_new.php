 <?php

        $groupid = $this->session->userdata('group_id');
        $table_array = array('4' => 'students','3' => 'tutors');
        $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
        $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
        $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

        if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

            $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
        }
        else{ 
            $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
            
        }

        $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
 ?>

 <div class="col-sm-3 t-profile-left">
    <div class="teacher-contact">
        <img src="<?=$profile_pic_path?>" alt="" class="img-responsive">
        <h3>Contact Info</h3>
        <p><span>Email:</span> <?php echo $profile->email;?></p>
        <p><span>Phone:</span> <?php if($groupid == '4') echo $profile->mobile; else echo $profile->phone_number;?></p>
        <ul class="list-unstyled">
            <li><a href=""><i class="fa fa-facebook"></i></a></li>
            <li><a href=""><i class="fa fa-twitter "></i></a></li>
            <li><a href=""><i class="fa fa-google-plus"></i></a></li>
            <li><a href=""><i class="fa fa-linkedin"></i></a></li>
        </ul>
        <?php /* <ul class="profile-submenu">
            <li class="<?php if(isset($side_menu) && $side_menu == 'forums') echo 'active';?>"><span><a href="<?=base_url()?>forums/forum_courses"><i class="fa fa-comment"></i>&nbsp;FORUMS</a></span></li>
            <li class="<?php if(isset($side_menu) && $side_menu == 'payments') echo 'active';?>"><span><a  href="<?=base_url()?>payments"><i class="fa fa-inr"></i>&nbsp;PAYMENTS</a></span></li>
            <li class="<?php if(isset($side_menu) && $side_menu == 'settings') echo 'active';?>"><span><a  href="<?=base_url()?>settings"><i class="fa fa-gear"></i>&nbsp;SETTINGS</a></span></li>
        </ul> */ ?>
                        
    </div>
</div>