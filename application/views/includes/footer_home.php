<?php $group_id = ($this->session->userdata('group_id') != '') ? $this->session->userdata('group_id') : '';?>
<!-- Footer Area section -->
<footer>
    <div class="container">
        <div class="row">
            <div class=" col-sm-12 footer-content-box">
                <div class="col-sm-4">
                    <h3><img src="<?php echo HTTP_IMAGES_PATH;?>/logo_ilama.png" alt=""></h3>
                    <p>To know more about the activities and operations of iLAMA eLearning platform 
     please contact via the below details</p>
                    <ul class="list-unstyled">
                        <li><span><i class="fa fa-phone footer-icon"></i></span>+968 9355 5657</li>
                        <li><span><i class="fa fa-envelope footer-icon"></i></span>info@ilamaelearing.com</li>
                        <li><span><i class="fa fa-map-marker footer-icon"></i></span>PWRA-163, Kakkanad West, Kochi-30</li>
                    </ul>
                </div>

                <div class="col-sm-4">
                    <h3>Courses</h3>
                    <ul class="list-unstyled">
                    <?php
                    $category_courses =$this->Courses_model->get_course_count_category_wise();
                    if($category_courses){
                        foreach($category_courses as $cat_row){
                            echo '<li><a ';
                            if($group_id == '3' || $group_id == '4'){  
                                echo 'href="#"'; 
                            } else { 
                                echo 'href="'.base_url().'courses/categories?cat_id='.urlencode(base64_encode($cat_row['category_id'].'_'.ENCRYPTION_KEY)).'"';
                            }
                            echo '><span><i class="fa fa-long-arrow-right footer-icon"></i></span>'.$cat_row['catagory_name'].'</a></li>';
                        }
                       }?>
                    </ul>
                </div>

                <div class="col-sm-4">
                    <h3>Links</h3>
                    <ul class="list-unstyled">
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'our_vision';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Our Vision</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'about_us';?>"<?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>About Us</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'work_with_us';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Work With Us</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'certification_programs';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Certification Programs</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'privacy_policy';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Privacy Policy</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'pricing';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Pricing</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'terms_of_service';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Terms of Service</a></li>
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'cancellation_refund_policy';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Cancellation & Refund Policy</a></li>
                        <!-- <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'gallery';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Gallery</a></li> -->
                        <li><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?> href="<?php echo base_url().'contact';?>" <?php } ?>><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Contact</a></li>
                    </ul>
                </div>

                <!-- <div class="col-sm-2">
                    <h3>Support</h3>
                    <ul class="list-unstyled">
                        <li><a href="#"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Documentation</a></li>
                        <li><a href="#"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Forums</a></li>
                        <li><a href="#"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Language Packs</a></li>
                        <li><a href="#"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Release Status</a></li>
                        <li><a href="#"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Documentation</a></li>
                        <li><a href="#"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Forums</a></li>
                    </ul>
                </div>   -->

                <!-- <div class="col-sm-3">
                    <h3>Get in touch</h3>
                    <p>Enter your email and we'll send you more information.</p>

                    <form action="#">
                        <div class="form-group">
                            <input placeholder="Your Email" type="email" required="" >
                            <div class="submit-btn">
                                <button type="submit" class="text-center">Subscribe</button>
                            </div>
                        </div>
                    </form>
                </div> -->
            </div>
        </div>
    </div>


    <div class="footer-bottom">
        <div class="container">
            <div class="footer-bottom-inner">
                <div class="row">
                    <div class="col-md-6 col-sm-12 footer-no-padding">
                    <p>&copy; Copyright <?php echo date('Y');?> iLAMA eLearning | All rights reserved</p>
                    </div>
                    <div class="col-md-6 col-sm-12 footer-no-padding">
                        <ul class="list-unstyled footer-menu text-right">
                            <li>Follow us:</li>
                            <li><a target="_blank" href="https://www.facebook.com/groups/729752991192338/"><i class="fa fa-facebook facebook"></i></a></li>
                            <li><a target="_blank" href="https://twitter.com/ElearningIlama"><i class="fa fa-twitter twitter"></i></a></li>
                            <li><a target="_blank" href="https://www.instagram.com/ilamaelearning/"><i class="fa fa-instagram instagram"></i></a></li>
                            <!-- <li><a href=""><i class="fa fa-google-plus google"></i></a></li> -->
                            <li><a target="_blank" href="https://www.linkedin.com/in/ilama-elearing-8321861b5/"><i class="fa fa-linkedin linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- ./ End footer-bottom -->     
</footer>
<!-- ./ End Footer Area section -->
    <!-- ============================
    JavaScript Files
    ============================= -->
    
    <!-- Bootstrap JS -->
    <script src="<?php echo HTTP_JS_PATH;?>vendor/jquery-1.12.4.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="<?php echo HTTP_JS_PATH;?>assets/bootstrap.min.js"></script>
	 <!-- owl carousel -->
    <script src="<?php echo HTTP_JS_PATH;?>assets/owl.carousel.min.js"></script>
 	<!-- Revolution Slider -->
	<script src="<?php echo HTTP_JS_PATH;?>assets/revolution/jquery.themepunch.revolution.min.js"></script>
	<script src="<?php echo HTTP_JS_PATH;?>assets/revolution/jquery.themepunch.tools.min.js"></script>   
	<!-- Popup -->
    <script src="<?php echo HTTP_JS_PATH;?>assets/jquery.magnific-popup.min.js"></script>
    <!-- Sticky JS -->
	<script src="<?php echo HTTP_JS_PATH;?>assets/jquery.sticky.js"></script>
	<!-- Counter Up -->
    <script src="<?php echo HTTP_JS_PATH;?>assets/jquery.counterup.min.js"></script>
    <script src="<?php echo HTTP_JS_PATH;?>assets/waypoints.min.js"></script>
   <!-- Slick Slider-->
    <script src="<?php echo HTTP_JS_PATH;?>assets/slick.min.js"></script>
    <!-- Main Menu -->
	<script src="<?php echo HTTP_JS_PATH;?>assets/jquery.meanmenu.min.js"></script>
	<!-- Revolution Extensions -->
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>assets/revolution/revolution.js"></script>
	<!-- Custom JS -->
	<script src="<?php echo HTTP_JS_PATH;?>custom.js"></script>
</body>
</html>
