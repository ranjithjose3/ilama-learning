<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $page_title;?> | <?php echo SITE_TITLE;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
  <!-- Custom CSS-->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css2?family=Red+Rose:wght@300;400;700&display=swap" rel="stylesheet">
</head>

<?php
$main_menu = isset($main_menu) && $main_menu != '' ?  $main_menu :'home'  ;   
$sub_menu1 = isset($sub_menu1) && $sub_menu1 != '' ?  $sub_menu1 :''  ;   
$sub_menu2 = isset($sub_menu2) && $sub_menu2 != '' ?  $sub_menu2 :''  ;
$sub_menu3 = isset($sub_menu3) && $sub_menu3 != '' ?  $sub_menu3 :''  ;
$group_id = $this->session->userdata('group_id') ? $this->session->userdata('group_id') : '';
 ?>	
 <body class="frontbody text-sm">
    <div class="wrapper container-fluid wrap-height">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark navblack">
            <a class="navbar-brand ptb2 navbar-title" href="<?php echo base_url();?>"> <img class="img-responsive dispinline" src="<?php echo HTTP_IMAGES_PATH;?>logo_ilama.png"> e-learning Park</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav mainmenu">
                    <li class="nav-item <?php if($main_menu == 'home') echo 'active';?>">
                        <a class="nav-link" href="<?php echo base_url();?>">Home </span></a>
                    </li>
                    <li class="nav-item <?php if($main_menu == 'courses') echo 'active';?>"><a class="nav-link" href="<?php echo base_url();?>courses">Courses</a></li>
                    <li class="nav-item <?php if($main_menu == 'tutors') echo 'active';?>"><a class="nav-link" href="<?php echo base_url();?>tutors">Tutors</a></li>
                    <?php if($group_id == '' || ($group_id != '3' && $group_id != '4')){ ?>
                        <li class="nav-item <?php if($main_menu == 'register') echo 'active';?>"><a class="nav-link" href="<?php echo base_url();?>registration">Register</a></li>
                        <li class="nav-item <?php if($main_menu == 'login') echo 'active';?>"><a class="nav-link" href="<?php echo base_url();?>login">Login</a></li>
                    <?php }
                    elseif($group_id != '' && ($group_id == '3' || $group_id == '4')){ ?>
                        <li class="nav-item <?php if($main_menu == 'my_account') echo 'active';?>"><a class="nav-link" href="<?php echo base_url();?>my_account">My Account <?php echo ' - '.strtoupper($this->Common_model->get_row('users',array('id' => $this->session->userdata('user_id')),'name'));?></a></li>
                        <li class="nav-item <?php if($main_menu == 'logout') echo 'active';?>"><a class="nav-link" href="<?php echo base_url();?>home/logout">Logout</a></li>
                    <?php } ?>
                </ul>
            </div>
        </nav>