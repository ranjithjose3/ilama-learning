<!doctype html>
<html class="no-js" lang="zxx">
<head>

<?php
$main_menu = isset($main_menu) && $main_menu != '' ?  $main_menu :'home'  ;   
$sub_menu1 = isset($sub_menu1) && $sub_menu1 != '' ?  $sub_menu1 :''  ;   
$sub_menu2 = isset($sub_menu2) && $sub_menu2 != '' ?  $sub_menu2 :''  ;
$sub_menu3 = isset($sub_menu3) && $sub_menu3 != '' ?  $sub_menu3 :''  ;
$group_id = $this->session->userdata('group_id') ? $this->session->userdata('group_id') : '';
 ?> 

    <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="description" content="EduRead - Education HTML5 Template">
  <meta name="keywords" content="college, education, institute, responsive, school, teacher, template, university">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>iLAMA eLearning| <?php echo $page_title;?></title> 
 <!--  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"> -->
 <link rel="icon" type="image/png" href="<?php echo HTTP_IMAGES_PATH;?>favicon.png"/>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>assets/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>croppie.css"> 
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>assets/font-awesome.min.css">
    <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:600,700%7COpen+Sans:400,600" rel="stylesheet">     
  <!-- Popup -->
  <link href="<?php echo HTTP_CSS_PATH;?>assets/magnific-popup.css" rel="stylesheet"> 
  <!-- Slick Slider -->
  <link href="<?php echo HTTP_CSS_PATH;?>assets/slick.css" rel="stylesheet">   
  <link href="<?php echo HTTP_CSS_PATH;?>assets/slick-theme.css" rel="stylesheet">      
  <!-- owl carousel -->
  <link href="<?php echo HTTP_CSS_PATH;?>assets/owl.carousel.css" rel="stylesheet">
  <link href="<?php echo HTTP_CSS_PATH;?>assets/owl.theme.css" rel="stylesheet">
  <!-- Main Menu-->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>assets/meanmenu.css">   
  <!-- Custom CSS -->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>style.css">
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>responsive.css"> 
</head>
<body class="<?=$body_class?>">
<!-- Preloader -->
<div id="preloader">
  <div id="status">&nbsp;</div>
</div>
<header id="header">
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-sm-6 col-xs-12 header-top-left">
          <ul class="list-unstyled">
            <li><i class="fa fa-phone top-icon"></i> +968 9355 5657</li>
            <li><i class="fa fa-envelope top-icon"></i> info@ilamaelearing.com</li>
          </ul>
        </div>
        <div class="col-sm-6 col-xs-12 header-top-right">
          <ul class="list-unstyled">
           
            <?php if($group_id == '' || ($group_id != '3' && $group_id != '4')){ ?>
                        <li><a href="<?php echo base_url();?>registration"><i class="fa fa-user-plus top-icon"></i> Sign up</a></li>
                        <li><a href="<?php echo base_url();?>login"><i class="fa fa-lock top-icon"></i>Login</a></li>
                 
              <?php }
              elseif($group_id != '' && ($group_id == '3' || $group_id == '4')){ ?>

                        <li><a href="<?php echo base_url();?>my_account"><i class="fa fa-user top-icon"></i> My Account <?php echo ' - '.strtoupper($this->Common_model->get_row('users',array('id' => $this->session->userdata('user_id')),'name'));?></a></li>
                        <li><a href="<?php echo base_url();?>home/logout"><i class="fa fa-sign-out top-icon"></i> SignOut</a></li>

                      
                  <?php } ?>


             
                  



          </ul>
        </div>
      </div>
    </div>
  </div><!-- Ends: .header-top -->

  <div class="header-body">
    <nav class="navbar edu-navbar">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
              <a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?>href="<?php echo base_url();?>" <?php } ?> class="navbar-brand  data-scroll"><img src="<?php echo HTTP_IMAGES_PATH;?>/logo_ilama.png" alt=""><span></span></a>
        </div>

        <div class="collapse navbar-collapse edu-nav main-menu" id="bs-example-navbar-collapse-1">
          <ul class="nav navbar-nav pull-right">
          <?php if($group_id != '' && ($group_id == '3' || $group_id == '4')){ ?>

            <li class="<?php if($main_menu == 'forums') echo 'active';?>" ><a data-scroll="" href="<?php echo base_url();?>forums/forum_courses">Forums</a></li>
            <!-- <li class="<?php if($main_menu == 'documents') echo 'active';?>" ><a data-scroll="" href="#">Documents</a></li> -->
            <li class="<?php if($main_menu == 'payments') echo 'active';?>" ><a data-scroll="" href="<?php echo base_url();?>payments">Payments</a></li>
            <li class="<?php if($main_menu == 'settings') echo 'active';?>" ><a data-scroll="" href="<?php echo base_url();?>settings">Settings</a></li>
              
          <?php }
          else{ ?>
              <li class="<?php if($main_menu == 'home') echo 'active';?>" ><a data-scroll="" href="<?php echo base_url();?>">Home</a></li>
              <li class="<?php if($main_menu == 'courses') echo 'active';?>" ><a data-scroll="" href="<?php echo base_url();?>courses">Courses</a></li>
              <li class="<?php if($main_menu == 'tutors') echo 'active';?>" ><a data-scroll="" href="<?php echo base_url();?>tutors">Tutors</a></li>
              <li class="<?php if($main_menu == 'contact') echo 'active';?>"><a data-scroll="" href="<?php echo base_url();?>contact">Contact</a></li>
          <?php } ?>

          <?php /* <li><a data-scroll href="#"><i class="fa fa-search search_btn"></i></a>
            <div id="search">
                <button type="button" class="close">×</button>
               <form>
                   <input type="search" value="" placeholder="Search here...."  required/>
                   <button type="submit" class="btn btn_common blue">Search</button>
               </form>
            </div>
          </li> */ ?>
        </ul>
        </div><!-- /.navbar-collapse -->
      </div><!-- /.container -->
    </nav>
    
    <?php 
    $main_menu_arr = array('home','courses','tutors','contact','certification_programs');
    if(in_array($main_menu,$main_menu_arr)){ ?>
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="intro-text ">
                <h1><?=$page_title?></h1>
                <p><span><a <?php if($group_id == '3' || $group_id == '4'){ ?> href="#" <?php } else { ?>href="<?php echo base_url();?>" <?php } ?>>Home <i class='fa fa-angle-right'></i></a></span><span class="b-active"> <?=$page_title?></span></p>
              </div>
            </div>
          </div><!-- /.row -->
        </div><!-- /.container -->
    <?php } ?>
  </div>
</header>
  <!--  End header section-->