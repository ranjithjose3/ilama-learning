<?php
$this->load->view('includes/header_new');
?>
<!-- Teachers Area section -->
<section class="register-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-12  learnpro-register-form">
                <div class="row">   


                    <?php   
                    if(isset($msg)){
                        $message = $msg;
                    }
                    else
                    $message ='';       
                    ?>



                    <?php echo form_open_multipart('registration/save'); ?>
                    <p class="lead">STUDENT REGISTRATION FORM</p>



                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <h1 class="page-heading stud-reg">STUDENT DETAILS</h1>
                    </div>
                </div>
                <div class="row">

                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">
                            
                            <input autocomplete="off" type="text" class="form-control" id="name" placeholder="Enter Name *" name="name" value="<?php echo $name;?>">
                            <?php echo form_error('name');?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">

                            <input autocomplete="off" type="text" class="form-control" id="email" placeholder="Enter Email Id *" name="email" value="<?php echo $email;?>">
                            <?php echo form_error('email');?>

                        </div>
                    </div>
                    <div class="col-sm-6  col-md-6 col-lg-4">
                        <div class="form-group">
                            <input autocomplete="off" type="password" class="form-control" id="password" placeholder="Enter Password *" name="password" value="<?php echo $password;?>">
                            <?php echo form_error('password');?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">

                            <input autocomplete="off" type="text" class="form-control" id="mobile" placeholder="Enter Mobile Number *" name="mobile" value="<?php echo $mobile;?>">
                            <?php echo form_error('mobile');?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">
                            <input type="hidden" name="courseid" value="<?php echo $courseid;?>">
                            <select class="form-control" id="course_id" placeholder="Select Course *" name="course_id">
                                <?php if($courseid != ''){
                                    $courses = $this->Common_model->get_all_rows('courses',array('id' => $courseid, 'status' => 'A', 'display_from <=' => date('Y-m-d'),'end_date >=' => date('Y-m-d')));
                                }
                                else{
                                    $courses = $this->Common_model->get_all_rows('courses',array('status' => 'A', 'display_from <=' => date('Y-m-d'),'end_date >=' => date('Y-m-d')));?>
                                    <option value="">--select a course--</option>
                                <?php } 
                                    if($courses){
                                        foreach($courses as $each){
                                            $cat_name = $this->Common_model->get_row('categories',array('id' => $each['category_id']),'name');            
                                ?>
                                            <option value="<?php echo $each['id'];?>" <?php if($each['id'] == $course_id) echo 'selected';?>><?php echo $each['title'].' - '.$cat_name;?></option>
                                <?php
                                        }
                                    }
                                ?>
                            

                            </select>
                            <?php echo form_error('course_id');?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <style>
                            .learnpro-register-form .custom-file-input {
                                position: relative;
                                z-index: 2;
                                width: 100%;
                                height: calc(2.25rem + 2px);
                                margin: 0;
                                opacity: 0;


                                min-height: 50px;
                                font-weight: 400;
                                display: block;
                                padding: 12px 15px;
                                font-size: 14px;
                                line-height: 1.428571429;
                                color: #555;
                                background-color: transparent;
                                background-image: none;
                                border-radius: 4px;
                                box-shadow: none;
                                -webkit-box-shadow: none;
                                border: 1px solid #e9edf0;


                            }
                            .learnpro-register-form .custom-file {
                                position: relative;
                                display: inline-block;
                                width: 100%;
                                height: calc(2.25rem + 2px);
                                min-height: 50px;

                                margin-bottom: 15px;
                            }
                            .learnpro-register-form .custom-control-label::before, .custom-file-label, .custom-select {
                                transition: background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
                            }
                            .learnpro-register-form .custom-file-label {
                                position: absolute;
                                top: 0;
                                right: 0;
                                left: 0;
                                z-index: 1;
                                height: calc(2.25rem + 2px);
                                min-height: 50px;
                                padding: .375rem .75rem;
                                font-weight: 400;
                                line-height: 14px;
                                color: #878b8f;
                                background-color: #fff;
                                border: 1px solid #dfe3e7;
                                border-radius: .25rem;
                            }
                            .learnpro-register-form .custom-file-label span{
                                font-size:12px;
                            }
                            .learnpro-register-form label {
                                display: inline-block;
                                margin-bottom: .5rem;
                            }

                            .learnpro-register-form .custom-file-label::after {
                                position: absolute;
                                top: 0;
                                right: 0;
                                bottom: 0;
                                z-index: 3;
                                display: block;
                                height: 2.25rem;
                                min-height: 50px;
                                padding: .375rem .75rem;
                                line-height: 40px;
                                color: #878b8f;
                                content: "Browse";
                                background-color: #e9ecef;
                                border-left: inherit;
                                border-radius: 0 .25rem .25rem 0;
                            }

                            .learnpro-register-form .text-red{
                                font-size: 12px;
                                text-align: right;
                                color: #d53131;
                                padding: 5px;
                                font-style: italic;
                                position: absolute;
                                right: 1.7rem;
                                bottom: 1.7rem;
                                
                            }
                            .learnpro-register-form .text-red-f {
                                font-size: 12px;
                                text-align: right;
                                color: #d53131;
                                padding: 0px;
                                font-style: italic;
                                position: absolute;
                                right: 6.7rem;
                                bottom: 0.1rem;
                                z-index: 10;
                            }
                        </style>
                        <div class="form-group custom-file ">
                             <div class="text-red-f"><?php echo $file_error;?></div>
                             <div id="image_error" class="text-red-f"></div>
                            <input type="file" class="custom-file-input" id="profile_pic" name="profile_pic">
                            <label class="custom-file-label" for="profile_pic">Profile Pic (jpg or png) *<br/><span>Min widthxheight: 260 X 290px)<span></label>
                        </div>
                        <div id="uploaded_image"></div>
                        <input type="hidden" name="profilepic" id="profilepic" value="">
                    </div>

                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">
                            <textarea class="form-control" rows="3" placeholder="Enter Address *" name="address" id="address"><?php echo $address;?></textarea>                         
                         
                            <?php echo form_error('address');?>

                        </div>
                    </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <h1 class="page-heading stud-reg">PARENT DETAILS</h1>
                </div>
            </div>
            <div class="row">
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">

                            <input autocomplete="off" type="text" class="form-control" id="parent_name" placeholder="Enter Parent Name" name="parent_name" value="<?php echo $parent_name;?>">
                            <?php echo form_error('parent_name');?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">
                            <input autocomplete="off" type="text" class="form-control" id="parent_mobile" placeholder="Parent Mobile" name="parent_mobile" value="<?php echo $parent_mobile;?>">
                            <?php echo form_error('parent_mobile');?>

                        </div>
                    </div>
                    <div class="col-sm-6 col-md-6 col-lg-4">
                        <div class="form-group">
                            
                            <input autocomplete="off" type="text" class="form-control" id="parent_email" placeholder="Enter Parent Email" name="parent_email" value="<?php echo $parent_email;?>">
                            <?php echo form_error('parent_email');?>

                        </div>
                    </div>
                   

                    
                    <div class="col-sm-4 col-sm-offset-4">
                        <div class="form-group register-btn">
                            <button class="btn btn-primary btn-lg" type="submit">  Register  </button>
                          
                        </div> 
                    </div>
                    <?php echo form_close();?>



                </div>
                
            </div> 
                                                          
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog reply-modal">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Upload & Crop Image</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
  					<div class="col-md-8 text-center">
						  <div id="image_demo" style="width:350px; margin-top:30px"></div>
  					</div>
  					<div class="col-md-4" style="padding-top:30px;">
  						<br />
  						<br />
  						<br/>
						  <button class="btn btn-success crop_image">Crop & Upload Image</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>


<?php
$this->load->view('includes/footer_new');
?>

<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:260,
      height:290,
    //   type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#profile_pic').on('change', function(){
    var reader = new FileReader();
    var image_type = this.files[0]['type'];
    var image_size = this.files[0]['size'];
    if(image_type == 'image/jpeg' || image_type == 'image/png'){
        if(image_size < 2048000){
            $("#image_error").html('');
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (event) {
                var image = new Image();

                image.src = event.target.result;
                image.onload = function () {

                    var height = this.height;
                    var width = this.width;
                    if (width < 260 || height < 290) {
                        $("#image_error").html('Min widthxheight must be 260x290 px');
                        $("#profile_pic").val('');
                        return false;
                    }
                    else{
                        $image_crop.croppie('bind', {
                            url: event.target.result
                        }).then(function(){
                            console.log('jQuery bind complete');
                        });
                        $('#uploadimageModal').modal('show');
                    }
                };
                
            }
        }
        else{
            $("#image_error").html('Image size should be less than 2 MB');
            $("#profile_pic").val('');
        }
    }
    else{
        $("#image_error").html('Please upload a jpeg or png file');
        $("#profile_pic").val('');
    }
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        var csrf = $('[name="csrf_test_name"]').val();
      $.ajax({
        url:"<?php echo base_url();?>registration/upload",
        type: "POST",
        data:{"image": response,"csrf_test_name":csrf},
        success:function(data)
        {
          var output = JSON.parse(data);
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(output[0]);
          $('#profilepic').val(output[1]);
        }
      });
    })
  });

});  
</script>
