<?php
$this->load->view('includes/header_new');
 if(isset($msg)){
            $message = $msg;
        }
        else
            $message ='';       
    ?>
<style type="text/css">
    .learnpro-register-form.login-r .text-red{
        position:inherit;                            
    }

</style>
<!-- Teachers Area section -->
<section class="login-area">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">

  
            <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>
      <?php echo form_open_multipart('login/change_password'); ?>
                <div class="learnpro-register-form login-r text-center">
                    <p class="lead">Change Password</p>                    
                    <div class="form-group"> 
                        <input type="hidden" name="id" value="<?php echo $id;?>">
                        <input autocomplete="off" class="required form-control" placeholder="Password *" name="password" type="password" required="required">
                         <?php echo form_error('password');?>
                    </div>
                    <div class="form-group">
                        <input autocomplete="off" class="required form-control" placeholder="Confirm Password *" name="confirm_password" type="password" required="required">
                        <?php echo form_error('confirm_password');?>
                    </div>      
                    <div class="form-group register-btn">
                         <button class="btn btn-primary btn-lg" type="submit" <?php if($output == '0')echo 'disabled';?>>Change Password</button>
                    </div>
                </div> 
                </form>
            </div>                                              
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->



<?php
$this->load->view('includes/footer_new');
?>
