<?php
$this->load->view('includes/header_new');
 if(isset($msg)){
            $message = $msg;
        }
        else
            $message ='';       
    ?>
<style type="text/css">
    .learnpro-register-form.login-r .text-red{
        position:inherit;                            
    }

</style>
<!-- Teachers Area section -->
<section class="login-area">
    <div class="container">

        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">

  
            <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>
      <?php echo form_open_multipart('login/verify'); ?>
                <div class="learnpro-register-form login-r text-center">
                    <p>Lost your password? Please enter your username. You will receive a link to create a new password via email.</p>	          
                    <div class="form-group"> 
                        <input autocomplete="off" class="required form-control" placeholder="Username *" name="user_name" type="text" required="required">
                         <?php echo form_error('user_name');?>
                    </div> 
                    <div class="form-group register-btn">
                         <button class="btn btn-primary btn-lg" type="submit">Forgot Password</button>
                        
                    </div>
                </div> 
                </form>
            </div>                                              
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->



<?php
$this->load->view('includes/footer_new');
?>
