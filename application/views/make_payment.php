<?php
$this->load->view('includes/header_new');


    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

    $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
?>

<?php
    $productinfo = $term_id. ' installment of Course - '.$coursetitle;
    $txnid = time();
    $surl = $surl;
    $furl = $furl;        
    $key_id = RAZOR_KEY_ID;
    $currency_code = $currency_code;            
    $total = ($fee_amount* 100); 
    $amount = $fee_amount;
    $merchant_order_id = $enroll_id;
    $card_holder_name = $student_name;
    $email = $student_email;
    $phone = $student_phone;
    $name = SITE_TITLE;
    $return_url = base_url().'payments/callback';
?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right payments">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>

                <div class="single-courses_v">
                    <div class="single-courses-area">
                        <div class="sidebar-content">
                        <?php echo form_open_multipart($return_url, array('name' => "razorpay-form", 'id' => "razorpay-form"))?>
                            <input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id" />
                            <input type="hidden" name="merchant_order_id" id="merchant_order_id" value="<?php echo $merchant_order_id; ?>"/>
                            <input type="hidden" name="merchant_trans_id" id="merchant_trans_id" value="<?php echo $txnid; ?>"/>
                            <input type="hidden" name="merchant_product_info_id" id="merchant_product_info_id" value="<?php echo $productinfo; ?>"/>
                            <input type="hidden" name="merchant_surl_id" id="merchant_surl_id" value="<?php echo $surl; ?>"/>
                            <input type="hidden" name="merchant_furl_id" id="merchant_furl_id" value="<?php echo $furl; ?>"/>
                            <input type="hidden" name="card_holder_name_id" id="card_holder_name_id" value="<?php echo $card_holder_name; ?>"/>
                            <input type="hidden" name="merchant_total" id="merchant_total" value="<?php echo $total; ?>"/>
                            <input type="hidden" name="merchant_amount" id="merchant_amount" value="<?php echo $amount; ?>"/>
                        </form>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="courses-features">
                                        <div class="features-hading">
                                            <h3>MAKE PAYMENT FOR 
                                            <?php echo ' - '.strtoupper($coursetitle);?>
                                            </h3>
                                        </div>
                                        <div class="features-text">
                                        <?php 
                                        if($group_id == '4'){
                                        ?>
                                                <div class="row mt10 pdlr15">
                                                
                                                    <p>Dear <?php echo $student_name;?>,<br/><br/>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Please click on the confirm button to pay an amount of <b>₹<?php echo $fee_amount;?></b> as <?php echo $term_id;?> installment.<br/><br/></p>  
                                                    <input type="hidden"  id="total_amount" value="<?php echo ($fee_amount * 100);?>">         
                                                    <input type="hidden"  id="enroll_id" value="<?php echo $enroll_id;?>"> 
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <a href="<?php echo base_url();?>payments" name="reset_add_emp" id="re-submit-emp" class="btn btn-warning"><i class="fa fa-mail-reply"></i> Back</a>
                                                        <input  id="submit-pay" type="submit" onclick="razorpaySubmit(this);" value="Pay Now" class="btn btn-primary" />
                                                    </div>
                                                </div>
                                        <?php 
                                        } ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="courses-features">
                                        <div class="features-hading pay-opt">
                                            <h3>Payment options</h3>
                                        </div>
                                        <div class="features-text pdt10">
                                            <div class="row pdlr15">
                                                    <b>1. Direct bank payment via bank</b><br/>
                                                    <p class="direct-pay">You can pay the course fee through your bank account or remit through any banks to the account of SOL CONNECT N CONSULT and send the details to info@ilamaelearning.com</p>

                                                    <p class="acct-details"><b>Account details: </b><br/>
                                                    A/C No: 50200044389794<br/>
                                                    HDFC Bank, Vazhakala Branch, Chembumukku, Kochi. <br/>
                                                    IFSC- HDFC0001508</p><br/>

                                                    <b>2. Pay online</b>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
                    </div>
                </div>

            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
  var csrf = $('[name="csrf_test_name"]').val();
  var razorpay_options = {
    key: "<?php echo $key_id; ?>",
    amount: "<?php echo $total; ?>",
    name: "<?php echo $name; ?>",
    description: "Payment # <?php echo $merchant_order_id; ?>",
    netbanking: true,
    currency: "<?php echo $currency_code; ?>",
    csrf_test_name: csrf,
    prefill: {
      name:"<?php echo $card_holder_name; ?>",
      email: "<?php echo $email; ?>",
      contact: "<?php echo $phone; ?>"
    },
    notes: {
      soolegal_order_id: "<?php echo $merchant_order_id; ?>",
    },
    handler: function (transaction) {
        document.getElementById('razorpay_payment_id').value = transaction.razorpay_payment_id;
        document.getElementById('razorpay-form').submit();
    },
    "modal": {
        "ondismiss": function(){
            location.reload()
        }
    }
  };
  var razorpay_submit_btn, razorpay_instance;
 
  function razorpaySubmit(el){
    if(typeof Razorpay == 'undefined'){
      setTimeout(razorpaySubmit, 200);
      if(!razorpay_submit_btn && el){
        razorpay_submit_btn = el;
        el.disabled = true;
        el.value = 'Please wait...';  
      }
    } else {
      if(!razorpay_instance){
        razorpay_instance = new Razorpay(razorpay_options);
        if(razorpay_submit_btn){
          razorpay_submit_btn.disabled = false;
          razorpay_submit_btn.value = "Pay Now";
        }
      }
      razorpay_instance.open();
    }
  }  
</script>