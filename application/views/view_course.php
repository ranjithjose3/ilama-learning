<?php
$this->load->view('includes/header_new');
?>
<?php $user_id = $this->session->userdata('user_id');  ?>
<div class="single-courses-area">
	<div class="container">
		<div class="row">
			<div class="col-sm-8 sidebar-left">
				<div class="single-curses-contert">
					<h2><?php echo strtoupper($course_details->title);?></h2>
					<div class="review-option">
						<div class="row">
							
							<div class="col-sm-3 col-xm-12  categories border-left  review-item-singel">
								<span>Category</span>
								<span><?php echo $category_name;?></span>
							</div>
                            <div class="col-sm-3 col-xm-12 students_105 border-left  review-item-singel">
								<span>Duration</span>
								<span><?php echo $course_details->duration;?></span>
							</div>
							<div class="col-sm-3 col-xm-12 students_105 border-left  review-item-singel">
								<span>Students</span>
								<span><?php echo $student_count;?></span>
                            </div>
                            <div class="col-sm-3 col-xm-12  review-rank border-left  review-item-singel">
								<span>Reviews</span>
								<div class="rank-icons">
									<ul class="list-unstyled">
										<li><i class="fa fa-star review-icon"></i></li>
										<li><i class="fa fa-star review-icon"></i></li>
										<li><i class="fa fa-star review-icon"></i></li>
										<li><i class="fa fa-star review-icon"></i></li>
										<li><i class="fa fa-star review-icon"></i></li>
										<li><span>(8 Reviews)</span></li>
									</ul>
								</div>
							</div>

						</div>
					</div>
					<div class="details-img-bxo">
                    <?php if($course_details->icon_name != '' && file_exists('uploads/courses/big/'.$course_details->icon_name)) { ?>
                                <img class="img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>courses/big/<?php echo $course_details->icon_name;?>">
                            <?php } 
                            else{ ?>
                                <img class="img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>courses/big/default.jpg">
                            <?php } ?>
					</div>
					<div class="description-content">
						<h2>Course Summary: </h2>
                        <?php echo $course_details->details;?>
                        <p><?php if($course_details->course_pdf != '' && file_exists('uploads/courses/pdf/'.$course_details->course_pdf)) { ?>
                                        <a class="btn btn-dark ft12" target="_blank" href="<?php echo HTTP_UPLOADS_PATH;?>courses/pdf/<?php echo $course_details->course_pdf;?>">VIEW&nbsp;&nbsp;COMPLETE&nbsp;&nbsp;TOPIC&nbsp;&nbsp;DETAILS&nbsp;&nbsp;<i class="fa fa-arrow-right"></i></a>
                                <?php } ?>
                            </p>
					</div>

					<div class="curriculum-text-box">
						<h2>Tutors Details: </h2>
						<div class="curriculum-section">
							<div class="panel-group" id="accordion">
								<?php 
									if(!empty($tutors_assigned)){
										foreach($tutors_assigned as $each){ 
											$active_courses = $this->Common_model->get_tutor_active_course($each['course_assigned']);
											?>
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title click">
															<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo 'tutor_'.$each['id'];?>" class="collapsed">
															<?php echo $each['name_title'].' '.strtoupper($each['name']);?></a>
														</h4>

													</div>
													<div id="<?php echo 'tutor_'.$each['id'];?>" class="panel-collapse collapse">
														<div class="panel-body t-profile-01 course-t-profile-0">
														<div class="row teacher-prolile-01">
															<div class="col-sm-3 t-profile-left">
																<div class="teacher-contact">
																<?php if($each['profile_pic'] != '' && file_exists('uploads/tutors/medium/'.$each['profile_pic'])) { ?>
																	<img class="img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/<?php echo $each['profile_pic'];?>">
																<?php } 
																else{ ?>
																	<img class="img-responsive" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/default.jpg">
																<?php } ?>
																
																<?php /* <h3>Contact Info</h3>
																<p><span>Email:</span> martin@EduRead.com</p>
																<p><span>Phone:</span> 61 3 8376 6284</p>
																<ul class="list-unstyled">
																	<li><a href=""><i class="fa fa-facebook"></i></a></li>
																	<li><a href=""><i class="fa fa-twitter "></i></a></li>
																	<li><a href=""><i class="fa fa-google-plus"></i></a></li>
																	<li><a href=""><i class="fa fa-linkedin"></i></a></li>
																</ul> */ ?>
															</div>
														</div>
													<div class="col-sm-9 t-profile-right">
														<div class="row all-corsess-wapper">
															<div class="col-sm-12">
																<div class="all-courses">
																	<!-- <h3><?php echo $each['name_title'].' '.strtoupper($each['name']);?></h3> -->
																	<div class="profile__courses__inner">
																		<ul class="profile__courses__list list-unstyled">
																			<li><i class="fa fa-graduation-cap"></i>Degree: </li>
																			<!-- <li><i class="fa fa-star"></i>Experience:</li>
																			<li><i class="fa fa-heart"></i>Hobbies:</li> -->
																			<li><i class="fa fa-bookmark"></i>My Courses:</li>
																			<li><i class="fa fa-caret-square-o-right"></i>Demo:</li>
																		</ul>
																		<ul class="profile__courses__right list-unstyled">
																			<li><?php echo $each['degrees'];?></li>
																			<!-- <li>20 Years in university Math</li>
																			<li>Music, Dancing and Family</li> -->
																			<li><?php echo $active_courses;?></li>
																			<li><?php if($each['demo_video'] != '' && file_exists('uploads/tutors/demo/'.$each['demo_video'])){ ?>
																										<a target="_blank" class="linkdemo" href="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $each['demo_video'];?>">VIEW DEMO</a>
																								<?php }
																								else{ ?>
																										<a target="_blank" class="linkdemo" href="#">VIEW DEMO </a>
																								<?php } ?></li>
																		</ul>
																	</div>
																
																</div>
															</div>
														</div>

														
													</div>															
												</div>
														</div>
													</div>
												</div>				
									<?php }
									}
								?>
							</div> <!-- .curriculum-section-text END -->
						</div>
					</div>
					<div class="row">
						<div class="col-sm-4">&nbsp;</div>
						<div class="col-sm-4">
							<br/><br/><a class="btn end-register-btn" href="<?php echo base_url();?>registration/index?id=<?php echo $encoded_id;?>">Register Now</a>
						</div>
						<div class="col-sm-4">&nbsp;</div>
					</div>
					<?php /* <div class="review-content">
						<h2>Reviews</h2>
						<div class="row">
							<div class="col-sm-12 col-md-12">
								<div class="row">
									<div class="col-md-4">
										<div class="five-star-rating">
											<div class="rating-box">
												<span class="five">5</span>
												<ul class="list-unstyled">
													<li><i class="fa fa-star"></i></li>
													<li><i class="fa fa-star"></i></li>
													<li><i class="fa fa-star"></i></li>
													<li><i class="fa fa-star"></i></li>
													<li><i class="fa fa-star"></i></li>
												</ul>
												<span>8 Ratings</span>					
											</div>
										</div>
									</div>
									<div class="col-md-8 rating-btom-colum">
										<div class="row">
											<div class="col-sm-12 rating-btom-padding">
												<div class="right-rating-text">
													<span>5 Stars</span>
													<span class="rating-color-yellow"></span>
												</div>
											</div>
											<div class="col-sm-12 rating-btom-padding">
												<div class="right-rating-text">
													<span>4 Stars</span>
													<span class="right-rating-color"></span>
												</div>
											</div>
											<div class="col-sm-12 rating-btom-padding">
												<div class="right-rating-text">
													<span>3 Stars</span>
													<span class="right-rating-color"></span>
												</div>
											</div>
											<div class="col-sm-12 rating-btom-padding">
												<div class="right-rating-text">
													<span>2 Stars</span>
													<span class="right-rating-color"></span>
												</div>
											</div>
											<div class="col-sm-12 rating-btom-padding">
												<div class="right-rating-text">
													<span>1 Stars</span>
													<span class="right-rating-color"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>								
					</div> 

					<div class="comments">
						<div class="row">
							<div class="col-sm-12">
							<h3>Comments</h3>
								<div class="row">
									<div class="col-sm-12 comment-single-item">
										<div class="col-sm-2 img-box">
											<img src="images/index/stu-parent-01.jpg" alt="" class="img-circle">
										</div>
										<div class="col-sm-10 comment-left-bar">
											<div class="comment-text">
												<ul class="list-unstyled">
													<li class="name">James Smith
														<div class="comment-author">
															<ul class="list-unstyled">
																<li>Rated:</li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
															</ul>
														</div>
													</li>
													<li class="comment-date">20 July 2019 AT 10.45 AM</li>
												</ul>
												<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in finibus neque. Vivamus in ipsum quis elit vehicula tempus vitae quis lacus. Vestibulum interdum diam non mi cursus venenatis. Morbi lacinia libero et elementum vulputate.</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12 comment-single-item">
										<div class="col-sm-2 img-box">
											<img src="images/index/stu-parent-02.jpg" alt="" class="img-circle">
										</div>
										<div class="col-sm-10 comment-left-bar">
											<div class="comment-text">
												<ul class="list-unstyled">
													<li class="name">James Smith
														<div class="comment-author">
															<ul class="list-unstyled">
																<li>Rated:</li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
															</ul>
														</div>
													</li>
													<li class="comment-date">20 July 2019 AT 10.45 AM</li>
												</ul>
												<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in finibus neque. Vivamus in ipsum quis elit vehicula tempus vitae quis lacus. Vestibulum interdum diam non mi cursus venenatis. Morbi lacinia libero et elementum vulputate.</p>
											</div>
										</div>
									</div>
									<div class="col-sm-12 comment-single-item">
										<div class="col-sm-2 img-box">
											<img src="images/index/stu-parent-03.jpg" alt="" class="img-circle">
										</div>
										<div class="col-sm-10 comment-left-bar">
											<div class="comment-text">
												<ul class="list-unstyled">
													<li class="name">James Smith
														<div class="comment-author">
															<ul class="list-unstyled">
																<li>Rated:</li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
																<li><i class="fa fa-star"></i></li>
															</ul>
														</div>
													</li>
													<li class="comment-date">20 July 2019 AT 10.45 AM</li>
												</ul>
												<p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in finibus neque. Vivamus in ipsum quis elit vehicula tempus vitae quis lacus. Vestibulum interdum diam non mi cursus venenatis. Morbi lacinia libero et elementum vulputate.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="leave_a_comment">
						<div class="row">
							<div class="col-sm-12">
							<h3>Leave A Comment</h3>
								<div class="row">
									<div class="col-sm-12">
							            <form>
							                <div class="form-group">
							                    <div class="row">
							                        <div class="col-sm-4">
							          					<label class="input-label">name</label>
							                            <input type="text" placeholder="Enter your name" id="name" name="name" required>
							                        </div>
							                        <div class="col-sm-4">
							                        	<label class="input-label">E-mail</label>
							                            <input type="email" placeholder="Enter your E-mail" name="email" required>
							                        </div>
							                        <div class="col-sm-4">
							                       	 <label class="input-label">Website</label>
							                            <input type="text" placeholder="https://" name="web" required>
							                        </div>
							                        <div class="col-sm-12">
							                        	<label class="input-label">Message</label>
							                            <textarea placeholder="Type your comment"  name="message" required></textarea>
							                        </div>
							                        <div class="col-sm-12">
							                            <button type="submit">Send Message</button>
							                        </div>
							                    </div>
							                </div>
							            </form>
									</div>
								</div>
							</div>
						</div>
					</div> */?>

				</div>
			</div>

			<div class="col-sm-4 sidebar-right">
				<div class="sidebar-content">
					<div class="row">
						<div class="col-sm-12">
							<div class="courses-price">
								<div class="price-hading">
									<h3>Course Fee</h3>
								</div>
								<div class="price-text">
									<span class="dolar-sign">₹</span><span><?php echo $course_details->fee;?></span>
									<!-- <p>One Time Payment</p> -->
									<!-- <div class="price-btn-box">
										<a class="price-btn" href="<?php echo base_url();?>registration/index?id=<?php echo $encoded_id;?>">Register Now</a>
                                   
									</div> -->
								</div>
							</div>
						</div>
					</div>
                    <?php 
                    //if fee structure exist
                    if(!empty($fee_arr)){?>
					<div class="row">
						<div class="col-sm-12">
							<div class="courses-features">
								<div class="features-hading">
									<h3>FEE STRUCTURE</h3>
								</div>
								<div class="features-text">
									<ul class="list-unstyled">
                                   <?php
                                    $i = 1;
                                    foreach($fee_arr as $each){ ?>
                                        <li><span><i class="fa fa-clipboard"></i>Part-<?php echo $i++;?></span><span>₹ <?php echo $each['amount'];?></span></li>
                                    <?php
                                    }?>
                                       
										
									</ul>
									<div class="price-btn-box">
										<a class="price-btn" href="<?php echo base_url();?>registration/index?id=<?php echo $encoded_id;?>">Register Now</a>
									</div>
								</div>
							</div>
						</div>
					</div>
                    <?php } ?>
					<?php /* <div class="row">
						<div class="col-sm-12">
							<div class="share-socila-link">
								<div class="social-hading">
									<h3>Share this course</h3>
								</div>
								<div class="social-icon">
									<div class="row">
										<div class="col-sm-12">
											<ul class="list-unstyled">
												<li><a href="" class="facebook"><i class="fa fa-facebook"></i></a></li>
												<li><a href="" class="google"><i class="fa fa-google-plus"></i></a></li>
												<li><a href="" class="instagram"><i class="fa fa-instagram"></i></a></li>
												<li><a href="" class="twitter"><i class="fa fa-twitter"></i></a></li>
												<li><a href="" class="skype"><i class="fa fa-skype"></i></a></li>
												<li><a href="" class="youtube"><i class="fa fa-youtube"></i></a></li>
												<li><a href="" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
												<li><a href="" class="pinterest"><i class="fa fa-pinterest-p"></i></a></li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div> */ ?>

					<div class="row">
						<div class="col-sm-12">
							<div class="latest-courses">
								<div class="latest-hading">
									<h3>Other Courses</h3>
								</div>
								<?php
								if($latest_courses){								
									echo '<div class="latest-courses-text">';
									foreach($latest_courses as $l_course){
										echo '<div class="latest-single">
											<div class="row">
												<div class="col-sm-5 course-image">';
													if($l_course['icon_name'] != '' && file_exists('uploads/courses/medium/'.$l_course['icon_name'])) 
														echo '<img src="'.HTTP_UPLOADS_PATH.'courses/medium/'.$l_course['icon_name'].'" alt="" >';
													else
														echo '<img  src="'.HTTP_UPLOADS_PATH.'courses/medium/default.jpg">';
													echo '</div>
													<div class=" col-sm-7 course-prefix">
														<h3><a href="'.base_url().'courses/view_course?id='.urlencode(base64_encode($l_course['id'].'_'.ENCRYPTION_KEY)).'">'.$l_course['title'].'</a></h3>';
														// <div class="latest-btn"><a href="#">₹'.$l_course['fee'].'</a></div>
													echo '</div>
											</div>
										</div>';
									}
									echo '</div>';
									 } ?>
									
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-sm-12">
							<div class="special-offer">
								<div class="special-text text-center">
									<p>INTRODUCTRY OFFER <br/>FOR +2 COURSES</p>
									<h2>Get 50% Off</h2>
									<div class="offer-btn-box">
										<a href="<?php echo base_url();?>special_offers" class="offer-btn">get started</a>
									</div>
									<!-- <img src="<?php echo HTTP_ASSETS_PATH;?>images/banners/special_offer.jpg" -->
								</div>
							</div>
						</div>
					</div>
						

				</div>
			</div>
		</div>
	</div>	
</div>


<?php
$this->load->view('includes/footer_new');
?>
