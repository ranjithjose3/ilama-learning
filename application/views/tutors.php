<?php
$this->load->view('includes/header_new');
?>

<!-- Teachers Area section -->
<section class="teachers-area">
    <div class="container">
        <div class="row teachers-wapper-02">
        <?php echo form_open_multipart(''); ?>
            
 <?php if($tutors){ 
    $tcount=0;

        foreach($tutors as $each){

             $tcount++;

            if($each['profile_pic'] != '' && file_exists('uploads/tutors/medium/'.$each['profile_pic'])) { 
                 $pr_img_path=HTTP_UPLOADS_PATH.'tutors/medium/'.$each['profile_pic'];
            }
            else{
                $pr_img_path=HTTP_UPLOADS_PATH.'tutors/medium/default.jpg';
            }


            if($each['demo_video'] != '' && file_exists('uploads/tutors/demo/'.$each['demo_video'])){ 
                $demo_lnk=HTTP_UPLOADS_PATH.'tutors/demo/'.$each['demo_video'];
            }
            else{
                $demo_lnk='#';
            }

           
            ?>

            <div class="col-sm-6 col-md-3 teacher-single">
                <figure class="text-center">
                    <div class="teacher-img-02">
                        <img src="<?=$pr_img_path?>" alt="" class="img-responsive">
                    </div>
                    <div class="teachers-name">
                        <h3><a href="<?php echo base_url().'tutors/view_profile?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>"><?php echo $each['name_title'].' '.strtoupper($each['name']);?></a> </h3>
                        <span><i class="fa fa-graduation-cap"></i> <?php echo $each['degrees'];?></span>
                    </div>
                    <figcaption>
                        <ul class="list-unstyled">
                                <li><a href="<?php echo base_url().'tutors/view_profile?id='.urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>"><i class="fa fa-user teacher-icon"></i></a></li>
                                <li><a class="modal-link" <?php if($demo_lnk != '#') { ?> onclick="open_demo_modal('<?php echo $demo_lnk;?>')" <?php } ?>><i class="fa fa-file-video-o teacher-icon"></i></a></li>
                          
                            
                        </ul>
                    </figcaption>
                </figure>
            </div>


    <?php 

    

    if($tcount ==4){

        echo '</div>
        <div class="row teachers-wapper-02">';

        $tcount=0;
    }else{
       

    }
    
        }
    }?>
<?php echo form_close(); ?>
</div>
       

                                            
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	<div class="modal-dialog reply-modal" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">DEMO VIDEO</h4>
			</div>
			<div class="modal-body" id="ViewModalBody1">
			
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>


<?php
$this->load->view('includes/footer_new');
?>
<script>
	function open_demo_modal(video_href){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody1" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>tutors/demo_video_modal",
            data:"video_href="+video_href+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody1").html(data);
                $( "#myModal" ).modal();
                
            }
        }); 
    }
	$('#myModal').on('shown.bs.modal', function () {
		$('#video1')[0].play();
	})
	$('#myModal').on('hidden.bs.modal', function () {
		$('#video1')[0].pause();
	})
</script>

