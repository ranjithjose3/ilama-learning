<?php
$this->load->view('includes/header_new');
?>
<section class="teachers-area" >
    <div class="container aboutus">	
		<div class="row">
            <div class="col-sm-12">
                <h1 class="page-heading"><?=$page_title?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">	
				<p>iLAMA eLearning is a group of enthusiasts involved in the field of Education, Communication, Art, Media and Information technology. Challenges of education in the fast changing world encourage us to create a new system of education by combining the strength of conventional education with the potential of advanced media technologies. <br/><br/>The backbone of iLAMA eLearning is a team of seasoned teachers having expertise in various subjects, a group of subject matter experts, creative artists with remarkable visualization skills and IT professionals.</p>
			</div>
		</div>
        <br/><br/>
        <div class="row teachers-wapper-02">
			<div class="col-xs-6 col-sm-6 col-md-3 teacher-single">
				<figure class="text-center">
					<div class="teacher-img-02">
						<img src="<?php echo HTTP_IMAGES_PATH;?>about_us/photo_shajahan.jpg" alt="" class="img-responsive">
					</div>
					<div class="teachers-name">
						<h3><a href="#" data-toggle="modal" data-target="#modalleadacademician">SHAH JAHAN SUKUMARAN</a></h3>
						<span>Co-Founder and Lead Academician</span>
					</div>
					<figcaption>
						<ul class="list-unstyled">
							<li><a href=""><i class="fa fa-facebook teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-twitter teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-instagram teacher-icon"></i></a></li>
						</ul>
					</figcaption>
				</figure>
				<div class="modal fade" id="modalleadacademician" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog reply-modal" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title aboutmgmnt" id="myModalLabel">SHAH JAHAN SUKUMARAN <br/><span>Co-Founder and Lead Academician</span></h4>
							</div>
							<div class="modal-body" id="ViewModalBody1">
								<p> Shah Jahan is a Teacher with more than 25 years of teaching experience in various classes and curricula. He holds two post graduation degree in linguistic studies form University of Kerala. <br/><br/>
								He started his career in the alternate education system and later continuing as a teacher in middle east for couple of decades. <br/><br/>
								His advocacy has been raised in many times for the improvisation of education systems which are now in practice. It is his vision and  ability to foresee the demands of today's education leads to the birth of ILAMA eLeraning platform. </p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
            </div>
            
            <div class="col-sm-6 col-xs-6 col-md-3 teacher-single">
				<figure class="text-center">
					<div class="teacher-img-02">
						<img src="<?php echo HTTP_IMAGES_PATH;?>about_us/photo_renjith.jpg" alt="" class="img-responsive">
					</div>
					<div class="teachers-name">
						<h3><a href="#" data-toggle="modal" data-target="#modalleadstrategist">RENJITH RANGARAJU</a></h3>
						<span>Co-Founder and Lead Strategist</span>
					</div>
					<figcaption>
						<ul class="list-unstyled">
							<li><a href=""><i class="fa fa-facebook teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-twitter teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-instagram teacher-icon"></i></a></li>
						</ul>
					</figcaption>
				</figure>
				<div class="modal fade" id="modalleadstrategist" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog reply-modal" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title aboutmgmnt" id="myModalLabel">RENJITH RANGARAJU<br/><span>Co-Founder and Lead Strategist</span></h4>
							</div>
							<div class="modal-body" id="ViewModalBody1">
								<p> Renjith is basically artist who has versatile experience in many domains such as graphic design, visual communication, motion graphics, new-media design, 3D modeling, landscape development etc. He holds a Degree in Fine Arts with specialization in Sculpture. <br/><br/>
								He started his career as a web designer and later headed  the role as a creative director, principal designer and  Landscape developer in reputed companies. His Creative instinct and versatile experience with internet technologies and visual design helped the dream of ILAM e Learning  become true. </p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
            </div>

            <?php /* <div class="col-sm-6 col-xs-6 col-md-3 teacher-single">
				<figure class="text-center">
					<div class="teacher-img-02">
						<img src="<?php echo HTTP_IMAGES_PATH;?>about_us/about3.jpg" alt="" class="img-responsive">
					</div>
					<div class="teachers-name">
						<h3><a href="#" data-toggle="modal" data-target="#modalcreativehead">CREATIVE HEAD</a></h3>
						<span>Creative Head</span>
					</div>
					<figcaption>
						<ul class="list-unstyled">
							<li><a href=""><i class="fa fa-facebook teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-twitter teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-instagram teacher-icon"></i></a></li>
						</ul>
					</figcaption>
				</figure>
				<div class="modal fade" id="modalcreativehead" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog reply-modal" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title aboutmgmnt" id="myModalLabel">CREATIVE HEAD<br/><span>Creative Head</span></h4>
							</div>
							<div class="modal-body" id="ViewModalBody1">
								<p> </p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
            </div> */ ?>
            
            <div class="col-sm-6 col-xs-6 col-md-3 teacher-single">
				<figure class="text-center">
					<div class="teacher-img-02">
						<img src="<?php echo HTTP_IMAGES_PATH;?>about_us/photo_ajithprasad.jpg" alt="" class="img-responsive">
					</div>
					<div class="teachers-name">
						<h3><a href="#" data-toggle="modal" data-target="#ithead">AJITH PRASAD EDASSERY</a></h3>
						<span>IT Head</span>
					</div>
					<figcaption>
						<ul class="list-unstyled">
							<li><a href=""><i class="fa fa-facebook teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-twitter teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-google-plus teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-linkedin teacher-icon"></i></a></li>
							<li><a href=""><i class="fa fa-instagram teacher-icon"></i></a></li>
						</ul>
					</figcaption>
				</figure>
				<div class="modal fade" id="ithead" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog reply-modal" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
								<h4 class="modal-title aboutmgmnt" id="myModalLabel">AJITH PRASAD EDASSERY<br/><span>IT Head</span></h4>
							</div>
							<div class="modal-body" id="ViewModalBody1">
								<p>Ajith Prasad is an IT veteran with a career spanning over 20 years with global corporate giants like SAP, GE, 3M etc, in India, Europe and United States. He has got immense management experience in a variety of Project, Portfolio and People centric roles.<br/><br/>He has been part of the IT revolution in India from early ’90s. Along his career path, he has involved in numerous projects and technologies ranging from Mainframe computing through Network Protocol Programming, N-tier, Embedded Systems, Web 1.0 till present, Mobility, CRM, Business Intelligence, Mobile Applications and to the current trending technologies and buzz words.<br/><br/>He has a Masters Degree in Computer discipline from College of Engineering, Thiruvananthapuram.<br/><br/>Ajith took an early retirement from his IT career with SAP Ag back in 2014 to take care of his online business interests and other research oriented topics.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
							</div>
						</div>
					</div>
				</div>
			</div>
										
		</div>
    </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
