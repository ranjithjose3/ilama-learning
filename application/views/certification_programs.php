<?php
$this->load->view('includes/header_new');
?>
<section class="page" >
    <div class="container certpgm">	
        <!-- <div class="row">
            <div class="col-sm-12">
                <h1 class="page-heading"><?=$page_title?></h1>
            </div>
        </div> -->
        <div class="row">
            <div class="col-sm-12">
                <p>There are many courses that do not require attendance in conventional classes. Nowadays, many Universities and educational boards are offering degrees and certificates via distant education programs. In this regard, iLAMA eLearning assists students who need support for obtaining degrees and certificates pertaining to those courses. iLAMA eLearning facilitates registration procedures and other academic requirements for the registered students.</p>
            </div>
        </div>
    </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
