<?php
$this->load->view('includes/header_new');
?>
<?php

        $groupid = $this->session->userdata('group_id');
        $table_array = array('4' => 'students','3' => 'tutors');
        $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
        $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
        $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

        if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

            $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
        }
        else{ 
            $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
            
        }

        $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
 ?>
<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>
                <?php if($groupid == '3'){ ?>
                        <div class="row all-corsess-wapper">
                            <div class="col-sm-12">
                                <div class="all-courses">
                                    <h3><?php echo $profile->name_title.' '.strtoupper($profile->name);?> </h3>
                                    <div class="profile__courses__inner">
                                        <ul class="profile__courses__list list-unstyled">
                                            <li><i class="fa fa-graduation-cap"></i>Degree:</li>
                                            <li><i class="fa fa-map-marker"></i>Address:</li>
                                        </ul>
                                        <ul class="profile__courses__right list-unstyled">
                                            <li><?=$profile->degrees?></li>
                                            <li><?=$profile->address?></li>
                                        </ul>
                                    </div>
                                    <p><?=$profile->profile?></p>
                                
                                </div>
                            </div>
                        </div>
                    
                        <div class="row courses-instuctor">
                            <div class="col-sm-12">
                                <h3 class="courses-title">Active Courses</h3>
                                <?php if($active_course){
                                    $active_course_arr = explode(',',$active_course);
                                    $i = 1;
                                    foreach($active_course_arr as $each){
                                        $course_details = $this->Common_model->get_row('courses',array('id' => $each),'');
                                        $course_category = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');
                                        if($i%2 == 1){    ?>
                                            <div class="row item-margin">
                                        <?php } ?>
                                                <div class="col-sm-6 instractor-single">
                                                    <div class="instractor-courses-single">
                                                        <div class="img-box">
                                                            <?php if($course_details->icon_name != '' && file_exists('uploads/courses/medium/'.$course_details->icon_name)) { ?>
                                                                <img src="<?php echo HTTP_UPLOADS_PATH;?>courses/medium/<?php echo $course_details->icon_name;?>" alt="" class="img-responsive">
                                                            <?php } 
                                                            else{ ?>
                                                                    <img src="<?php echo HTTP_UPLOADS_PATH;?>courses/medium/default.jpg" alt="" class="img-responsive">
                                                            <?php } ?>
                                                        </div>
                                                        <div class="instractor-courses-text">
                                                            <div class="instractor-parson">
                                                                
                                                                <p><a href=""><?php echo strtoupper($course_category);?></p>
                                                            </div>
                                                            <div class="text-bottom">
                                                                <h3><a target="_blank" href="<?php echo base_url();?>courses/view_course?id=<?php echo urlencode(base64_encode($each.'_'.ENCRYPTION_KEY));?>"><?php echo $course_details->title;?></a></h3>
                                                            </div>
                                                        </div>
                                                    </div>                                  
                                                </div>
                                    <?php if($i%2 == 0){ ?>
                                            </div>                        
                                    <?php  } 
                                    $i++; 
                                    }
                                } ?>
                            
                            </div>
                        </div>
                    
                <?php } 
                else {
                ?>
                        <div class="row all-corsess-wapper">
                            <div class="col-sm-12">
                                <div class="all-courses">
                                    <h3><?php echo strtoupper($profile->name);?> </h3>
                                    <div class="profile__courses__inner">
                                        <ul class="profile__courses__list list-unstyled">
                                            <li><i class="fa fa-user"></i>Parent Name:</li>
                                            <li><i class="fa fa-envelope"></i>Parent Email:</li>
                                            <li><i class="fa fa-phone"></i>Parent Mob:</li>
                                            <li><i class="fa fa-map-marker"></i>Address:</li>
                                        </ul>
                                        <ul class="profile__courses__right list-unstyled">
                                            <li><?php echo $profile->parent_name;?></li>
                                            <li><?php echo $profile->parent_email;?></li>
                                            <li><?php echo $profile->parent_mobile;?></li>
                                            <li><?php echo $profile->address;?></li>
                                        </ul>
                                    </div>
                                
                                </div>
                            </div>
                        </div>
                    
                        
                        <?php if($active_course != '') {
                            $course_details = $this->Common_model->get_row('courses',array('id' => $active_course->course_id),'');
                            $course_name = $course_details->title;
                            $course_category = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');
                            $actcourse = strtoupper($course_name); ?>
                            <div class="row courses-instuctor">
                                <div class="col-sm-12">
                                    <h3 class="courses-title">Active Course</h3>
                                    <div class="row item-margin">
                                        <div class="col-sm-6 instractor-single">
                                            <div class="instractor-courses-single">
                                                <div class="img-box">
                                                    <?php if($course_details->icon_name != '' && file_exists('uploads/courses/medium/'.$course_details->icon_name)) { ?>
                                                            <img src="<?php echo HTTP_UPLOADS_PATH;?>courses/medium/<?php echo $course_details->icon_name;?>" alt="" class="img-responsive">
                                                    <?php } 
                                                    else{ ?>
                                                            <img src="<?php echo HTTP_UPLOADS_PATH;?>courses/medium/default.jpg" alt="" class="img-responsive">
                                                    <?php } ?>
                                                </div>
                                                <div class="instractor-courses-text">
                                                    <div class="instractor-parson">
                                                        <p><a href=""><?php echo strtoupper($course_category);?></a></p>
                                                    </div>
                                                    <div class="text-bottom">
                                                        <h3><a target="_blank" href="<?php echo base_url();?>courses/view_course?id=<?php echo urlencode(base64_encode($course_details->id.'_'.ENCRYPTION_KEY));?>"><?php echo $actcourse;?></a></h3>
                                                        <p>
                                                            <?php 
                                                                $det_course = substr($course_details->details,0,150);
                                                                echo 'Subject: ';
                                                                if(strlen(trim($course_details->details))<=150)
                                                                {
                                                                    echo $course_details->details; 
                                                                }
                                                                else
                                                                {
                                                                    echo $det_course.'..';
                                                                }
                                                            ?>
                                                        </p>
                                                    </div>
                                                    <div class="price">
                                                        <ul class="list-unstyled">
                                                            <li>&nbsp;</li>
                                                            <li><i class="fa fa-bookmark"></i><?php echo $status_arr[$active_course->status];?></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>                                  
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }
                            else{ ?>
                                <div class="row courses-instuctor">
                                <div class="col-sm-12">
                                    <h3 class="courses-title">Active Course - NIL</h3>
                                </div>
                            </div>
                            <?php }
                        ?>
                
                <?php } ?>  
            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<?php
$this->load->view('includes/footer_new');
?>
