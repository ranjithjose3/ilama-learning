<?php
$this->load->view('includes/header_new');



    $groupid = $this->session->userdata('group_id');
    $table_array = array('4' => 'students','3' => 'tutors');
    $table_id = $this->Common_model->get_row($table_array[$groupid],array('user_id' => $this->session->userdata('user_id')),'id');
    $profile_pic = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),'profile_pic'); 
    $profile = $this->Common_model->get_row($table_array[$groupid],array('id' => $table_id),''); 

    if($profile_pic != '' && file_exists('uploads/'.$table_array[$groupid].'/medium/'.$profile_pic)){

        $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/medium/'.$profile_pic;
     }
    else{ 
         $profile_pic_path= HTTP_UPLOADS_PATH.$table_array[$groupid].'/default.jpg';
           
    }

    $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
?>

<!-- Teachers Area section -->
<section class="teacher-prolile-01">
    <div class="container">
        <?php   
            if(isset($msg)){
                $message = $msg;
            }
            else
                $message ='';		
        ?>
        <div class="row">
            <?php
                $this->load->view('includes/sidebar_new');
            ?>
            <div class="col-sm-9 t-profile-right settings">
                <?php if($message != '') { ?>
                    <div class="row">  
                        <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo urldecode($message);?>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="courses-features">
                            <div class="features-text">
                                <div class="row mt10 pdlr15">
                                    <div class="panel widthfull">
                                        <div class="panel-header">
                                            CHANGE PASSWORD
                                        </div>
                                        <div class="panel-body">
                                                <?php echo form_open_multipart('settings/password_save'); ?>
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                                                <label for="password">Password<span class="text-red"> *</span></label>
                                                                <input autocomplete="off" type="password" class="form-control" id="password" placeholder="Enter Current Password" name="password" value="<?php echo $password;?>">
                                                                <?php echo form_error('password');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="password">New Password<span class="text-red"> *</span></label>
                                                                <input autocomplete="off"  type="password" class="form-control" id="new_password" placeholder="Enter New Password" name="new_password" value="<?php echo $new_password;?>">
                                                                <?php echo form_error('new_password');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label for="password">Confirm Password<span class="text-red"> *</span></label>
                                                                <input autocomplete="off"  type="password" class="form-control" id="confirm_password" placeholder="Enter Confirm Password" name="confirm_password" value="<?php echo $confirm_password;?>">
                                                                <?php echo form_error('confirm_password');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="form-group">
                                                                <label>&nbsp;</label><br />
                                                                <input type="submit" class="btn btn-reg" name="submit" value="Change Password" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php echo form_close();?>
                                        </div>
                                    </div>
                                </div>
        
                                <div class="row mt10 pdlr15">
                                    <div class="panel widthfull">
                                        <div class="panel-header">
                                            CHANGE USER DETAILS
                                        </div>
                                        <div class="panel-body">
                                                <?php echo form_open_multipart('settings/student_details_save'); ?>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <input type="hidden" name="user_id" value="<?php echo $user_id;?>">
                                                                <input type="hidden" name="id" value="<?php echo $id;?>">
                                                                <label for="password">Email<span class="text-red"> *</span></label>
                                                                <input autocomplete="off"  type="email" class="form-control" id="email" placeholder="Enter email" name="email" value="<?php echo $email;?>">
                                                                <?php echo form_error('email');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="password">Contact No<span class="text-red"> *</span></label>
                                                                <input autocomplete="off"  type="text" class="form-control" id="mobile" placeholder="Enter Contact No." name="mobile" value="<?php echo $mobile;?>">
                                                                <?php echo form_error('mobile');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="password">Profile Pic <small>(jpg or png)</small><span class="text-red"> *</span></label>
                                                                <input type="file" class="form-control" id="profile_pic"  name="profile_pic" accept="image/x-png,image/jpeg">

                                                                <div class="text-red" id="image_error"></div>
                                                                <input type="hidden" name="profilepic" id="profilepic" value="">
                                                                <div id="uploaded_image"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="password">Address<span class="text-red"> *</span></label>
                                                                <textarea autocomplete="off"  class="form-control" id="address" placeholder="Enter Address" name="address"><?php echo $address;?></textarea>
                                                                <?php echo form_error('address');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="password">Parent Email</label>
                                                                <input autocomplete="off"  type="email" class="form-control" id="parent_email" placeholder="Enter Parent email" name="parent_email" value="<?php echo $p_email;?>">
                                                                <?php echo form_error('parent_email');?>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label for="password">Parent Contact No</label>
                                                                <input autocomplete="off" type="text" class="form-control" id="parent_mobile" placeholder="Enter Parent Contact No." name="parent_mobile" value="<?php echo $p_mobile;?>">
                                                                <?php echo form_error('parent_mobile');?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-8">&nbsp;</div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <label>&nbsp;</label><br />
                                                                <input type="submit" class="btn btn-reg floatright" name="submit" value="Change Details" >
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php echo form_close();?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>                                                        
        </div>
    </div>
</section>
<!-- ./ End Teachers Area section -->

<div id="uploadimageModal" class="modal" role="dialog">
	<div class="modal-dialog reply-modal">
		<div class="modal-content">
      		<div class="modal-header">
        		<button type="button" class="close" data-dismiss="modal">&times;</button>
        		<h4 class="modal-title">Upload & Crop Image</h4>
      		</div>
      		<div class="modal-body">
        		<div class="row">
  					<div class="col-md-8 text-center">
						  <div id="image_demo" style="width:350px; margin-top:30px"></div>
  					</div>
  					<div class="col-md-4" style="padding-top:30px;">
  						<br />
  						<br />
  						<br/>
						  <button class="btn btn-success crop_image">Crop & Upload Image</button>
					</div>
				</div>
      		</div>
      		<div class="modal-footer">
        		<button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
      		</div>
    	</div>
    </div>
</div>

<?php
$this->load->view('includes/footer_new');
?>

<script>  
$(document).ready(function(){

	$image_crop = $('#image_demo').croppie({
    enableExif: true,
    viewport: {
      width:260,
      height:290,
    //   type:'square' //circle
    },
    boundary:{
      width:300,
      height:300
    }
  });

  $('#profile_pic').on('change', function(){
    var reader = new FileReader();
    var image_type = this.files[0]['type'];
    var image_size = this.files[0]['size'];

    if(image_type == 'image/jpeg' || image_type == 'image/png'){
        if(image_size < 2048000){
            $("#image_error").html('');
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (event) {
                var image = new Image();

                image.src = event.target.result;
                image.onload = function () {

                    var height = this.height;
                    var width = this.width;
                    if (width < 260 || height < 290) {
                        $("#image_error").html('Min widthxheight must be 260x290 px');
                        $("#profile_pic").val('');
                        return false;
                    }
                    else{
                        $image_crop.croppie('bind', {
                            url: event.target.result
                        }).then(function(){
                            console.log('jQuery bind complete');
                        });
                        $('#uploadimageModal').modal('show');
                    }
                };
                
            }
        }
        else{
            $("#image_error").html('Image size should be less than 2 MB');
            $("#profile_pic").val('');
        }
    }
    else{
        $("#image_error").html('Please upload a jpeg or png file');
        $("#profile_pic").val('');
    }
  });

  $('.crop_image').click(function(event){
    $image_crop.croppie('result', {
      type: 'canvas',
      size: 'viewport'
    }).then(function(response){
        var csrf = $('[name="csrf_test_name"]').val();
      $.ajax({
        url:"<?php echo base_url();?>settings/student_upload",
        type: "POST",
        data:{"image": response,"csrf_test_name":csrf},
        success:function(data)
        {
          var output = JSON.parse(data);
          $('#uploadimageModal').modal('hide');
          $('#uploaded_image').html(output[0]);
          $('#profilepic').val(output[1]);
        }
      });
    })
  });

});  
</script>

