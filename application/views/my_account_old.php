<?php
$this->load->view('includes/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
           <div class="row no-margin">
               <div class="col-sm-2">
                    <?php $this->load->view('includes/sidebar');?>
                </div>
                <div class="col-sm-10">
                    <?php if($message != '') { ?>
                        <div class="row mt10 pdlr15">  
                            <div class="alert <?php echo $action; ?> alert-dismissable widthfull">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                                <?php echo urldecode($message);?>
                            </div>
                        </div>
                    <?php } ?>

                    <?php 
                        if($group_id == '4'){
                                $status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
                        ?>
                                <div class="row mt10 pdlr15">
                                    <div class="card widthfull">
                                        <div class="card-header bg-info whitetext">
                                            ACTIVE COURSE
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                <?php 
                                                    if($active_course != '') {
                                                        $course_details = $this->Common_model->get_row('courses',array('id' => $active_course->course_id),'');
                                                        $course_name = $course_details->title;
                                                        $course_category = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');
                                                        echo strtoupper($course_name).' - '.$course_category;
                                                    }
                                                    else{
                                                        echo 'No Active Course!!';
                                                    }
                                                ?>
                                            </h5>
                                            <?php if($active_course != '') { ?>
                                                <p class="card-text">
                                                    Status : <?php echo $status_arr[$active_course->status];?>
                                                    &nbsp;&nbsp;&nbsp;/&nbsp;&nbsp;&nbsp;Duration: <?php echo $course_details->duration;?><br/>
                                                </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                        } 
                        else{ ?>
                                <div class="row mt10 pdlr15">
                                    <div class="card widthfull">
                                        <div class="card-header bg-info whitetext">
                                            ACTIVE SUBJECTS
                                        </div>
                                        <div class="card-body">
                                            <h5 class="card-title">
                                                <?php 
                                                    echo $active_courses;
                                                ?>
                                            </h5>
                                        </div>
                                    </div>
                                </div>
                        <?php 
                        } ?>
                </div>
          </div>
        </div><!-- /.container-->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('includes/footer');
?>
