<?php
$this->load->view('includes/header_new');
?>
<section class="page" >
    <div class="container privacy-policy">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="page-heading"><?=$page_title?></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">	
                <h2 class="subtitle">Key Information</h2>
                <ul class="privpollist">
                    <li>ILAMA eLearning is the data controller of your personal information.</li>
                    <li>We collect the personal information set out in the "What Information We Collect" section of this Privacy Notice below, including account registration details such as name and email, details of Content Offerings you undertake, survey information (where you provide it), identity verification data, and information about your use of our site and Services.</li>
                    <li>We use your personal information for the purposes set out in the "How We Use the Information" section of this Privacy Notice below, including providing our site and Services to you, ensuring the security and performance of our site, conducting research relating to Content Offerings, sharing information with our Content Providers and our suppliers, direct marketing, and performing statistical analysis of the use of our site and Services.</li>
                    <li>You have a number of rights that you can exercise in relation to our use of your personal information, as set out in the "Updating or Deleting Your Personally Identifiable Information" section of this Privacy Notice below.</li>
                </ul>
                <br/>
                <h2 class="subtitle">Purpose and who we are</h2>
                <p class="privpolpara">
                    The purpose of this Privacy Notice is to describe how ILAMA eLearning PVT.Ltd.., our subsidiaries, and our international branches, ("ILAMA eLearning," "us," "we," or "our") collects, uses, and shares information about you through our online interfaces (e.g., websites and mobile applications) owned and controlled by us, including but not limited to <a class="privpolanc" href="<?php echo base_url();?>" target="_blank">www.ilamaearning.com</a> and similar deomains coming under the brand iLAMA eLearing (collectively referred to herein as the "Site"). Please read this Privacy Notice carefully to understand what we do. If you do not understand any aspects of our Privacy Notice, please feel free to contact us at <a class="privpolanc" href="#">privacy@lamaelearnigng.com</a>. Your use of our Site is also governed by our Terms of use. But not defined in this Privacy Notice can be found in our Terms of Use. ILAMA eLearning . is a Private limited company located in Kerala.
                </p>
                <br/>
                <h2 class="subtitle">What Information this Privacy Notice Covers</h2>
                <p class="privpolpara">
                    This Privacy Notice covers information we collect from you through our Site. Some of our Site’s functionality can be used without revealing any Personally Identifiable Information, though for features or Services related to the Content Offerings, Personally Identifiable Information is required. In order to access certain features and benefits on our Site, you may need to submit, or we may collect,<b>"Personally Identifiable Information"</b> (i.e., information that can be used to identify you)(may also be referred to as “personal data” or “personal information”). Personally Identifiable Information can include information such as your name, email address, IP address, and device identifier, among other things. You are responsible for ensuring the accuracy of the Personally Identifiable Information you submit to ILAMA eLearning. Inaccurate information may affect your ability to use the Site, the information you receive when using the Site, and our ability to contact you. For example, your email address should be kept current because that is one of the primary manners in which we communicate with you.
                </p>
                <br/>
                <h2 class="subtitle">What You Agree to by Using Our Site</h2>
                <p class="privpolpara">
                    We consider that the legal bases for using your personal information as set out in this Privacy Notice are as follows:
                </p>
                <ul class="privpollist paralist">
                    <li>Our use of your personal information is necessary to perform our obligations under any contract with you (for example, to comply with the Terms of Use of our Site which you accept by browsing our website and/or to comply with our contract to provide Services to you); or</li>
                    <li>Our use of your personal information is necessary for complying with our legal obligations; or</li>
                    <li>Use of your personal information is necessary for our legitimate interests or the legitimate interests of others. Our legitimate interests are to:</li>
                        <ul class="privpollist parasublist">
                            <li>Run, grow and develop our business;</li>
                            <li>Operate our Site and provide our Services;</li>
                            <li>Select appropriately skilled and qualified professionals;</li>
                            <li>Build relationships with partners and academic institutions;</li>
                            <li>Carry out research and statistical analysis;</li>
                            <li>Carry out marketing and business development; and</li>
                            <li>For internal administrative and auditing purposes.</li>
                        </ul>
                    <li>Consent, to send you certain communications or where you submit certain information to us.</li>
                </ul>
                <p class="privpolpara">
                    Which legal basis applies to a specific processing activity will depend on the type of personal information processed and the context in which it is processed.<br/><br/>If we rely on our (or another person's) legitimate interests for using your personal information, we will undertake a balancing test to ensure that our (or the other person's) legitimate interests are not outweighed by your interests or fundamental rights and freedoms which require protection of the personal information.<br/><br/>We may process your personal information in some cases for marketing purposes on the basis of your consent (which you may withdraw at any time as described below).<br/><br/>If we rely on your consent for us to use your personal information in a particular way, but you later change your mind, you may withdraw your consent by visiting your profile page and clicking the box to remove consent or delete your account and we will stop doing so. However, if you withdraw your consent, this may impact the ability for us to be able to provide our Services to you. 
                </p>
                <br/>
                <h2 class="subtitle">What Information We Collect</h2>
                <p class="privpolpara">
                    We gather two types of information about users through the Site:
                </p>
                <ul class="privpollist paralist numberli">
                    <li><b>Information relating to your use of our Site</b>. When users come to our Site, we may track, collect, and aggregate information indicating, among other things, which pages of our Site were visited, the order in which they were visited, when they were visited, and which hyperlinks were clicked. We also collect information from the URLs from which you linked to our Site. Collecting such information may involve logging the IP address, operating system and browser software used by each user of the Site. We may be able to determine from an IP address a user’s Internet Service Provider and the geographic location of his or her point of connectivity. We also use (or may use) cookies and web beacons when you visit our Site. For more information on our use of cookies and web beacons, please refer to our Cookie Policy.</li><br/>
                    <li><b>Personally Identifiable Information provided directly by you or via third parties</b>. We collect Personally Identifiable Information that you provide to us when you register for an account, update or change information for your account, purchase products or Services, complete a survey, sign-up for email updates, participate in our public forums, send us email messages, and/or participate in Content Offerings or other Services on our Site. We may use the Personally Identifiable Information that you provide to respond to your questions, provide you the specific Content Offering and/or Services you select, send you updates about Content Offerings or other iLAMA eLearnign events, and send you email messages about Site maintenance or updates.</li>
                </ul><br/>
                <ul class="privpollist paralist">
                    <li>Account Registration. If you register for an account on our Site, you may be required to provide us with Personally Identifiable Information such as your name and email address.</li>
                    <li>Updates. iLAMA eLearning may offer you the ability to receive updates either via email or by posting on portions of the Site only accessible to registered users. In order to subscribe to these Services, you may be required to provide us with Personally Identifiable Information such as your name and email address.</li>
                    <li>Forums. iLAMA eLearning  may offer public forums from time to time (the "Forums") where you can share comments and thoughts. In order to participate in the Forums, you may be required to register with us and/or provide us with Personally Identifiable Information such as your name and email address. Please keep in mind that information you post or make available in Forums will be publicly available. You should not include any Personally Identifiable Information or other information of a personal or sensitive nature, whether relating to you or another person, in a Forum post. Please also reference our Terms of Use and our Code of Conduct and additional information about proper use of our Forums.</li>
                    <li>Participation in Content Offerings. iLAMA eLearning offers users the opportunity to participate in an Content Offerings on or through the Site. If you desire to participate in a Content Offering, you will be asked to provide us with certain information necessary to conduct such a Content Offering. This information may include, among other things, your name and email address.</li>
                </ul><br/>
                <p class="privpolpara">
                    If you participate in a Content Offering, we may collect from you certain student-generated content, such as assignments you submit to instructors, peer-graded assignments, and peer grading student feedback. We also collect course data, such as student responses to in-video quizzes, standalone quizzes, exams, and surveys. You should not include any Personally Identifiable Information or other information of a personal or sensitive nature, whether relating to you or another person, on assignments, exams, or surveys, except for information required to participate or submit such assignments, exams, or surveys.
                </p>
                <ul class="privpollist paralist">
                    <li>Identity Verification. iLAMA eLearning may offer you the ability to verify your identity for select Services. In order to enroll for these Services, you may be required to provide us or our third-party identity verification vendor with Personally Identifiable Information such as your name, address, date of birth, a headshot taken using a webcam, and a photo identification document. Additionally, if you apply for financial aid in connection with these Services, you may be required to provide information regarding your income</li>
                    <li>Communications with iLAMA eLearning. We may receive Personally Identifiable Information when you send us an email message or otherwise contact us.</li>
                    <li>Third Party Sites. We may receive Personally Identifiable Information when you access or log-in to a third party site, e.g., Facebook, from our Sites. This may include the text and/or images of your Personally Identifiable Information available from the third party site.</li>
                    <li>Surveys. We may receive Personally Identifiable Information when you provide information in response to a survey operated by us or a Content Provider.</li>
                    <li>Partner sites. Partner sites providing Content Offering related tools and services to iLAMA eLearning users may collect nonfinancial individual level user data regarding the individual’s use of that partner site while the partner sites provide those services to iLAMA eLearning. The partner sites may share that data with iLAMA eLearning for the purpose of improving iLAMA eLearning’s Services, the partner site’s services, and the individual’s education experience. This data includes information such as the amount of time spent on the partner site and pages viewed.</li>
                    <li>Third Party Credit Card Processing. iLAMA eLearning provides you with the ability to pay for Content Offerings and other Services using a credit card through a third party payment processing service provider. Please note that our service provider – not iLAMA eLearning – collects and processes your credit card information.</li>
                </ul><br/>
                <h2 class="subtitle">How We Use the Information</h2>
                <ul class="privpollist paralist numberli">
                    <li><b>Information relating to your use of our Site. </b>. We use information relating to your use of the Site to build higher quality, more useful Services by performing statistical analyses of the collective characteristics and behavior of our users, and by measuring demographics and interests regarding specific areas of our Site. We may also use this information to ensure the security of our Services and the Site.</li><br/>
                    <li><b>Personally Identifiable Information provided directly by you or via third parties</b>. Except as set forth in this Privacy Notice or as specifically agreed to by you, iLAMA eLearning will not disclose any of your Personally Identifiable Information. In addition to the other uses set forth in this Privacy Notice, we may disclose and otherwise use Personally Identifiable Information as described below.</li>
                </ul><br/>
                <ul class="privpollist paralist">
                    <li>Providing the Site and our Services. We use Personally Identifiable Information which you provide to us in order to allow you to access and use the Site and in order to provide any information, products, or Services that you request from us.</li>
                    <li>Technical support and security. We may use Personally Identifiable Information to provide technical support to you, where required, and to ensure the security of our Services and the Site.</li>
                    <li>Updates. We use Personally Identifiable Information collected when you sign-up for our various email or update services to send you the messages in connection with the Site or Content Offerings. We may also archive this information and/or use it for future communications with you, where we are legally entitled to do so.</li>
                    <li>Forums. You should not post any Personally Identifiable Information or other information of a personal or sensitive nature, whether relating to you or another person, within a Forum post. If you choose to post Personally Identifiable Information, such Personally Identifiable Information may be collected during your use of the Forums. We may publish this information via extensions of our Platform that use third-party services, like mobile applications. We reserve the right to reuse Forum posts that contain Personally Identifiable Information in future versions of the Content Offerings, and to enhance future Content Offerings. We may archive this information and/or use it for future communications with you and/or your designee(s), and/or provide it to the Content Provider, business partner, or the instructor(s) associated with the courses you have taken. We may also use or publish posts submitted on the Forums without using Personally Identification Information.</li>
                    <li>Participation in Content Offerings. We use the Personally Identifiable Information that we collect from you when you participate in a Content Offering through the Site for processing purposes, including but not limited to tracking attendance, progress, and completion of the Content Offerings. We may also share your Personally Identifiable Information and your performance in a given Content Offering with the instructor or instructors who taught it, with teaching assistants or other individuals designated by the instructor or instructors to assist with the creation, modification, or operation of the Content Offering, and with the Content Provider(s) with which they are affiliated. We may also use the information generated when taking a Content Offering or using the Services for predictive analysis of your performance in the Content Offerings. Also, we may archive this information and/or use it for future communications with you, where we are legally entitled to do so.</li>
                    <li>Identity Verification. For Services that require identity verification, we may use the Personally Identifiable Information that we collect for verifying your identity, and for authenticating that submissions made on the Site were made by you. This Service may be provided through a third-party identity verification vendor. Your photo identification document will be deleted after successful verification of your profile information.</li>
                    <li>Communications with or from iLAMA eLearning. When you send us an email message or otherwise contact us, we may use the information provided by you to respond to your communication and/or as described in this Privacy Notice. We may also archive this information and/or use it for future communications with you where we are legally entitled to do so. Where we send you emails, we may track the way that you interact with these emails (such as when you open an email or click on a link inside an email). We use this information for the purposes of optimizing and better tailoring our communications to you.</li>
                    <li>Communications with iLAMA eLearnig  Business Partners. We may share your Personally Identifiable Information with Content Providers and other business partners of iLAMA eLearnign so that Content Providers and other business partners may share information about their products and services that may be of interest to you where they are legally entitled to do so.</li>
                    <li>Research. We may share general course data (including quiz or assignment submissions, grades, and forum discussions), information about your activity on our Site, and demographic data from surveys operated by us with our Content Providers and other business partners so that our Content Providers and other business partners may use the data for research related to online education.</li>
                    <li>Disclosure to iLAMA eLearnign Operations and Maintenance Contractors. We use various service providers, vendors and contractors (collectively, "Contractors") to assist us in providing our Services to you. Our Contractors may have limited access to your Personally Identifiable Information in the course of providing their products or services to us, so that we in turn can provide our Services to you. These Contractors may include vendors and suppliers that provide us with technology, services, and/or content related to the operation and maintenance of the Site or the Content Offerings. Access to your Personally Identifiable Information by these Contractors is limited to the information reasonably necessary for the Contractor to perform its limited function for us.</li>
                    <li>Government Authorities; Legal Rights and Actions. iLAMA eLearnign may share your Personally Identifiable Information with various government authorities in response to subpoenas, court orders, or other legal process; to establish or exercise our legal rights or to protect our property; to defend against legal claims; or as otherwise required by law. In such cases we reserve the right to raise or waive any legal objection or right available to us. We also may share your Personally Identifiable Information when we believe it is appropriate to investigate, prevent, or take action regarding illegal or suspected illegal activities; to protect and defend the rights, property, or safety of iLAMA eLearnign, the Site, our users, customers, or others; and in connection with our Terms of Use and other agreements.</li>
                    <li>Disclosure to Acquirers. iLAMA eLearnign may disclose and/or transfer your Personally Identifiable Information to an acquirer, assignee or other successor entity in connection with a sale, merger, or reorganization of all or substantially all of the equity, business, or assets of iLAMA eLearnign to which your Personally Identifiable Information relates.</li>
                    <li>e-Readers. If we receive any Personally Identifiable Information related to the extent to which you use designated e-Readers to access iLAMA eLearnign materials, we may archive it, and use it for research, business, or other purposes. </li>
                </ul><br/>
                <h2 class="subtitle">External Links</h2>
                <p class="privpolpara">
                    For your convenience we may provide links to sites operated by organizations other than iLAMA eLearnign ("Third Party Sites") that we believe may be of interest to you. We do not disclose your Personally Identifiable Information to these Third Party Sites unless we have a lawful basis on which to do so. We do not endorse and are not responsible for the privacy practices of these Third Party Sites. If you choose to click on a link to one of these Third Party Sites, you should review the privacy policy posted on the other site to understand how that Third Party Site collects and uses your Personally Identifiable Information.
                </p><br/>
                <h2 class="subtitle">Retention of Personally Identifiable Information</h2>
                <p class="privpolpara">
                    We keep your Personally Identifiable Information for no longer than necessary for the purposes for which the Personally Identifiable Information is collected and processed. The length of time we retain Personally Identifiable Information for depends on the purposes for which we collect and use it and/or as required to comply with applicable laws and to establish, exercise, or defend our legal rights.
                </p><br/>
                <h2 class="subtitle">Confidentiality & Security of Personally Identifiable Information</h2>
                <p class="privpolpara">
                    We consider the confidentiality and security of your information to be of the utmost importance. We will use industry standard physical, technical, and administrative security measures to keep your Personally Identifiable Information confidential and secure, and will not share it with third parties, except as otherwise provided in this Privacy Notice, or unless such disclosure is necessary in special cases, such as a physical threat to you or others, as permitted by applicable law. Because the Internet is not a 100% secure environment, we cannot guarantee the security of Personally Identifiable Information, and there is some risk that an unauthorized third party may find a way to circumvent our security systems or that transmission of your information over the Internet will be intercepted. It is your responsibility to protect the security of your login information. Please note that e-mails communications are typically not encrypted and should not be considered secure.
                </p>
                <br/>
                <h2 class="subtitle">Updating or Deleting Your Personally Identifiable Information</h2>
                <p class="privpolpara">
                    You have certain rights in relation to your Personally Identifiable Information. You can access your Personally Identifiable Information and confirm that it remains correct and up-to-date, choose whether or not you wish to receive material from us or some of our partners, and request that we delete or provide you with a copy of your personal data by logging into the Site and visiting your user account page.<br/><br/>If you would like further information in relation to your rights or would like to exercise any of them, you may also contact us via <a class="privpolanc" href="#">privacy@ilamaelearning.com</a>.  
                </p>
                <br/>
                <h2 class="subtitle">Questions, Suggestions, and Complaints</h2>
                <p class="privpolpara">
                    If you have any privacy-related questions, suggestions, unresolved problems, or complaints, you may contact us via <a class="privpolanc" href="#">privacy@ilamaelearning.com</a>.  
                </p>
                <br/>
                <h2 class="subtitle">What do we do with your information?</h2>
                <p class="privpolpara">
                    When you joined for a course we offers in ilamaeleraning.com, as a part of fee collection processes we collect the personnel information you give us such as your name, address and email address and telephone number. When you browse through the listed course pages, we also automatically receive your computer’s internet protocol (IP) address in order to provide us with information that help us learn about your browser and operating system.<br/><br/>
                    During the period you are enrolled for the course, email address is the means of sending notifications of classes as per the course schedules. Your telephone number may be used to contract you in person during the course period. Email marketing (if applicable): With your permission, we may send you emails about our other courses.<br/><br/>
                    <b><u>How do you get my consent?</u></b><br/>
                    When you provide us with personal information to complete a transaction, verify your credit card, place an order, arrange for a delivery or return a purchase, we imply that you consent to our collecting it and using it for that specific reason only. If we ask for your personal information for a secondary reason, like marketing, we will either ask you directly for your expressed consent, or provide you with an opportunity to say no.<br/><br/>
                    <b><u>How do I withdraw my consent?</u></b><br/>
                    If after you opt-in, you change your mind, you may withdraw your consent for us to contact you, for the continued collection, use or disclosure of your information, at anytime, by contacting us at <a class="privpolanc" href="#">info@ilamaelearing.com</a>  or mailing us at: SOL CONNECT N CONSULT,  PWRA 165, Chembumukk, Kakkanad West, Kochi - 682030, India.
                </p>
                <br/>
                <h2 class="subtitle">Disclosure</h2>
                <p class="privpolpara">
                    We may disclose your personal information if we are required by law to do so or if you violate our Terms of Service.
                </p>
                <br/>
                <h2 class="subtitle">Payment</h2>
                <p class="privpolpara">
                    We use Razorpay for processing payments. We/Razorpay do not store your card data on their servers. The data is encrypted through the Payment Card Industry Data Security Standard (PCI-DSS) when processing payment. Your purchase transaction data is only used as long as is necessary to complete your purchase transaction. After that is complete, your purchase transaction information is not saved.<br/><br/>
                    Our payment gateway adheres to the standards set by PCI-DSS as managed by the PCI Security Standards Council, which is a joint effort of brands like Visa, MasterCard, American Express and Discover. PCI-DSS requirements help ensure the secure handling of credit card information by our store and its service providers. For more insight, you may also want to read terms and conditions of razorpay on <a href="https://razorpay.com" target="_blank" class="privpolanc">https://razorpay.com</a>
                </p>
                <br/>
                <h2 class="subtitle">Third-Party Services</h2>
                <p class="privpolpara">
                    In general, the third-party providers used by us will only collect, use and disclose your information to the extent necessary to allow them to perform the services they provide to us. However, certain third-party service providers, such as payment gateways and other payment transaction processors, have their own privacy policies in respect to the information we are required to provide to them for your purchase-related transactions.<br/><br/>
                    For these providers, we recommend that you read their privacy policies so you can understand the manner in which your personal information will be handled by these providers. In particular, remember that certain providers may be located in or have facilities that are located a different jurisdiction than either you or us. So if you elect to proceed with a transaction that involves the services of a third-party service provider, then your information may become subject to the laws of the jurisdiction(s) in which that service provider or its facilities are located. Once you leave our ILAMA eLearning website or are redirected to a third-party website or application, you are no longer governed by this Privacy Policy or our website’s Terms of Service.<br/><br/>
                    <b><u>Links</u></b><br/>
                    When you click on links on our store, they may direct you away from our site. We are not responsible for the privacy practices of other sites and encourage you to read their privacy statements.
                </p>
                <br/>
                <h2 class="subtitle">Security</h2>
                <p class="privpolpara">
                    To protect your personal information, we take reasonable precautions and follow industry best practices to make sure it is not inappropriately lost, misused, accessed, disclosed, altered or destroyed.
                </p>
                <br/>
                <h2 class="subtitle">Cookies</h2>
                <p class="privpolpara">
                    We use cookies to maintain session of your user. It is not used to personally identify you on other websites.
                </p>
                <br/>
                <h2 class="subtitle">Age of Consent</h2>
                <p class="privpolpara">
                    By using this site, you represent that you are at least the age of majority in your state or province of residence, or that you are the age of majority in your state or province of residence and you have given us your consent to allow any of your minor dependents to use this site.
                </p>
                <br/>
                <h2 class="subtitle">Changes to this Privacy Policy</h2>
                <p class="privpolpara">
                    We reserve the right to modify this privacy policy at any time, so please review it frequently. Changes and clarifications will take effect immediately upon their posting on the website. If we make material changes to this policy, we will notify you here that it has been updated, so that you are aware of what information we collect, how we use it, and under what circumstances, if any, we use and/or disclose it. If our store is acquired or merged with another company, your information may be transferred to the new owners so that we may continue to sell products to you.
                </p>
                <br/>
                <h2 class="subtitle">Questions and Contact Information</h2>
                <p class="privpolpara">
                    If you would like to: access, correct, amend or delete any personal information we have about you, register a complaint, or simply want more information contact our Privacy Compliance Officer at  <a class="privpolanc" href="#">info@ilamaelearing.com</a>  or by mail at SOL CONNECT N CONSULT,  PWRA 165, Chembumukk, Kakkanad West, Kochi - 682030, India.
                </p>
                <br/>
            </div>
        </div>	
    </div>
</section>
<?php
$this->load->view('includes/footer_new');
?>
