<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> COURSES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">View Course</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
     <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">View Course - <?php echo strtoupper($title).' ('.$course_code.')';?></h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-7 left-pane">
                                    <div class="row">
                                        <?php if($icon_name != '' && file_exists('uploads/courses/big/'.$icon_name)) { ?>
                                            <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>courses/big/<?php echo $icon_name;?>">
                                        <?php }
                                        else{ ?>
                                            <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>courses/big/default.jpg">
                                        <?php } ?>
                                    </div>
                                   
                                    <!-- <div class="row pt-15">
                                        <p class="view-course"><b>Course Duration: </b></p>
                                    </div> -->
                                    <div class="row">
                                        <ul class="courses-assigned">
                                            <li><i class="fa fa-calendar"></i><?php echo $start_date.' - '.$end_date;?></li>
                                            <li><i class="fa fa-clock-o"></i><?php echo $duration;?></li>
                                            <li><i class="fa fa-download"></i><a class="black-text" target="_blank" href="<?php echo HTTP_UPLOADS_PATH;?>courses/pdf/<?php echo $course_pdf;?>" download="<?php echo $category.'_'.$title;?>">Download File</a></li>
                                        </ul>
                                    </div>
                                    <div class="row">
                                        <p><?php echo $details;?></p>
                                    </div>
                                </div>

                                <div class="col-sm-5">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-outline card-success card-no-bg ml10">
                                                <div class="card-header">
                                                    <?php if(!empty($tutors_assigned)){
                                                            $tutcount = sizeof($tutors_assigned);
                                                        }
                                                        else{
                                                            $tutcount = 0;
                                                        } ?>
                                                    <h3 class="card-title"><a target="_blank" class="black-text" href="<?php echo base_url();?>admin/courses/tutor_list?id=<?php echo urlencode(base64_encode($id.'_'.ENCRYPTION_KEY));?>"><b>TUTORS <span class="tutcount"><?php echo $tutcount;?><span></b></a></h3>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body tut-stud-sec">
                                                    <?php if(!empty($tutors_assigned)){
                                                        foreach($tutors_assigned as $each){
                                                            $each_detail = $this->Common_model->get_row('tutors',array('id' => $each),'');
                                                    ?>
                                                            <div class="row pb10">
                                                                <div class="col-sm-2">
                                                                <?php 
                                                                    if($each_detail->profile_pic !='' && file_exists('uploads/tutors/small/'.$each_detail->profile_pic)){ ?>
                                                                            <img class="ts-profpic" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/<?php echo $each_detail->profile_pic;?>">
                                                                <?php
                                                                    }
                                                                    else{ ?>
                                                                            <img class="ts-profpic" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/default.jpg">
                                                                <?php
                                                                    }
                                                                ?>
                                                                </div>
                                                                <div class="col-sm-10">
                                                                    <div class="col-sm-12">
                                                                        <p class="tut-name"><b><?php echo $each_detail->name_title.' '.strtoupper($each_detail->name);?></b>
                                                                        <?php if($each_detail->status != 'A')
                                                                            echo '<span class="text-red"> (Suspended)</span>';?>
                                                                    </p>
                                                                    </div>
                                                                    <div class="col-sm-12">
                                                                        <h5 class="tut-email"><?php echo $each_detail->email;?></h5>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    <?php
                                                        }

                                                    } ?>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="card card-outline card-warning card-no-bg ml10">
                                                <div class="card-header">
                                                    <?php if(!empty($students_enrolled)){
                                                            $studcount = sizeof($students_enrolled);
                                                        }
                                                        else{
                                                            $studcount = 0;
                                                        } ?>
                                                    <h3 class="card-title"><a target="_blank" class="black-text" href="<?php echo base_url();?>admin/courses/student_list?id=<?php echo urlencode(base64_encode($id.'_'.ENCRYPTION_KEY));?>"><b>STUDENTS <span class="scount"><?php echo $studcount;?><span></b></a></h3>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body stud-sec-height">
                                                    <?php if(!empty($students_enrolled)){
                                                        foreach($students_enrolled as $each){
                                                            $each_detail = $this->Common_model->get_row('students',array('id' => $each),'');
                                                    ?>
                                                        <div class="row pb10">
                                                            <div class="col-sm-2">
                                                            <?php 
                                                                if($each_detail->profile_pic !='' && file_exists('uploads/students/small/'.$each_detail->profile_pic)){ ?>
                                                                        <img class="ts-profpic" src="<?php echo HTTP_UPLOADS_PATH;?>students/small/<?php echo $each_detail->profile_pic;?>">
                                                            <?php
                                                                }
                                                                else{ ?>
                                                                        <img class="ts-profpic" src="<?php echo HTTP_UPLOADS_PATH;?>students/small/default.jpg">
                                                            <?php
                                                                }
                                                            ?>
                                                            </div>
                                                            <div class="col-sm-10">
                                                                <div class="col-sm-12">
                                                                    <?php 
                                                                    $studstatus = $this->Common_model->get_row('students_enrolled',array('course_id' => $id, 'student_id' => $each),'status');?>
                                                                    <p class="tut-name">
                                                                        <b>
                                                                        <?php 
                                                                            echo strtoupper($each_detail->name);
                                                                            if($studstatus == 'I')
                                                                                echo '(Suspended)';
                                                                        ?>
                                                                        </b>
                                                                    </p>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <h5 class="tut-email"><?php echo $each_detail->email;?></h5>
                                                                </div>
                                                            </div>
                                                        </div>
                                                <?php
                                                    }

                                                } ?>
                                            </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

