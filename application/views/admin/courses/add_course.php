<?php
$this->load->view('admin/common/header');
?>
<script src="<?php echo HTTP_PLUGIN_PATH;?>ckeditor/ckeditor.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> COURSES</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">Add Course</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-dim-secondary card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Add Course Form</h3>
                        </div>
                        <?php echo form_open_multipart('admin/courses/course_save'); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                            <label>Course Category<span class="text-red">*</span></label>
                                            <select name="course_category" class="form-control select2">
                                                <option value="">-- Select --</option>
                                            <?php 
                                                $categories = $this->Common_model->get_all_rows('categories');
                                                if($categories){
                                                    foreach($categories as $each){ ?>
                                                        <option value="<?php echo $each['id'];?>" <?php if($category_id == $each['id']) echo 'selected';?>><?php echo $each['name'];?></option>
                                            <?php
                                                    }
                                                }
                                            ?>
                                            </select>
                                            <?php echo form_error('course_category');?>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-5">
                                        <div class="form-group">
                                            <label>Course Title<span class="text-red">*</span></label>
                                            <input name="course_title" class="form-control" value="<?php echo $title;?>">
                                            <?php echo form_error('course_title');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Course Duration<span class="text-red">*</span></label>
                                            <input name="duration" class="form-control" value="<?php echo $duration;?>">
                                            <?php echo form_error('duration');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Course Fee (in INR)<span class="text-red">*</span></label>
                                            <input id="course_fee" name="course_fee" class="form-control" type="text" value="<?php echo $fee;?>" readonly>
                                            <?php echo form_error('course_fee');?>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Course Details<span class="text-red">*</span></label>
                                            <textarea name="course_details" id="details"  class="form-control"><?php echo $details;?></textarea>
                                            <?php echo form_error('course_details');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Course Code<span class="text-red">*</span></label>
                                                    <input name="course_code" class="form-control" value="<?php echo $course_code;?>">
                                                    <?php echo form_error('course_code');?>
                                                </div>
                                            </div>

                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label>Last Reg. No<span class="text-red">*</span></label>
                                                    <input id="last_reg_no" name="last_reg_no" class="form-control" type="number" value="<?php echo $last_reg_no;?>">
                                                    <?php echo form_error('last_reg_no');?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Start Date<span class="text-red">*</span></label> 
                                                    <div id='datetimepicker8'>
                                                        <div class="input-group">
                                                            <?php 
                                                                if($id != '' && $start_date != ''){
                                                                    $sdateexp = explode('/',$start_date);
                                                                    $sdate = $sdateexp[2].'-'.$sdateexp[1].'-'.$sdateexp[0];
                                                                    $s_date = date('Y-m-d',strtotime($sdate));

                                                                    $today = date('Y-m-d');
                                                                    if($s_date < $today){
                                                                        $sread = 'readonly';
                                                                    }
                                                                    else
                                                                        $sread = '';
                                                                }
                                                                else
                                                                    $sread = '';
                                                            ?>
                                                            <input type='text' class="form-control date" name="start_date" id="start_date" <?php if($start_date !='') echo 'value="'.$start_date.'"'; ?> <?php echo $sread;?>/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo form_error('start_date'); ?>  
                                                </div>        
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>End Date<span class="text-red">*</span></label> 
                                                    <div class='date' id='datetimepicker9'>
                                                        <div class="input-group">
                                                            <input type='text' class="form-control date" name="end_date" id="end_date" <?php if($end_date !='') echo 'value="'.$end_date.'"'; ?> />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo form_error('end_date'); ?>  
                                                </div>        
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Display From<span class="text-red">*</span></label> 
                                                    <div class='date'>
                                                        <div class="input-group">
                                                        <?php 
                                                            if($id != '' && $display_date != ''){
                                                                $ddateexp = explode('/',$display_date);
                                                                $ddate = $ddateexp[2].'-'.$ddateexp[1].'-'.$ddateexp[0];
                                                                $d_date = date('Y-m-d',strtotime($ddate));

                                                                $today = date('Y-m-d');
                                                                if($d_date < $today){
                                                                    $dread = 'readonly';
                                                                }
                                                                else
                                                                    $dread = '';
                                                            }
                                                            else
                                                                $dread = '';
                                                        ?>
                                                            <input type='text' class="form-control date" name="display_date" id="display_date" <?php if($display_date !='') echo 'value="'.$display_date.'"'; ?> <?php echo $dread;?>/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo form_error('display_date'); ?>  
                                                </div>        
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Course Icon (jpg or png, Min widthxheight: 781 X 450px)<span class="text-red">*</span></label>
                                                    <input name="course_icon" class="form-control" type="file" value="<?php echo $icon_name;?>"><?php echo $icon_name;?>
                                                    <?php 
                                                    if($icon_name != '' && file_exists('uploads/courses/'.$icon_name)) {  ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>courses/<?php echo $icon_name;?>">
                                                    <?php } ?>
                                                    <div class="text-red"><?php echo $file_error;?></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Upload PDF<span class="text-red">*</span></label>
                                                    <input name="course_pdf" class="form-control" type="file" value="<?php echo $course_pdf;?>"><?php echo $course_pdf;?>
                                                    <div class="text-red"><?php echo $pdf_error;?></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Add Tutors<span class="text-red">*</span></label>
                                                    <select name="tutors_assigned[]" class="form-control select2" multiple>
                                                        <option value="">-- Select --</option>
                                                    <?php 
                                                        $tutors_inactive = array(); 
                                                        $tutors = $this->Common_model->get_all_rows('tutors',array('status' => 'A'),array('name' => 'ASC'),'id,name,name_title');
                                        
                                                        if($id != ''){
                                                            if(!empty($tutors_assigned)){
                                                                $tutors_inactive = $this->Common_model->get_all_rows('tutors',array('status' => 'I'),array('name' => 'ASC'),'id,name,name_title',array('id' => $tutors_assigned));
                                                            }
                                                        }
                                                        
                                                        if($tutors){
                                                            foreach($tutors as $each){ ?>
                                                                <option value="<?php echo $each['id'];?>" <?php if(in_array($each['id'],$tutors_assigned)) echo 'selected';?>>
                                                                <?php 
                                                                    echo $each['name_title'].' '.$each['name'];
                                                                ?></option>
                                                    <?php
                                                            }
                                                        }

                                                        if($tutors_inactive){
                                                            foreach($tutors_inactive as $each){ ?>
                                                                <option value="<?php echo $each['id'];?>" <?php if(in_array($each['id'],$tutors_assigned)) echo 'selected';?>>
                                                                <?php echo  $each['name_title'].' '.$each['name'].'(Suspended)';?></option>
                                                    <?php
                                                            }
                                                        }
                                                    ?>
                                                    </select>
                                                    <?php echo form_error('tutors_assigned[]');?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label><u>PAYMENT STRUCTURE</u></label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row mt-15">
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <label>Part<span class="text-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Amount<span class="text-red">*</span></label>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label>Due Date<span class="text-red">*</span></label>
                                        </div>
                                    </div>
                                </div>
                                <div id="payment_structure_error" class="text-red"><?php echo $payment_structure_error;?></div>
                                <?php 
                                if($fee_structures != ''){ ?>
                                        <div class="field_wrapper1">
                                            <div class="row">
                                                <div class="col-sm-1">
                                                    <div class="form-group mt8">
                                                        PART 1
                                                    </div>
                                                </div>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <input type="hidden" name="fid[]" value="<?php if(isset($fee_structures[0]['id'])) echo $fee_structures[0]['id'];?>">
                                                        <input type="number" name="fee_part[]" class="form-control feechange"  value="<?php if(isset($fee_structures[0]['amount'])) echo $fee_structures[0]['amount'];?>" onblur="change_fee()">
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <div class="input-group">
                                                            <input type='text' class="form-control date" name="fee_date[]" value="<?php if(isset($fee_structures[0]['due_date']) && $fee_structures[0]['due_date'] != '0000-00-00') echo date('d/m/Y',strtotime($fee_structures[0]['due_date']));?>"/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <a href="javascript:void(0);" class="addButton" title="Add field">
                                                        <img  class="add-icon-img" src="<?php echo base_url(); ?>assets/images/add-icon.png"/>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>

                                        <?php if(isset($fee_structures) && sizeof($fee_structures) > 1){
                                                 for($ect = 1; $ect < sizeof($fee_structures); $ect++ ){ ?>
                                                        <div class="field_wrapper1">
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                    <div class="form-group mt8">
                                                                        PART <?php echo $ect+1;?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="fid[]" value="<?php if(isset($fee_structures[$ect]['id'])) echo $fee_structures[$ect]['id'];?>">
                                                                        <input type="number" name="fee_part[]" class="form-control feechange"  value="<?php if(isset($fee_structures[$ect]['amount'])) echo $fee_structures[$ect]['amount'];?>" onblur="change_fee()">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <input type='text' class="form-control date" name="fee_date[]" value="<?php if(isset($fee_structures[$ect]['due_date']) && $fee_structures[$ect]['due_date'] != '0000-00-00') echo date('d/m/Y',strtotime($fee_structures[$ect]['due_date']));?>"/>
                                                                            <div class="input-group-append">
                                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <a href="javascript:void(0);" class="remove_button" title="Remove field"><img  class="remove-icon-img" src="<?php echo base_url(); ?>assets/images/remove-icon.png"/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                        <?php    } 
                                              }
                                }      
                                else{ ?>
                                    <div class="field_wrapper1">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                <div class="form-group mt8">
                                                    PART 1
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <input type="hidden" name="fid[]" value="<?php if(isset($fid[0])) echo $fid[0];?>">
                                                    <input type="number" name="fee_part[]" class="form-control feechange" value="<?php if(isset($fee_part[0])) echo $fee_part[0];?>" onblur="change_fee()">
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <div class="input-group">
                                                        <input type='text' class="form-control date" name="fee_date[]" value="<?php if(isset($fee_date[0])) echo $fee_date[0];?>"/>
                                                        <div class="input-group-append">
                                                            <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-1">
                                                <a href="javascript:void(0);" class="addButton" title="Add field">
                                                    <img  class="add-icon-img" src="<?php echo base_url(); ?>assets/images/add-icon.png"/>
                                                </a>
                                            </div>
                                        </div>
                                    </div>

                                    <?php if(isset($fee_part) && sizeof($fee_part) > 1){
                                                 for($tct = 1; $tct < sizeof($fee_part); $tct++ ){ ?>
                                                        <div class="field_wrapper1">
                                                            <div class="row">
                                                                <div class="col-sm-1">
                                                                    <div class="form-group mt8">
                                                                        PART <?php echo $tct+1;?>
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <div class="form-group">
                                                                        <input type="hidden" name="fid[]" value="<?php if(isset($fid[$tct])) echo $fid[$tct];?>">
                                                                        <input type="number" name="fee_part[]" class="form-control feechange" value="<?php if(isset($fee_part[$tct])) echo $fee_part[$tct];?>" onblur="change_fee()">
                                                                    </div>
                                                                </div>
                                                                <div class="col-sm-3">
                                                                    <div class="form-group">
                                                                        <div class="input-group">
                                                                            <input type='text' class="form-control date" name="fee_date[]" value="<?php if(isset($fee_date[$tct])) echo $fee_date[$tct];?>"/>
                                                                            <div class="input-group-append">
                                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1">
                                                                    <a href="javascript:void(0);" class="remove_button" title="Remove field"><img  class="remove-icon-img" src="<?php echo base_url(); ?>assets/images/remove-icon.png"/></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                    <?php         }
                                           }
                                    
                                } ?>
                                

                                <div id="field_wrapper" class="field_wrapper"></div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                       
                        </form>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    $(function () {
        CKEDITOR.replace('details');
        CKEDITOR.config.toolbar =
        [
            { name: 'snddocument', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
            '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak','Iframe' ] },
            { name: 'styles', items : [ 'Styles','Format','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
        ];
        CKEDITOR.config.height = '398px';
       
        $('.select2').select2();

    });

    function change_fee(){
        var amount = 0;
        $(".feechange").each(function() {
            amount = +amount + +($(this).val());
        });
        $('#course_fee').val(amount);
    }

    $(document).ready(function () {
        var maxField = 10; //Input fields increment limitation
        var addButton = $('.addButton');
        var wrapper = $('.field_wrapper'); //Input field wrapper
        var wrapper1 = $('.field_wrapper1'); //Input field wrapper
        var officials_wrapper = $('#field_wrapper'); //Input field wrapper
        var x = 1; //Initial field counter is 1 
        $(addButton).click(function () { //Once add button is clicked
            var y = $('.remove_button').length;
            x = +y + 1;
            if (x < maxField) { //Check maximum number of input fields
                x++; //Increment field counter
                var fieldHTML = '<div class="row"><div class="col-sm-1"><div class="form-group mt8">PART'+x+'</div></div><div class="col-sm-2"><div class="form-group"> <input type="hidden" name="fid[]" value=""><input type="number" name="fee_part[]" class="form-control feechange" value="" onblur="change_fee()"></div></div><div class="col-sm-3"><div class="form-group"><div class="input-group"><input type="text" class="form-control date exp_date" name="fee_date[]" /><div class="input-group-append"><span class="input-group-text"><i class="fa fa-calendar"></i></span></div></div></div></div><div class="col-sm-1"><a href="javascript:void(0);" class="remove_button col-md-2" title="Remove field"><img class="remove-icon-img-dynamic" src="<?php echo base_url(); ?>assets/images/remove-icon.png"/></a></div></div>'; //New input field html 
                
                $( officials_wrapper).append(fieldHTML); // Add field html
                function change_fee(){
                    var amount = 0;
                    $(".feechange").each(function() {
                        amount = +amount + +($(this).val());
                    });
                    $('#course_fee').val(amount);
                }
            }
            $('.exp_date').datetimepicker({
                useCurrent: false, //Important! See issue #1075
                format: 'DD/MM/YYYY'
            });
        });
        $(wrapper).on('click', '.remove_button', function (e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            change_fee();
        });
        $(wrapper1).on('click', '.remove_button', function (e) { //Once remove button is clicked
            e.preventDefault();
            $(this).parent('div').parent('div').remove(); //Remove field html
            x--; //Decrement field counter
            change_fee();
        });
    });
</script>
