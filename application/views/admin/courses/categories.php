<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> COURSES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">Categories</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-5">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Category Form</h3>
                        </div>
                        <?php echo form_open_multipart('admin/courses/categories_save'); ?>
                            <div class="card-body">
                                <div class="form-group">
                                    <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                    <label for="name">Name<span class="text-red">*</span></label>
                                    <input type="text" name="name" class="form-control" placeholder="Category Name" value="<?php echo $name;?>">
                                    <?php echo form_error('name');?>
                                </div>
                                
                                <div class="form-group">
                                    <label for="status">Status<span class="text-red">*</span></label>
                                    <div class="form-group clearfix">
                                        <div class="icheck-success d-inline">
                                            <input type="radio" name="status" <?php if($status=='A' ||$status=='' ) echo 'checked';?> value="A" id="radioSuccess1">
                                            <label for="radioSuccess1">
                                                Active
                                            </label>
                                        </div>
                                        <div class="icheck-danger d-inline">
                                            <input type="radio" name="status" id="radioSuccess2" <?php if($status=='I' ) echo 'checked';?> value="I">
                                            <label for="radioSuccess2">
                                                Inactive
                                            </label>
                                        </div>
                                        <?php echo form_error('status');?>
                                    </div>
                                    <?php echo form_error('status');?>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                       
                        </form>
                    </div>
                </div>
               
                <div class="col-sm-12 col-md-7">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Category List</h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php if($categories){?>
                                <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th class="no-sort">Action</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($categories as $row){?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td>
                                                    <?php if($row['status'] == 'A')
                                                        echo 'Active';
                                                else
                                                        echo 'Inactive';
                                                    ?>
                                                </td>
                                                <td>
                                                    <a class="green-color" href="<?php echo base_url();?>admin/courses/categories?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

