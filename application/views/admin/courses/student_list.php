<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> COURSES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">Student List</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Student List - <?php echo $course_title;?></h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php 
                                if($students){?>
                                <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Profile Pic</th>
                                            <th>Student Id</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Mobile</th>
                                            <th>Parent Name</th>
                                            <th>Parent Email</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($students as $each){
                                        $row = (array)$this->Common_model->get_row('students',array('id' => $each));  
                                        $student_uname = $this->Common_model->get_row('users',array('id' => $row['user_id']),'user_name');  
                                    ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php 
                                                    if($row['profile_pic'] != '' && file_exists('uploads/students/small/'.$row['profile_pic'])){ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>students/small/<?php echo $row['profile_pic'];?>">
                                                    <?php
                                                    }
                                                    else{ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>students/small/default.jpg">
                                                    <?php }
                                                ?></td>
                                                <td><?php echo $student_uname; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo $row['email']; ?></td>
                                                <td><?php echo $row['mobile']; ?></td>
                                                <td><?php echo $row['parent_name']; ?></td>
                                                <td><?php echo $row['parent_email']; ?></td>
                                                <td>
                                                    <?php 
                                                        $status_arr = array('R' => 'Registered','A' => 'Admitted', 'I' => 'Suspended');
                                                        $studstatus = $this->Common_model->get_row('students_enrolled',array('course_id' => $course_id,'student_id' => $each),'status');
                                                        echo $status_arr[$studstatus]; ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

