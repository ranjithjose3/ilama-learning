<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> COURSES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">Tutor List</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Tutor List - <?php echo $course_title;?></h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php 
                                if($tutors){?>
                                <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Profile Pic</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Degrees</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($tutors as $each){
                                        $row = (array)$this->Common_model->get_row('tutors',array('id' => $each));    
                                    ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php 
                                                    if($row['profile_pic'] != '' && file_exists('uploads/tutors/small/'.$row['profile_pic'])){ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/<?php echo $row['profile_pic'];?>">
                                                    <?php
                                                    }
                                                    else{ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/default.jpg">
                                                    <?php }
                                                ?></td>
                                                <td><?php echo $row['name_title'].' '.$row['name']; ?></td>
                                                <td><?php echo $row['email']; ?></td>
                                                <td><?php echo $row['phone_number']; ?></td>
                                                <td><?php echo $row['degrees']; ?></td>
                                                <td>
                                                    <?php 
                                                        if($row['status'] == 'A')
                                                            echo 'Active';
                                                        elseif($row['status'] == 'I')
                                                            echo 'Suspended';
                                                     ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

