<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> COURSES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">Course List</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Course List</h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php 
                                $today = date('Y-m-d');
                                if($courses){?>
                                <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Title</th>
                                            <th>Category</th>
                                            <th>Icon</th>
                                            <th>Duration</th>
                                            <th>Fee Details</th>
                                            <th class="no-sort">Action</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($courses as $row){?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php echo $row['title'].'<br/>Code: '.$row['course_code']; ?></td>
                                                <td><?php 
                                                    echo $this->Common_model->get_row('categories',array('id' => $row['category_id']),'name');
                                                ?></td>
                                                <td><?php 
                                                    if($row['icon_name'] != '' && file_exists('uploads/courses/'.$row['icon_name'])){ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>courses/<?php echo $row['icon_name'];?>">
                                                    <?php
                                                    }
                                                ?></td>
                                                <td><?php 
                                                    echo date('d/m/Y',strtotime($row['start_date'])).' - '.date('d/m/Y',strtotime($row['end_date'])).'<br/>Duration:-  '.$row['duration'];
                                                ?></td>
                                                <td><?php 
                                                    echo 'Fee:- Rs. '.$row['fee'].'<br/>';
                                                    if($row['payment_structure'] == 1)
                                                        echo 'Single Payment';
                                                    else
                                                        echo $row['payment_structure'].' Installments';
                                                ?></td>
                                                <td>
                                                    <a title="Edit Course" class="green-color" href="<?php echo base_url();?>admin/courses/add_course?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a title="View course" class="green-color" href="<?php echo base_url();?>admin/courses/view_course?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <?php 
                                                    if($today < $row['display_from']){
                                                        if($row['status'] == 'A') { ?>
                                                            <a title="Suspend Course" href="<?php echo base_url();?>admin/courses/suspend_course?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to suspend this course?')">
                                                                <i class="fa fa-pause"></i>
                                                            </a>
                                                        <?php } 
                                                        elseif($row['status'] == 'I'){ ?>
                                                            <a title="Activate Course" href="<?php echo base_url();?>admin/courses/activate_course?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to activate this course?')">
                                                                <i class="fa fa-check-square-o"></i>
                                                            </a>
                                                        <?php }
                                                    } ?>
                                                    <a title="Tutor List" href="<?php echo base_url();?>admin/courses/tutor_list?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-address-book"></i>
                                                    </a>
                                                    <a title="Student List" href="<?php echo base_url();?>admin/courses/student_list?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-users"></i>
                                                    </a>

                                                    <?php /* <a title="Forum" href="<?php echo base_url();?>admin/forums/forum_view/<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-comments-o">Forum</i>
                                                    </a> */ ?>


                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

