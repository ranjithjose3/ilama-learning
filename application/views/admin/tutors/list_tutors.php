<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> TUTORS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Tutors</a>
                        </li>
                        <li class="breadcrumb-item active">Tutor List</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Tutor List</h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php 
                                if($tutors){?>
                                <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Profile Pic</th>
                                            <th>Name</th>
                                            <th>Contact Details</th>
                                            <th>Degrees</th>
                                            <th>Course Assigned</th>
                                            <th>Action</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($tutors as $row){   
                                    ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php 
                                                    if($row['profile_pic'] != '' && file_exists('uploads/tutors/small/'.$row['profile_pic'])){ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/<?php echo $row['profile_pic'];?>">
                                                    <?php
                                                    }
                                                    else{ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/small/default.jpg">
                                                    <?php }
                                                ?></td>
                                                <td><?php echo $row['name_title'].' '.$row['name']; ?></td>
                                                <td><?php echo 'Email:- '.$row['email'].'<br/>Mobile:- '.$row['phone_number']; ?></td>
                                                <td><?php echo $row['degrees']; ?></td>
                                                <td><?php 
                                                        $course_list = '';
                                                        if($row['course_assigned'] != '')
                                                            $course_assigned_arr = explode(',',$row['course_assigned']);
                                                        else
                                                            $course_assigned_arr = array();
                                                        if(!empty($course_assigned_arr)){
                                                            foreach($course_assigned_arr as $each){
                                                                $if_active = $this->Common_model->is_course_active($each);
                                                                if($if_active > 0){
                                                                    if($course_list != ''){
                                                                        $course_list .= ', '.$this->Common_model->get_row('courses',array('id' => $each),'title');
                                                                    }
                                                                    else{
                                                                        $course_list .= $this->Common_model->get_row('courses',array('id' => $each),'title');
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        echo $course_list;
                                                    ?>
                                                </td>
                                                <td>
                                                    <a title="Edit Tutor" class="green-color" href="<?php echo base_url();?>admin/tutors/add_tutor?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                            <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a title="View Tutor" class="green-color" href="<?php echo base_url();?>admin/tutors/view_tutor?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <?php if($row['status'] == 'A') { ?>
                                                            <a title="Suspend Tutor" href="<?php echo base_url();?>admin/tutors/suspend_tutor?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to suspend this tutor?')">
                                                                <i class="fa fa-pause"></i>
                                                            </a>
                                                        <?php } 
                                                        elseif($row['status'] == 'I'){ ?>
                                                            <a title="Activate Tutor" href="<?php echo base_url();?>admin/tutors/activate_tutor?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to activate this tutor?')">
                                                                <i class="fa fa-check-square-o"></i>
                                                            </a>
                                                        <?php } ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

