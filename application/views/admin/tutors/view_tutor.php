<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> TUTORS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Tutors</a>
                        </li>
                        <li class="breadcrumb-item active">View Tutor</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">View Tutor - <?php echo $name_title.' '.strtoupper($name);?></h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3 left-pane">
                                    <div class="row">
                                        <?php if($profile_pic != '' && file_exists('uploads/tutors/medium/'.$profile_pic)){ ?>
                                            <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/<?php echo $profile_pic;?>">
                                        <?php } 
                                        else { ?>
                                            <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/default.jpg">
                                        <?php } ?>
                                    </div>
                                    <div class="row pt-15">
                                        <p class="view-course"><b>Active Courses Assigned: </b></p>
                                    </div>
                                    <div class="row">
                                            <ul class="courses-assigned">
                                            <?php $course_list = '';
                                                if($course_assigned != '')
                                                    $course_assigned_arr = explode(',',$course_assigned);
                                                else
                                                    $course_assigned_arr = array();
                                                if(!empty($course_assigned_arr)){
                                                    foreach($course_assigned_arr as $each){
                                                        $if_active = $this->Common_model->is_course_active($each);
                                                        if($if_active > 0){
                                                            $course_list = $this->Common_model->get_row('courses',array('id' => $each),'title'); ?>
                                                            <li><i class="fa fa-book"></i><?php echo $course_list;?></li>
                                                       
                                                <?php
                                                        }
                                                    }
                                                } ?>
                                                
                                            </ul>
                                    </div>
                                </div>

                                <div class="col-sm-9">
                                    <div class="row">
                                        <div class="col-sm-8">
                                            <p class="tut-contact ml10">
                                                <i class="fa fa-map-marker"></i><?php echo $address;?><br/>
                                                <i class="fa fa-envelope fts15"></i><?php echo $email;?><br/>
                                                <i class="fa fa-mobile fts29"></i><?php echo $phone_number;?><br/>
                                                <i class="fa fa-graduation-cap fts17"></i><?php echo $degrees;?><br/>
                                                <i class="fa fa-video-camera ftdemo"></i>
                                                <?php 
                                                    if($demo_video != '' && file_exists('uploads/tutors/demo/'.$demo_video)){ ?>
                                                        <a class="black-text modal-link" onclick="open_demo_modal()" >Click here for demo video</a>
                                                        <!-- Demo Modal -->
                                                        <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2">
                                                            <div class="modal-dialog reply-modal" role="document">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title" id="myModalLabel2">DEMO VIDEO</h4>
                                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                    </div>
                                                                    <div class="modal-body" id="ViewModalBody2">
                                                                        <div class="row">
                                                                            <div class="col-sm-12 video-play-btn">
                                                                                <video id="video2" class="home-video" controls style="width:100%;padding:50px;">
                                                                                    <source src="<?php echo HTTP_UPLOADS_PATH;?>tutors/demo/<?php echo $demo_video;?>">
                                                                                </video>
                                                                            </div>
                                                                
                                                                        </div>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- Demo Modal -->
                                                    <?php }
                                                ?><br/>
                                            </p>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="card card-outline card-warning card-no-bg ml10">
                                                    <div class="card-header">
                                                        <h3 class="card-title"><b>All Assigned Courses </b></h3>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <div class="card-body all-assigned-courses">
                                                        <div class="row">
                                                                <ul class="courses-assigned">
                                                                <?php $course_list = '';
                                                                    if($course_assigned != '')
                                                                        $course_assigned_arr = explode(',',$course_assigned);
                                                                    else
                                                                        $course_assigned_arr = array();
                                                                    if(!empty($course_assigned_arr)){
                                                                        foreach($course_assigned_arr as $each){
                                                                            $course_list = $this->Common_model->get_row('courses',array('id' => $each),'title'); ?>
                                                                            <li><i class="fa fa-book"></i><?php echo $course_list;?></li>
                                                                        
                                                                    <?php
                                                                        }
                                                                    } ?>
                                                                    
                                                                </ul>
                                                        </div>
                                                    </div>
                                                    <!-- /.card-body -->
                                            </div>
                                                <!-- /.card -->
                                        </div>
                                    </div>
                                    <div class="row mt-25">
                                        <div class="col-sm-12">
                                            <div class="card card-outline card-success card-no-bg ml10">
                                                <div class="card-header">
                                                    <h3 class="card-title"><b>Profile </b></h3>
                                                </div>
                                                <!-- /.card-header -->
                                                <div class="card-body">
                                                    <?php echo $profile;?>
                                                </div>
                                                <!-- /.card-body -->
                                            </div>
                                            <!-- /.card -->
                                        </div>
                                    </div>

            
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
	function open_demo_modal(){
        $( "#myModal2" ).modal();
    }

	$('#myModal2').on('shown.bs.modal', function () {
		$('#video2')[0].play();
	})
	$('#myModal2').on('hidden.bs.modal', function () {
		$('#video2')[0].pause();
	})
</script>


