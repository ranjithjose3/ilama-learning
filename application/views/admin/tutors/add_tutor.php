<?php
$this->load->view('admin/common/header');
?>
<script src="<?php echo HTTP_PLUGIN_PATH;?>ckeditor/ckeditor.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> TUTORS</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Tutors</a>
                        </li>
                        <li class="breadcrumb-item active">Add Tutor</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Add Tutor Form</h3>
                        </div>
                        <?php echo form_open_multipart('admin/tutors/tutor_save'); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                            <label>Title<span class="text-red">*</span></label>
                                            <select name="name_title" class="form-control">
                                                <option value="">-- Select --</option>
                                            <?php foreach(json_decode(NAME_TITLES,TRUE) as $each){ ?>
                                                <option value="<?php echo $each;?>" <?php if($each == $name_title) echo 'selected';?>><?php echo $each;?></option>
                                            <?php } ?>
                                            </select>
                                            <?php echo form_error('name_title');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Name<span class="text-red">*</span></label>
                                            <input name="name" class="form-control" value="<?php echo $name;?>">
                                            <?php echo form_error('name');?>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label>Phone Number<span class="text-red">*</span></label>
                                            <input name="phone_number" class="form-control" value="<?php echo $phone_number;?>">
                                            <?php echo form_error('phone_number');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label>Email<span class="text-red">*</span></label>
                                            <input type="email" name="email" class="form-control" value="<?php echo $email;?>">
                                            <?php echo form_error('email');?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Profile<span class="text-red">*</span></label>
                                            <textarea name="profile" id="details"  class="form-control"><?php echo $profile;?></textarea>
                                            <?php echo form_error('profile');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Address<span class="text-red">*</span></label>
                                                    <textarea rows="5" name="address" class="form-control"><?php echo $address;?></textarea>
                                                    <?php echo form_error('address');?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Profile Pic (jpg or png, Min widthxheight: 260 X 290px)<span class="text-red">*</span></label>
                                                    <input name="profile_pic" class="form-control" type="file" value="<?php echo $profile_pic;?>"><?php echo $profile_pic;?>
                                                    <?php 
                                                    if($profile_pic != '' && file_exists('uploads/tutors/'.$profile_pic)) {  ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/<?php echo $profile_pic;?>">
                                                    <?php } ?>
                                                    <div class="text-red"><?php echo $file_error;?></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Degrees<span class="text-red">*</span></label>
                                                    <input id="degrees" name="degrees" class="form-control" type="text" value="<?php echo $degrees;?>">
                                                    <?php echo form_error('degrees');?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Demo Video (wmv,mp4,avi or mov)Max Size: 30MB <span class="text-red">*</span></label>
                                                    <input name="demo_video" class="form-control" type="file" value="<?php echo $demo_video;?>"><?php echo $demo_video;?>
                                                    <div class="text-red"><?php echo $video_error;?></div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-info pull-right">Save</button>
                            </div>
                       
                        </form>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    $(function () {
        CKEDITOR.replace('details');
        CKEDITOR.config.toolbar =
        [
            { name: 'snddocument', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
            '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak','Iframe' ] },
            { name: 'styles', items : [ 'Styles','Format','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
        ];
        CKEDITOR.config.height = '250px';

    });
</script>
