<?php   $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> MESSAGE MANAGEMENT </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Messages</a>
                        </li>
                        <li class="breadcrumb-item active">Messages Management</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php   if(isset($msg)){
			 $message = $msg;
			 
			 }
			 else
			 $message ='';		 
	   ?>
            <?php  
		 if($message!='')
			{ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>

            <?php }  ?>

            <div class="row">
            
                <div class="col-sm-12 col-md-12 pr-0">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h2 class="card-title">Message List</h2>
                    </div>
                    <div class="card-body">
                     <div id="status_div">
                        <?php if($messages){?>
                        <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable datatable-Blood_group" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="no-sort">#</th>
                                    <th>Subject</th>                                    
                                    <th>Courses</th>
                                    <th>Send Date</th>
                                    <th class="no-sort">Action</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach($messages as $row){?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $row['subject']; ?></td> 
                                        <td>
                                            
                                        </td>
                                        <td>
                                        <?php echo ($row['send_date']!='null') ? date('d/m/Y H:i:s',strtotime($row['send_date']))  : ''; ?>
                                        </td>
                                        <td>
                                            <a class="green-color" href="<?php echo base_url();?>admin/messages/index?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                <i class="fa fa-edit"></i>
                                            </a>   <a class="red-color" href="<?php echo base_url();?>admin/messages/view_recipients?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                <i class="fa fa-eye"></i>
                                            </a>
                                        </td>
                                        <td></td>
                                    </tr>
                                 <?php } ?>
                            </tbody>
                        </table>
                        <?php } 
                        else{
                            echo 'No details found!!';
                        }?>
                    </div>
                    </div>
                    </div>
                    
                    
                    </div>
                </div>
                </div>
                <!--row-->
    
            </div>
            <!-- content-->
        </div>
        <!-- content-wrapper-->
        <?php
    $this->load->view('admin/common/footer');
    ?>