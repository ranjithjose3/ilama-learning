<?php
$this->load->view('admin/common/header');
?>
<script src="<?php echo HTTP_PLUGIN_PATH;?>ckeditor/ckeditor.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> MESSAGES</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Messages</a>
                        </li>
                        <li class="breadcrumb-item active">Add Message</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Add Message Form</h3>
                        </div>
                        <?php echo form_open_multipart('admin/messages/message_save'); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                                    <label>Subject<span class="text-red">*</span></label>
                                                    <input type="text" name="subject" id="subject" value="<?php echo $subject;?>" class="form-control">
                                                    <?php echo form_error('subject');?>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Content<span class="text-red">*</span></label>
                                                    <textarea name="content" id="content"  class="form-control"><?php echo $content;?></textarea>
                                                    <?php echo form_error('content');?>
                                                </div>
                                            </div> 
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Attachment</label>
                                                    <input name="attachment" class="form-control" type="file" value="<?php echo $attachment;?>"><?php echo $attachment;?>
                                                    <?php 
                                                    if($attachment != '' && file_exists('uploads/messages/'.$attachment)) {  ?>
                                                       <?php echo $attachment;?>
                                                    <?php } ?>
                                                    <div class="text-red"><?php echo $file_error;?></div>
                                                </div>
                                            </div> 
                                                              
                                          </div>  
                                    </div>
                                <div class="col-md-6" >
                                    <div class="row"  >
                                    <div class="col-md-12">
                                            <div class="form-group">
                                    <label class="control-label">Message Type
                                        <span class="text-red">*</span>
                                    </label>
                                    <div class="form-group clearfix">
                                            <div class="icheck-success d-inline">
                                              <input type="radio" name="msg_type" <?php if($msg_type=='G' ||$msg_type=='' ) echo 'checked';?> value="G" id="radioSuccess1" onclick="hide_student()">
                                              <label for="radioSuccess1">
                                                  Group
                                              </label>
                                            </div>
                                            <div class="icheck-danger d-inline">
                                              <input type="radio" name="msg_type" id="radioSuccess2" <?php if($msg_type=='P' ) echo 'checked';?> value="P">
                                              <label for="radioSuccess2" onclick="show_student()">
                                                  Personal
                                              </label>
                                            </div>
                                            <?php echo form_error('msg_type');?>
                                          </div>
                                    

                                        </div>
                                        
                                        </div>
                                        <div class="col-sm-12">
                                                <div class="form-group" id="multicourse" <?php if($msg_type=='G' || $msg_type == '' ) echo 'style="display:block"'; else echo 'style="display:none;"'; ?>>
                                                    <label>Courses<span class="text-red">*</span></label>
                                                    <select name="courses[]" class="form-control select2" multiple >
                                                        <option value="">-- Select --</option>
                                                    <?php 
                                                       
                                                        if($courses){
                                                            foreach($courses as $each){ ?>
                                                                <option value="<?php echo $each['id'];?>" <?php if(in_array($each['id'],$selected_courses)) echo 'selected';?>>
                                                                <?php 
                                                                    echo $each['title'].'('.$course_categories[$each['category_id']].')';
                                                                ?></option>
                                                    <?php
                                                            }
                                                        }

                                                      ?>
                                                    </select>
                                                    <?php echo form_error('courses[]');?>
                                                </div>
                                                <div class="form-group" id="singlecourse" <?php if($msg_type=='G' || $msg_type == '' ) echo 'style="display:none"'; else echo 'style="display:block;"'; ?>>
                                                    <label>Course<span class="text-red">*</span></label>
                                                    <select name="course" class="form-control select2"    onchange="change_student_list(this.value)" style="width:100%">
                                                        <option value="">-- Select --</option>
                                                    <?php 
                                                       
                                                        if($courses){
                                                            foreach($courses as $each){ ?>
                                                                <option value="<?php echo $each['id'];?>" <?php if(in_array($each['id'],$selected_courses)) echo 'selected';?>>
                                                                <?php 
                                                                    echo $each['title'].'('.$course_categories[$each['category_id']].')';
                                                                ?></option>
                                                    <?php
                                                            }
                                                        }

                                                      ?>
                                                    </select>
                                                    <?php echo form_error('course');?>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 student_section" id="student_section" <?php if($msg_type=='G' || $msg_type == '' ) echo 'style="display:none"'; else echo 'style="display:block;"'; ?> >
                                                <div class="form-group">
                                                    <label>Students<span class="text-red">*</span></label>
                                                    <select name="students" class="form-control select2" style="width:100%;" >
                                                        <option value="">-- Select --</option>
                                                    <?php 
                                                       
                                                        if($students){
                                                            foreach($students as $each){ ?>
                                                                <option value="<?php echo $each['id'];?>" <?php if($each['id'] == $student) echo 'selected';?>>
                                                                <?php 
                                                                    echo $each['name'];
                                                                ?></option>
                                                    <?php
                                                            }
                                                        }

                                                      ?>
                                                    </select>
                                                    <?php echo form_error('students');?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-info pull-right">Send Mail</button>
                            </div>
                       
                        </form>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    $(function () {
        CKEDITOR.replace('content');
        CKEDITOR.config.toolbar =
        [
            { name: 'snddocument', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
            '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak','Iframe' ] },
            { name: 'styles', items : [ 'Styles','Format','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
        ];
        CKEDITOR.config.height = '320px';
       
        $('.select2').select2();

    });
function hide_student(){
    $('.student_section').hide();
    $('#singlecourse').hide();
    $('#multicourse').show();
    }
    function show_student(){
    $('.student_section').show();
    $('#singlecourse').show();
    $('#multicourse').hide();
    }
function change_student_list(course_id){
       var csrf = $('[name="csrf_test_name"]').val();
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/messages/ajax_student_list",
            data:"course_id="+course_id+"&csrf_test_name="+csrf,
            success:function(data){
                    $("#student_section").html(data);
                    $('.select2').select2();
            }
        });
}
  
</script>
