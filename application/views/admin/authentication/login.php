<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>LOGIN | <?php echo SITE_TITLE;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?php echo HTTP_IMAGES_PATH;?>favicon.png"/>
 <!-- Font Awesome -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>adminlte.min.css">
    <!-- Custom style -->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
<div class="login-logo">
  <a href=""><b>iLAMA eLearning</b></a>
</div>
<!-- /.login-logo -->
<div class="card">
  <div class="card-body login-card-body">
  <?php if(isset($error) && $error != ''){ ?>
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                <?php echo $error;?>
            </div>
          <?php } ?>
    <p class="login-box-msg">Sign in to start your session</p>

    <?php echo form_open('admin/authentication/auth_check');?>
      <div class="input-group mb-3">
        <input type="text" name="user_name" class="form-control" placeholder="User Name">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fa fa-envelope"></span>
          </div>
        </div>
      </div>
      <?php echo form_error('user_name');?>
      <div class="input-group mb-3">
        <input type="password" name="password" class="form-control" placeholder="Password">
        <div class="input-group-append">
          <div class="input-group-text">
            <span class="fa fa-lock"></span>
          </div>
        </div>
      </div>
      <?php echo form_error('password');?>
      <div class="row">
        <div class="col-8">
        <p class="mb-1">
        <a href="<?php echo base_url();?>admin/authentication/forgot_password">I forgot my password</a>
      </p>
        </div>
        <!-- /.col -->
        <div class="col-4">
          <button type="submit" class="btn btn-primary btn-block">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- <div class="social-auth-links text-center mb-3">
      <p>- OR -</p>
      <a href="#" class="btn btn-block btn-primary">
        <i class="fab fa-facebook mr-2"></i> Sign in using Facebook
      </a>
      <a href="#" class="btn btn-block btn-danger">
        <i class="fab fa-google-plus mr-2"></i> Sign in using Google+
      </a>
    </div> -->
    <!-- /.social-auth-links -->

   
    
  </div>
  <!-- /.login-card-body -->
</div>
</div>
<!-- /.login-box -->


<!-- jQuery -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo HTTP_JS_PATH;?>adminlte.min.js"></script>

</body>
</html>
