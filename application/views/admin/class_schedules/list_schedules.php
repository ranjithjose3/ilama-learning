<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> CLASS SCHEDULES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Class Schedules</a>
                        </li>
                        <li class="breadcrumb-item active">Schedule List</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Class Schedule List</h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php 
                                $today = date('Y-m-d');
                                if($class_schedules){?>
                                <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th>Course</th>
                                            <th>Tutor</th>
                                            <th class="no-sort">Action</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($class_schedules as $row){?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php 
                                                    echo date('d/m/Y',strtotime($row['date']));
                                                ?></td>
                                                <td><?php 
                                                    echo date('h:i A',strtotime($row['time'])).' - '.date('h:i A',strtotime($row['end_time']));
                                                ?></td>
                                                <td><?php echo $this->Common_model->get_row('courses',array('id' => $row['course_id']),'title'); ?></td>
                                                <td><?php 
                                                    echo $this->Common_model->get_row('tutors',array('id' => $row['tutor_id']),'name_title').' '.$this->Common_model->get_row('tutors',array('id' => $row['tutor_id']),'name');
                                                ?></td>
                                                <td>
                                                    <?php if((date('Y-m-d') < $row['date']) ||( (date('Y-m-d') < $row['date'] == $row['date']) && (date('H:i:s') < $row['time']))) { ?>
                                                        <a title="Edit Class Schedule" class="green-color" href="<?php echo base_url();?>admin/class_schedules/add_schedule?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                    <?php } ?>
                                                    <a title="View Message"  href="<?php echo base_url();?>admin/class_schedules/view_message?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <?php 
                                                        if((date('Y-m-d') < $row['date']) ||( (date('Y-m-d') < $row['date'] == $row['date']) && (date('H:i:s') < $row['time']))) { 
                                                            if($row['send_message_at'] == NULL){ ?>
                                                                <a title="Send Message"  href="<?php echo base_url();?>admin/class_schedules/send_message?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to send message to students about this class schedule?')">
                                                                    <i class="fa fa-envelope class_scheduling"></i>
                                                                </a>
                                                            <?php } ?>
                                                            <a title="Delete Schedule"  href="<?php echo base_url();?>admin/class_schedules/delete_schedule?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>" onclick="return confirm('Are you sure you want to delete this class schedule?')">
                                                                <i class="fa fa-trash"></i>
                                                            </a>
                                                            <?php } ?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

