<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> CLASS SCHEDULES </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Class Schedules</a>
                        </li>
                        <li class="breadcrumb-item active">View Message</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
     <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">View Class Schedule Message</h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                               <p>
                                   DEAR STUDENT<br/>PLEASE MAKE SURE TO ATTEND THE WEBINAR ON<br/><?php echo $start_date;?><br/><?php echo $time;?> IST<br/><br/><?php echo $course.' BY '.$tutor_title.' '.strtoupper($tutor);?><br/><br/>ON <?php echo $topic;?><br/><u><a target="_blank" href="<?php echo $webinar_link;?>" class="black-text" >PLEASE FOLLOW THE FOLLOWING WEBINAR LINK TO ENSURE YOUR PARTICIPATION</a></u><br/><br/><?php echo $message;?><br/><br/><u><a href="<?php echo base_url();?>technical_guidelines" class="black-text" target="_blank">TECHNICAL GUIDELINES LINK </a></u>

                               </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

