<?php
$this->load->view('admin/common/header');
?>
<script src="<?php echo HTTP_PLUGIN_PATH;?>ckeditor/ckeditor.js"></script>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> CLASS SCHEDULES</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Class Schedules</a>
                        </li>
                        <li class="breadcrumb-item active">Add Schedule</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $messg = $msg;
        }
		else
			$messg ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($messg!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($messg);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Add Class Schedule Form</h3>
                        </div>
                        <?php echo form_open_multipart('admin/class_schedules/schedule_save'); ?>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                            <label>Course<span class="text-red">*</span></label>
                                            <select id="course_id" name="course_id" class="form-control select2" onchange="change_tutors()" <?php if($id != '') echo 'disabled';?>>
                                                <option value="">-- Select --</option>
                                            <?php 
                                                $courses = $this->Common_model->get_all_rows('courses',array('status' => 'A','end_date >= ' => date('Y-m-d')));
                                                if($courses){
                                                    foreach($courses as $each){ ?>
                                                        <option value="<?php echo $each['id'];?>" <?php if($course_id == $each['id']) echo 'selected';?>><?php echo $each['title'];?></option>
                                            <?php
                                                    }
                                                }
                                            ?>
                                            </select>
                                            <?php echo form_error('course_id');?>
                                        </div>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <div id="tutor_div">
                                                <label>Tutor<span class="text-red">*</span></label>
                                                <select name="tutor_id" class="form-control select2">
                                                    <option value="">-- Select --</option>
                                                <?php 
                                                    $tutors = array();
                                                    if(($id != '') || ($id == '' && $course_id != '')){
                                                        $tutors_assigned = $this->Common_model->get_row('courses',array('id' => $course_id),'tutors_assigned');
                                                        $tutors_assigned_arr = explode(',',$tutors_assigned);
                                                        $tutors = $this->Common_model->get_all_rows('tutors',array('status' => 'A'),array('name' => 'ASC'),'id,name,name_title',array('id' => $tutors_assigned_arr));
                                                    }
                                                    
                                                   
                                                    if($tutors){
                                                        foreach($tutors as $each){ ?>
                                                            <option value="<?php echo $each['id'];?>" <?php if($tutor_id == $each['id']) echo 'selected';?>><?php echo $each['name_title'].' '.$each['name'];?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                            <?php echo form_error('tutor_id');?>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <label>Message<span class="text-red">*</span></label>
                                            <textarea name="message" id="details"  class="form-control"><?php echo $message;?></textarea>
                                            <?php echo form_error('message');?>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Date<span class="text-red">*</span></label> 
                                                    <div id='datetimepicker8'>
                                                        <div class="input-group">
                                                            <?php 
                                                                if($id != '' && $schedule_date != ''){
                                                                    $sdateexp = explode('/',$schedule_date);
                                                                    $sdate = $sdateexp[2].'-'.$sdateexp[1].'-'.$sdateexp[0];
                                                                    $s_date = date('Y-m-d',strtotime($sdate));

                                                                    $today = date('Y-m-d');
                                                                    if($s_date < $today){
                                                                        $sread = 'readonly';
                                                                    }
                                                                    else
                                                                        $sread = '';
                                                                }
                                                                else
                                                                    $sread = '';
                                                            ?>
                                                            <input type='text' class="form-control date" name="schedule_date" id="schedule_date" <?php if($schedule_date !='') echo 'value="'.$schedule_date.'"'; ?> <?php echo $sread;?>/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo form_error('schedule_date'); ?>  
                                                </div>        
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Start Time<span class="text-red">*</span></label> 
                                                    <div class='date'>
                                                        <div class="input-group">
                                                            <input type='text' class="form-control scheduletime" name="schedule_time" id="schedule_time" <?php if($schedule_time !='') echo 'value="'.$schedule_time.'"'; ?>/>
                                                        
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo form_error('schedule_time'); ?>  
                                                </div>        
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>End Time<span class="text-red">*</span></label> 
                                                    <div class='date'>
                                                        <div class="input-group">
                                                            <input type='text' class="form-control scheduletime" name="end_time" id="end_time" <?php if($end_time !='') echo 'value="'.$end_time.'"'; ?>/>
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php echo form_error('end_time'); ?>  
                                                </div>        
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Topic<span class="text-red">*</span></label>
                                                    <textarea rows="4" name="topic" class="form-control"><?php echo $topic;?></textarea>
                                                    <?php echo form_error('topic');?>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="form-group">
                                                    <label>Webinar Link<span class="text-red">*</span></label>
                                                    <input id="webinar_link" name="webinar_link" class="form-control" type="text" value="<?php echo $webinar_link;?>" >
                                                    <?php echo form_error('webinar_link');?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>

                            <div class="card-footer">
                                <button type="submit" class="btn btn-info pull-right" onclick="return beforeSubmit()">Save</button>
                            </div>
                       
                        </form>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    $(function () {
        CKEDITOR.replace('details');
        CKEDITOR.config.toolbar =
        [
            { name: 'snddocument', items : [ 'Source','-','Save','NewPage','DocProps','Preview','Print','-','Templates' ] },
            { name: 'clipboard', items : [ 'Undo','Redo' ] },
            { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
            { name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
            { name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
            '-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-' ] },
            { name: 'links', items : [ 'Link','Unlink','Anchor' ] },
            { name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','PageBreak','Iframe' ] },
            { name: 'styles', items : [ 'Styles','Format','FontSize' ] },
            { name: 'colors', items : [ 'TextColor','BGColor' ] },
            { name: 'tools', items : [ 'Maximize', 'ShowBlocks' ] }
        ];
        CKEDITOR.config.height = '305px';
       
        $('.select2').select2();

        $('#schedule_time').datetimepicker({
            format: 'hh:mm A',
            icons: {
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        })

        <?php if($schedule_time != '') { ?>
            $('#schedule_time').datetimepicker().val("<?php echo date('h:i A', strtotime($schedule_time));?>");
        <?php } ?>
      
        $('#end_time').datetimepicker({
            format: 'hh:mm A',
            icons: {
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            }
        })

        <?php if($end_time != '') { ?>
            $('#end_time').datetimepicker().val("<?php echo date('h:i A', strtotime($end_time));?>");
        <?php } ?>

    });

    function change_tutors(){
        var course_id = document.getElementById('course_id').value;
        var csrf = $('[name="csrf_test_name"]').val();
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/class_schedules/ajax_tutors",
            data:"course_id="+course_id+"&csrf_test_name="+csrf,
            success:function(data){
                    $("#tutor_div").html(data);
                    $('.select2').select2();
            }
        });
    }

    function beforeSubmit(){
        document.getElementById('course_id').disabled = false;
        return true;
    }
</script>
