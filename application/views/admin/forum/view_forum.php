<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> FORUM </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Courses</a>
                        </li>
                        <li class="breadcrumb-item active">View Forum</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">

 <div class="col-sm-12 col-md-12 col-lg-12">

<!-- Construct the card with style you want. Here we are using card-danger -->
<!-- Then add the class direct-chat and choose the direct-chat-* contexual class -->
<!-- The contextual class should match the card, so we are using direct-chat-danger -->
<div class="card card-secondary direct-chat direct-chat-info">
  <div class="card-header">
    <h3 class="card-title">Direct Chat</h3>
    <div class="card-tools">
      
      <button type="button" class="btn btn-tool" data-toggle="tooltip" title="Documents" data-widget="chat-pane-toggle">
        <i class="fa fa-file-text-o"></i>
      </button>
     
    </div>
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <!-- Conversations are loaded here -->
    <div class="direct-chat-messages">

<?php

$session_data = $this->session->all_userdata();



                     if($users_cmnts){

                         foreach($users_cmnts as $cmnts){


    if ($session_data['user_id'] == $cmnts['user_id']) {

        ?>




      <!-- Message to the right -->
      <div class="direct-chat-msg right">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-right"><?=$cmnts['name']?></span>
          <span class="direct-chat-timestamp float-left"><?=$cmnts['updated_on']?></span>
        </div>
        <!-- /.direct-chat-infos -->
        <img class="direct-chat-img" src="<?php echo HTTP_UPLOADS_PATH;?><?=$cmnts['img_path']?>" alt="user">
        <!-- /.direct-chat-img -->
        <div class="direct-chat-text">
          <?=$cmnts['cmnt']?> 

            <?php
            if($cmnts['doc_name']!=null){}
                    echo '&nbsp;&nbsp; <a href="   "><span class="right badge badge-danger"><i class=" fa fa-file-text-o"></i> &nbsp;Attachment</span></a>';
             ?>

          
        </div>
        <!-- /.direct-chat-text -->
      </div>
      <!-- /.direct-chat-msg -->
<?php 
}
else{ ?>


      <!-- Message. Default to the left -->
      <div class="direct-chat-msg">
        <div class="direct-chat-infos clearfix">
          <span class="direct-chat-name float-left"><?=$cmnts['name']?></span>
          <span class="direct-chat-timestamp float-right"><?=$cmnts['updated_on']?></span>
        </div>
        <!-- /.direct-chat-infos -->
        <img class="direct-chat-img" src="<?php echo HTTP_UPLOADS_PATH;?><?=$cmnts['img_path']?>" alt="user ">
        <!-- /.direct-chat-img -->
        <div class="direct-chat-text">
          <?=$cmnts['cmnt']?>
        </div>
        <!-- /.direct-chat-text -->
      </div>
      <!-- /.direct-chat-msg -->

<?php 

}

}


}

?>      
    </div>
    <!--/.direct-chat-messages-->
    <!-- Contacts are loaded here -->
    <div class="direct-chat-contacts">
      <ul class="contacts-list">

<?php 
if($files){


 foreach($files as $file){
  ?>

 <li>
          <a href="#">

            <!-- <img class="contacts-list-img" src="/docs/3.0/assets/img/user1-128x128.jpg"> -->
            <div class="contacts-list-info">
              <span class="contacts-list-name">
              
                <small class="contacts-list-date float-right"><?=$file['updated_on']?></small>
              </span>
              <span class="contacts-list-msg"> <i class=" fa fa-file-text-o"></i> &nbsp;  <?=$file['comment']?></span>
            </div>
            <!-- /.contacts-list-info -->
          </a>
        </li>
        <!-- End Contact Item -->
        <?php
    }

}
else{ ?>
     <li>
          <a href="#">
            <img class="contacts-list-img" src="/docs/3.0/assets/img/user1-128x128.jpg">
            <div class="contacts-list-info">
              <span class="contacts-list-name">
                Count Dracula
                <small class="contacts-list-date float-right">2/28/2015</small>
              </span>
              <span class="contacts-list-msg">How have you been? I was...</span>
            </div>
            <!-- /.contacts-list-info -->
          </a>
        </li>
        <!-- End Contact Item -->
<?php 
}
?>

       
  


        
      </ul>
      <!-- /.contacts-list -->
    </div>
    <!-- /.direct-chat-pane -->
  </div>
  <!-- /.card-body -->
  <div class="card-footer">
    <form action="#" method="post">
      <div class="input-group">
        <input type="text" name="message" placeholder="Type Message ..." class="form-control">
        <span class="input-group-append">
          <button type="button" class="btn btn-primary">Send</button>
        </span>
      </div>
    </form>
  </div>
  <!-- /.card-footer-->
</div>
<!--/.direct-chat -->

</div>


</div>

        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

