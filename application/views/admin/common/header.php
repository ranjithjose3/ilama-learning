<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $page_title;?> | <?php echo SITE_TITLE;?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/png" href="<?php echo HTTP_IMAGES_PATH;?>favicon.png"/>
 <!-- Font Awesome -->
 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>adminlte.min.css">
  <!--Select 2 plugin -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.css">
<link data-require="sweet-alert@*" data-semver="0.4.2" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
  <!--Datetime picker -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css">
 <!-- Datatable-->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.4/css/responsive.dataTables.min.css">

  <!-- Custom CSS-->
  <link rel="stylesheet" href="<?php echo HTTP_CSS_PATH;?>custom.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<?php
$main_menu = isset($main_menu) && $main_menu != '' ?  $main_menu :'home'  ;   
$sub_menu1 = isset($sub_menu1) && $sub_menu1 != '' ?  $sub_menu1 :''  ;   
$sub_menu2 = isset($sub_menu2) && $sub_menu2 != '' ?  $sub_menu2 :''  ;
$sub_menu3 = isset($sub_menu3) && $sub_menu3 != '' ?  $sub_menu3 :''  ;
$permissions = array();
$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
// get permissions and show error if they don't have any permissions at a particular category or all
if (!$this->permissions = $this->permission->get_category_permissions($groupID))
{
	show_error('You do not have any permissions!');
}

$username = $this->session->userdata('user_name');
$group_id= $this->session->userdata('user_id');
$name = $this->session->userdata('name');
 ?>	
 <body class="hold-transition layout-top-nav text-sm">
 <div class="wrapper">
 
	
 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand-md navbar-dark navbar-navy">
 <div class="container">
	 <a href="<?php echo base_url();?>admin/dashboard" class="navbar-brand">
		 
		 <span class="brand-text font-weight-light text-white">iLAMA eLearning</span>
	 </a>
	 
	 <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		 <span class="navbar-toggler-icon"></span>
	 </button>
 
	 <div class="collapse navbar-collapse order-3" id="navbarCollapse">
		 <!-- Left navbar links -->
		 <ul class="navbar-nav">
		 
			
			 <li class="nav-item dropdown">
				 <a id="dropdownSubMenu1" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle">User Management</a>
				 <ul aria-labelledby="dropdownSubMenu1" class="dropdown-menu border-0 shadow">
					 <li><a href="<?php echo base_url().'admin/users';?>" class="dropdown-item">Users </a></li>
					 <li class="dropdown-divider"></li>
					 <li><a href="<?php echo base_url().'admin/users/user_groups';?>" class="dropdown-item">User Group</a></li>
					 <li class="dropdown-divider"></li>
					 <li><a href="<?php echo base_url().'admin/users/permissions';?>" class="dropdown-item">Permissions</a></li>
					 <li class="dropdown-divider"></li>
					 
				 </ul>
				 </li>
				 <?php 
						$course_permissions = $this->permission->get_category_permissions($groupID,'Courses');
 						if($course_permissions != '') { 
					?>
								<li class="nav-item dropdown">
									<a id="dropdownSubMenuCourse" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle <?php if($main_menu == 'courses') echo 'active';?>">Courses</a>
									<ul aria-labelledby="dropdownSubMenuCourse" class="dropdown-menu border-0 shadow">
									<?php
										if (in_array('categories_management', $this->permissions))
										{ 
									?>	
											<li><a href="<?php echo base_url().'admin/courses/categories';?>" class="dropdown-item <?php if($sub_menu1 == 'categories') echo 'active';?>">Categories </a></li>
											<li class="dropdown-divider"></li>
									<?php }
									  if (in_array('courses_management', $this->permissions))
										{ 
									?>
											<li><a href="<?php echo base_url().'admin/courses/add_course';?>" class="dropdown-item <?php if($sub_menu1 == 'add_course') echo 'active';?>">Add Course</a></li>
											<li class="dropdown-divider"></li>
											<li><a href="<?php echo base_url().'admin/courses/list_courses';?>" class="dropdown-item <?php if($sub_menu1 == 'list_courses') echo 'active';?>">Course List</a></li>
											<li class="dropdown-divider"></li>
										<?php } ?>
									</ul>
								</li>
				 <?php } 
						$tutor_permissions = $this->permission->get_category_permissions($groupID,'Tutors');
 						if($tutor_permissions != '') { 
					?>
								<li class="nav-item dropdown">
									<a id="dropdownSubMenuCourse" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle <?php if($main_menu == 'tutors') echo 'active';?>">Tutors</a>
									<ul aria-labelledby="dropdownSubMenuCourse" class="dropdown-menu border-0 shadow">
								<?php
									  if (in_array('tutors_management', $this->permissions))
										{ 
									?>
											<li><a href="<?php echo base_url().'admin/tutors/add_tutor';?>" class="dropdown-item <?php if($sub_menu1 == 'add_tutor') echo 'active';?>">Add Tutor</a></li>
											<li class="dropdown-divider"></li>
											<li><a href="<?php echo base_url().'admin/tutors/list_tutors';?>" class="dropdown-item <?php if($sub_menu1 == 'list_tutors') echo 'active';?>">Tutor List</a></li>
											<li class="dropdown-divider"></li>
										<?php } ?>
									</ul>
								</li>
				 <?php } 
				 $student_permissions = $this->permission->get_category_permissions($groupID,'Students');
				 if($student_permissions != '') { 
			?>
						<li class="nav-item dropdown">
							<a id="dropdownSubMenuCourse" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle <?php if($main_menu == 'students') echo 'active';?>">Students</a>
							<ul aria-labelledby="dropdownSubMenuCourse" class="dropdown-menu border-0 shadow">
						<?php
							  if (in_array('students_management', $this->permissions))
								{ 
							?>
									<li><a href="<?php echo base_url().'admin/students/list_students';?>" class="dropdown-item <?php if($sub_menu1 == 'list_students') echo 'active';?>">Student List</a></li>
									<li class="dropdown-divider"></li>
								<?php } ?>
							</ul>
						</li>
		 <?php } 
				$classschedules_permissions = $this->permission->get_category_permissions($groupID,'Class Schedules');
				if($classschedules_permissions != '') { 
			?>
						<li class="nav-item dropdown">
							<a id="dropdownSubMenuCourse" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle <?php if($main_menu == 'class_schedules') echo 'active';?>">Class Schedules</a>
							<ul aria-labelledby="dropdownSubMenuCourse" class="dropdown-menu border-0 shadow">
						<?php
							if (in_array('class_schedules_management', $this->permissions))
								{ 
							?>
									<li><a href="<?php echo base_url().'admin/class_schedules/list_schedules';?>" class="dropdown-item <?php if($sub_menu1 == 'list_schedules') echo 'active';?>">Schedule List</a></li>
									<li class="dropdown-divider"></li>
									<li><a href="<?php echo base_url().'admin/class_schedules/add_schedule';?>" class="dropdown-item <?php if($sub_menu1 == 'add_schedule') echo 'active';?>">Add Schedule</a></li>
									<li class="dropdown-divider"></li>
								<?php } ?>
							</ul>
						</li>
		<?php } 
		$accounts_permissions = $this->permission->get_category_permissions($groupID,'Accounts');
		if($accounts_permissions != '') { 
			?>
						<li class="nav-item dropdown">
							<a id="dropdownSubMenuCourse" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle <?php if($main_menu == 'accounts') echo 'active';?>">Accounts</a>
							<ul aria-labelledby="dropdownSubMenuCourse" class="dropdown-menu border-0 shadow">
						<?php
							if (in_array('accounts_management', $this->permissions))
								{ 
							?>
									<li><a href="<?php echo base_url().'admin/accounts/students';?>" class="dropdown-item <?php if($sub_menu1 == 'students') echo 'active';?>">Students</a></li>
									<li class="dropdown-divider"></li>
									<li><a href="<?php echo base_url().'admin/accounts/tutors';?>" class="dropdown-item <?php if($sub_menu1 == 'tutors') echo 'active';?>">Tutors</a></li>
									<li class="dropdown-divider"></li>
								<?php } ?>
							</ul>
						</li>
		<?php } 
				
				 if (in_array('message_management', $this->permissions))
								{  ?>
				<li class="nav-item"><a  href="<?php echo base_url().'admin/messages/add_message';?>"  class="nav-link  <?php if($main_menu == 'messages') echo 'active';?>">Messages</a></li>
				<?php } ?>
		 </ul>  
	 </div>
	 <?php
	 //Total complaints status count
	 
	 ?>
	 <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
	 			<?php /* <li class="nav-item dropdown">
					 <a class="nav-link" data-toggle="dropdown" href="#">
						 <i class="fa fa-bell"></i>
						 <span class="badge badge-danger navbar-badge">15</span>
					 </a>
					 <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
						 <span class="dropdown-header">15 Notifications</span>
						 <div class="dropdown-divider"></div>
						 <a href="#" class="dropdown-item">
							 <i class="fas fa-envelope mr-2"></i> 4 new messages
							 <span class="float-right text-muted text-sm">3 mins</span>
						 </a>
						 <div class="dropdown-divider"></div>            
						 <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a>
					 </div>
				 </li> */?>
		 <li class="nav-item">
			 <a class="nav-link text-yellow" data-toggle="dropdown" href="<?php echo base_url().'profile';?>" title="Profile">
				 <i class="fa fa-user"></i> <?php echo $this->session->userdata('name');?>           
			 </a>
		 </li>
		 <li class="nav-item">
			 <a class="nav-link text-red" href="<?php echo base_url().'admin/authentication/logout';?>" title="Logout">
				 <i class="fa fa-sign-out"></i> SIGN OUT       
			 </a>
		 </li>
 
 </ul>
 </div>
 </nav>
 <!-- /.navbar -->
	