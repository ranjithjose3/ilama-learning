<!-- Main Footer -->
<aside class="control-sidebar control-sidebar-dark">
<!-- Control sidebar content goes here -->
<div class="p-3">
	<h5>Title</h5>
	<p>Sidebar content</p>
</div>
</aside>
<footer class="main-footer">
<!-- To the right -->
<div class="float-right d-none d-sm-inline">
 Designed &amp; Developed By <a>Team</a>
</div>
<!-- Default to the left -->
<strong>Copyright &copy; <?php echo date('Y');?> <a href="#">iLAMA eLearning</a>.</strong> All rights reserved.
</footer>

</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script type="text/javascript" src="<?php echo HTTP_JS_PATH;?>adminlte.min.js"></script>
<!-- Moment JS -->
<Script  type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.26.0/moment.min.js"></script>
<!-- Select 2 Plugin-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.min.js"></script>
<!-- Date time picker -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>
<!-- Datatable JS -->

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/responsive/2.2.4/js/dataTables.responsive.min.js"></script>
<script>

$(function() {
// setTimeout() function will be fired after page is loaded
// it will wait for 5 sec. and then will fire
// $("#successMessage").hide() function

$(".hide-it").hide(4000);

});

$(document).ready(function() {
datatable_display();
moment.updateLocale('en', {
week: {dow: 1} // Monday is the first day of the week
})

$('.date').datetimepicker({
format: 'DD/MM/YYYY',
locale: 'en'
})

$('.datetime').datetimepicker({
format: 'DD/MM/YYYY HH:mm:ss',
locale: 'en',
sideBySide: true
})

$('.timepicker').datetimepicker({
format: 'HH:mm:ss'
})

$('.select-all').click(function () {
let $select2 = $(this).parent().siblings('.select2')
$select2.find('option').prop('selected', 'selected')
$select2.trigger('change')
})
$('.deselect-all').click(function () {
let $select2 = $(this).parent().siblings('.select2')
$select2.find('option').prop('selected', '')
$select2.trigger('change')
})

$('.select2').select2();

} );
function datatable_display(){
$('#example').DataTable( {
	
	responsive: {
		details: {
			type: 'column',
			target: -1
		}
	},
	columnDefs: [ {
		className: 'control',
		orderable: false,
		targets:   -1
	} ],
	lengthMenu: [[10,20,30,50,100, -1], [10,20,30,50,100, "All"]]
} );
}
</script>


