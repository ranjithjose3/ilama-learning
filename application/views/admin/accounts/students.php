<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> ACCOUNTS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Accounts</a>
                        </li>
                        <li class="breadcrumb-item active">Students</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
     <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Accounts - Coursewise Students List</h3>
                        </div>
                        <div class="card-body">
                            <?php echo form_open_multipart(''); ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                                <label>Course<span class="text-red">*</span></label>
                                                <select id="course_id" name="course_id" class="form-control select2" onchange="get_details()">
                                                    <option value="">-- Select --</option>
                                                <?php 
                                                    $courses = $this->Common_model->get_all_rows('courses',array('status' => 'A'));
                                                    if($courses){
                                                        foreach($courses as $each){ ?>
                                                            <option value="<?php echo $each['id'];?>" <?php if($course_id == $each['id']) echo 'selected';?>><?php echo $each['title'];?></option>
                                                <?php
                                                        }
                                                    }
                                                ?>
                                                </select>
                                            </div>
                                    </div>
                                </div>
                            <?php echo form_close();?>
                            
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="status_div">
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });

    function get_details(){
        var course_id = document.getElementById('course_id').value;
        var csrf = $('[name="csrf_test_name"]').val();
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/accounts/students",
            data:"course_id="+course_id+"&csrf_test_name="+csrf+"&mode=ajax",
            success:function(data){
                    $("#status_div").html(data);
                    datatable_display();
            }
        });
    }
</script>

