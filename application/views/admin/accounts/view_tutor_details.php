<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> ACCOUNTS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Accounts</a>
                        </li>
                        <li class="breadcrumb-item active">View Tutor Details</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
     <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php if(isset($msg) && $msg != ''){ ?>
                <div class="row">  
                    <div class="col-lg-12">
                        <div class="alert <?php echo $action;?> alert-dismissable" >
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo $msg;?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">View Account Details - <?php echo strtoupper($name);?></h3>
                        </div>
                        <div class="card-body">
                            <?php echo form_open_multipart(''); ?>
                                <div class="row">
                                    <div class="col-sm-2 left-pane">
                                        <div class="row ml15">
                                            <?php if($profile_pic != '' && file_exists('uploads/tutors/medium/'.$profile_pic)) { ?>
                                                <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/<?php echo $profile_pic;?>">
                                            <?php }
                                            else{ ?>
                                                <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>tutors/medium/default.jpg">
                                            <?php } ?>
                                        </div>  
                                    </div>

                                    <div class="col-sm-8 left-pane">
                                        <ul class="view-student-details-accounts">
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Tutor Id -  '.$tutor_id;?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Tutor Name -  '.strtoupper($name);?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Course -  '.strtoupper($course);?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Mobile No -  '.$mobile;?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Status - '.$status;?></b></li>
                                        </ul>
                                    </div>

                                    <div class="col-sm-2">
                                            <div class="row add-new-btns">
                                                <a href="#" class="btn btn-success releasefont" onclick="add_new_payment(<?php echo "'".$tutor_id."','".$course_id."'";?>)">ADD PAYMENT DETAILS</a>
                                            </div>
                                            <?php 
                                                if((isset($staff_payment->total) && $staff_payment->total > $staff_payment->total_released)){
                                                    $disabled = '0';
                                                }
                                                else{
                                                    $disabled = '1';
                                                }
                                            ?>
                                            <div class="row add-new-btns">
                                            <a href="#" class="btn btn-primary releasefont" <?php if($disabled == '0') { ?> onclick="add_payment_released(<?php echo "'".$tutor_id."','".$course_id."'";?>)" <?php } ?> >ADD PAYMENT RELEASED</a>
                                            </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5 class="aligncenter"><u>PAYMENT DETAILS</u></h5><br/>
                                    </div>
                                </div>

                                <div class="row p10">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="25%">DATE</th>
                                                <th width="25%">TOPIC</th>
                                                <th width="20%">NO OF STUDENTS</th>
                                                <th width="25%">TUTION FEE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $i = 1;
                                                $dates = isset($staff_payment->dates) ? explode(',',$staff_payment->dates) : array();
                                                $topics = isset($staff_payment->topic) ? explode(',',$staff_payment->topic) : array();
                                                $no_of_students = isset($staff_payment->no_of_students) ? explode(',',$staff_payment->no_of_students) : array();
                                                $tution_fees = isset($staff_payment->tution_fees) ? explode(',',$staff_payment->tution_fees) : array();
                                                if(!empty($dates)){
                                                    foreach($dates as $index => $each){ ?>
                                                        <tr>
                                                            <td><?php echo $i++;?></td>
                                                            <td><?php echo date('d/m/Y',strtotime($each));?></td>
                                                            <td><?php echo $topics[$index];?></td>
                                                            <td><?php echo $no_of_students[$index];?></td>
                                                            <td><?php echo 'Rs. '.$tution_fees[$index];?></td>
                                                        </tr>

                                            <?php   }
                                    
                                                }
                                                else{ ?>
                                                        <tr>
                                                            <td colspan="5" class="aligncenter">No details found!!</td>
                                                        </tr>
                                            <?php
                                                }
                                            ?>
                                                <tr>
                                                    <td colspan="4">&nbsp;</td>
                                                    <td>
                                                        <?php if(isset($staff_payment->total) && $staff_payment->total != '0'){
                                                            echo '<b>Total Amount: Rs. '.$staff_payment->total.'</b>';
                                                        } ?>
                                                    </td>
                                                </tr>
                                        </tbody>
                                    </table>

                                </div>

                                <div class="row">
                                    <div class="col-sm-12">
                                        <h5 class="aligncenter"><u>PAYMENT RELEASED</u></h5><br/>
                                    </div>
                                </div>

                                <div class="row p10">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th width="5%">#</th>
                                                <th width="70%">DATE</th>
                                                <th width="25%">AMOUNT</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php 
                                                $j = 1;
                                                $released_dates = (isset($staff_payment->released_dates) && $staff_payment->released_dates != '') ? explode(',',$staff_payment->released_dates) : array();
                                                $released_amounts = (isset($staff_payment->released_amounts) && $staff_payment->released_amounts != '')? explode(',',$staff_payment->released_amounts) : array();
                                                if(!empty($released_dates)){
                                                    foreach($released_dates as $index => $each){ ?>
                                                        <tr>
                                                            <td><?php echo $j++;?></td>
                                                            <td><?php echo date('d/m/Y',strtotime($each));?></td>
                                                            <td><?php echo 'Rs. '.$released_amounts[$index];?></td>
                                                        </tr>

                                            <?php   }
                                    
                                                }
                                                else{ ?>
                                                        <tr>
                                                            <td colspan="3" class="aligncenter">No details found!!</td>
                                                        </tr>
                                            <?php
                                                }
                                            ?>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                    <td>
                                                        <?php if(isset($staff_payment->total_released) && $staff_payment->total_released != '0'){ ?>
                                                            <b>Total Amount Paid: Rs. <?php echo $staff_payment->total_released;?></b>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                               
                                                <?php if(isset($staff_payment->balance) && $staff_payment->balance != '0'){ ?>
                                                    <tr>
                                                        <td colspan="2">&nbsp;</td>
                                                        <td><b>Pending Payment: Rs. <?php echo $staff_payment->balance;?></b></td>
                                                    </tr>
                                                <?php } ?>    
                                        </tbody>
                                    </table>

                                </div>

                               
                            <?php form_close();?>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container -->

        <div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog inventory-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="exampleModalLabel">ADD TUTOR PAYMENT DETAILS</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="ViewModalBody">
                    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="validate_payment_details()">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ViewModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog inventory-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="exampleModalLabel">ADD PAYMENT RELEASED</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="ViewModalBody1">
                    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="validate_payment_released()">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    function add_new_payment(tutor_id,course_id){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/accounts/payment_details",
            data:"tutor_id="+tutor_id+"&course_id="+course_id+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody").html(data);
                $( "#ViewModal" ).modal();
                $('.date-modal').datetimepicker({
                    format: 'DD/MM/YYYY',
                    locale: 'en'
                })
                $('.select2').select2();
            }
        });
    }

    function add_payment_released(tutor_id,course_id){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody1" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/accounts/payment_released",
            data:"tutor_id="+tutor_id+"&course_id="+course_id+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody1").html(data);
                $( "#ViewModal1" ).modal();
                $('.date-modal').datetimepicker({
                    format: 'DD/MM/YYYY',
                    locale: 'en'
                })
            }
        });
    }

    function validate_payment_details(){
        var classdate = $('#start_date').val();
        var classdateArr = classdate.split("/");
        var classDate = new Date(classdateArr[2]+'-'+classdateArr[1]+'-'+classdateArr[0]);
        var todayDate = new Date();
        var topic = $('#topic').val();
        var amount = $('#amount').val();
        var no_of_students = $('#no_of_students').val();
        var tutor_id = $('#tutor_id').val();
        var course_id = $('#course_id').val();
        var encoded_tutor_id = $('#encoded_tutor_id').val();
        var encoded_course_id = $('#encoded_course_id').val();
        var csrf = $('[name="csrf_test_name"]').val();
        var error = 0;
        if(classdate == ''){
            document.getElementById('date_error').innerHTML = "Select a date.";
            error++;
        }
        else if(todayDate < classDate){
            document.getElementById('date_error').innerHTML = "Selected date can not be greater than today's date.";
            error++;
        }
        else{
            document.getElementById('date_error').innerHTML = "";
        }
        if(amount == ''){
            document.getElementById('amount_error').innerHTML = "Enter a payment amount.";
            error++;
        }
        else{
            document.getElementById('amount_error').innerHTML = "";
        }
        if(no_of_students == ''){
            document.getElementById('no_of_error').innerHTML = "Enter no: of student count.";
            error++;
        }
        else{
            document.getElementById('no_of_error').innerHTML = "";
        }
        if(topic == ''){
            document.getElementById('topic_error').innerHTML = "Select a topic.";
            error++;
        }
        else{
            document.getElementById('topic_error').innerHTML = "";
        }

        if(error == 0){
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>admin/accounts/payment_details_save",
                data:"course_id="+course_id+"&tutor_id="+tutor_id+"&date="+classdate+"&students_count="+no_of_students+"&amount="+amount+"&topic="+topic+"&csrf_test_name="+csrf,
                success:function(data){
                    if(data == '1'){
                        location.href = "<?php echo base_url(); ?>admin/accounts/view_tutor_details?id="+encoded_tutor_id+"&cid="+encoded_course_id+"&msg=Payment details added  successfully.";
                        $( "#ViewModal" ).modal('hide');
                    }
                }
            });
        }
    }

    function validate_payment_released(){
        var classdate = $('#start_date').val();
        var classdateArr = classdate.split("/");
        var classDate = new Date(classdateArr[2]+'-'+classdateArr[1]+'-'+classdateArr[0]);
        var todayDate = new Date();
        var amount = $('#amount').val();
        var max_amount = $('#max_amount').val();
        var tutor_id = $('#tutor_id').val();
        var course_id = $('#course_id').val();
        var encoded_tutor_id = $('#encoded_tutor_id').val();
        var encoded_course_id = $('#encoded_course_id').val();
        var csrf = $('[name="csrf_test_name"]').val();
        var error = 0;
        if(classdate == ''){
            document.getElementById('date_error').innerHTML = "Select a date.";
            error++;
        }
        else if(todayDate < classDate){
            document.getElementById('date_error').innerHTML = "Selected date can not be greater than today's date.";
            error++;
        }
        else{
            document.getElementById('date_error').innerHTML = "";
        }
        if(amount == ''){
            document.getElementById('amount_error').innerHTML = "Enter a payment amount.";
            error++;
        }
        else if(+amount > +max_amount){
            document.getElementById('amount_error').innerHTML = "Entered payment should not be greater than "+max_amount+".";
            error++;
        }
        else{
            document.getElementById('amount_error').innerHTML = "";
        }
        
        if(error == 0){
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>admin/accounts/payment_released_save",
                data:"course_id="+course_id+"&tutor_id="+tutor_id+"&date="+classdate+"&amount="+amount+"&csrf_test_name="+csrf,
                success:function(data){
                    if(data == '1'){
                        location.href = "<?php echo base_url(); ?>admin/accounts/view_tutor_details?id="+encoded_tutor_id+"&cid="+encoded_course_id+"&msg=Payment released added  successfully.&action=alert-success";
                        $( "#ViewModal1" ).modal('hide');
                    }
                    else if(data == '0'){
                        location.href = "<?php echo base_url(); ?>admin/accounts/view_tutor_details?id="+encoded_tutor_id+"&cid="+encoded_course_id+"&msg=Payment released added  successfully.&action=alert-danger";
                        $( "#ViewModal1" ).modal('hide');
                    }
                }
            });
        }
    }

</script>
