<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> ACCOUNTS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Accounts</a>
                        </li>
                        <li class="breadcrumb-item active">View Student Details</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    
     <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php if(isset($msg) && $msg != ''){ ?>
                <div class="row">  
                    <div class="col-lg-12">
                        <div class="alert alert-success alert-dismissable" >
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                            <?php echo $msg;?>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">View Account Details - <?php echo strtoupper($name);?></h3>
                        </div>
                        <div class="card-body">
                            <?php echo form_open_multipart(''); ?>
                                <div class="row">
                                    <div class="col-sm-3 left-pane">
                                        <div class="row">
                                            <?php if($student_detail->profile_pic != '' && file_exists('uploads/students/medium/'.$student_detail->profile_pic)) { ?>
                                                <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>students/medium/<?php echo $student_detail->profile_pic;?>">
                                            <?php }
                                            else{ ?>
                                                <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>students/medium/default.jpg">
                                            <?php } ?>
                                        </div>  
                                    </div>

                                    <div class="col-sm-9">
                                        <ul class="view-student-details-accounts">
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Student Id -  '.$this->Common_model->get_row('users',array('id' => $student_detail->user_id),'user_name');?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Student Name -  '.strtoupper($name);?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Course -  '.strtoupper($course);?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Mobile No -  '.$student_detail->mobile;?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Status - '.$status;?></b></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Total Course Fee -  Rs. '.$totalfee;?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Total Fee Paid -  Rs. '.$paid_fee;?></li>
                                            <li><i class="fa fa-snowflake-o"></i><?php echo 'Balance -  <b>Rs. '.$balance_fee;?></b></li>
                                        </ul>
                                    </div>
                                </div>

                                <?php if($fee_structure){ ?>
                                    <br/>
                                    <div class="row">
                                        <h5><u>PAYMENT DETAILS</u></h5>
                                    </div>

                                    <div class="row">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>INSTALLMENT</th>
                                                    <th>AMOUNT</th>
                                                    <th>DUE DATE</th>
                                                    <th>PAID DATE</th>
                                                    <th class="wdth30">&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $i = 1;
                                                    foreach($fee_structure as $index => $each){ ?>
                                                        <tr>
                                                            <td><?php echo 'PART - '.$i;?></td>
                                                            <td><?php echo 'Rs. '.$each['amount'];?></td>
                                                            <td><?php echo date('d/m/Y',strtotime($each['due_date']));?></td>
                                                            <td><?php 
                                                                    if(!empty($payment_dates) && isset($payment_dates[$index])){
                                                                        echo date('d/m/Y',strtotime($payment_dates[$index]));
                                                                    }
                                                                ?>
                                                            </td>
                                                            <td>
                                                                <?php 
                                                                if(!isset($payment_dates[$index])){
                                                                    echo '<a href="#" class="btn btn-info" onclick="receive_payment('. $each["id"].','. $id.','.$i.')">PAYMENT RECEIVED</a>&nbsp;&nbsp;<a href="#" class="btn btn-secondary" onclick="send_reminder('. $each["id"].','. $id.','. $i.')">SEND REMINDER</a>';
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>

                                                <?php 
                                                        $i++;
                                                    }
                                                ?>
                                            </tbody>
                                        </table>
                                    </div>

                                <?php } ?>
                            <?php form_close();?>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container -->

        <div class="modal fade" id="ViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog inventory-modal" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-info">
                        <h5 class="modal-title" id="exampleModalLabel">PAYMENT RECEIVED</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="ViewModalBody">
                    
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="validate_payment_received()">Save</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>
<script>
    function receive_payment(structure_id,enroll_id,part){
        var csrf = $('[name="csrf_test_name"]').val();
        $( "#ViewModalBody" ).html("");
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/accounts/receive_payment",
            data:"enroll_id="+enroll_id+"&structure_id="+structure_id+"&part="+part+"&csrf_test_name="+csrf,
            success:function(data){
                $( "#ViewModalBody").html(data);
                $( "#ViewModal" ).modal();
            }
        });
    }

    function validate_payment_received(){
        var enroll_id = $('#enroll_id').val();
        var part = $('#part').val();
        var amount = $('#amount').val();
        var encoded_enroll_id = $('#encoded_enroll_id').val();
        var payment_type = $('#payment_type').val();
        var remarks = $('#remarks').val();
        var csrf = $('[name="csrf_test_name"]').val();
        var error = 0;
        if(payment_type == ''){
            document.getElementById('payment_type_error').innerHTML = "Select a payment type.";
            error++;
        }
        else{
            document.getElementById('payment_type_error').innerHTML = "";
        }
        if(remarks == ''){
            document.getElementById('remarks_error').innerHTML = "Enter payment identification number.";
            error++;
        }
        else{
            document.getElementById('remarks_error').innerHTML = "";
        }

        if(error == 0){
            $.ajax({
                type:"post",
                url:"<?php echo base_url();?>admin/accounts/receive_payment_save",
                data:"enroll_id="+enroll_id+"&payment_type="+payment_type+"&remarks="+remarks+"&amount="+amount+"&part="+part+"&csrf_test_name="+csrf,
                success:function(data){
                    if(data == '1'){
                        location.href = "<?php echo base_url(); ?>admin/accounts/view_details?id="+encoded_enroll_id+"&msg=Payment Received successfully.";
                        $( "#ViewModal" ).modal('hide');
                    }
                }
            });
        }
    }

    function send_reminder(structure_id,enroll_id,part){
        var csrf = $('[name="csrf_test_name"]').val();
        $.ajax({
            type:"post",
            url:"<?php echo base_url();?>admin/accounts/send_reminder",
            data:"enroll_id="+enroll_id+"&structure_id="+structure_id+"&part="+part+"&csrf_test_name="+csrf,
            success:function(data){
                details = JSON.parse(data);
                if(details[0] == '1'){
                    location.href = "<?php echo base_url(); ?>admin/accounts/view_details?id="+details[1]+"&msg=Reminder Sent successfully.";
                }
            }
        });
    
    }

</script>
