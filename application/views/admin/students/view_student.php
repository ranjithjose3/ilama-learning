<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> STUDENTS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Students</a>
                        </li>
                        <li class="breadcrumb-item active">View Student</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

     <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">View Student - <?php echo strtoupper($name);
                                                if($active_class != '' && $active_class->status == 'I')
                                                    echo ' (Suspended)';
                                            ?>
                            </h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-sm-3 left-pane">
                                    <div class="row">
                                        <?php if($profile_pic != '' && file_exists('uploads/students/medium/'.$profile_pic)){ ?>
                                            <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>students/medium/<?php echo $profile_pic;?>">
                                        <?php } 
                                        else { ?>
                                            <img class="fullwidth" src="<?php echo HTTP_UPLOADS_PATH;?>students/medium/default.jpg">
                                        <?php } ?>
                                    </div>
                                    <div class="row pt-15">
                                        <p class="view-course"><b>Active Course: </b></p>
                                    </div>
                                    <div class="row">
                                            <ul class="courses-assigned">
                                            <?php 
                                                if($active_class){ ?>
                                                    <li><i class="fa fa-book"></i><?php echo $this->Common_model->get_row('courses',array('id' => $active_class->course_id),'title');?></li>
                                            <?php
                                                } 
                                            ?>
                                                
                                            </ul>
                                    </div>
                                   
                                </div>

                                <div class="col-sm-6 left-pane">
                                    <div class="row">
                                        <p class="tut-contact ml10">
                                            <i class="fa fa-address-book-o fts15"></i><?php echo 'Student Id: '.$student_uname;?><br/>
                                            <i class="fa fa-map-marker"></i><?php echo 'Address: '.$address;?><br/>
                                            <i class="fa fa-envelope fts15"></i><?php echo 'Email: '.$email;?><br/>
                                            <i class="fa fa-mobile fts29"></i><?php echo 'Mobile No: '.$mobile;?><br/>
                                            <i class="fa fa-id-badge"></i><?php echo 'Parent Name: '.$parent_name;?><br/>
                                            <i class="fa fa-envelope-o fts15"></i><?php echo 'Parent Email: '.$parent_email;?><br/>
                                        </p>
                                    </div>
                                   
                                </div>

                                <div class="col-sm-3">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="card card-outline card-success card-no-bg ml10">
                                            <div class="card-header">
                                                <h3 class="card-title"><b>ENROLLED COURSES </b></h3>
                                            </div>
                                            <!-- /.card-header -->
                                            <div class="card-body view-stud-sec">
                                                <div class="row">
                                                    <ul class="courses-assigned">
                                                    <?php 
                                                        $course_list = '';
                                                        if(!empty($enrolled_courses)){
                                                            foreach($enrolled_courses as $each){
                                                                $course_list = $this->Common_model->get_row('courses',array('id' => $each['course_id']),'title'); ?>
                                                                <li><i class="fa fa-book"></i><?php echo $course_list;?></li>
                                                    <?php
                                                            }
                                                        } 
                                                    ?>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                            <!-- /.card-body -->
                                        </div>
                                        <!-- /.card -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

