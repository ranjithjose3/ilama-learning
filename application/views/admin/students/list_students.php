<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> STUDENTS </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="<?php echo base_url();?>admin/dashboard">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">STUDENTS</a>
                        </li>
                        <li class="breadcrumb-item active">Student List</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <?php   
        if(isset($msg)){
            $message = $msg;
        }
		else
			$message ='';		
	?>

     <!-- Main content -->
    <div class="content">
        <div class="container">
        <?php  
            if($message!='')
            { 
        ?>
            <div class="row">  
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>
        <?php }  ?>

            <div class="row">
                <div class="col-sm-12 col-md-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Student List</h3>
                        </div>
                        <div class="card-body">
                            <div id="status_div">
                                <?php 
                                if($students){?>
                                 <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable">
                                    <thead>
                                        <tr>
                                            <th class="no-sort">#</th>
                                            <th>Profile Pic</th>
                                            <th>Student Id</th>
                                            <th>Name</th>
                                            <th>Contact Details</th>
                                            <th>Selected Course</th>
                                            <th>Status</th>
                                            <th>Action</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i=1;
                                    foreach($students as $row){ 
                                        $active_course_details = $this->Common_model->get_active_enrolled_course($row['id']);
                                    ?>
                                            <tr>
                                                <td><?php echo $i++; ?></td>
                                                <td><?php 
                                                    if($row['profile_pic'] != '' && file_exists('uploads/students/small/'.$row['profile_pic'])){ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>students/small/<?php echo $row['profile_pic'];?>">
                                                    <?php
                                                    }
                                                    else{ ?>
                                                        <img class="list-course-img" src="<?php echo HTTP_UPLOADS_PATH;?>students/small/default.jpg">
                                                    <?php }
                                                ?></td>
                                                <td><?php echo $row['user_name']; ?></td>
                                                <td><?php echo $row['name']; ?></td>
                                                <td><?php echo 'Email:- '.$row['email'].'<br/>Mobile:- '.$row['mobile']; ?></td>
                                                <td>
                                                    <?php 
                                                       if($active_course_details){
                                                           echo $this->Common_model->get_row('courses',array('id' => $active_course_details->course_id),'title');
                                                       }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php 
                                                       if($active_course_details){
                                                           if($active_course_details->status == 'R')
                                                                echo 'Registered';
                                                            elseif($active_course_details->status == 'A')
                                                                echo 'Admitted';
                                                            elseif($active_course_details->status == 'I')
                                                                echo 'Suspended';
                                                          
                                                       }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a title="View Student" href="<?php echo base_url();?>admin/students/view_student?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                    <?php 
                                                     if($active_course_details != '' && $active_course_details->status == 'R') { ?>
                                                        <a title="Admit Student" href="<?php echo base_url();?>admin/students/admit_student?id=<?php echo urlencode(base64_encode($active_course_details->id.'_'.ENCRYPTION_KEY));?>">
                                                            <i class="fa fa-plus-square admit-student"></i>
                                                        </a>
                                                     <?php } ?>

                                                     <?php 
                                                     if($active_course_details != '' && $active_course_details->status == 'A') { ?>
                                                        <a title="Suspend Student" href="<?php echo base_url();?>admin/students/suspend_student?id=<?php echo urlencode(base64_encode($active_course_details->id.'_'.ENCRYPTION_KEY));?>">
                                                            <i class="fa fa-pause"></i>
                                                        </a>
                                                     <?php } ?>

                                                     <?php 
                                                     if($active_course_details != '' && $active_course_details->status == 'I') { ?>
                                                        <a title="Re-admit Student" href="<?php echo base_url();?>admin/students/readmit_student?id=<?php echo urlencode(base64_encode($active_course_details->id.'_'.ENCRYPTION_KEY));?>">
                                                            <i class="fa fa-check-square-o admit-student"></i>
                                                        </a>
                                                     <?php } ?>
                                                       
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <?php } 
                                else{
                                    echo 'No details found!!';
                                }?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--row-->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->

</div><!-- content-wrapper-->
<?php
$this->load->view('admin/common/footer');
?>

