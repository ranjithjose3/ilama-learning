<?php
$this->load->view('admin/common/header');
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

<!-- Main content -->
<section class="content">
<!-- Small boxes (Stat box) -->
         
	<?php   if(isset($msg)){
			 $message = $msg;
			 
			 }
			 else
			 $message ='';		
	   ?>
		 <?php  
		 if($message!='')
			{ ?>
	<div class="row">  <div class="col-lg-12">
		<div class="alert <?php echo $action; ?> alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
            <?php echo urldecode($message);?>
        </div>
    </div>
</div>
<?php }  ?><br />
<?php /*$myIp = getHostByName(getHostName());
echo $myIp;*/
?>
		<div class="container-fluid">

			<div class="row">
				<div class="col-12 col-sm-6 col-md-3">
					<div class="info-box">
					<span class="info-box-icon bg-danger elevation-1"><i class="fa fa-archive"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Course Category</span>
						<span class="info-box-number">
						<?php echo $course_category;?>
						<!-- <small>%</small> -->
						</span>
					</div>
					<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.col -->

				<div class="col-12 col-sm-6 col-md-3">
					<div class="info-box mb-3">
					<span class="info-box-icon bg-warning elevation-1"><i class="fa fa-book"></i></span>
		
					<div class="info-box-content">
						<span class="info-box-text">Courses</span>
						<span class="info-box-number"><?php echo $course_count;?></span>
					</div>
					<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
			  </div>
			  <!-- /.col -->
	
			  <!-- fix for small devices only -->
			  <div class="clearfix hidden-md-up"></div>

			  <div class="col-12 col-sm-6 col-md-3">
					<div class="info-box">
					<span class="info-box-icon bg-success elevation-1"><i class="fa fa-user"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Tutors</span>
						<span class="info-box-number">
						<?php echo $tutors;?>
						<!-- <small>%</small> -->
						</span>
					</div>
					<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
				</div>
				<!-- /.col -->

				<div class="col-12 col-sm-6 col-md-3">
					<div class="info-box mb-3">
					<span class="info-box-icon bg-info elevation-1"><i class="fa fa-id-badge"></i></span>
		
					<div class="info-box-content">
						<span class="info-box-text">Students</span>
						<span class="info-box-number"><?php echo $students;?></span>
					</div>
					<!-- /.info-box-content -->
					</div>
					<!-- /.info-box -->
			  </div>
			  <!-- /.col -->
	

			</div>
			<br/>
			<div class="row">
				<?php if($courses){
					$bg_colors = array( '1' => 'bg-info', '2' => 'bg-success', '3' => 'bg-warning','4' => 'bg-danger','5' => 'bg-danger','6' => 'bg-warning','7' => 'bg-success','8' => 'bg-info');
					$count = 1;
					foreach($courses as $each){
					?>
						<div class="col-lg-3 col-6">
							<!-- small box -->
							<div class="small-box <?php echo $bg_colors[$count];?>">
								<div class="inner">
								<h3 class="dash-course-title"><?php echo strtoupper($each['title']);?></h3>

								<p><?php echo date('d/m/Y',strtotime($each['start_date'])).' - '.date('d/m/Y',strtotime($each['end_date']));?></p>
								</div>
								<div class="icon">
								<i class="fa fa-book"></i>
								</div>
								<a href="<?php echo base_url();?>admin/courses/view_course?id=<?php echo urlencode(base64_encode($each['id'].'_'.ENCRYPTION_KEY));?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
							</div>
						</div>
						<!-- ./col -->
					<?php 
						$count++;
					}
				} ?>
		
			</div>

			<div class="row">
				<div class="col-12 float-right">
					<a href="<?php echo base_url();?>admin/courses/list_courses" class="btn btn-primary float-right">View All Courses</a>
				</div>
			</div>
		</div>

	</section><!-- /.content -->
</div><!-- /.content-wrapper -->



<!--  PAge Code Ends here -->
<?php
$this->load->view('admin/common/footer');
?>
