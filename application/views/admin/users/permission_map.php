<?php $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> USER MANAGEMENT </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Users</a>
                        </li>
                        <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php if (isset($msg)) {
                $message = $msg;

            } else
                $message = '';
            ?>
            <?php 
            if ($message != '') { ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message); ?>
                    </div>
                </div>
            </div>

            <?php 
        } ?>

            <div class="row">
                <div class="card card-secondary  col-sm-12 col-md-12 pr-0 pl-0">
                    <div class="card-header">
                        <h2 class="card-title">Permission settings of
                            <?php echo $group_name; ?>
                        </h2>
                    </div>
                    <?php echo form_open('admin/users/permission_mapping_save'); ?>
                    <div class="card-body">
                        <div class="row">
                            <input type="hidden" name="group_id" value="<?php echo $group_id; ?>" />

                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="panel-group wrap">
                                    <div class="panel">
                                        <div class="panel-body">
                                            <?php

                                            if ($distinct_category) { ?>

                                                <?php foreach ($distinct_category as $cat) { ?>
                                                    <fieldset class="card card-secondary">
                                                        <legend class="card-title">
                                                            <?php echo strtoupper(str_replace("_", " ", $cat['category'])); ?>&nbsp;&nbsp;
                                                            <?php if ($cat['category'] != 'dashboard') { ?>
                                                            <input type="checkbox" name="check_all" id="check_all_<?php echo $cat['category']; ?>" onClick="select_all('<?php echo $cat['category']; ?>')">
                                                            <?php echo "selectall"; ?>
                                                            <?php 
                                                        } ?>
                                                        </legend>
                                                        <?php 
                                                        $cat_permissions = $this->Common_model->get_all_rows('permissions', array('category' => $cat['category']), array('id' => 'ASC'), '', array(), array());
                                                        if ($cat_permissions) { ?>
                                                        <div class="row">

                                                            <div class="col-md-12">
                                                                <?php	foreach ($cat_permissions as $cat_perm) { ?>
                                                                    <div class="col-md-3">
                                                                        <label>
                                                                            <input type="checkbox" name="permissions_array[]" class=" check_all_<?php echo $cat['category']; ?>" value="<?php echo $cat_perm['id']; ?>"
                                                                                <?php if (isset($permission_list)) {
                                                                                    if (in_array($cat_perm['id'], $permission_list)) {
                                                                                        echo 'checked';
                                                                                    }
                                                                                }
                                                                                if ($cat_perm['id'] == 1) echo ' checked disabled';
                                                                                echo
                                                                                    ' id="dashboard"'; ?> >
                                                                            <?php echo ucwords(str_replace("_", " ", $cat_perm['permission'])); ?>
                                                                            </label>
                                                                    </div>
                                                                    <?php 
                                                                } ?>
                                                            </div>

                                                        </div>
                                                        <?php 
                                                    } ?>
                                                    </fieldset>
                                                    <br/>
                                                <?php

                                                 }
                                               
                                            }?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                    <?php echo form_close(); ?>                                                    
                            
                </div>
               
            </div>
                </div>
                <!--row-->
    
            </div>
            <!-- content-->
        </div>
        <!-- content-wrapper-->
        <?php
    $this->load->view('admin/common/footer');
    ?>                  
                

        <script>
            function select_all(category_type) {
                if ($('#check_all_' + category_type + ':checkbox:checked').length > 0) {
                    // check select status
                    $('.check_all_' + category_type).each(function () {
                        //loop through each checkbox
                        this.checked = true;
                        //select all checkboxes with class "checkbox1"  

                    }
                    );
                }
                else {
                    $('.check_all_' + category_type).each(function () {
                        //loop through each checkbox
                        this.checked = false;
                        document.getElementById('dashboard').checked = true;
                        //deselect all checkboxes with class "checkbox1"                       
                    }
                    );
                }
            }
        </script>