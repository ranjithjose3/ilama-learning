<?php   $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> CHANGE PASSWORD</h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Users</a>
                        </li>
                        <li class="breadcrumb-item active">Change Password</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php   if(isset($msg)){
			 $message = $msg;
			 
			 }
			 else
			 $message ='';		 
	   ?>
            <?php  
		 if($message!='')
			{ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>

            <?php }  ?>

            <div class="row">
                <div class="card card-secondary col-sm-12 col-md-12 pr-0 pl-0">
                    <div class="card-header">
                        <h2 class="card-title">Change Password</h2>
                    </div>
                    <?php echo form_open_multipart( 'admin/users/change_password',array('id' => 'change_pass'));?>
                    <div class="card-body">
                        <input type="hidden" name="user_id" value="<?php echo $user_id; ?>" />
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>New Password</label>
                                    <input type="password" class="form-control" id="new_password" name="new_password" value="" />
                                    <?php echo form_error('new_password');?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Confirm Password</label>
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="" />
                                    <?php echo form_error('confirm_password');?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>&nbsp;</label><br />
                                    <input type="submit" class="btn btn-info " name="submit" value="Change Password" >
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>

                </div>
            </div>
    
            </div>
            <!--row-->

        </div>
        <!-- content-->
    </div>
    <!-- content-wrapper-->
    <?php
$this->load->view('admin/common/footer');
?>
 <script type="text/JavaScript">
	
	function validateForm()
		{
			
			 var error =0;
			 var conf_pass =$("#confirm_password").val();
			 var new_pass=$("#new_password").val();
			 if (new_pass=='') 
				 { 
				 
					 error++;
					 document.getElementById("new_error").innerHTML= "Enter your preferred password";
					  
				 } 
			else 
				{
					  document.getElementById("new_error").innerHTML= "";
				}
				
			if (conf_pass=='') 
				{ 
				 
				  error++;
					 document.getElementById("conf_error").innerHTML= "Re-enter your password for confirmation";
					  
				} 
			else 
				{
					  document.getElementById("conf_error").innerHTML= "";
				}
			
			if (conf_pass==new_pass ) 
				{
					 
					 document.getElementById("notsame_error").innerHTML='';
				}
			else 
				{
					error++;
				   document.getElementById("notsame_error").innerHTML="New Password and Confirm Password should be same";
					
				}
			// Validate end date
		  
		
			if(error>0)  
		  		return false;
		 	else
		 		$("#change_pass").submit()
		  
		}
</script>