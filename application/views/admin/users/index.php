<?php   $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> USER MANAGEMENT </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Users</a>
                        </li>
                        <li class="breadcrumb-item active">User Management</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php   if(isset($msg)){
			 $message = $msg;
			 
			 }
			 else
			 $message ='';		 
	   ?>
            <?php  
		 if($message!='')
			{ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>

            <?php }  ?>

            <div class="row">
            <div class="col-sm-12 col-md-5 pl-0">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h2 class="card-title">User Form</h2>
                    </div>
                    <?php echo form_open('admin/users/user_save');?>
                    <div class="card-body">

                        <div class="row">
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">User Name
                                        <span class="text-red">*</span>
                                    </label>

                                    <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                    <input type="text" name="user_name" class="form-control" placeholder="User Name" value="<?php echo $user_name;?>">
                                    <?php echo form_error('user_name');?>

                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Name
                                        <span class="text-red">*</span>
                                    </label>

                                    <input type="text" name="name" class="form-control" placeholder="Name" value="<?php echo $name;?>">
                                    <?php echo form_error('name');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Email
                                        <span class="text-red">*</span>
                                    </label>

                                    <input type="email" name="email" class="form-control" placeholder="Email" value="<?php echo $email;?>">
                                    <?php echo form_error('email');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Password
                                        <span class="text-red">*</span>
                                    </label>

                                    <input type="password" name="password" class="form-control" placeholder="password" value="<?php if($id!='') echo '0000';?>"
                                        <?php if($id!='' ) echo 'readonly';?> >
                                    <?php echo form_error('password');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">User Group
                                        <span class="text-red">*</span>
                                    </label>

                                    <select name="group_id" class="form-control">
                                        <option value="">---select ---</option>
                                        <?php
																	 foreach($user_groups as $user_group){?>
                                            <option value="<?php echo $user_group['id'];?>" <?php if($user_group[ 'id']==$group_id) echo 'selected';?>>
                                                <?php echo $user_group['name'];?>
                                            </option>
                                            <?php }
																	 ?>
                                    </select>
                                    <?php echo form_error('group_id');?>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="control-label">Status
                                        <span class="text-red">*</span>
                                    </label>
                                    <div class="form-group clearfix">
                                            <div class="icheck-success d-inline">
                                              <input type="radio" name="status" <?php if($status=='A' ||$status=='' ) echo 'checked';?> value="A" id="radioSuccess1">
                                              <label for="radioSuccess1">
                                                  Active
                                              </label>
                                            </div>
                                            <div class="icheck-danger d-inline">
                                              <input type="radio" name="status" id="radioSuccess2" <?php if($status=='I' ) echo 'checked';?> value="I">
                                              <label for="radioSuccess2">
                                                  Inactive
                                              </label>
                                            </div>
                                            <?php echo form_error('status');?>
                                          </div>
                                    

                                </div>
                            </div>
                        </div>


                       
                    </div>
                    <div class="card-footer">
                        <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                    <?php echo form_close();?>                                                    
                </div>
            </div>
            <div class=" col-sm-12 col-md-7 pr-0">   
               
                <div class="card card-secondary">
                    <div class="card-header">
                        <h2 class="card-title">User List</h2>
                    </div>
                    <div class="card-body">

                        <div id="status_div">
                            <?php if($users){?>
                            <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable datatable" style="width:100%">
                                <thead>
                                    <tr>
                                        <th class="no-sort">#</th>
                                        <th>User Name</th>
                                        <th>Group</th>
                                        <th>Status</th>
                                        <th class="no-sort">Action</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                         $i=1;
                                         foreach($users as $row){?>
                                        <tr>
                                            <td>
                                                <?php echo $i++; ?>
                                            </td>
                                            <td>
                                                <?php echo $row['user_name']; ?>
                                            </td>
                                            <td>
                                                <?php echo $user_group_list[$row['group_id']]; ?>
                                            </td>

                                            <td>
                                                <?php if($row['status'] == 'A')
                                                echo 'Active';
                                           else
                                                echo 'Inactive';
                                            ?>
                                            </td>
                                            <td>
                                                <a class="green-color" href="<?php echo base_url();?>admin/users/index?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <a class="green-color" href="<?php echo base_url();?>admin/users/change_password?user_id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                    <i class="fa fa-key"></i>
                                                </a>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <?php } ?>
                                </tbody>
                            </table>
                            <?php } 
                                    else{
                                        echo 'No details found!!';
                                    }?>
                        </div>
                    </div>
                </div>
              </div>  
                
                </div>
            </div>
            <!--row-->

        </div>
        <!-- content-->
    </div>
    <!-- content-wrapper-->
    <?php
$this->load->view('admin/common/footer');
?>