<?php   $this->load->view('admin/common/header'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark"> PERMISSION MANAGEMENT </h1>
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item">
                            <a href="#">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Users</a>
                        </li>
                        <li class="breadcrumb-item active">Permission Management</li>
                    </ol>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <?php   if(isset($msg)){
			 $message = $msg;
			 
			 }
			 else
			 $message ='';		 
	   ?>
            <?php  
		 if($message!='')
			{ ?>
            <div class="row">
                <div class="col-lg-12">
                    <div class="alert <?php echo $action; ?> alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">X</button>
                        <?php echo urldecode($message);?>
                    </div>
                </div>
            </div>

            <?php }  ?>

            <div class="row">
               <div class="col-sm-12 col-md-5 pl-0">
                <div class="card card-secondary">
                    <div class="card-header">
                        <h2 class="card-title">Permission Form</h2>
                    </div>
                    <?php echo form_open('admin/users/permission_save');?>
                    <div class="card-body">
                        <div class="row">
                            <div class="form-group col-md-12">
                                <label class="control-label">Permission Name<span class="text-red">*</span></label>
                                <input type="hidden" name="id" class="form-control" value="<?php echo $id;?>">
                                <input type="text" name="permission" class="form-control" placeholder="Permission Description" value="<?php echo $permission;?>">
                                <?php echo form_error('permission');?>
                            </div>
                            <div class="form-group  col-md-12">
                                <label class="control-label">Short Code<span class="text-red">*</span>
                                </label>
                                <input type="text" name="key" class="form-control" placeholder="key" value="<?php echo $key;?>">
                                <?php echo form_error('key');?>           
                                
                            </div>
                            <div class="form-group  col-md-12">
                                <label class="control-label">Category<span class="text-red">*</span></label>
                                <select name="category" class="form-control">
                                        <?php foreach(json_decode(PERMISSION_CATEGORIES,TRUE) as $cat){?>
                                            <option value="<?php echo $cat;?>" <?php if($cat == $category) echo 'selected';?>><?php echo $cat;?></option>
                                        <?php } ?>
                                </select>
                                <?php echo form_error('category');?>            
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <!-- <button type="reset" class="btn btn-default">Cancel</button> -->
                        <button type="submit" class="btn btn-info pull-right">Save</button>
                    </div>
                   <?php echo form_close();?>
                </div>
               </div>
               <div class="col-sm-12 col-md-7 pr-0">
                   <div class="card card-secondary">
                       <div class="card-header">
                           <h2 class="card-title">Permission List</h2>
                       </div>
                    <div class="card-body">
   
                     <div id="status_div">
                        <?php if($permissions){?>
                        <table id="example" class="display nowrap table table-bordered table-striped table-hover datatable datatable-Blood_group" style="width:100%">
                            <thead>
                                <tr>
                                    <th class="no-sort">#</th>
                                    <th>Permission</th>
                                    <th>Permission Key</th>
                                    <th>Category</th>
                                    <th class="no-sort">Action</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i=1;
                            foreach($permissions as $row){?>
                                    <tr>
                                        <td><?php echo $i++; ?></td>
                                        <td><?php echo $row['permission']; ?></td>
                                        <td><?php echo $row['key']; ?></td>
                                        <td><?php echo $row['category']; ?></td>
                                        <td>
                                            <a class="green-color" href="<?php echo base_url();?>admin/users/permissions?id=<?php echo urlencode(base64_encode($row['id'].'_'.ENCRYPTION_KEY));?>">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                        </td>
                                        <td></td>
                                    </tr>
                                 <?php } ?>
                            </tbody>
                        </table>
                        <?php } 
                        else{
                            echo 'No details found!!';
                        }?>
                    </div>
                    </div>
                    
                    
                    </div>
                </div>
                </div>
                <!--row-->
    
            </div>
            <!-- content-->
        </div>
        </div>
        <!-- content-wrapper-->
        <?php
    $this->load->view('admin/common/footer');
    ?>