<?php
$config  = array(
    'error_prefix' => '<div class="text-red">',
    'error_suffix' => '</div>',


    'admin/authentication/auth_check' =>array(
            array(
            'field' => 'user_name',
            'label' => 'User Name',
            'rules' => 'required|is_username_email_exist[users.user_name]'
            ),
            array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|is_password_match_to_username_email[user_name]'
            ),
        ),
    'admin/users/change_password' => array(
        array(
            'field' => 'new_password',
            'label' => 'Password',
            'rules' => 'required'
            ),
            array(
                'field' => 'confirm_password',
                'label' => 'Confirm Password',
                'rules' => 'required|matches[new_password]'
            ), 
        ),
    'admin/courses/categories_save' => array(
        array(
            'field' => 'name',
            'label' => 'Name',
            'rules' => 'required|is_field_repeat[categories.name.id.id]'
            ),
            array(
                'field' => 'status',
                'label' => 'Status',
                'rules' => 'required'
            ),
        ),
    'admin/courses/course_save' => array(
            array(
                'field' => 'course_title',
                'label' => 'Course Title',
                'rules' => 'required'
            ),
            array(
                'field' => 'course_code',
                'label' => 'Course Code',
                'rules' => 'required|is_field_repeat[courses.course_code.id.id]'
            ),
            array(
                'field' => 'last_reg_no',
                'label' => 'Last Reg. No',
                'rules' => 'required|integer'
            ),
            array(
                'field' => 'duration',
                'label' => 'Course Duration',
                'rules' => 'required'
            ),
            array(
                'field' => 'course_details',
                'label' => 'Course Details',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'course_category',
                'label' => 'Course Category',
                'rules' => 'required'
            ),
            array(
                'field' => 'start_date',
                'label' => 'Start Date',
                'rules' => 'required|startdate_before[id]'
            ),
            array(
                'field' => 'end_date',
                'label' => 'End Date',
                'rules' => 'required|date_compare[start_date]'
            ),
            array(
                'field' => 'display_date',
                'label' => 'Display From',
                'rules' => 'required|date_before[courses.display_from.id.id]'
            ),
            array(
                'field' => 'course_fee',
                'label' => 'Course Fee',
                'rules' => 'required'
            ),
            array(
                'field' => 'tutors_assigned[]',
                'label' => 'Add Tutors',
                'rules' => 'required'
            ),
        ),

        'admin/tutors/tutor_save' => array(
            array(
                'field' => 'name',
                'label' => 'Name',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'name_title',
                'label' => 'Title',
                'rules' => 'required'
            ),
            array(
                'field' => 'phone_number',
                'label' => 'Phone Number',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'email',
                'label' => 'Email',
                'rules' => 'required|trim|valid_email'
            ),
            array(
                'field' => 'profile',
                'label' => 'Profile',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'address',
                'label' => 'Address',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'degrees',
                'label' => 'Degrees',
                'rules' => 'required|trim'
            ),
        ),

        'admin/class_schedules/schedule_save' => array(
            array(
                'field' => 'course_id',
                'label' => 'Course',
                'rules' => 'required'
            ),
            array(
                'field' => 'tutor_id',
                'label' => 'Tutor',
                'rules' => 'required'
            ),
            array(
                'field' => 'message',
                'label' => 'Message',
                'rules' => 'required|trim'
            ),
            array(
                'field' => 'schedule_date',
                'label' => 'Date',
                'rules' => 'required|date_before[class_schedules.date.id.id]'
            ),
            array(
                'field' => 'schedule_time',
                'label' => 'Start Time',
                'rules' => 'required'
            ),
            array(
                'field' => 'end_time',
                'label' => 'End Time',
                'rules' => 'required|time_greater[schedule_time]'
            ),
            array(
                'field' => 'topic',
                'label' => 'Topic',
                'rules' => 'required'
            ),
            array(
                'field' => 'webinar_link',
                'label' => 'Webinar Link',
                'rules' => 'required|valid_url'
            ),
        ),
            
        'admin/users/permission_save' => array(
            array(
                'field' => 'permission',
                'label' => 'Permission',
                'rules' => 'required|is_field_repeat[permissions.permission.id.id]'
                ),
                array(
                    'field' => 'key',
                    'label' => 'Permission Key',
                    'rules' => 'required|is_field_repeat[permissions.key.id.id]'
                ), 
                array(
                    'field' => 'category',
                    'label' => 'Category',
                    'rules' => 'required'
                )
              
            ),
            'admin/users/user_group_save' => array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required|is_field_repeat[user_groups.name.id.id]'
                    ),                   
                    
                    array(
                        'field' => 'status',
                        'label' => 'Status',
                        'rules' => 'required'
                    ),
                ),
                'admin/users/user_save' => array(
                        array(
                        'field' => 'user_name',
                        'label' => 'User Name',
                        'rules' => 'required|is_field_repeat[users.user_name.id.id]'
                        ), 
                        array(
                        'field' => 'name',
                        'label' => 'Name',
                        'rules' => 'required'
                        ), 
                        array(
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required'
                        ), 
                        array(
                        'field' => 'group_id',
                        'label' => 'User Group',
                        'rules' => 'required'
                        ), 
                         
                        array(
                            'field' => 'email',
                            'label' => 'Email',
                            'rules' => 'valid_email|is_tutor_email_exist'
                        ),
                        array(
                            'field' => 'status',
                            'label' => 'Status',
                            'rules' => 'required'
                        ),
                    ),
            'registration/save' => array(
                array(
                    'field' => 'name',
                    'label' => 'Name',
                    'rules' => 'required'
                ), 
                array(
                    'field' => 'course_id',
                    'label' => 'Course Name',
                    'rules' => 'required'
                ), 
                array(
                    'field' => 'parent_name',
                    'label' => 'Parent Name',
                    'rules' => 'trim'
                ), 
                array(
                    'field' => 'parent_mobile',
                    'label' => 'Parent Mobile',
                    'rules' => 'trim'
                ), 
                array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'required'
                ), 
                array(
                    'field' => 'mobile',
                    'label' => 'Mobile',
                    'rules' => 'required|integer'
                ), 
                array(
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required|min_length[6]'
                ), 
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|valid_email'
                ),
                array(
                    'field' => 'parent_email',
                    'label' => 'Parent Email',
                    'rules' => 'valid_email'
                )
            ), 
            'login/do_login' =>array(
                array(
                    'field' => 'user_name',
                    'label' => 'User Name',
                    'rules' => 'required|is_username_exist[users.user_name]'
                ),
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required'
                ),
            ),
            'login/change_password' =>array(
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'confirm_password',
                    'label' => 'Confirm Password',
                    'rules' => 'required|matches[password]'
                ) 
            ),
            'settings/password_save' => array(
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required|match_current_password[user_id]'
                ),
                array(
                    'field' => 'new_password',
                    'label' => 'New Password',
                    'rules' => 'required'
                ),
                array(
                        'field' => 'confirm_password',
                        'label' => 'Confirm Password',
                        'rules' => 'required|matches[new_password]'
                ), 
            ),
            'settings/staff_password_save' => array(
                array(
                    'field' => 'password',
                    'label' => 'Password',
                    'rules' => 'required|match_current_password[user_id]'
                ),
                array(
                    'field' => 'new_password',
                    'label' => 'New Password',
                    'rules' => 'required'
                ),
                array(
                        'field' => 'confirm_password',
                        'label' => 'Confirm Password',
                        'rules' => 'required|matches[new_password]'
                ), 
            ),
            'settings/student_details_save' => array(
                array(
                    'field' => 'parent_mobile',
                    'label' => 'Parent Mobile',
                    'rules' => 'trim'
                ), 
                array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'required'
                ), 
                array(
                    'field' => 'mobile',
                    'label' => 'Mobile',
                    'rules' => 'required|integer'
                ), 
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|valid_email'
                ),
                array(
                    'field' => 'parent_email',
                    'label' => 'Parent Email',
                    'rules' => 'valid_email'
                )
            ), 
            'settings/staff_details_save' => array(
                array(
                    'field' => 'address',
                    'label' => 'Address',
                    'rules' => 'required'
                ), 
                array(
                    'field' => 'mobile',
                    'label' => 'Mobile',
                    'rules' => 'required|integer'
                ), 
                array(
                    'field' => 'email',
                    'label' => 'Email',
                    'rules' => 'required|valid_email|is_field_repeat[users.email.id.user_id]'
                )
            ), 
            'messages/message_save' => array(
                array(
                    'field' => 'subject',
                    'label' => 'Subject',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'content',
                    'label' => 'Content',
                    'rules' => 'required'
                ),
                array(
                    'field' => 'attachment',
                    'label' => 'Attachment',
                    'rules' => ''
                )
                ),

            'forums/thread_save' => array(
                array(
                    'field' => 'title',
                    'label' => 'Title',
                    'rules' => 'required'
                ), 
                array(
                    'field' => 'description',
                    'label' => 'Description',
                    'rules' => 'required'
                )
            ), 
            
);
 ?>
