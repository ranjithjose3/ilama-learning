<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once dirname(__FILE__) . '/fpdf/fpdf.php';

class Pdf extends FPDF
{


    function __construct()
    {

        parent::__construct();

    }

    var $widths;
    var $aligns;

    function SetWidths($w)
    {
        //Set the array of column widths
        $this->widths=$w;
    }
    function SetBorders($b)
    {
        //Set the array of column widths
        $this->borders=$b;
    }
    function SetAligns($a)
    {
        //Set the array of column alignments
        $this->aligns=$a;
    }

    function Row($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //if($hnew)
        //$h=$hnew;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];

            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $br=isset($this->borders[$i]) ? $this->borders[$i] : '1';

            if($br!=0)
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //$this->Cell($w,5,$data[$i],0,1,'L');
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
            //$this->Cell($w,5,'',0,1,'C');
    }
    function RowTop($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb+6;


        //if($hnew)
        //$h=$hnew;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            if($i==1){
             $this->SetFont('Times','',10);
            }
            else{
                $this->SetFont('Times','B',10);
            }

            $w=$this->widths[$i];

            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $br=isset($this->borders[$i]) ? $this->borders[$i] : '1';

            if($br!=0)
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,11,$data[$i],0,$a);
            //$this->Cell($w,5,$data[$i],0,1,'L');
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
            //$this->Cell($w,5,'',0,1,'C');
    }


    function Rownoborder($data)
    {
        //Calculate the height of the row
        $nb=0;
        for($i=0;$i<count($data);$i++)
            $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
        $h=5*$nb;
        //if($hnew)
        //$h=$hnew;
        //Issue a page break first if needed
        $this->CheckPageBreak($h);
        //Draw the cells of the row
        for($i=0;$i<count($data);$i++)
        {
            $w=$this->widths[$i];
            if($i==1)
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
            else
            $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'C';
            //Save the current position
            $x=$this->GetX();
            $y=$this->GetY();
            //Draw the border
            $br=isset($this->borders[$i]) ? $this->borders[$i] : '1';

            if($br!=0)
            $this->Rect($x,$y,$w,$h);
            //Print the text
            $this->MultiCell($w,5,$data[$i],0,$a);
            //$this->Cell($w,5,$data[$i],0,1,'L');
            //Put the position to the right of the cell
            $this->SetXY($x+$w,$y);
        }
        //Go to the next line
        $this->Ln($h);
            //$this->Cell($w,5,'',0,1,'C');
    }
    function CheckPageBreak($h)
    {
        //If the height h would cause an overflow, add a new page immediately
        if($this->GetY()+$h>$this->PageBreakTrigger)
            $this->AddPage($this->CurOrientation);
    }

    function NbLines($w,$txt)
    {
        //Computes the number of lines a MultiCell of width w will take
        $cw=&$this->CurrentFont['cw'];
        if($w==0)
            $w=$this->w-$this->rMargin-$this->x;
        $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
        $s=str_replace("\r",'',$txt);
        $nb=strlen($s);
        if($nb>0 and $s[$nb-1]=="\n")
            $nb--;
        $sep=-1;
        $i=0;
        $j=0;
        $l=0;
        $nl=1;
        while($i<$nb)
        {
            $c=$s[$i];
            if($c=="\n")
            {
                $i++;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
                continue;
            }
            if($c==' ')
                $sep=$i;
            $l+=$cw[$c];
            if($l>$wmax)
            {
                if($sep==-1)
                {
                    if($i==$j)
                        $i++;
                }
                else
                    $i=$sep+1;
                $sep=-1;
                $j=$i;
                $l=0;
                $nl++;
            }
            else
                $i++;
        }
        return $nl;
    }

// Page header
function Header()
{
    // Logo
   // $this->Image('logo.png',10,6,30);

    $this->Image(HTTP_IMAGES_PATH."jyothi_logo.png",8,5,14);
    // Arial bold 15
    $this->SetFont('Times','B',12);
    $this->SetX(1);
    $this->Cell(0,0,'JYOTHI ENGINEERING COLLEGE, CHERUTHURUTHY',0,0,'C');
    $this->SetFont('Times','B',10);
    $this->SetX(1);
    $this->Cell(0,10,'FACULTY APPRAISAL FORM : YEAR 2019 - 2020',0,0,'C');
    $this->SetX(5);
    $this->SetFont('Times','',11);
    $this->Cell(0,20,'Evaluation based on Self Appraisal',0,0,'C');
    $this->Line(5, 28, 205, 28);

    // Line break
    $this->Ln(24);

    $this->SetFont('Arial','B',50);
    $this->SetTextColor(255,192,203);
    $this->RotatedText(75,190,'Draft Copy',45);



}

function Footer()
{
    //Position at 1.5 cm from bottom
    $this->SetY(-12);
    //$this->Line(5, 289, 205, 289);
    //Arial italic 8
    $this->SetFont('Arial','IB',7);
    //Page number

     $this->Cell(0,8,'Generated on '.date("d-m-Y h:i:sa"),0,0,'L');
     $this->Cell(0,8,'Page '.$this->PageNo().' of {nb}',0,0,'R');
}
function RotatedText($x, $y, $txt, $angle)
{
    //Text rotated around its origin
    $this->Rotate($angle,$x,$y);
    $this->Text($x,$y,$txt);
    $this->Rotate(0);
}

var $angle=0;

function Rotate($angle,$x=-1,$y=-1)
{
    if($x==-1)
        $x=$this->x;
    if($y==-1)
        $y=$this->y;
    if($this->angle!=0)
        $this->_out('Q');
    $this->angle=$angle;
    if($angle!=0)
    {
        $angle*=M_PI/180;
        $c=cos($angle);
        $s=sin($angle);
        $cx=$x*$this->k;
        $cy=($this->h-$y)*$this->k;
        $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm',$c,$s,-$s,$c,$cx,$cy,-$cx,-$cy));
    }
}



}

/* End of file Pdf.php */
/* Location: ./application/libraries/Pdf.php */
