<?php

class Forum_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
  
    
// function for insert intot a table
	function get_comments($course_id =null ,$cmnt_count =null)
	{

		$forum_cmnts = $this->get_all_rows('forum_cmnts', array('course_id' => $course_id), array('id' => 'DESC'), array(), array(), array(), array());



		if ($forum_cmnts) {

			if (count($forum_cmnts) ==$cmnt_count  ) {
			 return;
			}
			

			$all_user_cmnts= array();
			foreach($forum_cmnts as $cmnt){
				$user_cmnt= array();

				if ($cmnt['user_group_id'] == 3) {

					$user = $this->Common_model->get_row('tutors',array('user_id' =>$cmnt['user_id']));

					$user_cmnt = array( 'user_id' => $cmnt['user_id'] ,
									'user_group_id' => $cmnt['user_group_id'] ,
									'name' => $user->name_title.' '.$user->name ,
									'img_path' => 'tutors/small/'.$user->profile_pic, 
									'cmnt' => $cmnt['comment'] , 
									'doc_name' => $cmnt['doc_name'] ,
									'doc_type' => $cmnt['doc_type'] ,
									'cmnt' => $cmnt['comment'] ,
									'updated_on' => $cmnt['updated_on']
								);

					# code...
				}
				elseif ($cmnt['user_group_id'] == 4) {
					$user = $this->Common_model->get_row('students',array('user_id' =>$cmnt['user_id']));

					$user_cmnt = array( 'user_id' => $cmnt['user_id'] ,
									'user_group_id' => $cmnt['user_group_id'] ,
									'name' => $user->name ,
									'img_path' => 'students/small/'.$user->profile_pic, 
									'cmnt' => $cmnt['comment'] , 
									'doc_name' => $cmnt['doc_name'] ,
									'doc_type' => $cmnt['doc_type'] ,
									'cmnt' => $cmnt['comment'] ,
									'updated_on' => $cmnt['updated_on']
								);
				}
				elseif ($cmnt['user_group_id'] == 2) {

					$user = $this->Common_model->get_row('users',array('id' =>$cmnt['user_id']));

					$user_cmnt = array( 'user_id' => $cmnt['user_id'] ,
									'user_group_id' => $cmnt['user_group_id'] ,
									'name' => $user->name ,
									'img_path' => '', 
									'cmnt' => $cmnt['comment'] , 
									'doc_name' => $cmnt['doc_name'] ,
									'doc_type' => $cmnt['doc_type'] ,
									'cmnt' => $cmnt['comment'] ,
									'updated_on' => $cmnt['updated_on']
								);
					
				}
				else{

				}

				$all_user_cmnts[]=$user_cmnt;
				
			}
			return $all_user_cmnts;

		}
		else{
			return;
		}


	}


	// function to get a row from database
	function get_row($table, $search = array(), $select = '')
	{
		if ($select != '')
			$this->db->select($select);
		foreach ($search as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get($table, 1);
		if ($query->num_rows() > 0) {
			if ($select != '')
				return $query->row($select);
			else
				return $query->row();
		} else {
			return;
		}
	}




	function get_all_rows($table, $search = array(), $order_by = array(), $select = '', $where_in = array(), $where_not = array(), $search_like = array())
	{

		if ($select != '')
			$this->db->select($select);
		foreach ($search as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		foreach ($search_like as $field1 => $value1) {
			if ($value1 != '')
				$this->db->like($field1, $value1);
		}
		foreach ($where_in as $key => $val) {
			if (is_array($val))
				$this->db->where_in($key, $val);
		}
		foreach ($where_not as $n_key => $n_val) {
			if (is_array($n_val))
				$this->db->where_not_in($n_key, $n_val);
		}
		foreach ($order_by as $order_value => $order) {
			if ($order != '')
				$this->db->order_by($order_value, $order);
			else
				$this->db->order_by($order_value, 'ASC');
		}
		$query = $this->db->get($table);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return;
		}
	}






  
	
}
