<?php

class Courses_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
  
    function get_course_count_category_wise()
    {
        $sql = "SELECT DISTINCT(s.category_id),count(*) as count,c.name as catagory_name FROM courses as s join categories as c on c.id=s.category_id  WHERE s.display_from <= '" . date('Y-m-d') . "' AND s.end_date >= '" . date('Y-m-d') . "' AND s.status='A' GROUP BY s.category_id";
        $query = $this->db->query($sql );
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        else
        return 0;
    }
	function get_courses_category_wise()
    {
        
        $select_main = "DISTINCT(category_id) as category_id,end_date ";
        $this->db->select($select_main);
        $this->db->where('display_from <=', date('Y-m-d'));
        $this->db->where('end_date >= ',date('Y-m-d'));    
        $this->db->where('status','A');		   		
        $this->db->order_by('end_date', 'DESC');
       
        $query = $this->db->get('courses');
        if ($query->num_rows() > 0) {
            $categories = $query->result_array();
            $result_categories = array_unique(array_column($categories, 'category_id'));
			$courses = array();
			
            foreach ($result_categories as $category_id) {
				$courses[$category_id]['category_name'] = $this->Common_model->get_row('categories',array('id'=>$category_id),'name');
                $courses_data_query = $this->db->query("SELECT * FROM courses WHERE category_id = " . $category_id. " AND display_from <= '" . date('Y-m-d') . "' AND end_date >= '" . date('Y-m-d') . "' AND status='A'");


                if ($courses_data_query->num_rows() > 0) {
                    $courses_data = $courses_data_query->result_array();
                    $courses[$category_id]['courses_data'] = $courses_data;
                }
                else{                 
                    $courses[$category_id]['courses_data'] = array();            
                }
            
            }
         //  print_r($subject_handled);exit;
            return  $courses;
        } else {
            return;
        }

    }
	
}
