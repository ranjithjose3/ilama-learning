<?php

class Common_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}
  
    
// function for insert intot a table
	function insert($table, $data)
	{
		$this->db->insert($table, $data);
		return $this->db->insert_id();
	}
  // function for update all tables 
	function update_with_array($table = '', $where = array(), $data = array())
	{
		return $this->db->update($table, $data, $where);  
	 //return $this->db->last_query();
	}

	// function for update all tables 
	function update_with_batch($table = '', $data= array(), $update_id)
	{
		$this->db->update_batch($table, $data, $update_id);
		return;  
	//return $this->db->last_query();
	}
	function delete($table = '', $datas = array())
	{
		$this->db->delete($table, $datas);
	}  
 // function to get a row from database
	function get_row($table, $search = array(), $select = '')
	{
		if ($select != '')
			$this->db->select($select);
		foreach ($search as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		$this->db->order_by('id', 'ASC');
		$query = $this->db->get($table, 1);
		if ($query->num_rows() > 0) {
			if ($select != '')
				return $query->row($select);
			else
				return $query->row();
		} else {
			return;
		}
	}

	function get_all_rows($table, $search = array(), $order_by = array(), $select = '', $where_in = array(), $where_not = array(), $search_like = array())
	{

		if ($select != '')
			$this->db->select($select);
		foreach ($search as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		foreach ($search_like as $field1 => $value1) {
			if ($value1 != '')
				$this->db->like($field1, $value1);
		}
		foreach ($where_in as $key => $val) {
			if (is_array($val))
				$this->db->where_in($key, $val);
		}
		foreach ($where_not as $n_key => $n_val) {
			if (is_array($n_val))
				$this->db->where_not_in($n_key, $n_val);
		}
		foreach ($order_by as $order_value => $order) {
			if ($order != '')
				$this->db->order_by($order_value, $order);
			else
				$this->db->order_by($order_value, 'ASC');
		}
		$query = $this->db->get($table);

		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return;
		}
	}

//Function to select rows from database
	function get_limited_rows($table, $search = array(), $order_by = array(), $select = '', $result_type = 'result_array', $where_in = array(), $where_not = array(), $search_like = array(), $limit = '', $start = '', $search_field_comma = array())
	{
		if ($select != '')
			$this->db->select($select);
		foreach ($search as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		foreach ($search_like as $field1 => $value1) {
			if ($value1 != '')
				$this->db->like($field1, $value1);
		}
		if (!empty($search_field_comma)) {
			$like_str = '';
			foreach ($search_field_comma as $key => $value) {
				$value1 = $value2 = $value3 = '';
				$value1 = ',' . $value . ',';
				$value2 = $value . ',';
				$value3 = ',' . $value;
				$like_str .= "(" . $key . " = " . $value . " OR " . $key . " LIKE '%" . $value1 . "%' OR " . $key . " LIKE '" . $value2 . "%' OR " . $key . " LIKE '%" . $value3 . "')";
				$this->db->where($like_str);
			}
		}
		foreach ($where_in as $key => $val) {
			if (is_array($val))
				$this->db->where_in($key, $val);
		}
		foreach ($where_not as $n_key => $n_val) {
			if (is_array($n_val))
				$this->db->where_not_in($n_key, $n_val);
		}
		if ($limit != '' && $start != '')
			$this->db->limit($limit, $start);

		foreach ($order_by as $order_value => $order) {
			if ($order != '')
				$this->db->order_by($order_value, $order);
			else
				$this->db->order_by($order_value, 'ASC');
		}
		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			if ($result_type == 'num_rows')
				return $query->num_rows();
			else
				return $query->result_array();
		} else {
			if ($result_type == 'num_rows')
				return 0;
			else
				return ;
		}
	}
 //to get distinct values
	function get_distinct($table, $search, $where = array())
	{
		$this->db->distinct();
		foreach ($where as $field => $value) {
			if ($value != '')
				$this->db->where($field, $value);
		}
		$this->db->select($search);
		$query = $this->db->get($table);
		if ($query->num_rows() > 0) {
			return $query->result_array();
		} else {
			return;
		}
	}

	function array_column(array $input, $columnKey)
	{
		$array = array();
		foreach ($input as $value) {

			$array[] = $value[$columnKey];

		}
		return $array;
	}

	//function insert user log
	function set_user_log($data_to_log=array()){	
		$this->db->insert('log_table', $data_to_log);
		return $this->db->insert_id();
	}

	function tutor_class_exist($tutor_id,$startdate,$start_time,$end_time,$id){
		if($id != ''){
			$sql = "SELECT id from class_schedules where tutor_id = '".$tutor_id."' and date = '".$startdate."' and ((time <= '".$start_time."' and end_time >= '".$start_time."') or (time <= '".$end_time."' and end_time >= '".$end_time."') or (time >= '".$start_time."' and end_time <= '".$end_time."')) and deleted_at = '0' and id != '".$id."'";
		}
		else{
			$sql = "SELECT id from class_schedules where tutor_id = '".$tutor_id."' and date = '".$startdate."' and ((time <= '".$start_time."' and end_time >= '".$start_time."') or (time <= '".$end_time."' and end_time >= '".$end_time."') or (time >= '".$start_time."' and end_time <= '".$end_time."')) and deleted_at = '0'";
		}
		

		$query =$this->db->query($sql);
		if($query->num_rows()>0){
		   return $query->num_rows();
		}
		else{
			return 0;
		}
	}

	function course_class_exist($id){
		$sql = "SELECT id from class_schedules where course_id = '".$id."' and ((date = '".date('Y-m-d')."' and time >= '".date('H:i:s')."') or ( date > '".date('Y-m-d')."')) and deleted_at = '0'";
		

		$query =$this->db->query($sql);
		if($query->num_rows()>0){
		   return $query->num_rows();
		}
		else{
			return 0;
		}
	}

	function tutor_active_class_exist($id){
		$sql = "SELECT id from class_schedules where tutor_id = '".$id."' and ((date = '".date('Y-m-d')."' and time >= '".date('H:i:s')."') or ( date > '".date('Y-m-d')."')) and deleted_at = '0'";
		

		$query =$this->db->query($sql);
		if($query->num_rows()>0){
		   return $query->num_rows();
		}
		else{
			return 0;
		}
	}

	function get_email_verified_students(){

		$sql = "SELECT  students.*,user_name FROM students JOIN users  ON users.id=students.user_id WHERE email_verified='1' ";

		$query =$this->db->query($sql);
		if($query->num_rows()>0){
		   return $query->result_array();
		}
		else{
			return ;
		}
	}

	function get_active_enrolled_course($student_id){
		
		$sql = "SELECT students_enrolled.id,students_enrolled.status,course_id FROM students_enrolled JOIN courses ON courses.id=students_enrolled.course_id WHERE students_enrolled.student_id = '".$student_id."' and courses.end_date >= '".date('Y-m-d')."'";
		$query =$this->db->query($sql);
		
		if($query->num_rows()>0){
			return $query->row();
		}
		else{
			return ;
		}
	}

	function is_course_active($course_id){
		
		$sql = "SELECT id FROM courses WHERE id='".$course_id."' and end_date >= '".date('Y-m-d')."' and status = 'A'";
		$query =$this->db->query($sql);
		
		if($query->num_rows()>0){
			return $query->num_rows();
		}
		else{
			return 0;
		}
	}

	function get_tutor_active_course($course_list_db,$flag="get_name"){
		$course_list = '';
		if( $course_list_db != '')
			$course_assigned_arr = explode(',',$course_list_db);
		else
			$course_assigned_arr = array();
		if(!empty($course_assigned_arr)){
			foreach($course_assigned_arr as $each){
				$if_active = $this->is_course_active($each);
				if($flag == "get_name"){
					if($if_active > 0){
						if($course_list == '')
							$course_list .= $this->get_row('courses',array('id' => $each),'title'); 
						else
							$course_list .= ', '.$this->get_row('courses',array('id' => $each),'title'); 
					}
				}
				else{
					if($if_active > 0){
						if($course_list == '')
							$course_list .= $each; 
						else
							$course_list .= ','.$each;  
					}
				}
			}
		} 
		return $course_list;
	}
	
}
