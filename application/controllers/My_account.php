<?php
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 class My_account extends CI_Controller
   {

	 var $permissions = array();
     public function __construct()
       {
         parent::__construct();
         $this->load->library('form_validation');
		    $this->load->library('permission');
        $this->load->model('Common_model');
        $this->load->model('Courses_model');
        date_default_timezone_set("Asia/Kolkata");
         if (!$this->session->userdata('is_ilama_admin_login'))
           {
             redirect('login');
           } //!$this->session->userdata('is_admin_login')
		    $groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
       }
    
     public function index($msg = '',$action='')
       {
          if (!in_array('access_dashboard', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }

        
          $arr['body_class'] = 'other-pages t-profile-01';
          $arr['main_menu'] = 'my_account';
          $arr['page_title'] = 'My Account';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $user_id = $this->session->userdata('user_id');
          $groupID = $this->session->userdata('group_id');
          $arr['group_id'] = $groupID;
          $arr['profile'] = '';

          if($groupID == '4'){

              $student_id = $this->Common_model->get_row('students',array('user_id' => $user_id),'id');
              $arr['active_course'] = $this->Common_model->get_active_enrolled_course($student_id);
              $arr['profile'] = $this->Common_model->get_row('students',array('user_id' => $user_id),'');
          }
          else{

              $tutor_id = $this->Common_model->get_row('tutors',array('user_id' => $user_id),'id');
              $tutor_details = $this->Common_model->get_row('tutors',array('id' =>$tutor_id));
              if($tutor_details)
                $arr['profile'] = $tutor_details;
              $course_assigned = $tutor_details->course_assigned;

              if($course_assigned != ''){
                    $active_courses = $this->Common_model->get_tutor_active_course($course_assigned,'get_id');
              }
              else{
                    $active_courses = '';
              }
              $arr['active_course'] =  $active_courses;
          }
          $this->load->view('my_account', $arr);
       }


   

   }
?>