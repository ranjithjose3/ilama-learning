<?php
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 class Courses extends CI_Controller
   {

	 var $permissions = array();
     public function __construct()
       {
         parent::__construct();
         $this->load->library('form_validation');
		     $this->load->library('permission');
         $this->load->model('Common_model');
         $this->load->model('Courses_model');
         date_default_timezone_set("Asia/Kolkata");
         $groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
         // get permissions and show error if they don't have any permissions at a particular category or all
         if ($groupID == '3' || $groupID == '4') {
             redirect('forums/forum_courses');
         }
       }
    
    public function index($msg = '',$action='')
    {
        $arr['main_menu'] = 'courses';
        $arr['body_class'] = 'courses';
        $arr['page_title'] = 'Featured Courses';
        $arr['msg'] = $this->session->flashdata('msg');
        $arr['action'] = $this->session->flashdata('action'); 
        $arr['course_categories'] = $this->Common_model->get_all_rows('categories',array('status' => 'A'));
        $arr['courses'] = $this->Courses_model->get_courses_category_wise();
        $this->load->view('courses', $arr);
    }

    public function view_course()
    {
          $arr['body_class']='single-courses_v';
          $arr['main_menu'] = 'courses';
          $arr['page_title'] = 'Course Details';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $course_details ='';
          $student_count = $tutor_count =0;
          $category_name = '';
          $tutors_arr = array();
          $fee_arr = array();
          $arr['latest_courses'] = '';
          $arr['encoded_id'] = $encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
          if($encoded_id !=''){
            $enId = base64_decode($encoded_id);
            $enIdSplit = explode('_',$enId);
            $id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
            
            if($id > 0)
              $course_details = $this->Common_model->get_row('courses',array('id' =>$id));
            if($course_details)			
            {
              
              $tutors_assigned = $course_details->tutors_assigned;
              if($tutors_assigned != ''){
                $tutors_assign_arr = explode(',',$tutors_assigned);
              }
              else{
                $tutors_assign_arr = array();
              }	
              $student_count =($course_details->students_enrolled !='') ? sizeof(explode(',',$course_details->students_enrolled)) : '0';
              $tutor_count =($course_details->tutors_assigned !='') ? sizeof(explode(',',$course_details->tutors_assigned)) : '0';
              
              $category_id = ($course_details->category_id !='') ? $course_details->category_id : '0';
              $category_name =$this->Common_model->get_row('categories',array('id'=>$category_id),'name');
              if(!empty($tutors_assign_arr))
                  $tutors_arr = $this->Common_model->get_all_rows('tutors',array('status' => 'A'),array(), '', array('id' => $tutors_assign_arr), array(), array());
              $fee_arr = $this->Common_model->get_all_rows('fee_structure',array('course_id' => $id),array('due_date' => 'ASC'));

              $arr['latest_courses'] =$this->Common_model->get_limited_rows('courses', array('display_from <='=> date('Y-m-d'),'end_date >= '=>date('Y-m-d'),'status'=>'A','category_id' => $category_id, 'id != ' => $id ), $order_by = array('display_from'=>'DESC'), 'id,title,fee,icon_name','result_array',  array(), array(),  array(), $limit = '4', $start = '0');
            }
      
          }
          $arr['category_name'] =$category_name;
          $arr['student_count'] = $student_count;
          $arr['tutor_count'] = $tutor_count;
          $arr['tutors_assigned'] = $tutors_arr;	
          $arr['fee_arr'] = $fee_arr;	
          $arr['course_details'] = $course_details;
          $arr['category_courses'] =$this->Courses_model->get_course_count_category_wise();
          $this->load->view('view_course', $arr);
    }

    function categories(){
      $encoded_id = ($this->input->get('cat_id')!='') ? urldecode($this->input->get('cat_id')) : '';
      if($encoded_id !=''){
        $enId = base64_decode($encoded_id);
        $enIdSplit = explode('_',$enId);
        $cat_id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
        if($cat_id >0){
          $arr['courses'] = $this->Common_model->get_all_rows('courses',array('category_id'=>$cat_id,'display_from <=' =>date('Y-m-d'),'end_date >=' =>date('Y-m-d'),'status' =>'A'),array('end_date'=>'desc'));
          $arr['body_class']='courses';
          $arr['main_menu'] = 'courses';
          $arr['page_title'] = 'Course Category - '.$this->Common_model->get_row('categories',array('id'=>$cat_id),'name');
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $this->load->view('category_courses', $arr);
        }
        else{
          redirect('courses');
        }
    }
    else{
      redirect('courses');
    }
    
  }
   }
?>