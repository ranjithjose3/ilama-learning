<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Work_with_us extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		date_default_timezone_set("Asia/Kolkata");
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
		// get permissions and show error if they don't have any permissions at a particular category or all
		if ($groupID == '3' || $groupID == '4') {
			redirect('forums/forum_courses');
		}
	}
	
	//function to load home page
	function index(){
		$arr['page_title'] = 'Tutor Registration';
		$arr['main_menu'] = 'tutor_registration';
		$arr['sub_menu1'] = '';
		$arr['body_class'] = 'other-pages become_teachers';
		$this->load->view('tutor_registration',$arr);
	}

	public function tutor_enquiry()
	{
        
        $to_email = "contact@ilamaelarning.com";
		$subject = "Tutor Registration Enquiry";
		$message = 'You have received a tutor registration enquiry request from:<br/><br/>
		NAME: '.strtoupper($_POST['name']).'<br/>
		EMAIL ID: '.$_POST['email'].'<br/>
		PHONE: '.$_POST['phone'];
		
		$headers = "From: iLAMA eLearning <info@ilamaelearning.com>\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
		$mail_stat = mail($to_email,$subject,$message,$headers);
		
        if($mail_stat) {
    		echo "<div class='alert alert-success alert-dismissable' style='height:45'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Your Request has been Sent.</div>";
        } else {
        	echo "<div class='alert alert-danger alert-dismissable' style='height:45'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Problem in Sending Mail.</div>";
        }
	}
}


