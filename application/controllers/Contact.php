<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Contact extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		date_default_timezone_set("Asia/Kolkata");
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
        // get permissions and show error if they don't have any permissions at a particular category or all
        if ($groupID == '3' || $groupID == '4') {
            redirect('forums/forum_courses');
        }
	}
	
	//function to load home page
	function index(){
		$arr['page_title'] = 'Contact';
		$arr['main_menu'] = 'contact';
		$arr['sub_menu1'] = '';
		$arr['body_class'] = 'contact';
		$this->load->view('contact',$arr);
	}

	public function send_mail()
	{
        
        $toEmail = "info@ilamaelarning.com";
        $mailHeaders = "From: " . $_POST["name"] . "<".$_POST["email"].">\r\n";
        if(mail($toEmail, $_POST["subject"], $_POST["message"], $mailHeaders)) {
    		echo "<div class='alert alert-success alert-dismissable' style='height:45'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Your Mail has been Sent.</div>";
        } else {
        	echo "<div class='alert alert-danger alert-dismissable' style='height:45'><a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>Problem in Sending Mail.</div>";
        }
	}

}


