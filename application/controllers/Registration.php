<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registration extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		date_default_timezone_set("Asia/Kolkata");
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
        // get permissions and show error if they don't have any permissions at a particular category or all
        if ($groupID == '3' || $groupID == '4') {
            redirect('forums/forum_courses');
        }
	}
	
	//function to load page to register students
	function index($msg = '', $action = ''){
		$enc_course_id = ($this->input->get('id') != '') ?  $this->input->get('id') : '';
		if($enc_course_id != ''){
			$enId = base64_decode($enc_course_id);
			$enIdSplit = explode('_',$enId);
			$course_id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
		}
		else{
			$course_id = '';
		}
		$arr = array(
			'courseid' => $course_id,
			'name' => '',
			'email' => '',
			'mobile' => '',
			'file_error' => '',
			'address' => '',
			'parent_name' => '',
			'parent_email' => '',
			'parent_mobile' => '',
			'password' => '',
			'course_id' => ''
		);
		
		$arr['body_class'] = 'other-pages register';
		$arr['page_title'] = 'Student Registration';
		$arr['main_menu'] = 'register';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $msg;
		$arr['action'] = $action;
		$this->load->view('student_registration',$arr);
	}

	function save(){
		$file_error = '';
		if (isset($_FILES["profile_pic"]["name"]) && $_FILES["profile_pic"]["name"] != '') {
			$allowedExts = array("jpg", "jpeg", "png");
			$temp = explode(".", $_FILES["profile_pic"]["name"]);
			$extension = end($temp);
			$image_wh_name = $_FILES["profile_pic"]["tmp_name"];
			list($width, $height) = getimagesize($image_wh_name);
			
			if ($_FILES["profile_pic"]["error"] > 0) {
				$file_error .= "Error opening the file<br />";
			}

			if (!in_array($extension, $allowedExts)) {
				$file_error .= "Please upload jpg or png image file<br />";
			}
			if ($_FILES["profile_pic"]["size"] > 2048000) {
				$file_error .= "File size should be less than 2 mB<br />";
			}
			if ($width < "260" || $height < "290") {
				$file_error .= 'Min. width x height should be 260 x 290';
			}
		}
		else{
			$file_error .= 'Profile Pic is required.';
		}
	
		if ($this->form_validation->run() == false || $file_error != '' ) {
			$arr['name'] = $this->input->post('name');
			$arr['email'] = $this->input->post('email');
			$arr['password'] = $this->input->post('password');
			$arr['mobile'] = $this->input->post('mobile');
			$arr['parent_name'] = $this->input->post('parent_name');
			$arr['parent_email'] = $this->input->post('parent_email');
			$arr['parent_mobile'] = $this->input->post('parent_mobile');
			$arr['address'] = $this->input->post('address');
			$arr['profile_pic'] = '';
			$arr['file_error'] = $file_error;
			$arr['courseid'] = $this->input->post('courseid');
			$arr['course_id'] = $this->input->post('course_id');
			$arr['page_title'] = 'Student Registration';
			$arr['main_menu'] = 'register';
			$arr['sub_menu1'] = '';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');
			$arr['body_class'] = 'other-pages register';
			$this->load->view('student_registration',$arr);
		} 
		else {
			$form_password = $this->input->post('password');

			$save_data = array(
			'name' => strtoupper($this->input->post('name')),
			'email' =>$this->input->post('email'),
			'mobile' => $this->input->post('mobile'),
			'address' => $this->input->post('address'),
			'parent_name' => $this->input->post('parent_name'),
			'parent_email' => $this->input->post('parent_email'),
			'parent_mobile' => $this->input->post('parent_mobile'),
			'registered_on' =>date('Y-m-d H:i:s'),
			'enrolled_courses' => '',
			'user_id' =>  '0',
			'status' => 'A',
			);
			
			$image_file = $this->input->post('profilepic');
			$id = $this->Common_model->insert('students',$save_data);
			$new_name = $id.'_'.rand();
			$file_config = array(
				'upload_path' => "uploads/students/",
				'allowed_types' => "*",
				'overwrite' => TRUE,
				'file_name' => $new_name,
				'max_size' => "2048000" 
			);
			if ($image_file != '') {
				$img_ext = explode('.',$image_file);
				$file_name = $new_name.'.'.$img_ext[1];
				rename('uploads/students/tmp/'.$image_file, 'uploads/students/tmp/'.$file_name);
				$this->upload_file($file_config, 'uploads/students/', 'profile_pic', FALSE);
				$this->create_thumbs($file_name);
				unlink('uploads/students/tmp/'.$file_name);
				$this->Common_model->update_with_array('students',array('id'=>$id),array('profile_pic' => $file_name));
			} 

			//save data to users table
			$course_id = $this->input->post('course_id');
			$course_details = $this->Common_model->get_row('courses',array('id' => $course_id),'');
			$course_code = $course_details->course_code;
			$last_reg_no = $course_details->last_reg_no;
			$course_name = strtoupper($course_details->title);
			$reg_no = $last_reg_no + 1;
			$stud_user_name = strtoupper($course_code).$reg_no;

			$users_data = array(
				'user_name' => $stud_user_name,
				'name' => $this->input->post('name'),
				'email' => $this->input->post('email'),
				'password' => sha1(PASS_SALT.$form_password),
				'group_id' => '4',
				'email_verified' => '0',
				'status' => 'A'
			);

			$user_id = $this->Common_model->insert('users',$users_data);
			if($user_id > 0){
				//update courses table
				$this->Common_model->update_with_array('courses',array('id'=>$course_id),array('last_reg_no' => $reg_no));

				//update students table with user id
				$this->Common_model->update_with_array('students',array('id'=>$id),array('user_id' => $user_id));

				/* send email to registered user */
				$to_email = $this->input->post('email');
				$subject = 'Confirm Your Email Address';
				$email_data = array(
					'name' => strtoupper($this->input->post('name')),
					'uid' => $user_id,
					'course_id' => $course_id,
					'course' => $course_name,
					'username' => $stud_user_name,
				);
				$message = $this->load->view('email_template/verify_email',$email_data,true);

				$headers = "From: iLAMA eLearning <contact@ilamaelearning.com>\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to_email,$subject,$message,$headers);

				$msg = 'Your registration has been successful. To continue, please verify your account via the email we have sent to you. If you have not received it, please check your spam folder, then mark it as NOT spam.';
				$action	= 'alert-success';

				//user log
				$update_date = date('Y-m-d H:i:s');
				$data_to_log = array(
					'user_id' => $user_id,
					'table_name' => 'students',
					'table_id' => $id,
					'description' => 'Student #id '.$id.' has been registered',
					'operation' => 'Registration',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('login/index/'.urlencode($msg).'/'.urlencode($action));

			}

			
		}

	}

	/* function to verify email */
	public function verify_email($encoded_id = '',$encoded_courseid=''){
		$enId = base64_decode($encoded_id);
		$enIdSplit = explode('_',$enId);
		$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';

		$enCourseId = base64_decode($encoded_courseid);
		$enCourseIdSplit = explode('_',$enCourseId);
		$course_id = isset($enCourseIdSplit[0]) ? $enCourseIdSplit[0] : '';

		$course_name = $this->Common_model->get_row('courses',array('id' => $course_id),'title');

		//get email verified column detail
		$email_verified_db = $this->Common_model->get_row('users',array('id' => $id),'email_verified');
		$email = $this->Common_model->get_row('users',array('id' => $id),'email');
		$name = $this->Common_model->get_row('users',array('id' => $id),'name');

		if($email_verified_db != '0'){
			$msg = 'Your email has already been verified';
			$action = 'alert-warning';
			redirect('login/index/'.$msg.'/'.$action);
		}
		else{
			$user_data = array(
				'email_verified' => '1'
			);
			$user_id = $this->Common_model->update_with_array('users',array('id' => $id),$user_data);
			$username = $this->Common_model->get_row('users',array('id' => $id),'user_name');
			$student_id = $this->Common_model->get_row('students',array('user_id' => $id),'id');

			$msg = 'Your email has been verified. Please login below';
			$action = 'alert-success';

			//register for the course
			$this->register_course($course_id,$student_id);

			/* send email to registered user */
			$to_email = $email;
			$subject = 'Registration Successfull';
			$email_data = array(
				'name' => strtoupper($name),
				'course' => $course_name,
				'username' => $username
			);
			$message = $this->load->view('email_template/registration_success',$email_data,true);

			$headers = "From: iLAMA eLearning <contact@ilamaelearning.com>\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to_email,$subject,$message,$headers);

			redirect('login/index/'.$msg.'/'.$action);
		}

	}

	/* function to register to course */
	public function register_course($id,$student_id)
    {   
     
		$course_details ='';
		if($id !=''){
			$course_details = $this->Common_model->get_row('courses',array('id' =>$id));
			if($course_details)			
			{
				//insert to students enrolled table
				$update_date = date('Y-m-d H:i:s');
				$data_to_save = array(
					'student_id' => $student_id,
					'course_id' => $id,
					'status' => 'R',
					'admitted_date' => NULL,
					'payment_types' => '',
					'payment_ids' => '',
					'payment_amount' => '',
					'payment_dates' => '',
					'paid_fee' => 0,
					'balance_fee' => $course_details->fee,
					'updated_datetime' => $update_date
				);

				$enroll_id = $this->Common_model->insert('students_enrolled',$data_to_save);

				if($enroll_id > 0){
					$sname = $this->Common_model->get_row('students',array('id' => $student_id),'name');
					$update_user = $this->Common_model->get_row('students',array('id' => $student_id),'user_id');
					$data_to_log = array(
						'user_id' => $update_user,
						'table_name' => 'students_enrolled',
						'table_id' => $enroll_id,
						'description' => 'The student ' . $sname . ' has registered for the course ' . $course_details->title,
						'operation' => 'Registered',
						'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);
				}
			}
	
		}
    }

	function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
	{
		$this->upload->initialize($config);
		if ($this->upload->do_upload($file_name)) {
			$upload_data = $this->upload->data();
			$file_path = explode($destination, $upload_data['full_path']);
			$imagename = $filename = $file_path[1];
			if ($resize == FALSE){
				return $filename;
			}
			elseif ($resize == TRUE) {
				// settings for resize image

				$config['source_image'] = $upload_data['full_path'];
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				if (file_exists($upload_data['full_path'])) {
					unlink($upload_data['full_path']);//after resize delete orginal image
				}
				$filename = explode('.', $filename);
				$image_thumb = $filename[0] . '_thumb.' . $filename[1];
				rename($destination . $image_thumb, $destination . $imagename);
				return $imagename;
			}
		}
		else{
			print_r($this->upload->display_errors());exit;
		}
	}

	function create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // Medium Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/students/tmp/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 260,
				'height'        => 290,
				'quality' 		=> '100',
                'new_image'     => './uploads/students/medium/'.$file_name
                ),
            //thumb image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/students/tmp/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 62,
				'height'        => 63,
				'quality' 		=> '100',
                'new_image'     => './uploads/students/small/'.$file_name
			),
			//very small image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/students/tmp/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 45,
				'height'        => 45,
				'quality' 		=> '100',
                'new_image'     => './uploads/students/very_small/'.$file_name
                )
			);
           
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
	}

	function upload(){

		if(isset($_POST["image"]))
		{

			$folderPath = 'uploads/students/tmp/'; 

			$data = $_POST["image"];

			$image_array_1 = explode(";", $data);

			$image_array_2 = explode(",", $image_array_1[1]);

			$data = base64_decode($image_array_2[1]);
			
			$imgname = time() . '.png';
			
			$imageName = $folderPath.$imgname;
		
			file_put_contents($imageName, $data);
		
			$outpt = '<img src="'.HTTP_UPLOADS_PATH.'students/tmp/'.$imgname.'" style="width:18%;" class="img-thumbnail" />';

			echo json_encode(array($outpt,$imgname));
		}
	}

}


