<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		date_default_timezone_set("Asia/Kolkata");
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
        // get permissions and show error if they don't have any permissions at a particular category or all
        if ($groupID == '3' || $groupID == '4') {
            redirect('forums/forum_courses');
        }
	}
	
	//function to load login page
	function index($msg = '',$action = ''){

		
		$arr['body_class'] = 'other-pages login';
		$arr['page_title'] = 'Login';
		$arr['main_menu'] = 'login';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $msg;
		$arr['action'] = $action;
		$arr['page_url'] = ($this->input->get('pageurl') != '') ? $this->input->get('pageurl') : '';
		$this->load->view('login',$arr);
	}

	public function do_login()
	{
  		
	  $user = ($this->input->post('user_name', true) != '') ? $this->input->post('user_name', true) : '';
	  $password = ($this->input->post('password', true) != '') ? $this->input->post('password', true) : '';
	  $page_url = ($this->input->post('page_url', true) != '') ? $this->input->post('page_url', true) : '';
	  if ($this->form_validation->run() == false) {
				// if page have some error then redirect to login page with error message
	  	$arr['body_class'] = 'other-pages login';
				$arr['page_title'] = 'Login';
				$arr['main_menu'] = 'login';
				$arr['page_url'] = $this->input->post('page_url');
				$this->load->view('login',$arr);
	  } 
	  else {
				   // Check the username and password are exist in the db 
		$enc_pass = sha1(PASS_SALT.$password);
		$sql = "SELECT * FROM users WHERE user_name = '$user' and password = '$enc_pass' and status='A' and ( group_id = '3' or group_id = '4')";
		$val = $this->db->query($sql);
		if ($val->num_rows() > 0) {
  
		  $user_data = $val->row();
			if($user_data->email_verified == '1'){
				$this->session->set_userdata(array(
					'user_id' => $user_data->id,
					'user_name' => $user_data->user_name,
					'name' => $user_data->name,
					'group_id' => $user_data->group_id,            
					'is_ilama_admin_login' => true,
				));
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'users',
					'table_id' => $update_user,
					'description' => 'The user ' . $user_name . ' has successfully logged in at ' . $update_date,
					'operation' => 'Sign In',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);
				if($page_url != '')
						redirect($page_url);
				else
					redirect('forums/forum_courses'); 
			}
			else{
				$msg = 'Please verify your email to sign in.';
				$action = 'alert-danger';
				redirect('login/index/'.$msg.'/'.$action);
			}
		} //$val->num_rows
		else {
			$arr['page_title'] = 'Login';
			$arr['main_menu'] = 'login';
			$arr['msg'] = 'Incorrect username or password';
			$arr['action'] = 'alert-danger';
			$arr['body_class'] = 'other-pages login';
			$arr['sub_menu1'] = '';
			$arr['page_url'] = '';
		  $this->load->view('login',$arr);
		}
	  }
			
	}

	//function to forgot password page
	function forgot_password($msg = '',$action = ''){
		$arr['body_class'] = 'other-pages login';
		$arr['page_title'] = 'Login';
		$arr['main_menu'] = 'login';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $msg;
		$arr['action'] = $action;
		$this->load->view('forgot_password',$arr);
	}

	function verify(){
		$uname = $this->input->post('user_name');
		$uname_exist = $this->Common_model->get_row('users',array('user_name' => $uname),'');
		if($uname_exist){
		  if($uname_exist->status != 'A'){
			$msg = 'User is inactive. Contact admin to activate the user and try again.';
			$action = 'alert-danger';
			redirect('login/forgot_password/'.$msg.'/'.$action);
		  }
		  elseif($uname_exist->email_verified == '0'){
			$msg = 'Email has not been verified. Verify your email id and try again.';
			$action = 'alert-danger';
			redirect('login/forgot_password/'.$msg.'/'.$action);
		  }
		  elseif($uname_exist->reset_password == '0'){
			$msg = 'Email has already been send. Click on the link to reset your password.';
			$action = 'alert-danger';
			redirect('login/forgot_password/'.$msg.'/'.$action);
		  }
		  else{
			  /* send email to registered user */
				$to_email = $uname_exist->email;
				$subject = 'Reset Password Link';
				$email_data = array(
					'name' => strtoupper($uname_exist->name),
					'uid' => $uname_exist->id,
					'username' => $uname_exist->user_name
				);
				$message = $this->load->view('email_template/reset_password',$email_data,true);

				$headers = "From: Ilama eLearning <contact@ilamaelearning.com>\r\n";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
				mail($to_email,$subject,$message,$headers);

				$this->Common_model->update_with_array('users', array('id' => $uname_exist->id), array('reset_password' => '0'));

				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $uname_exist->id;
				$user_name = $uname_exist->user_name;
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'users',
					'table_id' => $update_user,
					'description' => 'Password Reset request has been received from the user ' . $user_name . ' at ' . $update_date,
					'operation' => 'Reset Password Request',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				$msg = 'Email has been send. Click on the link to reset your password.';
				$action = 'alert-success';
				redirect('login/forgot_password/'.$msg.'/'.$action);

		  }
		}
		else{
		  $msg = 'Username has not been registered.';
		  $action = 'alert-danger';
		  redirect('login/forgot_password/'.$msg.'/'.$action);
		}
	}

	function reset_password($encoded_id = ''){
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$user_details = $this->Common_model->get_row('users',array('id' =>$id));
			if($user_details != '' && $user_details->reset_password == '1')			
			{
				$arr['body_class'] = 'other-pages login';
				$arr['page_title'] = 'Login';
				$arr['main_menu'] = 'login';
				$arr['sub_menu1'] = '';
				$arr['id'] = $id;
				$arr['msg'] = 'This link has already been used to reset the password. Click <a href="'.base_url().'login/forgot_password" >here</a> to go back to Forgot Password page to resend email.';
				$arr['action'] = 'alert-danger';
				$arr['output'] = '0';
				$this->load->view('reset_password',$arr);
			}
			elseif($user_details != '' && $user_details->reset_password == '0')	{
				$arr['body_class'] = 'other-pages login';
				$arr['page_title'] = 'Login';
				$arr['main_menu'] = 'login';
				$arr['sub_menu1'] = '';
				$arr['msg'] = '';
				$arr['id'] = $id;
				$arr['action'] = '';
				$arr['output'] = '1';
				$this->load->view('reset_password',$arr);
			}

		}
  	}


	function change_password()
	{
		$id = $this->input->post('id');
		$new_pwd = $this->input->post('password');
		$confirm_password = $this->input->post('confirm_password');
		$user_details = $this->Common_model->get_row('users',array('id' => $id),'');
		if ($this->form_validation->run() == false) {
			$arr['body_class'] = 'other-pages login';
			$arr['page_title'] = 'Login';
			$arr['main_menu'] = 'login';
			$arr['sub_menu1'] = '';
			$arr['msg'] = '';
			$arr['id'] = $id;
			$arr['action'] = '';
			$arr['output'] = '2';
			$this->load->view('reset_password', $arr);
		} 
		else {
			$change_data = array(
				'password' =>sha1(PASS_SALT.$confirm_password),
				'reset_password' => '1'
			);
			$update = $this->Common_model->update_with_array('users',array('id'=>$id), $change_data); 


			if ($update) {
				$msg = 'Password has been updated successfully.';
				$action = 'alert-success';
			
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $id;
				$user_name = $user_details->user_name;
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'users',
					'table_id' => $update_user,
					'description' => 'The user ' . $user_name . ' has successfully changed the password at ' . $update_date,
					'operation' => 'Reset Password',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				$arr['body_class'] = 'other-pages login';
				$arr['page_title'] = 'Login';
				$arr['main_menu'] = 'login';
				$arr['sub_menu1'] = '';
				$arr['msg'] = $msg;
				$arr['action'] = $action;
				$arr['page_url'] = '';
				$this->load->view('login',$arr);
			}
		}
	
	
	}
}


