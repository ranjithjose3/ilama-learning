<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		date_default_timezone_set("Asia/Kolkata");
	}
	
	//function to load home page
	function index(){
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
        // get permissions and show error if they don't have any permissions at a particular category or all
        if ($groupID == '3' || $groupID == '4') {
            redirect('forums/forum_courses');
		}
		else{
			$arr['page_title'] = 'Home';
			$arr['main_menu'] = '';
			$arr['sub_menu1'] = '';
			$arr['body_class'] = '';
			$arr['categories'] = $this->Common_model->get_limited_rows('categories', array('status' => 'A'), array(), 'id,name', 'result_array', array(), array(), array(), '3', '', array());

			$arr['category_title'] = $this->Common_model->get_row('categories',array('id' => HOME_COURSE_CATEGORY),'name');
			$arr['courses'] = $this->Common_model->get_limited_rows('courses', array('status' => 'A', 'display_from <= '=> date('Y-m-d'), 'end_date >= ' => date('Y-m-d')), array(), '', 'result_array', array(), array(), array(), '6', '0', array());
			
			$arr['demo_icons'] = array('home_demo_icon_1.jpg','home_demo_icon_2.jpg','home_demo_icon_3.jpg');
			$arr['demos'] = $this->Common_model->get_limited_rows('tutors', array('status' => 'A'), array('id' => rand()), 'demo_video', 'result_array', array(), array(), array(), '3', '', array());

			$this->load->view('home',$arr);
		}
	}

	public function demo_video_modal(){
		$video_href = $this->input->post('video_href');

		$message = '';
		$message .= '<div class="row">
						<div class="col-sm-12 video-play-btn">
							<video id="video1" class="home-video" controls style="width:100%;padding:50px">
								<source src="'.$video_href.'">
							</video>
						</div>
			
					</div>';
					
		echo $message;
	}

	public function logout()
	{
		$msg = 'Logout successfull';
		//user log
		$update_date = date('Y-m-d H:i:s');
		$user_name = $this->session->userdata('user_name');
		$update_user =$this->session->userdata('user_id');
		if($update_user != ''){
			$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'users',
				'table_id' => $update_user,
				'description' => 'The user ' . $user_name . ' logged out successfully at ' . $update_date,
				'operation' => 'Sign Out',
				'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);
		}
  
		$array_items = array('user_id' => '', 'user_name' => '', 'name' => '', 'group_id' => '', 'is_ilama_admin_login' => '');
		$this->session->unset_userdata($array_items);
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('user_name');
		$this->session->unset_userdata('group_id');
		$this->session->unset_userdata('name');
		
		$this->session->unset_userdata('is_ilama_admin_login');
		$this->session->sess_destroy();
		$this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
		$this->output->set_header("Pragma: no-cache");
		redirect('home', 'refresh');
	}
}


