<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Forums extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		$this->load->model('Forum_model');
        date_default_timezone_set("Asia/Kolkata");
        if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('login');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
		
	}

	function forum_courses(){
		$arr['body_class'] = 'other-pages t-profile-01';
		$arr['forum_threads']=false;
		$arr['main_menu']= 'forums';
		$arr['page_title']='Forums';
		$arr['msg'] =$this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action'); 
		$user_id = $this->session->userdata('user_id');
		$groupID = $this->session->userdata('group_id');
		if($groupID == '4'){
			
			$student_id = $this->Common_model->get_row('students',array('user_id' => $user_id),'id');
			$arr['active_course'] = $this->Common_model->get_active_enrolled_course($student_id);
			$arr['profile'] = $this->Common_model->get_row('students',array('user_id' => $user_id),'');
		}
		else{

			$tutor_id = $this->Common_model->get_row('tutors',array('user_id' => $user_id),'id');
			$tutor_details = $this->Common_model->get_row('tutors',array('id' =>$tutor_id));
			if($tutor_details)
			$arr['profile'] = $tutor_details;
			$course_assigned = $tutor_details->course_assigned;

			if($course_assigned != ''){
				$active_courses = $this->Common_model->get_tutor_active_course($course_assigned,'get_id');
			}
			else{
				$active_courses = '';
			}
			$arr['active_courses'] =  $active_courses;
		}
		$arr['course_id'] = '';
		$arr['threads'] = '';
		$arr['course_name'] = '';
	    $this->load->view('forum/forum_courses',$arr); 
	}


	function threads($enccourse_id = ''){
		$course_id = $this->input->post('course_id');
		$mode = $this->input->post('mode');
	
		$encoded_course_id = $enccourse_id;
		$enId = base64_decode($encoded_course_id);
		$enIdSplit = explode('_',$enId);
		$course_id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
		
		$arr['course_id'] = $course_id;
		$arr['course_name'] = $this->Common_model->get_row('courses',array('id' => $course_id),'title');
		$arr['threads'] = $this->Common_model->get_all_rows('forum_threads',array('course_id' => $course_id, 'deleted' => '0000-00-00 00:00:00', 'status' => 'A'),array('created_on' => 'DESC'));
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');

		$arr['body_class'] = 'other-pages t-profile-01';
		$arr['forum_threads']=false;
		$arr['main_menu']= 'forums';
		$arr['page_title']='Forums';
		$user_id = $this->session->userdata('user_id');
		$groupID = $this->session->userdata('group_id');
		if($groupID == '4'){
			$student_id = $this->Common_model->get_row('students',array('user_id' => $user_id),'id');
			$arr['assigned_courses'] = $this->Common_model->get_active_enrolled_course($student_id);
		}
		elseif($groupID == '3'){
			$tutor_details = $this->Common_model->get_row('tutors',array('user_id' =>$user_id));
			
			if($tutor_details->course_assigned)			
			{
				$arr['assigned_courses'] = $this->Common_model->get_tutor_active_course($tutor_details->course_assigned,'get_id');
				
			}
			else{
				$arr['assigned_courses']=false;
			}
		}
		$this->load->view('forum/threads',$arr);

	}


	function thread_add(){

		$course_id = $this->input->post('course_id') ? $this->input->post('course_id') : '';
		$arr = array(
			'title' => '',
			'file_error' => '',
			'description' => '',
			'thread_image' => '',
			'id' => '',
		);
		
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$thread_details = $this->Common_model->get_row('forum_threads',array('id' =>$id),'');
			if($thread_details)			
			{
				$arr['id'] = $thread_details->id;
				$arr['title'] = $thread_details->title;	
				$arr['description'] = $thread_details->description;	
				$arr['thread_image'] = $thread_details->image;	
				$arr['file_error'] = '';
				$arr['course_id'] = $course_id = $thread_details->course_id;			
			}

		}
		$arr['courses'] = $this->Common_model->get_row('courses',array('id' => $course_id),'');
		$arr['body_class'] = 'other-pages t-profile-01';
		$arr['forum_threads']=false;
		$arr['main_menu']= 'forums';
		$arr['page_title']='Add Forum Thread';

		$this->load->view('forum/thread_add',$arr);

	}


	function thread_save(){

		$file_error = '';
		
		if (isset($_FILES["thread_image"]["name"]) && $_FILES["thread_image"]["name"] != '') {
			$allowedExts = array("jpg", "jpeg", "png");
			$temp = explode(".", $_FILES["thread_image"]["name"]);
			$extension = end($temp);
			$image_wh_name = $_FILES["thread_image"]["tmp_name"];
			list($width, $height) = getimagesize($image_wh_name);
			
			if ($_FILES["thread_image"]["error"] > 0) {
				$file_error .= "Error opening the file<br />";
			}

			if (!in_array($extension, $allowedExts)) {
				$file_error .= "Please upload jpg or png image file<br />";
			}
			if ($_FILES["thread_image"]["size"] > 2048000) {
				$file_error .= "File size should be less than 2 mB<br />";
			}
			if ($width < "760" || $height < "350") {
				$file_error .= 'Min. width x height should be 760 x 350';
			}
		}

		if ($this->form_validation->run() == false || $file_error != '' ) {
			$arr['id'] = $this->input->post('id');
			$arr['title'] = $this->input->post('title');
			$arr['description'] = $this->input->post('description');
			$arr['course_id'] = $this->input->post('course_id');
			$arr['courses'] = $this->Common_model->get_row('courses',array('id' => $this->input->post('course_id')),'');
			$arr['thread_image'] = '';
			$arr['file_error'] = $file_error;
			$arr['body_class'] = 'other-pages t-profile-01';
			$arr['forum_threads']=false;
			$arr['main_menu']= 'forums';
			$arr['page_title']='Add Forum Thread';
	
			$this->load->view('forum/thread_add',$arr);	
		} 
		else{
			$id = $this->input->post('id');
			$encoded_course_id = urlencode(base64_encode($this->input->post('course_id').'_'.ENCRYPTION_KEY));
			$update_date = date('Y-m-d H:i:s');
			$save_data = array(
				'course_id' => $this->input->post('course_id'),
				'title' =>$this->input->post('title'),
				'description' =>$this->input->post('description'),
				'updated_on' => $update_date
			);
			if($id != ''){
				$thread_det = $this->Common_model->get_row('forum_threads',array('id' => $id),'');
				$new_name = $id.'_'.rand();
				$file_config = array(
					'upload_path' => "uploads/forums/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" 
				);

				if ($_FILES['thread_image']['name'] != '') {
					$save_data['image'] = $pic_name = $this->upload_file($file_config, 'uploads/forums/', 'thread_image', FALSE);
					$this->create_thumbs($pic_name);
					if ($thread_det->image != '') {
						if(file_exists('uploads/forums/'.$thread_det->image))
							unlink('uploads/forums/' . $thread_det->image);
						if(file_exists('uploads/forums/medium/'.$thread_det->image))
							unlink('uploads/forums/medium/' . $thread_det->image);
						if(file_exists('uploads/forums/small/'.$thread_det->image))
							unlink('uploads/forums/small/' . $thread_det->image);
					}
				} else
					$save_data['image'] = $thread_det->image;

				$this->Common_model->update_with_array('forum_threads',array('id'=>$id),$save_data);
				$this->session->set_flashdata('msg','Thread Details has been updated.');
				$this->session->set_flashdata('action', 'alert-success');
				//user log
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'forum_threads',
				'table_id' => $id,
				'description' => 'Details of forum thread id #'.$id.' updated',
				'operation' => 'Update',
				'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

			}
			else{
				$save_data['status'] = 'A';
				$save_data['created_on'] = $update_date;
				$save_data['created_by'] = $this->session->userdata('user_id');
				$id = $this->Common_model->insert('forum_threads',$save_data);
				$new_name = $id.'_'.rand();
				$file_config = array(
					'upload_path' => "uploads/forums/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" 
				);
			
				if ($_FILES['thread_image']['name'] != '') {
					$file_name = $this->upload_file($file_config, 'uploads/forums/', 'thread_image', FALSE);
					$this->create_thumbs($file_name);
					$this->Common_model->update_with_array('forum_threads',array('id'=>$id),array('image' => $file_name));
				} 

				$this->session->set_flashdata('msg', 'Thread Details has been inserted');
				$this->session->set_flashdata('action', 'alert-success');
				//user log
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'forum_threads',
				'table_id' => $id,
				'description' => 'Details of thread id #'.$id.' inserted',
				'operation' => 'Insert',
				'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);
			}

			redirect('forums/threads/'.$encoded_course_id);
		}

	}

	function delete_thread(){

		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			$course_id = $this->Common_model->get_row('forum_threads',array('id' => $id),'course_id');
			$encoded_course_id = urlencode(base64_encode($course_id.'_'.ENCRYPTION_KEY));

			$update_date = date('Y-m-d H:i:s');
			$save_data = array(
				'deleted' => $update_date,
				'status' => 'I',
				'updated_on' => $update_date
			);
		
			$this->Common_model->update_with_array('forum_threads',array('id'=>$id),$save_data);

			//delete thread comments
			$save_data_comments = array(
				'deleted' => $update_date,
				'status' => 'I',
				'updated_on' => $update_date
			);
		
			$this->Common_model->update_with_array('forum_thread_comments',array('thread_id'=>$id),$save_data_comments);

			$this->session->set_flashdata('msg','Thread Details has been deleted.');
			$this->session->set_flashdata('action', 'alert-success');
			//user log
			$update_user =$this->session->userdata('user_id');
			$user_name = $this->session->userdata('user_name');
			$data_to_log = array(
			'user_id' => $update_user,
			'table_name' => 'forum_threads',
			'table_id' => $id,
			'description' => 'Details of forum thread id #'.$id.' deleted',
			'operation' => 'Delete',
			'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);

			redirect('forums/threads/'.$encoded_course_id);
		
		}
	}

	function view_thread(){
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			$arr['thread_details'] = $this->Common_model->get_row('forum_threads',array('id' => $id),'');
			$arr['body_class'] = 'other-pages t-profile-01';
			$arr['forum_threads']=false;
			$arr['main_menu']= 'forums';
			$arr['page_title']='View Forum Thread';
			$arr['msg'] = ($this->input->get('msg')!='') ? urldecode($this->input->get('msg')) : '';
			$arr['action'] = ($this->input->get('action')!='') ? urldecode($this->input->get('action')) : '';
			$this->load->view('forum/view_thread',$arr);
		}
	}

	function comment_save(){
		$groupid = $this->session->userdata('group_id');
		$update_date = date('Y-m-d H:i:s');
		$update_user = $this->session->userdata('user_id');

		$thread_id = $_POST['thread_id'];
		$comment = $_POST['comment'];

		
		$save_data = array(
			'thread_id' => $thread_id,
			'parent_id' => '0',
			'comment' => $comment,
			'commented_on' => $update_date,
			'commented_by' =>$update_user,
			'deleted' => '0000-00-00 00:00:00',
			'status' => 'A',
			'updated_on' => $update_date
		);

		$id = $this->Common_model->insert('forum_thread_comments',$save_data);

		$new_name = $id.'_'.rand();
		$file_config = array(
			'upload_path' => "uploads/forums/comments/",
			'allowed_types' => "*",
			'overwrite' => TRUE,
			'file_name' => $new_name,
			'max_size' => "2048000" 
		);

		if (isset($_FILES['comment_image']['name']) &&  $_FILES['comment_image']['name'] != '') {
			$pic_name = $this->upload_file($file_config, 'uploads/forums/comments/', 'comment_image', FALSE);
			$this->Common_model->update_with_array('forum_thread_comments',array('id'=>$id),array('image' => $pic_name));
		} 
		
		//user log
		$data_to_log = array(
			'user_id' => $update_user,
			'table_name' => 'forum_thread_comments',
			'table_id' => $id,
			'description' => 'Comment with #id '.$id.' has been added',
			'operation' => 'Insert Comment',
			'updated_date' => $update_date
		);
		$this->Common_model->set_user_log($data_to_log);

		$output = '';
		$main_comments = $this->Common_model->get_all_rows('forum_thread_comments',array('thread_id' => $thread_id,'parent_id' => '0','deleted' => '0000-00-00 00:00:00', 'status' => 'A'),array('commented_on' => 'ASC'));

		if($main_comments){
			foreach($main_comments as $each){ 
				$sub_comments = $this->Common_model->get_all_rows('forum_thread_comments',array('thread_id' => $thread_id,'parent_id' => $each['id'],'deleted' => '0000-00-00 00:00:00', 'status' => 'A'),array('commented_on' => 'ASC'));

				$commented_on_details = $this->Common_model->get_row('users',array('id' => $each['commented_by']),'');
				if($commented_on_details->group_id == '3'){
					$commentor_det = $this->Common_model->get_row('tutors',array('user_id' => $commented_on_details->id),'');
					$commentor_name = $commentor_det->name;
					$commentor_image = $commentor_det->profile_pic;
					$commentor_folder = 'tutors';
				}
				elseif($commented_on_details->group_id == '4'){
					$commentor_det = $this->Common_model->get_row('students',array('user_id' => $commented_on_details->id),'');
					$commentor_name = $commentor_det->name;
					$commentor_image = $commentor_det->profile_pic;
					$commentor_folder = 'students';
				}
				else{
					
					$commentor_name = $commented_on_details->name;
					$commentor_image = '';
					$commentor_folder = '';
				}
		
			$output .= '<div class="col-sm-12 comment-single-item"><div class="col-sm-1 img-box">';
					if($commentor_image != "" && file_exists("uploads/".$commentor_folder."/very_small/".$commentor_image)){ 
						$output .= '<img src="'.HTTP_UPLOADS_PATH.$commentor_folder.'/very_small/'.$commentor_image.'" alt="" class="img-circle">';
					} else{ 
						$output .= '<img src="'.HTTP_UPLOADS_PATH.'default.jpg" alt="" class="img-circle">';
					} 
				$output .= '</div><div class="col-sm-11 comment-left-bar"><div class="comment-text">
						<ul class="list-unstyled comment-author-box">
							<li class="nameli"> <span class="name">'.$commentor_name.'</span> <span>'.date('M d, Y',strtotime($each['commented_on'])).'</span></li>';
			$output .= '</ul>
						</ul>
						<p>“'.str_replace("\n","<br/>",$each['comment']);
						if($each['image'] != '' && file_exists('uploads/forums/comments/'.$each['image'])){ 
							$output .= '<br/><a class="txt-black" target="blank" href="'.HTTP_UPLOADS_PATH.'forums/comments/'.$each['image'].'">Click here to view the image</a>';
						}
						$output .= '</p>
						<ul class="list-unstyled comment-author-box">';
						if($this->session->userdata('user_id') == $each['commented_by']){
							$output .= '<li class="reply"><a  onclick="edit(\''.$each['id'].'\')" >Edit</a></li>';
						} 
						if(($groupid == '4' && $this->session->userdata('user_id') == $each['commented_by'] && $sub_comments == '') || ($groupid == '3' && ($commented_on_details->group_id == '4' || ($this->session->userdata('user_id') == $each['commented_by'] && $sub_comments == '')))){ 
							$output .= '<li class="reply"><a  onclick="del_comment(\''.$each['id'].'\')" >Delete</a></li>';
						}
						$output .= '<li class="reply"><a href="#">Reply</a></li>
						</ul>
					</div>
				</div>
			</div>';
			
			if($sub_comments){
				foreach($sub_comments as $sub){
					$commented_subon_details = $this->Common_model->get_row('users',array('id' => $sub['commented_by']),'');
					if($commented_subon_details->group_id == '3'){
						$commentor_subdet = $this->Common_model->get_row('tutors',array('user_id' => $commented_subon_details->id),'');
						$commentor_subname = $commentor_subdet->name;
						$commentor_subimage = $commentor_subdet->profile_pic;
						$commentor_subfolder = 'tutors';
					}
					elseif($commented_subon_details->group_id == '4'){
						$commentor_subdet = $this->Common_model->get_row('students',array('user_id' => $commented_subon_details->id),'');
						$commentor_subname = $commentor_subdet->name;
						$commentor_subimage = $commentor_subdet->profile_pic;
						$commentor_subfolder = 'students';
					}
					else{
						
						$commentor_subname = $commented_subon_details->name;
						$commentor_subimage = '';
						$commentor_subfolder = '';
					}

					$output .= '<div class="col-sm-12 comment-single-item reply_text">
						<div class="row">
							<div class="col-sm-1 img-box">';
								if($commentor_subimage != '' && file_exists('uploads/'.$commentor_subfolder.'/very_small/'.$commentor_subimage)){ 
									$output .= '<img src="'.HTTP_UPLOADS_PATH.$commentor_subfolder.'/very_small/'.$commentor_subimage.'" alt="" class="img-circle">';
								} else{ 
									$output .= '<img src="'.HTTP_UPLOADS_PATH.'default.jpg" alt="" class="img-circle">';
								} 
							$output .= '</div>
							<div class="col-sm-11 comment-left-bar">
								<div class="comment-text">
									<ul class="list-unstyled comment-author-box">
										<li class="nameli"> <span class="name">'.$commentor_subname.'</span> <span>
										'.date('M d, Y',strtotime($sub['commented_on'])).'</span></li>';
										if(($groupid == '4' && $this->session->userdata('user_id') == $sub['commented_by']) || ($groupid == '3' && ($commented_subon_details->group_id == '4' || $this->session->userdata('user_id') == $sub['commented_by'])))
										{ 
											$output .= '<li class="reply"><a onclick="del_comment(\''.$sub['id'].'\')" >Delete</a></li>';
										}
										if($this->session->userdata('user_id') == $sub['commented_by']){
											$output .= '<li class="reply"><a onclick="edit(\''.$sub['id'].'\')" >Edit</a></li>';
										} 
						$output .= '</ul>
									<p>“'.str_replace("\n","<br/>",$sub['comment']);
										if($sub['image'] != '' && file_exists('uploads/forums/comments/'.$sub['image'])){ 
											$output .= '<br/><a class="txt-black" target="blank" href="'.HTTP_UPLOADS_PATH.'forums/comments/'.$sub['image'].'">Click here to view the image</a>';
										} 
										$output .= '</p>
									<ul class="list-unstyled comment-author-box">';
										if($this->session->userdata('user_id') == $sub['commented_by']){
											$output .= '<li class="reply"><a onclick="edit(\''.$sub['id'].'\')" >Edit</a></li>';
										} 
										if(($groupid == '4' && $this->session->userdata('user_id') == $sub['commented_by']) || ($groupid == '3' && ($commented_subon_details->group_id == '4' || $this->session->userdata('user_id') == $sub['commented_by'])))
										{ 
											$output .= '<li class="reply"><a onclick="del_comment(\''.$sub['id'].'\')" >Delete</a></li>';
										}
						$output .= '</ul>
								</div>
							</div>
						</div>
					</div>';

					}
				}
				
			
			}
		} 

		echo $output;

	}

	function reply_modal(){
		$parent_comment_id = $this->input->post('comment_id');
		$thread_id = $this->Common_model->get_row('forum_thread_comments',array('id' => $parent_comment_id),'thread_id');
	
		$encoded_thread_id = urlencode(base64_encode($thread_id.'_'.ENCRYPTION_KEY));
		
        $message ='';
        if($parent_comment_id != '') {
			$message .= '<div class="col-sm-12">
			<div class="leave-comment-box">
				<div class="comment-respond">
					<div class="comment-form">';
						$message .= '<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<input type="hidden" id="parent_comment_id" name="parent_comment_id" value="'.$parent_comment_id.'">
										<input type="hidden" id="thread_id" name="thread_id" value="'.$thread_id.'">
										<input type="hidden" id="enc_thread_id" name="enc_thread_id" value="'.$encoded_thread_id.'">
										<textarea class="form-control" rows="8" placeholder="Type Your Comments *" name="reply_comment" id="reply_comment"></textarea>
										<div class="text-red" id="reply_comment_error" ></div>
									</div>
								</div>
								<div class="col-sm-12">
										<div class="form-group custom-file custom-file-position">
											<input type="file" class="custom-file-input" id="reply_comment_image" name="reply_comment_image">
											<label class="custom-file-label" for="reply_comment_image">Comment Image (jpg or png) </label>
										</div>
										<div class="text-red" id="reply_comment_image_error"></div>
								</div>	
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>';
           
           echo $message;
        } 

	}

	function reply_comment_save(){
		$update_date = date('Y-m-d H:i:s');
		$update_user = $this->session->userdata('user_id');

		$parent_comment_id = $_POST['parent_comment_id'];
		$comment = $_POST['comment'];
		$thread_id = $_POST['thread_id'];

		
		$save_data = array(
			'thread_id' => $thread_id,
			'parent_id' => $parent_comment_id,
			'comment' => $comment,
			'commented_on' => $update_date,
			'commented_by' =>$update_user,
			'deleted' => '0000-00-00 00:00:00',
			'status' => 'A',
			'updated_on' => $update_date
		);

		$id = $this->Common_model->insert('forum_thread_comments',$save_data);

		$new_name = $id.'_'.rand();
		$file_config = array(
			'upload_path' => "uploads/forums/comments/",
			'allowed_types' => "*",
			'overwrite' => TRUE,
			'file_name' => $new_name,
			'max_size' => "2048000" 
		);

		if (isset($_FILES['comment_image']['name']) &&  $_FILES['comment_image']['name'] != '') {
			$pic_name = $this->upload_file($file_config, 'uploads/forums/comments/', 'comment_image', FALSE);
			$this->Common_model->update_with_array('forum_thread_comments',array('id'=>$id),array('image' => $pic_name));
		} 
		
		//user log
		$data_to_log = array(
			'user_id' => $update_user,
			'table_name' => 'forum_thread_comments',
			'table_id' => $id,
			'description' => 'Reply Comment with #id '.$id.' has been added',
			'operation' => 'Insert Reply Comment',
			'updated_date' => $update_date
		);
		$this->Common_model->set_user_log($data_to_log);

		echo '1';

	}

	function edit_modal(){
		$comment_id = $this->input->post('comment_id');
		$thread_id = $this->Common_model->get_row('forum_thread_comments',array('id' => $comment_id),'thread_id');
		$comment_details = $this->Common_model->get_row('forum_thread_comments',array('id' => $comment_id),'');
		$encoded_thread_id = urlencode(base64_encode($thread_id.'_'.ENCRYPTION_KEY));
		
        $message ='';
        if($comment_id != '') {
			$message .= '<div class="col-sm-12">
			<div class="leave-comment-box">
				<div class="comment-respond">
					<div class="comment-form">';
						$message .= '<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<input type="hidden" id="comment_id" name="comment_id" value="'.$comment_id.'">
										<input type="hidden" id="thread_id" name="thread_id" value="'.$thread_id.'">
										<input type="hidden" id="enc_thread_id" name="enc_thread_id" value="'.$encoded_thread_id.'">
										<textarea class="form-control" rows="8" placeholder="Type Your Comments *" name="edit_comment" id="edit_comment">'.$comment_details->comment.'</textarea>
										<div class="text-red" id="edit_comment_error" ></div>
									</div>
								</div>
								<div class="col-sm-12">
										<div class="form-group custom-file custom-file-position">';
											if($comment_details->image != '' && file_exists('uploads/forums/comments/'.$comment_details->image)){
												$message .= '<div class="text-red-f"><a class="txt-black" target="_blank" href="'.HTTP_UPLOADS_PATH.'forums/comments/'.$comment_details->image .'">'.$comment_details->image.'</a></div>';
											}
											$message .= '<input type="file" class="custom-file-input" id="edit_comment_image" name="edit_comment_image">
											<label class="custom-file-label" for="edit_comment_image">Comment Image (jpg or png) </label>
										</div>
										<div class="text-red" id="edit_comment_image_error"></div>
								</div>	
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>';
           
           echo $message;
        } 

	}

	function edit_comment_save(){
		$update_date = date('Y-m-d H:i:s');
		$update_user = $this->session->userdata('user_id');

		$id = $_POST['comment_id'];
		$comment = $_POST['comment'];
		$thread_id = $_POST['thread_id'];
		$parent_id = $this->Common_model->get_row('forum_thread_comments',array('id' => $id),'parent_id');
		
		$save_data = array(
			'comment' => $comment,
			'updated_on' => $update_date
		);

		$this->Common_model->update_with_array('forum_thread_comments',array('id' => $id),$save_data);

		$new_name = $id.'_'.rand();
		$file_config = array(
			'upload_path' => "uploads/forums/comments/",
			'allowed_types' => "*",
			'overwrite' => TRUE,
			'file_name' => $new_name,
			'max_size' => "2048000" 
		);

		if (isset($_FILES['comment_image']['name']) &&  $_FILES['comment_image']['name'] != '') {
			$pic_name = $this->upload_file($file_config, 'uploads/forums/comments/', 'comment_image', FALSE);
			$this->Common_model->update_with_array('forum_thread_comments',array('id'=>$id),array('image' => $pic_name));
		} 
		
		//user log
		if($parent_id == '0'){
			$description = 'Comment';
		}
		else{
			$description = 'Reply Comment';
		}
		$data_to_log = array(
			'user_id' => $update_user,
			'table_name' => 'forum_thread_comments',
			'table_id' => $id,
			'description' => $description.' with #id '.$id.' has been updated',
			'operation' => 'Update '.$description,
			'updated_date' => $update_date
		);
		$this->Common_model->set_user_log($data_to_log);

		echo '1';

	}

	function delete_comment(){
		$update_date = date('Y-m-d H:i:s');
		$update_user = $this->session->userdata('user_id');

		$id = $this->input->post('comment_id');
		$parent_id = $this->Common_model->get_row('forum_thread_comments',array('id' => $id),'parent_id');
		$thread_id = $this->Common_model->get_row('forum_thread_comments',array('id' => $id),'thread_id');
		$enc_thread_id = urlencode(base64_encode($thread_id.'_'.ENCRYPTION_KEY));

		$save_data = array(
			'deleted' => $update_date,
			'status' => 'I',
			'updated_on' => $update_date
		);

		$this->Common_model->update_with_array('forum_thread_comments',array('id'=>$id),$save_data);

		$save_data_of_child = array(
			'deleted' => $update_date,
			'status' => 'I',
			'updated_on' => $update_date
		);

		$this->Common_model->update_with_array('forum_thread_comments',array('parent_id'=>$id),$save_data_of_child);


		if($parent_id == '0'){
			$description = 'Comment';
		}
		else{
			$description = 'Reply Comment';
		}

		//user log
		$data_to_log = array(
			'user_id' => $update_user,
			'table_name' => 'forum_thread_comments',
			'table_id' => $id,
			'description' => $description.' with #id '.$id.' has been deleted',
			'operation' => 'Delete '.$description,
			'updated_date' => $update_date
		);
		$this->Common_model->set_user_log($data_to_log);

		echo $enc_thread_id;

	}
	
	function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
	{
		$this->upload->initialize($config);
		if ($this->upload->do_upload($file_name)) {
			$upload_data = $this->upload->data();
			$file_path = explode($destination, $upload_data['full_path']);
			$imagename = $filename = $file_path[1];
			if ($resize == FALSE){
				return $filename;
			}
			elseif ($resize == TRUE) {
				// settings for resize image

				$config['source_image'] = $upload_data['full_path'];
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				if (file_exists($upload_data['full_path'])) {
					unlink($upload_data['full_path']);//after resize delete orginal image
				}
				$filename = explode('.', $filename);
				$image_thumb = $filename[0] . '_thumb.' . $filename[1];
				rename($destination . $image_thumb, $destination . $imagename);
				return $imagename;
			}

		}
		else{
			print_r($this->upload->display_errors());exit;
		}
	}

	function create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // Medium Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/forums/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 760,
				'height'        => 350,
				'quality' 		=> '100',
                'new_image'     => './uploads/forums/medium/'.$file_name
                ),
            //thumb image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/forums/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 80,
				'height'        => 80,
				'quality' 		=> '100',
                'new_image'     => './uploads/forums/small/'.$file_name
                ));
           
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
	}

}


