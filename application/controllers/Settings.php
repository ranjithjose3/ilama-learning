<?php
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 class Settings extends CI_Controller
   {

	 var $permissions = array();
     public function __construct()
       {
         parent::__construct();
         $this->load->library('form_validation');
        $this->load->library('permission');
        $this->load->library('upload');
        $this->load->model('Common_model');
        $this->load->model('Courses_model');
        date_default_timezone_set("Asia/Kolkata");
         if (!$this->session->userdata('is_ilama_admin_login'))
           {
             redirect('login');
           } //!$this->session->userdata('is_admin_login')
		    $groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
       }
    
    public function index()
    {
          if (!in_array('settings_section', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
          $arr['body_class'] = 'other-pages t-profile-01';
          $arr['main_menu'] = 'settings';
          $arr['page_title'] = 'Settings';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $arr['user_id'] = $user_id = $this->session->userdata('user_id');
          $groupID = $this->session->userdata('group_id');
          $arr['group_id'] = $groupID;
          $arr['password'] = '';
          $arr['new_password'] = '';
          $arr['confirm_password'] = '';
          if($groupID == '3'){
              $details = $this->Common_model->get_row('tutors',array('user_id' => $user_id),'');
              $arr['id'] = $details->id;
              $arr['email'] = $details->email;
              $arr['mobile'] = $details->phone_number;
              $arr['address'] = $details->address;
              $this->load->view('staff_settings', $arr);
          }
          elseif($groupID == '4'){
              $details = $this->Common_model->get_row('students',array('user_id' => $user_id),'');
              $arr['id'] = $details->id;
              $arr['email'] = $details->email;
              $arr['mobile'] = $details->mobile;
              $arr['address'] = $details->address;
              $arr['p_email'] = $details->parent_email;
              $arr['p_mobile'] = $details->parent_mobile;
              $arr['prof_photo'] = $details->profile_pic;
              $this->load->view('settings', $arr);
          }
         
    }

    function student_upload(){
      
      if(isset($_POST["image"]))
      {
  
        $folderPath = 'uploads/students/tmp/'; 
  
        $data = $_POST["image"];
  
        $image_array_1 = explode(";", $data);
  
        $image_array_2 = explode(",", $image_array_1[1]);
  
        $data = base64_decode($image_array_2[1]);
        
        $imgname = time() . '.png';
        
        $imageName = $folderPath.$imgname;
      
        file_put_contents($imageName, $data);
      
        $outpt = '<img src="'.HTTP_UPLOADS_PATH.'students/tmp/'.$imgname.'" style="width:18%;" class="img-thumbnail" />';
  
        echo json_encode(array($outpt,$imgname));
      }
    }

    public function password_save()
    {
      if($this->form_validation->run() == false){
          $arr['body_class'] = 'other-pages t-profile-01';
          $arr['main_menu'] = 'settings';
          $arr['page_title'] = 'Settings';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $arr['user_id'] = $user_id = $this->session->userdata('user_id');
          $details = $this->Common_model->get_row('students',array('user_id' => $user_id),'');
          $arr['id'] = $details->id;
          $arr['email'] = $details->email;
          $arr['mobile'] = $details->mobile;
          $arr['address'] = $details->address;
          $arr['p_email'] = $details->parent_email;
          $arr['p_mobile'] = $details->parent_mobile;
          $arr['prof_photo'] = $details->profile_pic;
          $groupID = $this->session->userdata('group_id');
          $arr['group_id'] = $groupID;
          $arr['password'] = $this->input->post('password');
          $arr['new_password'] = $this->input->post('new_password');
          $arr['confirm_password'] = $this->input->post('confirm_password');
          $this->load->view('settings', $arr);
      }
      else{
          $user_id = $this->input->post('user_id');
          $password = $this->input->post('new_password');
          $enc_password = sha1(PASS_SALT.$password);

          $data_to_save = array(
            'password' => $enc_password
          );

          $this->Common_model->update_with_array('users',array('id' => $user_id),$data_to_save);
          $this->session->set_flashdata('msg','Password has been updated');
          $this->session->set_flashdata('action','alert-success');

          //user log
          $update_date = date('Y-m-d H:i:s');
          $data_to_log = array(
            'user_id' => $user_id,
            'table_name' => 'users',
            'table_id' => $user_id,
            'description' => 'Password of user #id '.$user_id.' has been updated',
            'operation' => 'Password Change',
            'updated_date' => $update_date
          );
          $this->Common_model->set_user_log($data_to_log);
        
          redirect('settings/index');
      }
    }

    public function staff_password_save()
    {
      if($this->form_validation->run() == false){
          $arr['body_class'] = 'other-pages t-profile-01';
          $arr['main_menu'] = 'settings';
          $arr['page_title'] = 'Settings';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $arr['user_id'] = $user_id = $this->session->userdata('user_id');
          $details = $this->Common_model->get_row('tutors',array('user_id' => $user_id),'');
          $arr['id'] = $details->id;
          $arr['email'] = $details->email;
          $arr['mobile'] = $details->phone_number;
          $arr['address'] = $details->address;
          $groupID = $this->session->userdata('group_id');
          $arr['group_id'] = $groupID;
          $arr['password'] = $this->input->post('password');
          $arr['new_password'] = $this->input->post('new_password');
          $arr['confirm_password'] = $this->input->post('confirm_password');
          $this->load->view('staff_settings', $arr);
      }
      else{
          $user_id = $this->input->post('user_id');
          $password = $this->input->post('new_password');
          $enc_password = sha1(PASS_SALT.$password);

          $data_to_save = array(
            'password' => $enc_password
          );

          $this->Common_model->update_with_array('users',array('id' => $user_id),$data_to_save);
          $this->session->set_flashdata('msg','Password has been updated');
          $this->session->set_flashdata('action','alert-success');

          //user log
          $update_date = date('Y-m-d H:i:s');
          $data_to_log = array(
            'user_id' => $user_id,
            'table_name' => 'users',
            'table_id' => $user_id,
            'description' => 'Password of user #id '.$user_id.' has been updated',
            'operation' => 'Password Change',
            'updated_date' => $update_date
          );
          $this->Common_model->set_user_log($data_to_log);

          redirect('settings/index');
      }
    }


    public function student_details_save()
    {
      if($this->form_validation->run() == false){
          $arr['body_class'] = 'other-pages t-profile-01';
          $arr['main_menu'] = 'settings';
          $arr['page_title'] = 'Settings';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $arr['user_id'] = $user_id = $this->session->userdata('user_id');
          $arr['id'] = $this->input->post('id');
          $arr['email'] = $this->input->post('email');
          $arr['mobile'] = $this->input->post('mobile');
          $arr['address'] = $this->input->post('address');
          $arr['p_email'] = $this->input->post('parent_email');
          $arr['p_mobile'] = $this->input->post('parent_mobile');
          $arr['prof_photo'] = $this->Common_model->get_row('students',array('id' => $arr['id']),'profile_pic');
          $groupID = $this->session->userdata('group_id');
          $arr['group_id'] = $groupID;
          $arr['password'] = '';
          $arr['new_password'] = '';
          $arr['confirm_password'] = '';
          $this->load->view('settings', $arr);
      }
      else{
          $user_id = $this->input->post('user_id');
          $id = $this->input->post('id');
        
          $data_to_save_users = array(
            'email' => $this->input->post('email')
          );

          $data_to_save_students = array(
            'email' => $this->input->post('email'),
            'mobile' => $this->input->post('mobile'),
            'address' => $this->input->post('address'),
            'parent_email' => $this->input->post('parent_email'),
            'parent_mobile' => $this->input->post('parent_mobile')
          );

          $this->Common_model->update_with_array('users',array('id' => $user_id),$data_to_save_users);
          $this->Common_model->update_with_array('students',array('id' => $id),$data_to_save_students);

          $image_file = $this->input->post('profilepic');
          $new_name = $id.'_'.rand();
          $file_config = array(
            'upload_path' => "uploads/students/",
            'allowed_types' => "*",
            'overwrite' => TRUE,
            'file_name' => $new_name,
            'max_size' => "2048000" 
          );
          if ($image_file != '') {
            $img_ext = explode('.',$image_file);
            $file_name = $new_name.'.'.$img_ext[1];
            rename('uploads/students/tmp/'.$image_file, 'uploads/students/tmp/'.$file_name);
            $this->upload_file($file_config, 'uploads/students/', 'profile_pic', FALSE);
            $this->create_thumbs($file_name);
            unlink('uploads/students/tmp/'.$file_name);
            $this->Common_model->update_with_array('students',array('id'=>$id),array('profile_pic' => $file_name));
          } 

          
          $this->session->set_flashdata('msg','User details has been updated');
          $this->session->set_flashdata('action','alert-success');

          //user log
          $update_date = date('Y-m-d H:i:s');
          $data_to_log = array(
            'user_id' => $user_id,
            'table_name' => 'students',
            'table_id' => $id,
            'description' => 'Details of student #id '.$id.' has been updated',
            'operation' => 'Update',
            'updated_date' => $update_date
          );
          $this->Common_model->set_user_log($data_to_log);

          redirect('settings/index');
      }
    }

    public function staff_details_save()
    {
      if($this->form_validation->run() == false){
          $arr['body_class'] = 'other-pages t-profile-01';
          $arr['main_menu'] = 'settings';
          $arr['page_title'] = 'Settings';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $arr['user_id'] = $user_id = $this->session->userdata('user_id');
          $arr['id'] = $this->input->post('id');
          $arr['email'] = $this->input->post('email');
          $arr['mobile'] = $this->input->post('mobile');
          $arr['address'] = $this->input->post('address');
          $groupID = $this->session->userdata('group_id');
          $arr['group_id'] = $groupID;
          $arr['password'] = '';
          $arr['new_password'] = '';
          $arr['confirm_password'] = '';
          $this->load->view('staff_settings', $arr);
      }
      else{
          $user_id = $this->input->post('user_id');
          $id = $this->input->post('id');
        
          $data_to_save_users = array(
            'email' => $this->input->post('email')
          );

          $data_to_save_tutors = array(
            'email' => $this->input->post('email'),
            'phone_number' => $this->input->post('mobile'),
            'address' => $this->input->post('address')
          );

          $this->Common_model->update_with_array('users',array('id' => $user_id),$data_to_save_users);
          $this->Common_model->update_with_array('tutors',array('id' => $id),$data_to_save_tutors);
          $this->session->set_flashdata('msg','User details has been updated');
          $this->session->set_flashdata('action','alert-success');

          //user log
          $update_date = date('Y-m-d H:i:s');
          $data_to_log = array(
            'user_id' => $user_id,
            'table_name' => 'tutors',
            'table_id' => $id,
            'description' => 'Details of tutor #id '.$id.' has been updated',
            'operation' => 'Update',
            'updated_date' => $update_date
          );
          $this->Common_model->set_user_log($data_to_log);

          redirect('settings/index');
      }
    }

    function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
    {
      $this->upload->initialize($config);
      if ($this->upload->do_upload($file_name)) {
        $upload_data = $this->upload->data();
        $file_path = explode($destination, $upload_data['full_path']);
        $imagename = $filename = $file_path[1];
        if ($resize == FALSE){
          return $filename;
        }
        elseif ($resize == TRUE) {
          // settings for resize image
  
          $config['source_image'] = $upload_data['full_path'];
          $this->image_lib->clear();
          $this->image_lib->initialize($config);
          $this->image_lib->resize();
          if (file_exists($upload_data['full_path'])) {
            unlink($upload_data['full_path']);//after resize delete orginal image
          }
          $filename = explode('.', $filename);
          $image_thumb = $filename[0] . '_thumb.' . $filename[1];
          rename($destination . $image_thumb, $destination . $imagename);
          return $imagename;
        }
      }
      else{
        print_r($this->upload->display_errors());exit;
      }
    }
  
    function create_thumbs($file_name){
          // Image resizing config
          $config = array(
              // Medium Image
              array(
                  'image_library' => 'GD2',
                  'source_image'  => './uploads/students/tmp/'.$file_name,
                  'maintain_ratio'=> FALSE,
                  'width'         => 260,
          'height'        => 290,
          'quality' 		=> '100',
                  'new_image'     => './uploads/students/medium/'.$file_name
                  ),
              //thumb image
              array(
                  'image_library' => 'GD2',
                  'source_image'  => './uploads/students/tmp/'.$file_name,
                  'maintain_ratio'=> FALSE,
                  'width'         => 62,
          'height'        => 63,
          'quality' 		=> '100',
                  'new_image'     => './uploads/students/small/'.$file_name
        ),
        //very small image
              array(
                  'image_library' => 'GD2',
                  'source_image'  => './uploads/students/tmp/'.$file_name,
                  'maintain_ratio'=> FALSE,
                  'width'         => 45,
          'height'        => 45,
          'quality' 		=> '100',
                  'new_image'     => './uploads/students/very_small/'.$file_name
                  )
        );
             
   
          $this->load->library('image_lib', $config[0]);
          foreach ($config as $item){
              $this->image_lib->initialize($item);
              if(!$this->image_lib->resize()){
                  return false;
              }
              $this->image_lib->clear();
          }
    }  

   }
?>