<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Special_offers extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Courses_model');
		date_default_timezone_set("Asia/Kolkata");
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
		// get permissions and show error if they don't have any permissions at a particular category or all
		if ($groupID == '3' || $groupID == '4') {
			redirect('forums/forum_courses');
		}
	}
	
	//function to load home page
	function index(){
		$arr['page_title'] = 'Special Offers';
		$arr['main_menu'] = 'special_offers';
		$arr['sub_menu1'] = '';
		$arr['body_class'] = 'other-pages contact';
		$this->load->view('special_offers',$arr);
	}

}


