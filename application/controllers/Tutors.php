<?php
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 class Tutors extends CI_Controller
   {

	 var $permissions = array();
      public function __construct()
      {
         parent::__construct();
         $this->load->library('form_validation');
		     $this->load->library('permission');
         $this->load->model('Common_model');
         $this->load->model('Courses_model');
         date_default_timezone_set("Asia/Kolkata");
         $groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : '';
         // get permissions and show error if they don't have any permissions at a particular category or all
         if ($groupID == '3' || $groupID == '4') {
             redirect('forums/forum_courses');
         }
      }
    
      public function index($msg = '',$action='')
      {
          $arr['body_class'] = 'teachers-01';
          $arr['main_menu'] = 'tutors';
          $arr['page_title'] = 'Featured Tutors';
          $arr['msg'] =$this->session->flashdata('msg');
          $arr['action'] = $this->session->flashdata('action'); 
          $arr['tutors'] = $this->Common_model->get_all_rows('tutors',array('status' => 'A'));
          $this->load->view('tutors', $arr);
      }

      public function view_profile()
      {
        $arr['body_class'] = 't-profile-01';;
        $arr['main_menu'] = 'tutors';
            $arr['page_title'] = 'Tutor Details';
            $arr['msg'] =$this->session->flashdata('msg');
            $arr['action'] = $this->session->flashdata('action'); 
            $details ='';
          
            $encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
            
            if($encoded_id !=''){
              $enId = base64_decode($encoded_id);
              $enIdSplit = explode('_',$enId);
              $id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
              if($id > 0)
                $details = $this->Common_model->get_row('tutors',array('id' =>$id));
              if($details)			
              {
                
               
              }
        
            }
            $arr['details'] = $details;
            $this->load->view('view_tutor', $arr);
      }

      public function demo_video_modal(){
        $video_href = $this->input->post('video_href');
    
        $message = '';
        $message .= '<div class="row">
                <div class="col-sm-12 video-play-btn">
                  <video id="video1" class="home-video" controls style="width:100%;padding:50px">
                    <source src="'.$video_href.'">
                  </video>
                </div>
          
              </div>';
              
        echo $message;
      }
      
   }
?>