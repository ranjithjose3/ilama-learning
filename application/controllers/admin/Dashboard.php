<?php
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 class Dashboard extends CI_Controller
   {

	 var $permissions = array();
     public function __construct()
       {
         parent::__construct();
         $this->load->library('form_validation');
		  $this->load->library('permission');
		 $this->load->model('Common_model');
         if (!$this->session->userdata('is_ilama_admin_login'))
           {
             redirect('admin/authentication');
           } //!$this->session->userdata('is_admin_login')
		   $groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
       }
     /*--------------------------------------------------
     index  function
     -------------
     Function : Public
     Index function of this controller. 
     Menu: dashboard
     Variables :events, news, transfer_students, transfer_staffs
     Libraries : -
     Model : event_model->getAllActiveEvent,news_model->getAllActiveNews,activity_model->getAllActivities,common_model
     view : dashboard
     
     --------------------------------------------------*/
     public function index($msg = '',$action='')
       {
          if (!in_array('access_dashboard', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
     $arr['main_menu'] = 'home';
     $arr['page_title'] = 'Dashboard';
		 $arr['msg'] =$this->session->flashdata('msg');
		 $arr['action'] = $this->session->flashdata('action');
     $group_id=$this->session->userdata('groupid');
     $arr['courses'] = $this->Common_model->get_limited_rows('courses',array(),array('start_date' =>'DESC'),'','result_array',array(), array(),array(), '8', $start = '0');
     $arr['course_category'] = $this->Common_model->get_limited_rows('categories',array('status' => 'A'),array(),'','num_rows',array(), array(),array(), '', '');
     $arr['tutors'] = $this->Common_model->get_limited_rows('tutors',array('status' => 'A'),array(),'','num_rows',array(), array(),array(), '', '');
     $arr['students'] = $this->Common_model->get_limited_rows('students',array('status' => 'A'),array(),'','num_rows',array(), array(),array(), '', '');
     $arr['course_count'] = $this->Common_model->get_limited_rows('courses',array('status' => 'A'),array(),'','num_rows',array(), array(),array(), '', '');
			$arr['user_updates'] = $this->Common_model->get_limited_rows('log_table',array(),array('updated_date' =>'DESC'),'user_id,description,operation,updated_date','result_array',array(), array(),array(), '5', $start = '0');
	
         $this->load->view('admin/dashboard', $arr);
       }
   }
?>