<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Messages extends CI_Controller {
    var $permissions = array();
    public function __construct() {
        parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('upload');
		$this->load->library('permission');
		$this->load->helper('url');			
		$this->load->model('Common_model');
                date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
	}
	//function for add categories
	function index(){
		if (!in_array('message_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$arr['page_title'] = 'Messages';
		$arr['main_menu'] = 'messages';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['messages']=$this->Common_model->get_all_rows('messages');		
		$this->load->view('admin/messages/index',$arr);
	}
	function add_message(){
		if (!in_array('message_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['page_title'] = 'Messages';
		$arr['main_menu'] = 'messages';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		
		$arr['id'] = '';
		$arr['subject'] = '';
		$arr['msg_type'] = '';		
		$arr['content'] = '';
		$arr['attachment'] = '';
		$arr['selected_courses'] = array();
		$arr['student'] = '';
		$arr['students'] = '';
		$arr['selected_courses'] = array();
		
		$messsage_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
			$messsage_details = $this->Common_model->get_row('messages',array('id' =>$id));
			if($messsage_details)			
			{
				$arr['id'] = $messsage_details->id;
				$arr['subject'] = $messsage_details->subject;			
				$arr['content'] = $messsage_details->content;
				$arr['attachment'] = '';
				$arr['selected_courses'] = array();
				$arr['msg_type'] = '';
			}

		}
		$arr['courses'] = $this->Common_model->get_all_rows('courses',array('status' =>'A'),array(),'id,category_id,title');
		$course_categories = $this->Common_model->get_all_rows('categories',array('status' =>'A'),array(),'id,name');
		$arr['course_categories'] = array_column($course_categories,'name','id');
		$arr['file_error'] = '';
		$this->load->view('admin/messages/add_message',$arr);
	}
	function message_save(){
		if (!in_array('message_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		set_time_limit(0);
		$subject = $this->input->post('subject');
		$content = $this->input->post('content');
		$msg_type = $this->input->post('msg_type');
		$courses =[];
		$students = [];
		if ($this->form_validation->run() == false) {
			$this->session->set_flashdata('msg','Please fill all fields');
			$this->session->set_flashdata('action', 'alert-danger');
			$arr['page_title'] = 'Messages';
			$arr['main_menu'] = 'messages';
			$arr['sub_menu1'] = '';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');
			
			$arr['id'] = '';
			$arr['subject'] = '';
			$arr['msg_type'] = '';		
			$arr['content'] = '';
			$arr['attachment'] = '';
			$arr['selected_courses'] = array();
			$arr['student'] = '';
			$arr['students'] = '';
			$arr['selected_courses'] = array();
			$arr['courses'] = $this->Common_model->get_all_rows('courses',array('status' =>'A'),array(),'id,category_id,title');
			$course_categories = $this->Common_model->get_all_rows('categories',array('status' =>'A'),array(),'id,name');
			$arr['course_categories'] = array_column($course_categories,'name','id');
			$arr['file_error'] = '';
			$this->load->view('admin/messages/add_message',$arr);
		}
		else{
		if($msg_type=='P'){
			$course = $this->input->post('course');
			$student = $this->input->post('students');
			$students = ($student)?explode(',',$student):array();
			$courses = ($course) ? explode(',',$course) : array();
		}
		else{
			 $courses = $this->input->post('courses');
			 $course = implode(',',$courses);
			 $enrolled_students_string ='';
			 if($courses){
				foreach($courses as $course_id){
					$enrolled_students = $this->Common_model->get_row('courses',array('id' =>$course_id),'students_enrolled');
					if($enrolled_students_string=='')
						$enrolled_students_string =$enrolled_students;
					else
					$enrolled_students_string .=','.$enrolled_students;
				}
				$students = array_unique(explode(',',$enrolled_students_string));
			}
			
			
		}
		//
		$file_name = $this->input->post('attachment'); 
		$pdf_config = array(
			'upload_path' => "uploads/messages/",
			'allowed_types' => "*",
			'overwrite' => false,
			'file_name' => $file_name,
			'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
		);
		$file_name = $this->upload_file($pdf_config, 'uploads/messages/', 'attachment', FALSE);
		$save_data = array(
			'subject' =>$subject,
			'content' =>$content,
			'msg_type' =>$msg_type,
			'attachments' => $file_name,
			'courses' =>implode(',',$courses),
			'recipients' =>implode(',',$students),
			'inserted_date' =>date('Y-m-d H:i:s',time()),
			'send_date' =>date('Y-m-d H:i:s',time())
		);
		$message_id = $this->Common_model->insert('messages',$save_data);
		//personal message
		$message ='';
        foreach($students as $student){
			$to_email = $this->Common_model->get_row('students',array('id' => $student),'email');
			$to_name = strtoupper($this->Common_model->get_row('students',array('id' => $student),'name'));
			$message .= $this->load->view('email_template/email_header',array('email_title' =>$subject),true);
			$message .='<tr>
			<td class="email-body" width="100%">
				<table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
				<!-- Body content -->
				<tr class="black-text-row">
					<td class="content-cell">
					<h2>DEAR '.$to_name.',</h2>
					'.$content.'
					
					<p>Thanks,<br>iLAMA eLearning Team</p>
					</td>
				</tr>
				</table>
			</td>
		</tr>';		
			$message .= $this->load->view('email_template/email_footer',array(),true);
			// $headers = "From: iLAMA eLearning <contact@ilamaelearning.com>\r\n";
			// $headers .= "MIME-Version: 1.0\r\n";
			// $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			$attachment = base_url().'uploads/messages/'.$file_name;
			$this->load->library('email');
			$this->email->to($to_email);
			$this->email->from('contact@ilamaelearning.com', 'iLAMA eLearning');  // email From
			$this->email->subject($subject);  // Subject
			$this->email->message($message);  // Message
			if($file_name!='')
				$this->email->attach($attachment);
			if($this->email->send() > 0){
				$save_data = array(
					'message_id' =>$message_id,
					'to_mail' => $to_email,
					'student_id' =>$student,
					'send_date' =>date('Y-m-d H:i:s',time()),
					'send_status' =>'send'
				);
				$this->Common_model->insert('message_log',$save_data);
			}
			else{
				$save_data = array(
					'message_id' =>$message_id,
					'to_mail' => $to_email,
					'student_id' =>$student,
					'send_date' =>date('Y-m-d H:i:s',time()),
					'send_status' =>'failed'
				);
				$this->Common_model->insert('message_log',$save_data);
			}
		}
			//	echo $message;exit;
			$this->session->set_flashdata('msg','Message delivered');
			$this->session->set_flashdata('action', 'alert-success');
			redirect('admin/messages/add_message');
		}
		
	}
function ajax_student_list(){
	$course_id = $this->input->post('course_id');
	$enrolled_students = $this->Common_model->get_row('courses',array('id' =>$course_id),'students_enrolled');
	$enrolled_students_array = explode(',',$enrolled_students);
	$students = $this->Common_model->get_all_rows('students',array('status' =>'A'),array('name'=>'asc'),'id,name',array('id' =>$enrolled_students_array));
	$output ='';
	$output .= '<div class="form-group">
	<label>Students<span class="text-red">*</span></label>
	<select name="students" class="form-control select2" style="width:100%;" >
		<option value="">-- Select --</option>
	';
		if($students){
			foreach($students as $each){ 
				$output .= '<option value="'.$each['id'].'" >'.$each['name'].'</option>';
	
			}
		}

	  
		$output .= '</select></div>';
		echo $output;
}

function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
{
	$this->upload->initialize($config);
	if ($this->upload->do_upload($file_name)) {
		$upload_data = $this->upload->data();
		$file_path = explode($destination, $upload_data['full_path']);
		$imagename = $filename = $file_path[1];
		if ($resize == FALSE){
			return $filename;
		}
		elseif ($resize == TRUE) {
			// settings for resize image

			$config['source_image'] = $upload_data['full_path'];
			$this->image_lib->clear();
			$this->image_lib->initialize($config);
			$this->image_lib->resize();
			if (file_exists($upload_data['full_path'])) {
				unlink($upload_data['full_path']);//after resize delete orginal image
			}
			$filename = explode('.', $filename);
			$image_thumb = $filename[0] . '_thumb.' . $filename[1];
			rename($destination . $image_thumb, $destination . $imagename);
			return $imagename;
		}

	}
	else{
		print_r($this->upload->display_errors());exit;
	}
}

   
}



