<?php
if (!defined('BASEPATH'))
	exit('No direct script access allowed');
class Users extends CI_Controller
{

	var $permissions = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->helper('url');	
		$this->load->model('Common_model');
		date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login'))
		{
		  redirect('admin/authentication');
		}  //!$this->session->userdata('is_admin_login')
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
		if (!$this->permissions = $this->permission->get_category_permissions($groupID)) {
			show_error('You do not have any permissions!');
		}
	}
    //function for User
	function user_groups(){
		if (!in_array('user_group_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['id'] = '';
		$arr['name'] = '';
		$arr['status'] = '';
		
		$user_group_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
			$user_group_details = $this->Common_model->get_row('user_groups',array('id' =>$id));
			if($user_group_details)			
			{
				$arr['id'] = $user_group_details->id;
				$arr['name'] = $user_group_details->name;
				$arr['status'] = $user_group_details->status;
			}	
		}
		$arr['page_title'] = 'User Groups';
		$arr['main_menu'] = 'user_groups';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['user_groups']=$this->Common_model->get_all_rows('user_groups');		
		$this->load->view('admin/users/user_groups',$arr);
	}
	function user_group_save(){
		if (!in_array('user_group_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		if ($this->form_validation->run() == false) {
			$arr['id'] = $this->input->post('id');
			$arr['name'] = $this->input->post('name');
			$arr['status'] = $this->input->post('status');		
			$arr['page_title'] = 'User Groups';
			$arr['main_menu'] = 'user_groups';
			$arr['sub_menu1'] = '';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');
			$arr['user_groups']=$this->Common_model->get_all_rows('user_groups');		
			$this->load->view('admin/users/user_groups',$arr);
		   } 
		   else {
			$save_data = array(
				'name' =>$this->input->post('name'),				
				'status' => $this->input->post('status')			
			   );
			   if($this->input->post('id') !=''){
				   $id = $this->input->post('id');
				   $this->Common_model->update_with_array('user_groups',array('id'=>$id),$save_data);
				   $this->session->set_flashdata('msg','Details of user group has been updated.');
				   $this->session->set_flashdata('action', 'alert-success');  
				    //user log
					 $update_date = date('Y-m-d H:i:s');
					 $update_user =$this->session->userdata('user_id');
					 $user_name = $this->session->userdata('user_name');
					 $data_to_log = array(
					   'user_id' => $update_user,
					   'table_name' => 'user_groups',
					   'table_id' => $id,
					   'description' => 'Details of user group id #'.$id.' updated',
					   'operation' => 'Update',
					   'updated_date' => $update_date
					 );
					 $this->Common_model->set_user_log($data_to_log);
			   }
			   else{
					$id = $this->Common_model->insert('user_groups',$save_data);
					$this->session->set_flashdata('msg', 'Details of user group has been inserted');
					$this->session->set_flashdata('action', 'alert-success');		
					 //user log
					 $update_date = date('Y-m-d H:i:s');
					 $update_user =$this->session->userdata('user_id');
					 $user_name = $this->session->userdata('user_name');
					 $data_to_log = array(
					   'user_id' => $update_user,
					   'table_name' => 'user_groups',
					   'table_id' => $id,
					   'description' => 'Details of user group id #'.$id.' inserted',
					   'operation' => 'Insert',
					   'updated_date' => $update_date
					 );
					 $this->Common_model->set_user_log($data_to_log);			
			   }

			   redirect('admin/users/user_groups');
		   }

	} 
/*--------------------------------------------------
Change password function
----------------------------------------------------*/
	public function change_password()
	{
		$group_id = $this->session->userdata('group_id');
		$arr['page_title'] = 'Change Password';
		$arr['main_menu'] = 'users';
		$arr['sub_menu1'] = '';
		if ($this->input->get('user_id') == '') {
			$arr['user_id'] = $user_id = $this->session->userdata('user_id'); // if logged user is not admin then user id taken from SESSION
			$id = $this->input->post('user_id');
		} //$this->input->get('user_id') == '' end
		else {
			$user_id = $this->input->get('user_id'); //if admin is change password of any user then user id taken using GET method
			$enId = base64_decode($user_id);
			$enIdSplit = explode('_',$enId);
			$arr['user_id'] = $id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
		}
		if ($this->input->post('submit')) {
			$new_pwd = $this->input->post('new_password');
			$confirm_password = $this->input->post('new_password');
			//$this->form_validation->set_error_delimiters('<div class="error">', '</div>');
			if ($this->form_validation->run() == false) {
				$this->load->view('admin/users/change_password', $arr);
  			 } //$this->form_validation->run() == FALSE
   			else {
			$change_data = array(
				'password' =>sha1(PASS_SALT.$confirm_password)
			);
			
			$update = $this->Common_model->update_with_array('users',array('id'=>$id), $change_data); //update password


			if ($update) {
				$msg = 'Password has been Updated successfully.';
				$action = 'alert-success';
				$this->session->set_flashdata('msg',$msg);
				$this->session->set_flashdata('action',$action);	
					//insertion to web_log_table
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');				
				$data_to_log = array(
					'user_id' => $update_user,	
					'table_name' => 'users',
					'table_id' => $id,				
					'description' => 'Password of the user : ' . $this->Common_model->get_row('users', array('id' => $id), 'name') . ' has been updated',
					'operation' => 'Change Password',					
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);
				if ($id == $update_user) {
					redirect('admin/dashboard');
				} elseif ( $id != $update_user) {
					redirect('admin/users/index');
				}
			}
		}

		} //$this->input->post('submit')
		else {
			$this->load->view('admin/users/change_password', $arr);
		}
	}
       
      //category list page
	public function index()
	{
		if (!in_array('user_management', $this->permissions)) {
			show_error('You do not have access to this page!');
		}
		
		$arr['id'] = '';
		$arr['user_name'] = '';
		$arr['name'] = '';
		$arr['email'] = '';
		$arr['password'] = '';
		$arr['group_id'] = '';		
		$arr['status'] = '';
		$user_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
			$user_details = $this->Common_model->get_row('users',array('id' =>$id));
			if($user_details)			
			{
				$arr['id'] = $user_details->id;
				$arr['user_name'] = $user_details->user_name;
				$arr['name'] = $user_details->name;
				$arr['email'] = $user_details->email;
				$arr['password'] = $user_details->password;
				$arr['group_id'] = $user_details->group_id;				
				$arr['status'] =$user_details->status;
			}	
		}	
		$arr['page_title'] = 'Users';
		$arr['main_menu'] = 'users';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$group_id = $this->session->userdata('user_type');
		$user_name = $this->session->userdata('username');
		$user_id = $this->session->userdata('id');		
		$arr['users'] = $this->Common_model->get_all_rows('users');		
		$arr['user_groups'] = $user_groups= $this->Common_model->get_all_rows('user_groups',array('status' =>'A'),array('sort_order' =>'ASC'));
		$arr['user_group_list'] = array_column($user_groups,'name','id')  ;	
		
		$this->load->view('admin/users/index', $arr);
	}
	public function user_save()
	{
		if (!in_array('user_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		if ($this->form_validation->run() == false) {
			$arr['id'] = $this->input->post('id');		
			$arr['user_name'] = $this->input->post('user_name');
			$arr['name'] = $this->input->post('name');
			$arr['email'] = $this->input->post('email');
			$arr['password'] = $this->input->post('password');
			$arr['group_id'] = $this->input->post('group_id');			
			$arr['status'] =$this->input->post('status');		
			$arr['page_title'] = 'Users';
			$arr['main_menu'] = 'users';
			$arr['sub_menu1'] = '';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');
			$group_id = $this->session->userdata('user_type');
			$user_name = $this->session->userdata('username');
			$user_id = $this->session->userdata('id');		
			$arr['users'] = $this->Common_model->get_all_rows('users');		
			$arr['user_groups'] = $user_groups= $this->Common_model->get_all_rows('user_groups',array('status' =>'A'),array('sort_order' =>'ASC'));
			$arr['user_group_list'] = array_column($user_groups,'name','id')  ;	
			
			$this->load->view('admin/users/index', $arr);
		
		} 
		else {
			$save_data = array(				
				'group_id' => $this->input->post('group_id'),
				'user_name' => $this->input->post('user_name'),									
				'email' => $this->input->post('email'),					
				'name' => $this->input->post('name'),				
				'status' => $this->input->post('status'),							
			);
			if($this->input->post('id') != ''){
				$id = $this->input->post('id');
				$this->Common_model->update_with_array('users',array('id'=>$id),$save_data);
				$this->session->set_flashdata('msg','Details of user has been updated.');
				$this->session->set_flashdata('action', 'alert-success');  
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
				  'user_id' => $update_user,
				  'table_name' => 'users',
				  'table_id' => $id,
				  'description' => 'Details of user id #'.$id.' updated',
				  'operation' => 'Update',
				  'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);
			}
			else{
				$save_data['password'] = sha1(PASS_SALT .$this->input->post('password'));
				$id = $this->Common_model->insert('users',$save_data);
				$this->session->set_flashdata('msg', 'Details of user has been inserted');
				$this->session->set_flashdata('action', 'alert-success');	
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
				  'user_id' => $update_user,
				  'table_name' => 'users',
				  'table_id' => $id,
				  'description' => 'Details of user id #'.$id.' inserted',
				  'operation' => 'Insert',
				  'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);				
		   }

		   redirect('admin/users/index');
	
		}
	}
//function for add permissions
function permissions(){
	if (!in_array('permission_management', $this->permissions))
	{
		show_error('You do not have access to this page!');
	}
	$arr['id'] = '';
	$arr['permission'] = '';
	$arr['key'] = '';
	$arr['category'] = '';
	
	$permission_details ='';
	$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
	if($encoded_id !=''){
		$enId = base64_decode($encoded_id);
		$enIdSplit = explode('_',$enId);
		$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
		if($id > 0)
		$permission_details = $this->Common_model->get_row('permissions',array('id' =>$id));
		if($permission_details)			
		{
			$arr['id'] = $permission_details->id;
			$arr['permission'] = $permission_details->permission;
			$arr['key'] = $permission_details->key;
			$arr['category'] = $permission_details->category;
		}	
	}
	$arr['page_title'] = 'Permissions';
	$arr['main_menu'] = 'permissions';
	$arr['sub_menu1'] = '';
	$arr['msg'] = $this->session->flashdata('msg');
	$arr['action'] = $this->session->flashdata('action');
	$arr['permissions']=$this->Common_model->get_all_rows('permissions');		
	$this->load->view('admin/users/permissions',$arr);
}
function permission_save(){
	if (!in_array('permission_management', $this->permissions))
	{
		show_error('You do not have access to this page!');
	}
	if ($this->form_validation->run() == false) {
		$arr['id'] = $this->input->post('id');
		$arr['permission'] = $this->input->post('permission');
		$arr['key'] = $this->input->post('key');
		$arr['category'] = $this->input->post('category');
	
		$arr['page_title'] = 'Permissions';
		$arr['main_menu'] = 'permissions';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['permissions']=$this->Common_model->get_all_rows('permissions');		
		$this->load->view('admin/users/permissions',$arr);
	   } 
	   else {
		   $save_data = array(
			'permission' =>$this->input->post('permission'),
			'key' => $this->input->post('key'),
			'category' => $this->input->post('category')				
		   );
		   if($this->input->post('id') !=''){
			   $id = $this->input->post('id');
			   $this->Common_model->update_with_array('permissions',array('id'=>$id),$save_data);
			   $this->session->set_flashdata('msg','Details of permission has been updated.');
			   $this->session->set_flashdata('action', 'alert-success');  

			   //user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'permissions',
					'table_id' => $id,
					'description' => 'Permission #id '.$id.' has been updated',
					'operation' => 'Update',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

		   }
		   else{
				$id = $this->Common_model->insert('permissions',$save_data);
				$this->session->set_flashdata('msg', 'Details of permission has been inserted');
				$this->session->set_flashdata('action', 'alert-success');	
				
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'permissions',
					'table_id' => $id,
					'description' => 'Permission #id '.$id.' has been added',
					'operation' => 'Insert',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);
		   }

		   redirect('admin/users/permissions');
	   }

}
	function permission_map(){
		if (!in_array('user_group_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['permission_list'] = [];
		$arr['group_id'] = '';	
		$arr['group_name'] = '';	
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$group_id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($group_id > 0){
				$permissions_arr=$this->Common_model->get_all_rows('permission_map',array('group_id' => $group_id),array('permission_id' => 'ASC'),'permission_id',array(),array());			
				$arr['group_id'] =$group_id;
				$arr['group_name'] = $this->Common_model->get_row('user_groups',array('id' => $group_id),'name');
				if($permissions_arr)
					$arr['permission_list'] =array_column($permissions_arr,'permission_id');
			}
		}

		$arr['page_title'] = 'User Groups';
		$arr['main_menu'] = 'user_groups';
		$arr['sub_menu1'] = '';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['distinct_category']=$this->Common_model->get_distinct('permissions','category');
		$arr['user_groups'] = $this->Common_model->get_all_rows('user_groups',array(),array('sort_order' => 'ASC'),'',array(),array());
		
		$this->load->view('admin/users/permission_map',$arr);
	}

	function permission_mapping_save(){
		if (!in_array('user_group_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$group_id=$this->input->post('group_id');		
		$permissions_arr=$this->Common_model->get_all_rows('permission_map',array('group_id' => $group_id),array('permission_id' => 'ASC'),'permission_id',array(),array('permission_id' => array('1')));
		if($permissions_arr)
			$permissions_old = array_column($permissions_arr, 'permission_id');
		else
			$permissions_old = [];
		$permission_ids=$this->input->post('permissions_array');
		if(!is_array($permission_ids))
			$permission_ids = [];
        $perm_to_add = array_diff($permission_ids,$permissions_old);
        $perm_to_remove = array_diff($permissions_old,$permission_ids);		
		if(!empty($perm_to_add)){
			foreach($perm_to_add as $add_perm){
				$data_to_add = array(
					'group_id' => $group_id,
					'permission_id' => $add_perm
					);
					$perm_ins = $this->Common_model->insert('permission_map',$data_to_add);
			}
		}
		if(!empty($perm_to_remove)){
			foreach($perm_to_remove as $del_perm){
				$data_to_del = array(
					'group_id' => $group_id,
					'permission_id' => $del_perm
					);
					$perm_del = $this->Common_model->delete('permission_map',$data_to_del);
			}
		}
		$this->session->set_flashdata('msg', 'Permissions of user group updated successfully');
		$this->session->set_flashdata('action', 'alert-success');	
		redirect('admin/users/user_groups');
	}

}
?>
