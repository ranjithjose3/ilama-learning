<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Courses extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
        date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
	}
	
	//function to add categories
	function categories(){
		if (!in_array('categories_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['id'] = '';
		$arr['name'] = '';		
		$arr['status'] = '';
		$category_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$category_details = $this->Common_model->get_row('categories',array('id' =>$id));
			if($category_details)			
			{
				$arr['id'] = $category_details->id;
				$arr['name'] = $category_details->name;			
				$arr['status'] = $category_details->status;
			}

		}
		$arr['page_title'] = 'Categories';
		$arr['main_menu'] = 'courses';
		$arr['sub_menu1'] = 'categories';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['categories']=$this->Common_model->get_all_rows('categories');		
		$this->load->view('admin/courses/categories',$arr);
	}
	
	function categories_save(){
		if (!in_array('categories_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		if ($this->form_validation->run() == false) {
			$arr['id'] = $this->input->post('id');
			$arr['name'] = $this->input->post('name');
			$arr['status'] = $this->input->post('status');
			$arr['page_title'] = 'Categories';
			$arr['main_menu'] = 'general_settings';
			$arr['sub_menu1'] = 'categories';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');
			$arr['categories']=$this->Common_model->get_all_rows('categories');		
			$this->load->view('admin/courses/categories',$arr);
		   } 
		   else {
			   $save_data = array(
				'name' =>$this->input->post('name'),
				'status' => $this->input->post('status'),
				'added_by' =>$this->session->userdata('user_id') ,
				'added_on' =>date('Y-m-d H:i:s'),
			   );
			   if($this->input->post('id') !=''){
				   $id = $this->input->post('id');
				   $this->Common_model->update_with_array('categories',array('id'=>$id),$save_data);
				   $this->session->set_flashdata('msg','Details of category has been updated.');
				   $this->session->set_flashdata('action', 'alert-success');
				   //user log
					 $update_date = date('Y-m-d H:i:s');
					 $update_user =$this->session->userdata('user_id');
					 $user_name = $this->session->userdata('user_name');
					 $data_to_log = array(
					   'user_id' => $update_user,
					   'table_name' => 'categories',
					   'table_id' => $id,
					   'description' => 'Details of category id #'.$id.' updated',
					   'operation' => 'Update',
					   'updated_date' => $update_date
					 );
					 $this->Common_model->set_user_log($data_to_log);

			   }
			   else{
					$id = $this->Common_model->insert('categories',$save_data);
					$this->session->set_flashdata('msg', 'Details of category has been inserted');
					$this->session->set_flashdata('action', 'alert-success');
					 //user log
					 $update_date = date('Y-m-d H:i:s');
					 $update_user =$this->session->userdata('user_id');
					 $user_name = $this->session->userdata('user_name');
					 $data_to_log = array(
					   'user_id' => $update_user,
					   'table_name' => 'categories',
					   'table_id' => $id,
					   'description' => 'Details of category id #'.$id.' inserted',
					   'operation' => 'Insert',
					   'updated_date' => $update_date
					 );
					 $this->Common_model->set_user_log($data_to_log);
			   }

			   redirect('admin/courses/categories');
		   }

	}

	//function to add courses
	function add_course(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['id'] = '';
		$arr['category_id'] = '';
		$arr['title'] = '';	
		$arr['duration'] = '';		
		$arr['start_date'] = '';	
		$arr['end_date'] = '';
		$arr['display_date'] = '';	
		$arr['icon_name'] = '';	
		$arr['course_pdf'] = '';		
		$arr['details'] = '';	
		$arr['fee'] = '';
		$arr['course_code'] = '';
		$arr['last_reg_no'] = '1000';	
		$arr['payment_structure_error'] = '';	
		$arr['tutors_assigned'] = array();	
		$arr['file_error'] = '';
		$arr['pdf_error'] = '';
		$arr['fee_structures'] = '';		
		$course_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$course_details = $this->Common_model->get_row('courses',array('id' =>$id));
			if($course_details)			
			{
				$arr['id'] = $course_details->id;
				$arr['category_id'] = $course_details->category_id;		
				$arr['title'] = $course_details->title;	
				$arr['duration'] = $course_details->duration;	
				$arr['start_date'] = date('d/m/Y',strtotime($course_details->start_date));
				$arr['end_date'] = date('d/m/Y',strtotime($course_details->end_date));	
				$arr['display_date'] = date('d/m/Y',strtotime($course_details->display_from));	
				$arr['icon_name'] = $course_details->icon_name;	
				$arr['course_pdf'] = $course_details->course_pdf;	
				$arr['details'] = $course_details->details;	
				$arr['fee'] = $course_details->fee;	
				$arr['course_code'] = $course_details->course_code;	
				$arr['last_reg_no'] = $course_details->last_reg_no;		
				$arr['payment_structure'] = $course_details->payment_structure;	
				$tutors_assigned = $course_details->tutors_assigned;
				if($tutors_assigned != ''){
					$tutors_arr = explode(',',$tutors_assigned);
				}
				else{
					$tutors_arr = array();
				}
				$arr['tutors_assigned'] = $tutors_arr;	
				$arr['file_error'] = '';	
				$arr['pdf_error'] = '';	
				$arr['fee_structures'] = $this->Common_model->get_all_rows('fee_structure',array('course_id' => $id),array('due_date' => 'ASC'));			
			}

		}
		$arr['page_title'] = 'Add Course';
		$arr['main_menu'] = 'courses';
		$arr['sub_menu1'] = 'add_course';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$this->load->view('admin/courses/add_course',$arr);
	}

	function course_save(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
		}
		$file_error = $payment_structure_error = $pdf_error = '';
		if (isset($_FILES["course_icon"]["name"]) && $_FILES["course_icon"]["name"] != '') {
			$allowedExts = array("jpg", "jpeg", "png");
			$temp = explode(".", $_FILES["course_icon"]["name"]);
			$extension = end($temp);
			$image_wh_name = $_FILES["course_icon"]["tmp_name"];
			list($width, $height) = getimagesize($image_wh_name);

			if ($_FILES["course_icon"]["error"] > 0) {
				$file_error .= "Error opening the file<br />";
			}

			if (!in_array($extension, $allowedExts)) {
				$file_error .= "Please upload jpg or png image file<br />";
			}
			if ($_FILES["course_icon"]["size"] > 2048000) {
				$file_error .= "File size should be less than 2 mB<br />";
			}
			if ($width < "781" || $height < "450") {
				$file_error .= 'Min. width x height should be 781 x 450';
			}
		}
		else{
			if($this->input->post('id') == ''){
				$file_error .= "Course Icon image is required<br />";
			}
		}

		if (isset($_FILES["course_pdf"]["name"]) && $_FILES["course_pdf"]["name"] != '') {
			$pallowedExts = array("pdf");
			$ptemp = explode(".", $_FILES["course_pdf"]["name"]);
			$pextension = end($ptemp);
			
			if ($_FILES["course_pdf"]["error"] > 0) {
				$pdf_error .= "Error opening the file<br />";
			}

			if (!in_array($pextension, $pallowedExts)) {
				$pdf_error .= "Please upload pdf file<br />";
			}
			if ($_FILES["course_pdf"]["size"] > 2048000) {
				$pdf_error .= "File size should be less than 2 mB<br />";
			}
		}
		else{
			if($this->input->post('id') == ''){
				$pdf_error .= "Upload PDF is required<br />";
			}
		}

		$fids = $this->input->post('fid');
		$fee_part = $this->input->post('fee_part');
		$fee_date = $this->input->post('fee_date');

		for($count = 0; $count < sizeof($fee_part); $count++){
			if($fee_part[$count] == '' &&  $fee_date[$count] == ''){
			  	$payment_structure_error .= 'Amount and Due date field of Row: '.($count+1).' is mandatory.<br/>';
			}
			elseif($fee_part[$count] == '' &&  $fee_date[$count] != ''){
				$payment_structure_error .= 'Amount field of Row: '.($count+1).' is mandatory.<br/>';
			}
			elseif($fee_part[$count] != '' &&  $fee_date[$count] == ''){
				$payment_structure_error .= 'Due date field of Row: '.($count+1).' is mandatory.<br/>';
			  }
			  
			if($fee_date[$count] != ''){
				$exp_feedate = explode('/',$fee_date[$count]);
				$feedt = $exp_feedate[2].'-'.$exp_feedate[1].'-'.$exp_feedate[0];
				$form_feedate = date('Y-m-d',strtotime($feedt));
				$today = date('Y-m-d');
				if($this->input->post('id') != ''){
					if($fids[$count] != ''){
						$db_due_date = $this->Common_model->get_row('fee_structure',array('id' => $fids[$count]),'due_date');
						if($db_due_date > $today){
							if($form_feedate < $today){
								$payment_structure_error .= 'Due date field of Row: '.($count+1).' should not be less than today\'s date.<br/>';
							}
						}
						else{
							if($form_feedate < $db_due_date){
								$payment_structure_error .= 'Due date field of Row: '.($count+1).' should not be less than today\'s date.<br/>';
							}
						}
					}
					else{
						if($form_feedate < $today){
							$payment_structure_error .= 'Due date field of Row: '.($count+1).' should not be less than today\'s date.<br/>';
						}
					}
				}
				else{
					if($form_feedate < $today){
						$payment_structure_error .= 'Due date field of Row: '.($count+1).' should not be less than today\'s date.<br/>';
					}
				}
			}
		}

		if ($this->form_validation->run() == false || $file_error != '' || $pdf_error != '' || $payment_structure_error != '') {
			$arr['id'] = $this->input->post('id');
			$arr['category_id'] = $this->input->post('course_category');
			$arr['title'] = $this->input->post('course_title');
			$arr['duration'] = $this->input->post('duration');
			$arr['start_date'] = $this->input->post('start_date');
			$arr['end_date'] = $this->input->post('end_date');
			$arr['display_date'] = $this->input->post('display_date');
			$arr['details'] = $this->input->post('course_details');
			$arr['course_code'] = $this->input->post('course_code');
			$arr['last_reg_no'] = $this->input->post('last_reg_no');
			$arr['fee'] = $this->input->post('course_fee');
			$arr['fee_part'] = $this->input->post('fee_part');
			$arr['fee_date'] = $this->input->post('fee_date');
			$arr['fid'] = $this->input->post('fid');
			$arr['payment_structure_error'] = $payment_structure_error;
			$arr['tutors_assigned'] = ($this->input->post('tutors_assigned')) ? $this->input->post('tutors_assigned') : array();
			$arr['file_error'] = $file_error;
			$arr['pdf_error'] = $pdf_error;
			if( $this->input->post('id') != ''){
				$courseid =  $this->input->post('id');
				$course_det = $this->Common_model->get_row('courses', array('id' => $courseid), '');
				$arr['icon_name'] = $course_det->icon_name;
				$arr['course_pdf'] = $course_det->course_pdf;
			}
			else{
				$arr['icon_name'] = $arr['course_pdf'] = '';
			}
			$arr['fee_structures'] = '';
			$arr['page_title'] = 'Add Course';
			$arr['main_menu'] = 'courses';
			$arr['sub_menu1'] = 'add_course';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');	
			$this->load->view('admin/courses/add_course',$arr);
		} 
		else {
			$id = $this->input->post('id');
			$start_date = $this->input->post('start_date');
			$exploded_start_date = explode('/',$start_date);
			$startdate = date('Y-m-d',strtotime($exploded_start_date[2].'-'.$exploded_start_date[1].'-'.$exploded_start_date[0]));

			$end_date = $this->input->post('end_date');
			$exploded_end_date = explode('/',$end_date);
			$enddate = date('Y-m-d',strtotime($exploded_end_date[2].'-'.$exploded_end_date[1].'-'.$exploded_end_date[0]));

			$display_date = $this->input->post('display_date');
			$exploded_display_date = explode('/',$display_date);
			$displaydate = date('Y-m-d',strtotime($exploded_display_date[2].'-'.$exploded_display_date[1].'-'.$exploded_display_date[0]));


			$tutors_assigned = implode(',',$this->input->post('tutors_assigned'));
			$tutors_assigned_arr = $this->input->post('tutors_assigned');
			$removed_tutors = array();

			$save_data = array(
			'category_id' => $this->input->post('course_category'),
			'title' =>$this->input->post('course_title'),
			'duration' =>$this->input->post('duration'),
			'details' => $this->input->post('course_details'),
			'course_code' => strtoupper($this->input->post('course_code')),
			'last_reg_no' => $this->input->post('last_reg_no'),
			'tutors_assigned' => $tutors_assigned,
			'fee' => $this->input->post('course_fee'),
			'payment_structure' => sizeof($fee_part),
			'start_date' => $startdate,
			'end_date' => $enddate,
			'display_from' => $displaydate,
			'added_by' =>$this->session->userdata('user_id') ,
			'added_on' =>date('Y-m-d H:i:s'),
			);
			if( $this->input->post('id') != ''){
				$id = $this->input->post('id');
				$course_det = $this->Common_model->get_row('courses', array('id' => $id), '');

				$tut_assign = explode(',',$course_det->tutors_assigned);
				$removed_tutors = array_diff($tut_assign,$tutors_assigned_arr);

				$new_name = $id.'_'.rand();
				$file_config = array(
					'upload_path' => "uploads/courses/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000"
				);

				$pdf_config = array(
					'upload_path' => "uploads/courses/pdf/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);

				if ($_FILES['course_icon']['name'] != '') {
					$save_data['icon_name'] = $pic_name = $this->upload_file($file_config, 'uploads/courses/', 'course_icon', FALSE);
					$this->create_thumbs($pic_name);
					if ($course_det->icon_name != '') {
						if(file_exists('uploads/courses/'.$course_det->icon_name))
							unlink('uploads/courses/' . $course_det->icon_name);
						if(file_exists('uploads/courses/big/'.$course_det->icon_name))
							unlink('uploads/courses/big/' . $course_det->icon_name);
						if(file_exists('uploads/courses/medium/'.$course_det->icon_name))
							unlink('uploads/courses/medium/' . $course_det->icon_name);
						if(file_exists('uploads/courses/small/'.$course_det->icon_name))
							unlink('uploads/courses/small/' . $course_det->icon_name);
					}
				} else
					$save_data['icon_name'] = $course_det->icon_name;

				if ($_FILES['course_pdf']['name'] != '') {
					$save_data['course_pdf'] = $this->upload_file($pdf_config, 'uploads/courses/pdf/', 'course_pdf', FALSE);
					if ($course_det->course_pdf != '') {
						unlink('uploads/courses/pdf/' . $course_det->course_pdf);
					}
				} else
					$save_data['course_pdf'] = $course_det->course_pdf;

				$this->Common_model->update_with_array('courses',array('id'=>$id),$save_data);
				$this->session->set_flashdata('msg','Details of course has been updated.');
				$this->session->set_flashdata('action', 'alert-success');
				//user log
					$update_date = date('Y-m-d H:i:s');
					$update_user =$this->session->userdata('user_id');
					$user_name = $this->session->userdata('user_name');
					$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'courses',
					'table_id' => $id,
					'description' => 'Details of course id #'.$id.' updated',
					'operation' => 'Update',
					'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);

			}
			else{
				$save_data['status'] = 'A';
				$id = $this->Common_model->insert('courses',$save_data);
				$new_name = $id.'_'.rand();
				$file_config = array(
					'upload_path' => "uploads/courses/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" 
				);
			
				$pdf_config = array(
					'upload_path' => "uploads/courses/pdf/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);

				if ($_FILES['course_icon']['name'] != '') {
					$file_name = $this->upload_file($file_config, 'uploads/courses/', 'course_icon', FALSE);
					$this->create_thumbs($file_name);
					$this->Common_model->update_with_array('courses',array('id'=>$id),array('icon_name' => $file_name));
				} 

				if ($_FILES['course_pdf']['name'] != '') {
					$pdf_name = $this->upload_file($pdf_config, 'uploads/courses/pdf/', 'course_pdf', FALSE);
					$this->Common_model->update_with_array('courses',array('id'=>$id),array('course_pdf' => $pdf_name));
				} 

				$this->session->set_flashdata('msg', 'Details of course has been inserted');
				$this->session->set_flashdata('action', 'alert-success');
					//user log
					$update_date = date('Y-m-d H:i:s');
					$update_user =$this->session->userdata('user_id');
					$user_name = $this->session->userdata('user_name');
					$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'courses',
					'table_id' => $id,
					'description' => 'Details of course id #'.$id.' inserted',
					'operation' => 'Insert',
					'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);
			}

			$updated_ids = '';
			$updated_ids_arr = array();
	
			//already existing fee structure ids
			$fee_structure_dets = $this->Common_model->get_all_rows('fee_structure', array('course_id' => $id), array('id' => 'ASC'), 'id', array(), array(), array());
			$fee_structure_dets_id_arr = array();
			if($fee_structure_dets != ''){
			  $fee_structure_dets_id_arr = $this->Common_model->array_column($fee_structure_dets, 'id');
			}
			
			for($i = 0; $i < sizeof($fee_part); $i++){
			  if($fee_date[$i] != ''){
				$sdate_exp = explode('/',$fee_date[$i]);
				$feedate = $sdate_exp[2].'-'.$sdate_exp[1].'-'.$sdate_exp[0];
			  }
			  else{
				$feedate = '0000-00-00';
			  }
			  
	
			  $data_to_save = array(
				'course_id' => $id,
				'amount' => $fee_part[$i],
				'due_date' => $feedate,
				'added_by' =>$this->session->userdata('user_id') ,
				'added_on' =>date('Y-m-d H:i:s'),
			  );
	
			  if($fids[$i] != ''){
				  $this->Common_model->update_with_array('fee_structure',array('id'=>$fids[$i]),$data_to_save);
				  array_push($updated_ids_arr,$fids[$i]);
				  if($updated_ids == '')
					  $updated_ids .= $fids[$i];
				  else
					  $updated_ids .= ', '.$fids[$i];
			  }
			  else{
				  $insert_id = $this->Common_model->insert('fee_structure',$data_to_save);
				  if($updated_ids == '')
					  $updated_ids .= $insert_id;
				  else
					  $updated_ids .= ', '.$insert_id;
			  }
			}
			
			//to delete removed fee structure details from table
			$to_delete = array_diff($fee_structure_dets_id_arr,$updated_ids_arr);
			if(!empty($to_delete)){
			  foreach($to_delete as $each){
				$this->Common_model->delete('fee_structure',array('id'=>$each));
			  }
			}

			//to add course id to tutors table
			if(!empty($tutors_assigned_arr)){
				foreach($tutors_assigned_arr as $each){
					$course_assigned_list = $this->Common_model->get_row('tutors',array('id' => $each),'course_assigned');
					$course_arr = array();
					if($course_assigned_list){
						$course_arr = explode(',',$course_assigned_list);
					}
					if(!in_array($id,$course_arr)){
						array_push($course_arr,$id);
						$this->Common_model->update_with_array('tutors',array('id'=>$each),array('course_assigned' => implode(',',$course_arr)));
					}
				}
			}

			//to remove course id from tutors table of removed tutors
			if(!empty($removed_tutors)){
				foreach($removed_tutors as $each){
					$rcourse_assigned_list = $this->Common_model->get_row('tutors',array('id' => $each),'course_assigned');
					$rcourse_arr = array();
					if($rcourse_assigned_list){
						$rcourse_arr = explode(',',$rcourse_assigned_list);
					}
					if(in_array($id,$rcourse_arr)){
						$course_index = array_search($id,$rcourse_arr);
						unset($rcourse_arr[$course_index]);
						$this->Common_model->update_with_array('tutors',array('id'=>$each),array('course_assigned' => implode(',',$rcourse_arr)));
					}
				}
			}

			redirect('admin/courses/list_courses');
		}

	}

	//function to list courses
	function list_courses(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
	
		$arr['page_title'] = 'List Courses';
		$arr['main_menu'] = 'courses';
		$arr['sub_menu1'] = 'list_courses';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['courses']=$this->Common_model->get_all_rows('courses');		
		$this->load->view('admin/courses/list_courses',$arr);
	}

	function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
	{
		$this->upload->initialize($config);
		if ($this->upload->do_upload($file_name)) {
			$upload_data = $this->upload->data();
			$file_path = explode($destination, $upload_data['full_path']);
			$imagename = $filename = $file_path[1];
			if ($resize == FALSE){
				return $filename;
			}
			elseif ($resize == TRUE) {
				// settings for resize image

				$config['source_image'] = $upload_data['full_path'];
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				if (file_exists($upload_data['full_path'])) {
					unlink($upload_data['full_path']);//after resize delete orginal image
				}
				$filename = explode('.', $filename);
				$image_thumb = $filename[0] . '_thumb.' . $filename[1];
				rename($destination . $image_thumb, $destination . $imagename);
				return $imagename;
			}

		}
		else{
			print_r($this->upload->display_errors());exit;
		}
	}

	function create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // large Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/courses/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 781,
				'height'        => 450,
				'quality' 		=> '100',
                'new_image'     => './uploads/courses/big/'.$file_name
                ),
            // Medium Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/courses/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 360,
				'height'        => 245,
				'quality' 		=> '100',
                'new_image'     => './uploads/courses/medium/'.$file_name
                ),
            //thumb image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/courses/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 100,
				'height'        => 80,
				'quality' 		=> '100',
                'new_image'     => './uploads/courses/small/'.$file_name
                ));
           
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
	}
	
	//function to view course details
	function view_course(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$course_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$course_details = $this->Common_model->get_row('courses',array('id' =>$id));
			if($course_details)			
			{
				$arr['id'] = $course_details->id;
				$arr['category_id'] = $course_details->category_id;	
				$arr['category'] = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');	
				$arr['title'] = $course_details->title;	
				$arr['duration'] = $course_details->duration;	
				$arr['start_date'] = date('d/m/Y',strtotime($course_details->start_date));
				$arr['end_date'] = date('d/m/Y',strtotime($course_details->end_date));	
				$arr['icon_name'] = $course_details->icon_name;	
				$arr['course_pdf'] = $course_details->course_pdf;	
				$arr['details'] = $course_details->details;	
				$arr['fee'] = $course_details->fee;		
				$arr['course_code'] = $course_details->course_code;		
				$arr['payment_structure'] = $course_details->payment_structure;	
				$tutors_assigned = $course_details->tutors_assigned;
				if($tutors_assigned != ''){
					$tutors_arr = explode(',',$tutors_assigned);
				}
				else{
					$tutors_arr = array();
				}
				$arr['tutors_assigned'] = $tutors_arr;
				$students_enrolled = $course_details->students_enrolled;
				if($students_enrolled != ''){
					$students_arr = explode(',',$students_enrolled);
				}
				else{
					$students_arr = array();
				}
				$arr['students_enrolled'] = $students_arr;					
			}

		}
		$arr['page_title'] = 'View Course';
		$arr['main_menu'] = 'courses';
		$arr['sub_menu1'] = 'list_courses';
		$this->load->view('admin/courses/view_course',$arr);
	}

	//function to suspend course
	function suspend_course(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$course_details = $this->Common_model->get_row('courses',array('id' =>$id));
			if($course_details)			
			{
				//check if any class has been scheduled for future
				$class_schedules = $this->Common_model->course_class_exist($id);
				if($class_schedules){
					$this->session->set_flashdata('msg', 'Active class schedules exist for this course. Remove or inactivate the same before suspending the course.');
					$this->session->set_flashdata('action', 'alert-danger');
					redirect('admin/courses/list_courses');
				}
				else{
					$update_date = date('Y-m-d H:i:s');
					$update_user =$this->session->userdata('user_id');
					$data_to_save = array(
						'status' => 'I',
						'added_by' =>$this->session->userdata('user_id') ,
						'added_on' =>$update_date
					);
					$this->Common_model->update_with_array('courses',array('id'=>$id),$data_to_save);
					$this->session->set_flashdata('msg', 'Course has been suspended');
					$this->session->set_flashdata('action', 'alert-success');

					//user log
					$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'courses',
					'table_id' => $id,
					'description' => 'Course with id #'.$id.' has been suspended',
					'operation' => 'Suspended',
					'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);

					redirect('admin/courses/list_courses');
				}
			}

		}
	}

	//function to activate course
	function activate_course(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$course_details = $this->Common_model->get_row('courses',array('id' =>$id));
			if($course_details)			
			{
				$update_date = date('Y-m-d H:i:s');
				$data_to_save = array(
					'status' => 'A',
					'added_by' =>$this->session->userdata('user_id') ,
					'added_on' => $update_date
				);
				$this->Common_model->update_with_array('courses',array('id'=>$id),$data_to_save);
				$this->session->set_flashdata('msg', 'Course has been activated');
				$this->session->set_flashdata('action', 'alert-success');

				//user log
				$data_to_log = array(
					'user_id' => $this->session->userdata('user_id'),
					'table_name' => 'courses',
					'table_id' => $id,
					'description' => 'Course with id #'.$id.' has been activated',
					'operation' => 'Activated',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('admin/courses/list_courses');
			}

		}
	}

	//function to list tutors
	function tutor_list(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$tutors_exploded = array();
		$course_title = '';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0){
				$course_title =  $this->Common_model->get_row('courses',array('id' =>$id),'title');
				$tutor_details = $this->Common_model->get_row('courses',array('id' =>$id),'tutors_assigned');
			}
			if($tutor_details)			
			{
				$tutors_exploded = explode(',',$tutor_details);
			}
		}
		$arr['page_title'] = 'Tutor List';
		$arr['main_menu'] = 'courses';
		$arr['sub_menu1'] = 'list_courses';
		$arr['tutors']=$tutors_exploded;
		$arr['course_title'] = $course_title;	
		$this->load->view('admin/courses/tutor_list',$arr);
	}

	//function to list students
	function student_list(){
		if (!in_array('courses_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$students_exploded = array();
		$course_title = $id = '';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0){
				$course_title =  $this->Common_model->get_row('courses',array('id' =>$id),'title');
				$students_details = $this->Common_model->get_row('courses',array('id' =>$id),'students_enrolled');
			}
			if($students_details)			
			{
				$students_exploded = explode(',',$students_details);
			}
		}
		$arr['page_title'] = 'Student List';
		$arr['main_menu'] = 'courses';
		$arr['sub_menu1'] = 'list_courses';
		$arr['students']=$students_exploded;
		$arr['course_title'] = $course_title;	
		$arr['course_id'] = $id;	
		$this->load->view('admin/courses/student_list',$arr);
	}

}


