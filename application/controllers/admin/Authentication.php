<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');
class Authentication extends CI_Controller
{

  var $permissions = array();
  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('permission');
    $this->load->model('Common_model');
    date_default_timezone_set("Asia/Kolkata");
  }
  
  public function index()
  {
         /* If user not logged in then rediect to login page else rediresct to dashboard*/
    if ($this->session->userdata('is_ilama_admin_login')) {
      $user_group = $this->session->userdata('group_id');
      redirect('admin/dashboard');
    } //$this->session->userdata('is_admin_login')
    else {
      $arr['msg'] = $this->session->flashdata('msg');
      $arr['action'] = $this->session->flashdata('action');
      $this->load->view('admin/authentication/login',$arr);
    }
  }
    /*--------------------------------------------------
     authentication check function
     ------------------
     Function : Public
     do_login function used to validate user and based 
     on their user type set permissions to access dashboard.
     Variables : User name and Password
     Libraries : Form Validation
     Model :  -
     
     --------------------------------------------------*/
     public function auth_check()
     {
   
       $user = ($this->input->post('user_name', true) != '') ? $this->input->post('user_name', true) : '';
       $password = ($this->input->post('password', true) != '') ? $this->input->post('password', true) : '';
       
       if ($this->form_validation->run() == false) {
                    // if page have some error then redirect to login page with error message
         $err['error'] = '<strong>Access Denied</strong> Invalid Username/Password';
   
         $this->load->view('admin/authentication/login');
       } //$this->form_validation->run() == FALSE
       else {
                    // Check the username and password are exist in the db 
         $enc_pass = sha1(PASS_SALT . $password);
         $sql = "SELECT * FROM users WHERE (user_name = '$user' OR email = '$user') and password = '$enc_pass' and status='A' and group_id!='3' and group_id!='4' ";
         $val = $this->db->query($sql);
         if ($val->num_rows() > 0) {
   
           $user_data = $val->row();
           $this->session->set_userdata(array(
             'user_id' => $user_data->id,
             'user_name' => $user_data->user_name,
             'name' => $user_data->name,
             'group_id' => $user_data->group_id,            
             'is_ilama_admin_login' => true,
           ));

           $ip = $this->input->ip_address();

            $getloc = json_decode(file_get_contents("http://ipinfo.io/"));
            

            $info= $ip.'~'.$getloc->ip.'~'.$getloc->city;

        

           //user log
           $update_date = date('Y-m-d H:i:s');
           $update_user =$this->session->userdata('user_id');
           $user_name = $this->session->userdata('user_name');
           $data_to_log = array(
             'user_id' => $update_user,
             'table_name' => 'users',
             'table_id' => $update_user,
             'description' => 'The user ' . $user_name . ' logined successfully at ' . $update_date,
             'operation' => 'Sign In',
             'info' =>$info,
             'updated_date' => $update_date
           );
           $this->Common_model->set_user_log($data_to_log);
           redirect('admin/dashboard'); // redirect to dashboard
         } //$val->num_rows
         else {
          // echo 'Hi';exit;           //if user not exist then redirect to login page with error message
           $err['error'] = '<strong>Access Denied</strong> Invalid Username/Password';
           $this->load->view('admin/authentication/login', $err);
         }
       }
             // }
     }
        /*--------------------------------------------------
        logout function
        ------------------
        Function : Public
        Remove all varibles from session
        
        --------------------------------------------------*/
     public function logout()
     {
       $msg = 'Logout successfull';

        $ip = $this->input->ip_address();

            $getloc = json_decode(file_get_contents("http://ipinfo.io/"));
            

            $info= $ip.'~'.$getloc->ip.'~'.$getloc->city;

        //user log
        $update_date = date('Y-m-d H:i:s');
        $user_name = $this->session->userdata('user_name');
        $update_user =$this->session->userdata('user_id');
        if($update_user != ''){
            $data_to_log = array(
              'user_id' => $update_user,
              'table_name' => 'users',
              'table_id' => $update_user,
              'description' => 'The user ' . $user_name . ' logged out successfully at ' . $update_date,
              'operation' => 'Sign Out',
              'info' =>$info,
              'updated_date' => $update_date
            );
            $this->Common_model->set_user_log($data_to_log);
        }
   
       $array_items = array('user_id' => '', 'user_name' => '', 'name' => '', 'group_id' => '', 'is_ilama_admin_login' => '');
       $this->session->unset_userdata($array_items);
       $this->session->unset_userdata('user_id');
       $this->session->unset_userdata('user_name');
       $this->session->unset_userdata('group_id');
       $this->session->unset_userdata('name');
     
       $this->session->unset_userdata('is_ilama_admin_login');
       $this->session->sess_destroy();
       $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
       $this->output->set_header("Pragma: no-cache");
       redirect('admin/authentication', 'refresh');
     }

     public function forgot_password(){
      
      $data['page_title'] ='Forgot Password';
      $data['menu'] ='';
      if ($this->session->userdata('is_ilama_admin_login'))
      {
        redirect('admin/dashboard');
      } 
      else
      {
        $this->load->view('admin/authentication/forgot_password',$data);
        
      }
    }
  
    public function forgot_password_mail(){
      
      $data['page_title'] ='Forgot Password';
      $data['menu'] ='';
      if ($this->session->userdata('is_ilama_admin_login'))
      {
        redirect('admin/dashboard');
      } 
      else
      {
        if(isset($_POST['email']))
          $email     = $_POST['email'];
        else
          $email = '';
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
        $this->form_validation->set_rules('email', 'Email', 'required|is_email_exist[users.email]'); 
        if ($this->form_validation->run() == FALSE)
        {
          $data['page_title'] ='Forgot Password';
          $data['menu'] ='';
          // $this->session->set_flashdata('msg','Message delivered');
          // $this->session->set_flashdata('action', 'alert-success');
          // $data['msg']=$this->session->flashdata('msg');
          // $data['action'] = $this->session->flashdata('action');
          $this->load->view('admin/authentication/forgot_password',$data);
        }
        else{
          $to =$email;
          $user_details = $this->Common_model->get_row('users',array('email'=>$to));
          $user_id = $user_details->id;
          
          $full_name =($user_details->name !='') ? $user_details->name : '';
          $subject = "Reset Password of Ilama e-Learning Account ";
          $message = '';
          $message .= $this->load->view('email_template/email_header',array('email_title' =>$subject),true);
          $message .=' <tr>
							<td class="email-body" width="100%">
							  <table class="email-body_inner" align="center" width="570" cellpadding="0" cellspacing="0">
								<!-- Body content -->
								<tr>
								  <td class="content-cell">
									<h1>Dear '.$full_name.',</h1>
									<p>We have received a request to reset the password of your Ilama e-Learning account. Please click on the button below to reset your password</p>
									<!-- Action -->
									<table class="body-action" align="center" width="100%" cellpadding="0" cellspacing="0">
									  <tr>
										<td align="center">
										  <div>
											<a href="'.base_url().'admin/authentication/reset_password/'.urlencode(base64_encode($user_id .'_'.ENCRYPTION_KEY)).'" class="button button--blue" style="color:#fff;">Reset Password</a>
										  </div>
										</td>
									  </tr>
									</table>
									<p>Thanks,<br>The Ilama e-Learning Team</p>
									<!-- Sub copy -->
									<table class="body-sub">
									  <tr>
										<td>
										  <p class="sub">Please ignore this mail if you did not request to reset your account password.
										  </p>
										</td>
									  </tr>
									</table>
								  </td>
								</tr>
							  </table>
							</td>
						  </tr>';		
          $message .= $this->load->view('email_template/email_footer',array(),true);
         echo  $message ;exit;
          $this->load->library('email');
          $this->email->to($to);
          $this->email->from('contact@ilamaelearning.com', 'iLAMA eLearning');  // email From
          $this->email->subject($subject);  // Subject
          $this->email->message($message); 
          //if($this->email->send() > 0){
            $this->session->set_flashdata('msg','An email has been sent to <b>'.$to.'</b>. Please check your inbox to reset the password.');
            $this->session->set_flashdata('action', 'alert-success');
            redirect('admin/authentication/forgot_password');
        //  }
          
        }
      }
    }
    public function reset_password($user_id=''){
      $urldecoded = urldecode($user_id);
      $decodedid = base64_decode($urldecoded);
      $decodedid_exp = explode('_',$decodedid);
      $decoded_id = isset($decodedid_exp[0]) ? $decodedid_exp[0] : '';
      $data['page_title'] ='Reset Password';
      $data['menu'] ='';
      $data['encoded_id'] = $user_id;
      
      if ($this->session->userdata('is_ilama_admin_login'))
      {
        redirect('admin/dashboard');
      }  
        else
      {
        if($decoded_id > 0){
          $user_details = $this->Common_model->get_row('users',array('id'=>$decoded_id));
          if($user_details)
          { 
            $id = $user_details->id;
            $this->load->view('admin/authentication/reset_password',$data);
          }
          else{
            redirect('admin/authentication/login');
          }
        }
        else{
          redirect('admin/authentication/login');
        }
      }
     }
  
 public function reset_password_confirm(){
   $data_login['page_title'] ='Reset Password | Apollo Competitions';
   $data_login['menu'] ='';
   if ($this->session->userdata('is_ilama_admin_login'))
   {
     redirect('admin/dashboard');
   }  
   else
   {
     $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
     $this->form_validation->set_rules('password', 'Password', 'required|max_length[200]|min_length[4]');
     $this->form_validation->set_rules('confirm_password', 'Confirm Password', 'required|max_length[200]|min_length[4]|matches[password]');
     if ($this->form_validation->run() == FALSE)
     {
       
       $data['encoded_id'] = $this->input->post('encoded_id');
       $data['page_title'] ='Reset Password';
       $data['menu'] ='';
       $this->load->view('admin/authentication/reset_password',$data);
     }
     else{
       $urldecoded = urldecode($this->input->post('encoded_id'));
       $decodedid = base64_decode($urldecoded);
       $decodedid_exp = explode('_',$decodedid);
       $id = isset($decodedid_exp[0]) ? $decodedid_exp[0] : '';
       if($id >0){
         $password = sha1(PASS_SALT.$this->input->post('password'));
         $this->Common_model->update_with_array('users',array('id'=>$id ),array('password' =>$password ));
         $this->session->set_flashdata('msg','Password reset successfully. Please login');
         $this->session->set_flashdata('action', 'alert-success');
         redirect('admin/authentication');
       }
     }
   }
  
 }
  
}