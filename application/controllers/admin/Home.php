<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');
class Home extends CI_Controller
{

  var $permissions = array();
  public function __construct()
  {
    parent::__construct();
    $this->load->library('form_validation');
    $this->load->library('permission');
    $this->load->model('Common_model');
    date_default_timezone_set("Asia/Kolkata");
    if (!$this->session->userdata('is_ilama_admin_login'))
    {
      redirect('admin/authentication');
    }
    else{
      redirect('admin/dashboard');
    } 
  }
     /*--------------------------------------------------
     index function
     ------------------
     Function : Public
     This is the default page of software.
     User logged in then redirect to dashboard else go to login panel
     Variables : User name and Password
     Libraries : Form Validation
     Model :  -
     
     --------------------------------------------------*/
  public function index()
  {
         /* If user not logged in then rediect to login page else rediresct to dashboard*/
    if ($this->session->userdata('is_ilama_admin_login')) {
     
      redirect('admin/dashboard');
    } //$this->session->userdata('is_admin_login')
    else {
      $this->load->view('admin/authentication');
    }
  }
     /*--------------------------------------------------
     do_login function
     ------------------
     Function : Public
     do_login function used to validate user and based 
     on their user type set permissions to access dashboard.
     Variables : User name and Password
     Libraries : Form Validation
     Model :  -
     
     --------------------------------------------------*/
  
}
?>