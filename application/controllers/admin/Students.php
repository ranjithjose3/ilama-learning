<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Students extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
        date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
	}
	

	//function to list students
	function list_students(){
		if (!in_array('students_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
	
		$arr['page_title'] = 'List Students';
		$arr['main_menu'] = 'students';
		$arr['sub_menu1'] = 'list_students';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['students']=$this->Common_model->get_email_verified_students();		
		$this->load->view('admin/students/list_students',$arr);
	}

	//function to view student details
	function view_student(){
		if (!in_array('students_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$student_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$student_details = $this->Common_model->get_row('students',array('id' =>$id));
			if($student_details)			
			{
				$arr['id'] = $student_details->id;
				$arr['name'] = $student_details->name;		
				$arr['address'] = $student_details->address;	
				$arr['mobile'] = $student_details->mobile;
				$arr['email'] = $student_details->email;
				$arr['parent_name'] = $student_details->parent_name;
				$arr['profile_pic'] = $student_details->profile_pic;
				$arr['parent_email'] = $student_details->parent_email;
				$arr['active_class'] = $this->Common_model->get_active_enrolled_course($student_details->id);
				$arr['enrolled_courses'] = $this->Common_model->get_all_rows('students_enrolled',array('student_id' => $student_details->id),array('id' => 'DESC'),'course_id');
				$arr['student_uname'] = $this->Common_model->get_row('users',array('id' => $student_details->user_id),'user_name');
			}

		}
		$arr['page_title'] = 'View Student';
		$arr['main_menu'] = 'students';
		$arr['sub_menu1'] = 'list_students';
		$this->load->view('admin/students/view_student',$arr);
	}

	//function to admit student
	function admit_student(){
		if (!in_array('students_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$updated_date = date('Y-m-d H:i:s');
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$details = $this->Common_model->get_row('students_enrolled',array('id' =>$id),'');
			if($details)			
			{
				//save data to student_enrolled table
				$data_to_save = array(
					'status' => 'A',
					'admitted_date' => $updated_date,
					'updated_datetime' => $updated_date
				);
				$this->Common_model->update_with_array('students_enrolled',array('id'=>$id),$data_to_save);

				//update to courses table
				$course_details = $this->Common_model->get_row('courses',array('id' => $details->course_id),'students_enrolled');
				if($course_details != ''){
					$students_enrolled = $course_details.','.$details->student_id;
				}
				else{
					$students_enrolled = $details->student_id;
				}

				$this->Common_model->update_with_array('courses',array('id'=>$details->course_id),array('students_enrolled' => $students_enrolled));

				//update to students table
				$this->Common_model->update_with_array('students',array('id'=>$details->student_id),array('enrolled_courses' => $details->course_id));

				$this->session->set_flashdata('msg', 'Student has been admitted to the course');
				$this->session->set_flashdata('action', 'alert-success');

				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'students_enrolled',
					'table_id' => $id,
					'description' => 'Student with id #'.$details->student_id.' has been admitted to the course '.$details->course_id,
					'operation' => 'Admitted',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('admin/students/list_students');
			
				
			}

		}
	}

	//function to suspend student
	function suspend_student(){
		if (!in_array('students_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$updated_date = date('Y-m-d H:i:s');
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$details = $this->Common_model->get_row('students_enrolled',array('id' =>$id),'');
			if($details)			
			{
				//save data to student_enrolled table
				$data_to_save = array(
					'status' => 'I',
					'updated_datetime' => $updated_date
				);
				$this->Common_model->update_with_array('students_enrolled',array('id'=>$id),$data_to_save);

				//save data to students table
				$studentsdata_to_save = array(
					'status' => 'I'
				);
				$this->Common_model->update_with_array('students',array('id'=>$details->student_id),$studentsdata_to_save);

				$this->session->set_flashdata('msg', 'Student has been suspended from the course');
				$this->session->set_flashdata('action', 'alert-success');

				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'students_enrolled',
					'table_id' => $id,
					'description' => 'Student with id #'.$details->student_id.' has been suspended from the course '.$details->course_id,
					'operation' => 'Suspended',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('admin/students/list_students');
			
				
			}

		}
	}

	//function to readmit student
	function readmit_student(){
		if (!in_array('students_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$updated_date = date('Y-m-d H:i:s');
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$details = $this->Common_model->get_row('students_enrolled',array('id' =>$id),'');
			if($details)			
			{
				//save data to student_enrolled table
				$data_to_save = array(
					'status' => 'A',
					'updated_datetime' => $updated_date
				);
				$this->Common_model->update_with_array('students_enrolled',array('id'=>$id),$data_to_save);

				//save data to students table
				$studentsdata_to_save = array(
					'status' => 'A'
				);
				$this->Common_model->update_with_array('students',array('id'=>$details->student_id),$studentsdata_to_save);

				$this->session->set_flashdata('msg', 'Student has been readmitted to the course');
				$this->session->set_flashdata('action', 'alert-success');

				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'students_enrolled',
					'table_id' => $id,
					'description' => 'Student with id #'.$details->student_id.' has been readmitted to the course '.$details->course_id,
					'operation' => 'Re-admitted',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('admin/students/list_students');
			
				
			}

		}
	}

}


