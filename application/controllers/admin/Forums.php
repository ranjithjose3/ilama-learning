<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Forums extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
		$this->load->model('Forum_model');
        date_default_timezone_set("Asia/Kolkata");
        if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
		
	}


	function forum_view($course_id=null){

		if (!in_array('forum_management_admin', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }



		$encoded_id = ($course_id!='') ? urldecode($course_id) : '';

		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			

			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$course_details = $this->Common_model->get_row('courses',array('id' =>$id));
				
			


			if($course_details)			
			{
				$arr['id'] = $course_details->id;
				$arr['category_id'] = $course_details->category_id;	
				$arr['category'] = $this->Common_model->get_row('categories',array('id' => $course_details->category_id),'name');	
				$arr['title'] = $course_details->title;	
				$arr['duration'] = $course_details->duration;	
				$arr['start_date'] = date('d/m/Y',strtotime($course_details->start_date));
				$arr['end_date'] = date('d/m/Y',strtotime($course_details->end_date));	
				$arr['icon_name'] = $course_details->icon_name;	
				$arr['course_pdf'] = $course_details->course_pdf;	
				$arr['details'] = $course_details->details;	
				$arr['fee'] = $course_details->fee;		
				$arr['payment_structure'] = $course_details->payment_structure;	
				$tutors_assigned = $course_details->tutors_assigned;
				if($tutors_assigned != ''){
					$tutors_arr = explode(',',$tutors_assigned);
				}
				else{
					$tutors_arr = array();
				}
				$arr['tutors_assigned'] = $tutors_arr;
				$students_enrolled = $course_details->students_enrolled;
				if($students_enrolled != ''){
					$students_arr = explode(',',$students_enrolled);
				}
				else{
					$students_arr = array();
				}
				$arr['students_enrolled'] = $students_arr;	

				$arr['files'] = $this->Common_model->get_all_rows('forum_cmnts', array('course_id' => $course_details->id), array('updated_on' => 'ASC'), 'id,doc_name,doc_type,updated_on,comment', array(), array(), array());

				$arr['users_cmnts'] = $this->Forum_model->get_comments($id,null);


	
				$this->load->view('admin/forum/view_forum',$arr);



			}
			else{
				show_error('Course not exist!');
			}

		}
	}
	
	

}


