<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Accounts extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
        date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
	}
	
	//function to list students
	function students(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		if($this->input->post('mode')=='ajax'){
            if($this->input->post('course_id') != ''){
				$course_id = $this->input->post('course_id');
			}
			else{
				$course_id = '';
			}
			
			$search = array(
				'course_id' => $course_id
			);

			$print ='';

			if($course_id == ''){
				$print .=  '<p class="card-description">Sorry!! No details found.</p>';
			}
			else{
			
				$students = $this->Common_model->get_all_rows('students_enrolled', $search, array(), '', array(), array(), array());
           
				if($students){
					$print .= '<table id="example" class="display nowrap table table-bordered table-striped table-hover datatable datatable-Blood_group" style="width:100%">
						<thead>
							<tr>
								<th class="no-sort">#</th>
								<th>Name</th>
								<th>Student Id</th>
								<th>Course</th>
								<th>Total Fee</th>
								<th>Paid Fee</th>
								<th>Balance Fee</th>
								<th>Status</th>
								<th class="no-sort">Action</th>
								<th></th>
							</tr>
						</thead>
						<tbody>';
							$i=1;
							foreach($students as $each){
								$student_user_id = $this->Common_model->get_row('students',array('id' => $each['student_id']),'user_id');
									$print .= '<tr id="'.$each['id'].'">
												<td>'.$i++.'</td>
												<td>'.$this->Common_model->get_row('students',array('id' => $each['student_id']),'name').'</td>
												<td>'.$this->Common_model->get_row('users',array('id' => $student_user_id),'user_name').'</td>
												<td>'.$this->Common_model->get_row('courses',array('id' => $each['course_id']),'title').'</td>
												<td> Rs. '.$this->Common_model->get_row('courses',array('id' => $each['course_id']),'fee').'</td>
												<td> Rs. '.$each['paid_fee'].'</td>
												<td> Rs. '.$each['balance_fee'].'</td> 
												<td>';
												if($each['status'] == 'R'){
													$print .= 'Registered';
												}
												elseif($each['status'] == 'A'){
													$print .= 'Admitted';
												}
												else{
													$print .= 'Suspended';
												}
												$print .= '</td> 
												<td>';
													
												$print .= '<a title="View  Details" href="'.base_url().'admin/accounts/view_details?id='.urlencode(base64_encode($each['id']."_".ENCRYPTION_KEY)).'"><i class="fa fa-eye"></i></a>';
													
									$print .= '</td>
											<td></td> 
											</tr>';
							}
							
						$print .= '</tbody>
					</table>';
					} 
					else{
						$print .=  '<p class="card-description">Sorry!! No details found.</p>';
					}
					
			}
                 echo $print; 
        }
        else
        {  
			$arr = array(
				'course_id' => ''
			);
			
			$arr['page_title'] = 'Accounts - Students';
			$arr['main_menu'] = 'accounts';
			$arr['sub_menu1'] = 'students';
			$arr['students'] = '';
			$this->load->view('admin/accounts/students',$arr);
		}
	}

	//function to view student payment details
	function view_details(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		$msg = ($this->input->get('msg')!='') ? urldecode($this->input->get('msg')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$details = $this->Common_model->get_row('students_enrolled',array('id' =>$id));
			if($details)			
			{
				$status_arr = array('R' => 'Registered', 'A' => 'Admitted', 'I' => 'Suspended');
				$arr['id'] = $details->id;
				$arr['course_id'] = $details->course_id;	
				$arr['course'] = $this->Common_model->get_row('courses',array('id' => $details->course_id),'title');	
				$arr['student_id'] = $details->student_id;	
				$arr['name'] = $this->Common_model->get_row('students',array('id' => $details->student_id),'name');	
				$arr['student_detail'] = $this->Common_model->get_row('students',array('id' => $details->student_id),'');
				$arr['totalfee'] = $this->Common_model->get_row('courses',array('id' => $details->course_id),'fee');		
				$arr['paid_fee'] = $details->paid_fee;
				$arr['balance_fee'] = $details->balance_fee;
				$arr['fee_structure'] = $this->Common_model->get_all_rows('fee_structure',array('course_id' => $details->course_id),array('due_date' => 'ASC'));	
				if($details->payment_dates != ''){
					$arr['payment_dates'] = explode(',',$details->payment_dates);
				}
				else{
					$arr['payment_dates'] = array();
				}
				$arr['status'] = $status_arr[$details->status];
				
			}

		}
		$arr['page_title'] = 'View Account Details';
		$arr['main_menu'] = 'accounts';
		$arr['sub_menu1'] = 'students';
		$arr['msg'] = $msg;
		$this->load->view('admin/accounts/view_student_details',$arr);
	}

	function receive_payment(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$enroll_id = $this->input->post('enroll_id');
		$encoded_enroll_id = urlencode(base64_encode($enroll_id.'_'.ENCRYPTION_KEY));
		$structure_id = $this->input->post('structure_id');
		$part = $this->input->post('part');
        $details = $this->Common_model->get_row('fee_structure',array('id' =>$structure_id),''); 
        $message ='';
        if($details) {
            $message ='';
			$message .= '<div class="table-responsive">
				<table class="table table-bordered" style="table-layout:fixed;">
					<tr>
						<td>
							<input type="hidden" name="enroll_id" id="enroll_id" value="'.$enroll_id.'">
							<input type="hidden" name="encoded_enroll_id" id="encoded_enroll_id" value="'.$encoded_enroll_id.'">
							<input type="hidden" name="part" id="part" value="'.$part.'">
							<input type="hidden" name="amount" id="amount" value="'.$details->amount.'">
							<label>Amount: </label> Rs. '. $details->amount.' 
						</td>
					</tr>
					<tr>
						<td><label>Payment Type: <span class="text-red">*</span></label>
							<select name="payment_type" id="payment_type" class="form-control">
								<option value="">--Select--</option>
								<option value="Cheque">Cheque</option>
								<option value="DD">DD</option>
								<option value="Others">Others</option>
                    		</select>
            				<div class="text-red" id="payment_type_error"></div>
						</td>
					</tr>
					<tr>
						<td><label>Cheque/DD/Others No: <span class="text-red">*</span></label>
							<input name="remarks" id="remarks" class="form-control" value="">
							<div class="text-red" id="remarks_error"></div>
						</td>
					</tr>';
            $message .='</table></div>';
            
           echo $message;
        }   

	}

	function receive_payment_save(){
		$enroll_id = $this->input->post('enroll_id');
		$part = $this->input->post('part');
		$amount = $this->input->post('amount');
		$payment_type = $this->input->post('payment_type');
		$remarks = $this->input->post('remarks');
		$trans_date = date('Y-m-d H:i:s');

		$pay_type_arr = array('Cheque' => 'cheque_no', 'DD' => 'dd_no', 'Others' => 'others');
		//insert to payments table
		$save_to_payments = array(
			'enrollment_id' => $enroll_id,
			'payment_mode' => $payment_type,
			'transaction_id' => '',
			'transaction_status' => 'Success',
			'transaction_amount' => $amount,
			'transaction_date' => $trans_date,
			'acknowledge' => 'A',
			$pay_type_arr[$payment_type] => $remarks,
			'bank_name' => '',
			'updated_datetime' => $trans_date
		);

		$payment_id = $this->Common_model->insert('payments',$save_to_payments);

		if($payment_id > 0){
			//send email to student regarding the payment
			$student_id = $this->Common_model->get_row('students_enrolled',array('id' => $enroll_id),'student_id');
			$course_id = $this->Common_model->get_row('students_enrolled',array('id' => $enroll_id),'course_id');
			$to_email = $this->Common_model->get_row('students',array('id' => $student_id),'email');
			
			$subject = 'Payment Received';
			$email_data = array(
				'name' => strtoupper($this->Common_model->get_row('students',array('id' => $student_id),'name')),
				'course' => $this->Common_model->get_row('courses',array('id' => $course_id),'title'),
				'part_no' => $part,
				'amount' => 'Rs. '.$amount,
				'payment_type' => $payment_type,
				'payment_id' => $remarks,
				'trans_date' => date('d/m/Y',strtotime($trans_date))
			);
			$message = $this->load->view('email_template/payment_received',$email_data,true);

			$headers = "From: iLAMA eLearning <contact@ilamaelearning.com>\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to_email,$subject,$message,$headers);


			//insert to students_enrolled table
			$enroll_details = $this->Common_model->get_row('students_enrolled',array('id' => $enroll_id),'');

			if($enroll_details->payment_types != ''){
				$payment_types = $enroll_details->payment_types.','.$payment_type;
			}
			else{
				$payment_types = $payment_type;
			}

			if($enroll_details->payment_ids != ''){
				$payment_ids = $enroll_details->payment_ids.','.$payment_id;
			}
			else{
				$payment_ids = $payment_id;
			}

			if($enroll_details->payment_amount != ''){
				$payment_amount = $enroll_details->payment_amount.','.$amount;
			}
			else{
				$payment_amount = $amount;
			}


			if($enroll_details->payment_dates != ''){
				$payment_dates = $enroll_details->payment_dates.','.date('Y-m-d');
			}
			else{
				$payment_dates = date('Y-m-d');
			}

			$paid_fee = $enroll_details->paid_fee + $amount;
			$balance_fee = $enroll_details->balance_fee - $amount;

			$save_to_enroll = array(
				'payment_types' => $payment_types,
				'payment_ids' => $payment_ids,
				'payment_amount' => $payment_amount,
				'payment_dates' => $payment_dates,
				'paid_fee' => $paid_fee,
				'balance_fee' => $balance_fee,
				'updated_datetime' => $trans_date
			);

			$this->Common_model->update_with_array('students_enrolled',array('id' => $enroll_id),$save_to_enroll);

			//user log to payments table
			$update_date = date('Y-m-d H:i:s');
			$update_user = $this->session->userdata('user_id');
			$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'payments',
				'table_id' => $payment_id,
				'description' => 'Payment #id '.$enroll_id.' has been made',
				'operation' => 'Payment Received',
				'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);

			echo '1';

		}

		
	}

	function send_reminder(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$enroll_id = $this->input->post('enroll_id');
		$encoded_enroll_id = urlencode(base64_encode($enroll_id.'_'.ENCRYPTION_KEY));
		$structure_id = $this->input->post('structure_id');
		$part = $this->input->post('part');
		$details = $this->Common_model->get_row('fee_structure',array('id' =>$structure_id),''); 
		$student_id = $this->Common_model->get_row('students_enrolled',array('id' => $enroll_id),'student_id');
		$course_id = $this->Common_model->get_row('students_enrolled',array('id' => $enroll_id),'course_id');
        $message ='';
        if($student_id){
			$send_msg_at = date('Y-m-d H:i:s');
	
			$to_email = $this->Common_model->get_row('students',array('id' => $student_id),'email');
			
			$subject = 'Payment Reminder';
			$email_data = array(
				'name' => strtoupper($this->Common_model->get_row('students',array('id' => $student_id),'name')),
				'course' => $this->Common_model->get_row('courses',array('id' => $course_id),'title'),
				'part_no' => $part,
				'amount' => 'Rs. '.$details->amount,
				'due_date' => date('d/m/Y',strtotime($details->due_date))
			);
			$message = $this->load->view('email_template/send_reminder',$email_data,true);

			$headers = "From: iLAMA eLearning <contact@ilamaelearning.com>\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
			mail($to_email,$subject,$message,$headers);

			//user log
			$update_date = date('Y-m-d H:i:s');
			$update_user = $this->session->userdata('user_id');
			$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'students',
				'table_id' => $student_id,
				'description' => 'Reminder has been send to student #id '.$student_id,
				'operation' => 'Send Reminder',
				'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);
			
			echo json_encode(array('1',$encoded_enroll_id));
		}
	}

	//function to list tutors
	function tutors(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		if($this->input->post('mode')=='ajax'){
            if($this->input->post('course_id') != ''){
				$course_id = $this->input->post('course_id');
			}
			else{
				$course_id = '';
			}
			
			$search = array(
				'id' => $course_id
			);

			$print ='';

			if($course_id == ''){
				$print .=  '<p class="card-description">Sorry!! No details found.</p>';
			}
			else{
			
				$tutor_list = $this->Common_model->get_row('courses', $search, 'tutors_assigned');
				if($tutor_list != '')
					$tutors = explode(',',$tutor_list);
				else
					$tutors = array();
           
				if(!empty($tutors)){
					$print .= '<table id="example" class="display nowrap table table-bordered table-striped table-hover datatable datatable-Blood_group" style="width:100%">
						<thead>
							<tr>
								<th class="no-sort">#</th>
								<th>Name</th>
								<th>Course</th>
								<th>Total Payment</th>
								<th>Paid Amount</th>
								<th>Balance</th>
								<th>Status</th>
								<th class="no-sort">Action</th>
								<th></th>
							</tr>
						</thead>
						<tbody>';
							$i=1;
							foreach($tutors as $row){
								$each = $this->Common_model->get_row('tutors',array('id' => $row),'');
								$payment_details = $this->Common_model->get_row('staff_payment',array('tutor_id' => $row, 'course_id' => $course_id),'');
									$print .= '<tr>
												<td>'.$i++.'</td>
												<td>'.$each->name_title.' '.strtoupper($each->name).'</td>
												<td>'.$this->Common_model->get_row('courses',array('id' => $course_id),'title').'</td>
												<td>';
													if($payment_details && isset($payment_details->total))
															$print .= 'Rs. '.$payment_details->total;
												$print .= '</td>
												<td>';
													if($payment_details && isset($payment_details->total_released))
															$print .= 'Rs. '.$payment_details->total_released;
												$print .= '</td>
												<td>';
													if($payment_details && isset($payment_details->balance))
															$print .= 'Rs. '.$payment_details->balance;
												$print .= '</td> 
												<td>';
												if($each->status == 'A'){
													$print .= 'Active';
												}
												else{
													$print .= 'Suspended';
												}
												$print .= '</td> 
												<td>';
													
												$print .= '<a title="View  Details" href="'.base_url().'admin/accounts/view_tutor_details?id='.urlencode(base64_encode($row."_".ENCRYPTION_KEY)).'&cid='.urlencode(base64_encode($course_id."_".ENCRYPTION_KEY)).'"><i class="fa fa-eye"></i></a>';
													
									$print .= '</td>
											<td></td> 
											</tr>';
							}
							
						$print .= '</tbody>
					</table>';
				} 
				else{
					$print .=  '<p class="card-description">Sorry!! No details found.</p>';
				}
					
			}
                 echo $print; 
        }
        else
        {  
			$arr = array(
				'course_id' => ''
			);
			
			$arr['page_title'] = 'Accounts - Tutors';
			$arr['main_menu'] = 'accounts';
			$arr['sub_menu1'] = 'tutors';
			$arr['tutors'] = '';
			$this->load->view('admin/accounts/tutors',$arr);
		}
	}

	//function to view tutor payment details
	function view_tutor_details(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		$encoded_course_id = ($this->input->get('cid')!='') ? urldecode($this->input->get('cid')) : '';
		$msg = ($this->input->get('msg')!='') ? urldecode($this->input->get('msg')) : '';
		$action = ($this->input->get('action')!='') ? urldecode($this->input->get('action')) : 'alert-success';
		if($encoded_id !='' && $encoded_course_id != ''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';

			$enCId = base64_decode($encoded_course_id);
			$enCIdSplit = explode('_',$enCId);
			$course_id = isset($enCIdSplit[0]) ? $enCIdSplit[0] : '';

			if($id > 0)
				$details = $this->Common_model->get_row('tutors',array('id' =>$id),'');
			if($details)			
			{
				$status_arr = array('A' => 'Active', 'I' => 'Suspended');
				$staff_payment = $this->Common_model->get_row('staff_payment',array('tutor_id' => $id, 'course_id' => $course_id),'');

				$arr['tutor_id'] = $details->id;
				$arr['course_id'] = $course_id;	
				$arr['course'] = $this->Common_model->get_row('courses',array('id' => $course_id),'title');	
				$arr['name'] = $details->name_title.' '.$details->name;	
				$arr['mobile'] = $details->phone_number;
				$arr['profile_pic'] = $details->profile_pic;
				$arr['staff_payment'] = $staff_payment;	
				$arr['status'] = $status_arr[$details->status];
				
			}

		}
		$arr['page_title'] = 'View Account Details';
		$arr['main_menu'] = 'accounts';
		$arr['sub_menu1'] = 'tutors';
		$arr['msg'] = $msg;
		$arr['action'] = $action;
		$this->load->view('admin/accounts/view_tutor_details',$arr);
	}

	function payment_details(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
		}
		
		$tutor_id = $this->input->post('tutor_id');
		$encoded_tutor_id = urlencode(base64_encode($tutor_id.'_'.ENCRYPTION_KEY));
		$course_id = $this->input->post('course_id');
		$encoded_course_id = urlencode(base64_encode($course_id.'_'.ENCRYPTION_KEY));

		$class_schedules = $this->Common_model->get_distinct('class_schedules', 'topic', array('tutor_id' => $tutor_id, 'course_id' => $course_id, 'deleted_at' => '0', 'status' => 'A'));
        $message ='';
        if($tutor_id != '' && $course_id != '') {
            $message ='';
			$message .= '<div class="row pb10">
							<div class="col-sm-12">
								<input type="hidden" name="tutor_id" id="tutor_id" value="'.$tutor_id.'">
								<input type="hidden" name="encoded_tutor_id" id="encoded_tutor_id" value="'.$encoded_tutor_id.'">
								<input type="hidden" name="course_id" id="course_id" value="'.$course_id.'">
								<input type="hidden" name="encoded_course_id" id="encoded_course_id" value="'.$encoded_course_id.'">
							
								<label>Date<span class="text-red">*</span></label> <br/>
								<div id="datetimepicker8">
									<div class="input-group">
										<input type="text" class="form-control date-modal" name="start_date" id="start_date"  autocomplete="off" />
										<div class="input-group-append">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="text-red" id="date_error"></div>
							</div>
						</div>
						<div class="row pb10">
							<div class="col-sm-12">
								<label>Topic: <span class="text-red">*</span></label><br/>
								<select name="topic" id="topic" class="form-control select2" style="width:100%;">
									<option value="">--select--</option>';
									if($class_schedules){
										foreach($class_schedules as $each){
											$message .= '<option value="'.$each['topic'].'">'.$each['topic'].'</option>';
										}
									}
									$message .= '</select>
								<div class="text-red" id="topic_error"></div>
							</div>
						</div>
						<div class="row pb10">
							<div class="col-sm-6">
								<label>No of Students: <span class="text-red">*</span></label>
								<input type="number" name="no_of_students" id="no_of_students" class="form-control" value="">
								<div class="text-red" id="no_of_error"></div>
							</div>
							<div class="col-sm-6">
								<label>Amount: <span class="text-red">*</span></label>
								<input type="number" name="amount" id="amount" class="form-control" value="">
								<div class="text-red" id="amount_error"></div>
							</div>
						</div>';
           
           echo $message;
        }   

	}

	function payment_details_save(){
		$course_id = $this->input->post('course_id');
		$amount = $this->input->post('amount');
		$tutor_id = $this->input->post('tutor_id');
		$topic = $this->input->post('topic');
		$students_count = $this->input->post('students_count');
		$date = $this->input->post('date');
		$exp_date = explode('/',$date);
		$formatted_date = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0];
		$trans_date = date('Y-m-d H:i:s');

		//insert to staff_payment table
	
		$payment_details = $this->Common_model->get_row('staff_payment',array('tutor_id' => $tutor_id, 'course_id' => $course_id),'');

		if($payment_details != ''){
			if($payment_details->dates != ''){
				$payment_dates = $payment_details->dates .','.$formatted_date;
			}
			else{
				$payment_dates = $formatted_date;
			}

			if($payment_details->topic != ''){
				$payment_topic = $payment_details->topic .','.$topic;
			}
			else{
				$payment_topic = $topic;
			}

			if($payment_details->no_of_students != ''){
				$payment_students = $payment_details->no_of_students .','.$students_count;
			}
			else{
				$payment_students = $students_count;
			}

			if($payment_details->tution_fees != ''){
				$payment_tution_fees = $payment_details->tution_fees .','.$amount;
			}
			else{
				$payment_tution_fees = $amount;
			}

			$total = $payment_details->total + $amount;
			$balance = $payment_details->balance + $amount;

			$save_to_payment = array(
				'dates' => $payment_dates,
				'topic' => $payment_topic,
				'no_of_students' => $payment_students,
				'tution_fees' => $payment_tution_fees,
				'total' => $total,
				'balance' => $balance,
				'added_by' => $this->session->userdata('user_id'),
				'added_on' => $trans_date
			);
	
			$this->Common_model->update_with_array('staff_payment',array('course_id' => $course_id, 'tutor_id' => $tutor_id),$save_to_payment);

			//user log
			$update_date = date('Y-m-d H:i:s');
			$update_user = $this->session->userdata('user_id');
			$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'staff_payment',
				'table_id' => $payment_details->id,
				'description' => 'Payment details of tutor #id '.$tutor_id.' has been added',
				'operation' => 'Payment Details',
				'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);
	
		}
		else{
			$payment_dates = $formatted_date;
			$payment_topic = $topic;
			$payment_students = $students_count;
			$payment_tution_fees = $amount;
			$total = $amount;
			$balance = $amount;

			$save_to_payment = array(
				'tutor_id' => $tutor_id,
				'course_id' => $course_id,
				'dates' => $payment_dates,
				'topic' => $payment_topic,
				'no_of_students' => $payment_students,
				'tution_fees' => $payment_tution_fees,
				'total' => $total,
				'released_dates' => '',
				'released_amounts' => '',
				'total_released' => '',
				'balance' => $balance,
				'added_by' => $this->session->userdata('user_id'),
				'added_on' => $trans_date
			);
	
	
			$payid = $this->Common_model->insert('staff_payment',$save_to_payment);

			//user log
			$update_date = date('Y-m-d H:i:s');
			$update_user = $this->session->userdata('user_id');
			$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'staff_payment',
				'table_id' => $payid,
				'description' => 'Payment Details of tutor #id '.$tutor_id.' has been added',
				'operation' => 'Payment Details',
				'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);
		}

		echo '1';		
	}

	function payment_released(){
		if (!in_array('accounts_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
		}
		
		$tutor_id = $this->input->post('tutor_id');
		$encoded_tutor_id = urlencode(base64_encode($tutor_id.'_'.ENCRYPTION_KEY));
		$course_id = $this->input->post('course_id');
		$encoded_course_id = urlencode(base64_encode($course_id.'_'.ENCRYPTION_KEY));

		$balance = $this->Common_model->get_row('staff_payment',array('tutor_id' => $tutor_id, 'course_id' => $course_id),'balance');

        $message ='';
        if($tutor_id != '' && $course_id != '') {
            $message ='';
			$message .= '<div class="row pb10">
							<div class="col-sm-12">
								<input type="hidden" name="tutor_id" id="tutor_id" value="'.$tutor_id.'">
								<input type="hidden" name="encoded_tutor_id" id="encoded_tutor_id" value="'.$encoded_tutor_id.'">
								<input type="hidden" name="course_id" id="course_id" value="'.$course_id.'">
								<input type="hidden" name="encoded_course_id" id="encoded_course_id" value="'.$encoded_course_id.'">
							
								<label>Date<span class="text-red">*</span></label> <br/>
								<div id="datetimepicker8">
									<div class="input-group">
										<input type="text" class="form-control date-modal" name="start_date" id="start_date"  autocomplete="off" />
										<div class="input-group-append">
											<span class="input-group-text"><i class="fa fa-calendar"></i></span>
										</div>
									</div>
								</div>
								<div class="text-red" id="date_error"></div>
							</div>
						</div>
						
						<div class="row pb10">
							<div class="col-sm-12">
								<label>Amount: <span class="text-red">*</span></label>
								<input type="hidden" name="max_amount" id="max_amount" value="'.$balance.'">
								<input type="number"  name="amount" id="amount" class="form-control" value="" >
								<div class="text-red" id="amount_error"></div>
							</div>
						</div>';
           
           echo $message;
        }   

	}

	function payment_released_save(){
		$course_id = $this->input->post('course_id');
		$amount = $this->input->post('amount');
		$tutor_id = $this->input->post('tutor_id');
		$date = $this->input->post('date');
		$exp_date = explode('/',$date);
		$formatted_date = $exp_date[2].'-'.$exp_date[1].'-'.$exp_date[0];
		$trans_date = date('Y-m-d H:i:s');

		//insert to staff_payment table
	
		$payment_details = $this->Common_model->get_row('staff_payment',array('tutor_id' => $tutor_id, 'course_id' => $course_id),'');

		if($payment_details != ''){
			if($payment_details->released_dates != ''){
				$payment_dates = $payment_details->released_dates .','.$formatted_date;
			}
			else{
				$payment_dates = $formatted_date;
			}

			if($payment_details->released_amounts != ''){
				$payment_amounts = $payment_details->released_amounts .','.$amount;
			}
			else{
				$payment_amounts = $amount;
			}

			$total_released = $payment_details->total_released + $amount;
			$balance = $payment_details->balance - $amount;

			$save_to_payment = array(
				'released_dates' => $payment_dates,
				'released_amounts' => $payment_amounts,
				'total_released' => $total_released,
				'balance' => $balance,
				'added_by' => $this->session->userdata('user_id'),
				'added_on' => $trans_date
			);
	
			$this->Common_model->update_with_array('staff_payment',array('course_id' => $course_id, 'tutor_id' => $tutor_id),$save_to_payment);

			//user log
			$update_date = date('Y-m-d H:i:s');
			$update_user = $this->session->userdata('user_id');
			$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'staff_payment',
				'table_id' => $payment_details->id,
				'description' => 'Payment of tutor #id '.$tutor_id.' has been released',
				'operation' => 'Payment Released',
				'updated_date' => $update_date
			);
			$this->Common_model->set_user_log($data_to_log);

			echo '1';
	
		}
		else{
			echo '0';
		}
				
	}

}


