<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Class_schedules extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
        date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
	}
	
	//function to add class schedule
	function add_schedule(){
		if (!in_array('class_schedules_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['id'] = '';
		$arr['course_id'] = '';
		$arr['tutor_id'] = '';	
		$arr['schedule_date'] = '';		
		$arr['schedule_time'] = '';	
		$arr['end_time'] = '';	
		$arr['topic'] = '';
		$arr['webinar_link'] = '';	
		$arr['message'] = '';				
		$schedule_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$schedule_details = $this->Common_model->get_row('class_schedules',array('id' =>$id));
			if($schedule_details)			
			{
				$arr['id'] = $schedule_details->id;	
				$arr['course_id'] =  $schedule_details->course_id;	
				$arr['tutor_id'] =  $schedule_details->tutor_id;	
				$arr['schedule_date'] =  date('d/m/Y',strtotime($schedule_details->date));		
				$arr['schedule_time'] =  $schedule_details->time;	
				$arr['end_time'] =  $schedule_details->end_time;	
				$arr['topic'] =  $schedule_details->topic;	
				$arr['webinar_link'] = $schedule_details->webinar_link;		
				$arr['message'] =  $schedule_details->message;	
			}

		}
		$arr['page_title'] = 'Add Schedule';
		$arr['main_menu'] = 'class_schedules';
		$arr['sub_menu1'] = 'add_schedule';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$this->load->view('admin/class_schedules/add_schedule',$arr);
	}

	function schedule_save(){
		if (!in_array('class_schedules_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
		}

		$tutor_error = '';
		$id = $this->input->post('id');
		$tutor_id = $this->input->post('tutor_id');
		$start_time = $this->input->post('schedule_time');
		$end_time = $this->input->post('end_time');
		if($start_time != '')
			$starttime = date('H:i:s',strtotime($start_time));
		else
			$starttime = '';

		if($end_time != '')
			$endtime = date('H:i:s',strtotime($end_time));
		else
			$enddtime = '';

		$start_date = $this->input->post('schedule_date');
		if($start_date != ''){
			$exploded_start_date = explode('/',$start_date);
			$startdate = date('Y-m-d',strtotime($exploded_start_date[2].'-'.$exploded_start_date[1].'-'.$exploded_start_date[0]));

			$other_class_exist = $this->Common_model->tutor_class_exist($tutor_id,$startdate,$starttime,$endtime,$id);
		}
		else{
			$other_class_exist = 0;
		}

		if($other_class_exist > 0){
			$tutor_error .= 'Class conflict exist for this tutor.';
		}

		if ($this->form_validation->run() == false || $tutor_error != '') {
			$arr['id'] =  $this->input->post('id');
			$arr['course_id'] =  $this->input->post('course_id');
			$arr['tutor_id'] =  $this->input->post('tutor_id');
			$arr['schedule_date'] =  $this->input->post('schedule_date');		
			$arr['schedule_time'] =  $this->input->post('schedule_time');
			$arr['end_time'] =  $this->input->post('end_time');
			$arr['topic'] =  $this->input->post('topic');
			$arr['webinar_link'] = $this->input->post('webinar_link');	
			$arr['message'] =  $this->input->post('message');
			
			$arr['page_title'] = 'Add Schedule';
			$arr['main_menu'] = 'class_schedules';
			$arr['sub_menu1'] = 'add_schedule';
			if($tutor_error != ''){
				$arr['msg'] = $tutor_error;
				$arr['action'] = 'alert-danger';
			}
			else{
				$arr['msg'] = $this->session->flashdata('msg');
				$arr['action'] = $this->session->flashdata('action');
			}
			$this->load->view('admin/class_schedules/add_schedule',$arr);
		} 
		else {
			$start_date = $this->input->post('schedule_date');
			$exploded_start_date = explode('/',$start_date);
			$startdate = date('Y-m-d',strtotime($exploded_start_date[2].'-'.$exploded_start_date[1].'-'.$exploded_start_date[0]));


			$save_data = array(
			'course_id' => $this->input->post('course_id'),
			'tutor_id' =>$this->input->post('tutor_id'),
			'message' =>$this->input->post('message'),
			'webinar_link' => $this->input->post('webinar_link'),
			'date' => $startdate,
			'time' => $starttime,
			'end_time' => $endtime,
			'topic' => $this->input->post('topic'),
			'added_by' =>$this->session->userdata('user_id') ,
			'added_on' =>date('Y-m-d H:i:s'),
			);
			if( $this->input->post('id') != ''){
				$id = $this->input->post('id');
				$this->Common_model->update_with_array('class_schedules',array('id'=>$id),$save_data);

				$this->session->set_flashdata('msg','Details of class schedules has been updated.');
				$this->session->set_flashdata('action', 'alert-success');
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'class_schedules',
				'table_id' => $id,
				'description' => 'Details of class schedules id #'.$id.' updated',
				'operation' => 'Update',
				'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

			}
			else{
				$save_data['status'] = 'A';
				$id = $this->Common_model->insert('class_schedules',$save_data);

				$this->session->set_flashdata('msg', 'Details of class schedules has been inserted');
				$this->session->set_flashdata('action', 'alert-success');
					//user log
					$update_date = date('Y-m-d H:i:s');
					$update_user =$this->session->userdata('user_id');
					$user_name = $this->session->userdata('user_name');
					$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'class_schedules',
					'table_id' => $id,
					'description' => 'Details of class schedules id #'.$id.' inserted',
					'operation' => 'Insert',
					'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);
			}

			redirect('admin/class_schedules/list_schedules');
		}

	}

	//function to list class schedules
	function list_schedules(){
		if (!in_array('class_schedules_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
	
		$arr['page_title'] = 'List Class Schedules';
		$arr['main_menu'] = 'class_schedules';
		$arr['sub_menu1'] = 'list_schedules';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['class_schedules']=$this->Common_model->get_all_rows('class_schedules',array('deleted_at' => '0'),array('date' => 'DESC','end_time' => 'DESC'));		
		$this->load->view('admin/class_schedules/list_schedules',$arr);
	}

	//function to view class scheduling message
	function view_message(){
		if (!in_array('class_schedules_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$schedule_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$schedule_details = $this->Common_model->get_row('class_schedules',array('id' =>$id));
			if($schedule_details)			
			{
				$arr['id'] = $schedule_details->id;
				$arr['course_id'] = $schedule_details->course_id;	
				$arr['course'] = $this->Common_model->get_row('courses',array('id' => $schedule_details->course_id),'title');	
				$arr['tutor_id'] = $schedule_details->tutor_id;	
				$arr['tutor_title'] = $this->Common_model->get_row('tutors',array('id' => $schedule_details->tutor_id),'name_title');
				$arr['tutor'] = $this->Common_model->get_row('tutors',array('id' => $schedule_details->tutor_id),'name');	
				$arr['start_date'] = date('Y F d',strtotime($schedule_details->date));
				$arr['time'] = date('h i A',strtotime($schedule_details->time));	
				$arr['topic'] = $schedule_details->topic;	
				$arr['webinar_link'] = $schedule_details->webinar_link;	
				$arr['message'] = strip_tags($schedule_details->message);					
			}

		}
		$arr['page_title'] = 'View Message';
		$arr['main_menu'] = 'class_schedules';
		$arr['sub_menu1'] = 'list_class_schedules';
		$this->load->view('admin/class_schedules/view_message',$arr);
	}


	//function to list tutors of a specific course
	function ajax_tutors(){
		$course_id = $this->input->post('course_id');
		$output = '';
		$tutors_assigned = $this->Common_model->get_row('courses',array('id' => $course_id),'tutors_assigned');
		$tutors_assigned_arr = explode(',',$tutors_assigned);
		$tutors = $this->Common_model->get_all_rows('tutors',array('status' => 'A'),array('name' => 'ASC'),'id,name,name_title',array('id' => $tutors_assigned_arr));

		$output .= '<label>Tutor<span class="text-red">*</span></label>
		<select name="tutor_id" class="form-control select2">
			<option value="">-- Select --</option>';

			if($tutors){
				foreach($tutors as $each){
					$output .= '<option value="'.$each['id'].'">'.$each['name_title'].' '.$each['name'].'</option>';
				}
			}
		$output .= '</select>';

		echo $output;
	}

	//function to send class scheduling message to active students
	function send_message(){
		if (!in_array('class_schedules_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$schedule_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$schedule_details = $this->Common_model->get_row('class_schedules',array('id' =>$id));
			if($schedule_details)			
			{
				
				/* send email to students */

				//get all registered students
				$enrolled_students = $this->Common_model->get_row('courses',array('id' => $schedule_details->course_id),'students_enrolled');
				if($enrolled_students != ''){
					$enrolled_students_arr = explode(',',$enrolled_students);
				}
				else{
					$enrolled_students_arr = array();
				}
				if(!empty($enrolled_students_arr)){
					$send_msg_at = date('Y-m-d H:i:s');
					foreach($enrolled_students_arr as $each){
						$to_email = $this->Common_model->get_row('students',array('id' => $each),'email');
						
						$subject = 'Class Schedule';
						$email_data = array(
							'name' => strtoupper($this->Common_model->get_row('students',array('id' => $each),'name')),
							'id'=> $schedule_details->id,
							'course' => $this->Common_model->get_row('courses',array('id' => $schedule_details->course_id),'title'),
							'tutor_title' => $this->Common_model->get_row('tutors',array('id' => $schedule_details->tutor_id),'name_title'),
							'tutor' => $this->Common_model->get_row('tutors',array('id' => $schedule_details->tutor_id),'name'),	
							'start_date' => date('Y F d',strtotime($schedule_details->date)),
							'time' => date('h i A',strtotime($schedule_details->time)),
							'topic' => $schedule_details->topic,	
							'webinar_link' => $schedule_details->webinar_link,
							'message' => strip_tags($schedule_details->message)	
						);
						$message = $this->load->view('email_template/send_to_students',$email_data,true);
		
						$headers = "From: iLAMA eLearning <contact@ilamaelearning.com>\r\n";
						$headers .= "MIME-Version: 1.0\r\n";
						$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
						mail($to_email,$subject,$message,$headers);
							
					}
					
					$this->session->set_flashdata('msg','Class scheduling mail has been sent to students');
					$this->session->set_flashdata('action','alert-success');

					//user log
					$update_date = date('Y-m-d H:i:s');
					$update_user = $this->session->userdata('user_id');
					$data_to_log = array(
						'user_id' => $update_user,
						'table_name' => 'class_schedules',
						'table_id' => $id,
						'description' => 'Class scheduling message #id '.$id.' has been sent',
						'operation' => 'Send Message',
						'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);

					//insert mail sent date time to class schedules table
					$this->Common_model->update_with_array('class_schedules',array('id' => $id),array('send_message_at' => $send_msg_at));
				}
				else{
					$this->session->set_flashdata('msg','No students have been enrolled for this course');
					$this->session->set_flashdata('action','alert-danger');
				}
				
				redirect('admin/class_schedules/list_schedules');
			}

		}
	}

	//function to delete class schedule
	function delete_schedule(){
		if (!in_array('class_schedules_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$schedule_details = $this->Common_model->get_row('class_schedules',array('id' =>$id));
			if($schedule_details)			
			{
		
				$data_to_save = array(
					'deleted_at' => date('Y-m-d H:i:s'),
					'added_by' =>$this->session->userdata('user_id') ,
					'added_on' =>date('Y-m-d H:i:s')
				);
				$this->Common_model->update_with_array('class_schedules',array('id'=>$id),$data_to_save);

				$this->session->set_flashdata('msg', 'Class Schedule has been deleted.');
				$this->session->set_flashdata('action', 'alert-success');

				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'class_schedules',
					'table_id' => $id,
					'description' => 'Class schedule #id '.$id.' has been deleted',
					'operation' => 'Deleted',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('admin/class_schedules/list_schedules');
			
				
			}

		}
	}


}


