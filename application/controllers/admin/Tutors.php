<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Tutors extends CI_Controller {
/**
 *  Ilama eLearning Management System
 *
 */
    var $permissions = array();
    public function __construct() {
        parent::__construct();
        $this->load->library('form_validation');
		$this->load->library('permission');
		$this->load->library('upload');
		$this->load->helper('url');		
		$this->load->model('Common_model');
        date_default_timezone_set("Asia/Kolkata");
		if (!$this->session->userdata('is_ilama_admin_login')) {
            redirect('admin/authentication');
        }
		$groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
	}
	
	//function to add tutor
	function add_tutor(){
		if (!in_array('tutors_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$arr['id'] = '';
		$arr['name_title'] = '';	
		$arr['name'] = '';		
		$arr['address'] = '';	
		$arr['phone_number'] = '';
		$arr['email'] = '';	
		$arr['profile_pic'] = '';		
		$arr['degrees'] = '';	
		$arr['profile'] = '';	
		$arr['demo_video'] = '';	
		$arr['file_error'] = '';
		$arr['video_error'] = '';		
		$tutor_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$tutor_details = $this->Common_model->get_row('tutors',array('id' =>$id));
			if($tutor_details)			
			{
				$arr['id'] = $tutor_details->id;
				$arr['name_title'] = $tutor_details->name_title;		
				$arr['name'] = $tutor_details->name;		
				$arr['address'] = $tutor_details->address;	
				$arr['phone_number'] = $tutor_details->phone_number;
				$arr['email'] = $tutor_details->email;
				$arr['profile'] = $tutor_details->profile;
				$arr['profile_pic'] = $tutor_details->profile_pic;
				$arr['degrees'] = $tutor_details->degrees;
				$arr['demo_video'] = $tutor_details->demo_video;
				$arr['file_error'] = '';
				$arr['video_error'] = '';				
			}

		}
		$arr['page_title'] = 'Add Tutor';
		$arr['main_menu'] = 'tutors';
		$arr['sub_menu1'] = 'add_tutor';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$this->load->view('admin/tutors/add_tutor',$arr);
	}

	function tutor_save(){
		if (!in_array('tutors_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
		}
		$file_error = $video_error = '';
		if (isset($_FILES["profile_pic"]["name"]) && $_FILES["profile_pic"]["name"] != '') {
			$allowedExts = array("jpg", "jpeg", "png");
			$temp = explode(".", $_FILES["profile_pic"]["name"]);
			$extension = end($temp);
			$image_wh_name = $_FILES["profile_pic"]["tmp_name"];
			list($width, $height) = getimagesize($image_wh_name);
			
			if ($_FILES["profile_pic"]["error"] > 0) {
				$file_error .= "Error opening the file<br />";
			}

			if (!in_array($extension, $allowedExts)) {
				$file_error .= "Please upload jpg or png image file<br />";
			}
			if ($_FILES["profile_pic"]["size"] > 2048000) {
				$file_error .= "File size should be less than 2 mB<br />";
			}
			if ($width < "260" || $height < "290") {
				$file_error .= 'Min. width x height should be 260 x 290';
			}
		}
		else{
			if($this->input->post('id') == ''){
				$file_error .= "Profile Pic is required<br />";
			}
		}

		if (isset($_FILES["demo_video"]["name"]) && $_FILES["demo_video"]["name"] != '') {
			$vallowedExts = array("wmv", "mp4", "avi", "mov");
			$vtemp = explode(".", $_FILES["demo_video"]["name"]);
			$vextension = end($vtemp);
			
			if ($_FILES["demo_video"]["error"] > 0) {
				$video_error .= "Error opening the file<br />";

			}
			if (!in_array($vextension, $vallowedExts)) {
				$video_error .= "Please upload wmv, mp4, avi or mov file<br />";
			}
			if ($_FILES["demo_video"]["size"] > 30720000) {
				$video_error .= "File size should be less than 30 mB<br />";
			}
		}
		else{
			if($this->input->post('id') == ''){
				$video_error .= "Demo Video is required<br />";
			}
		}

		if ($this->form_validation->run() == false || $file_error != '' || $video_error != '') {
			$arr['id'] = $this->input->post('id');
			$arr['name_title'] = $this->input->post('name_title');
			$arr['name'] = $this->input->post('name');
			$arr['phone_number'] = $this->input->post('phone_number');
			$arr['email'] = $this->input->post('email');
			$arr['profile'] = $this->input->post('profile');
			$arr['address'] = $this->input->post('address');
			$arr['degrees'] = $this->input->post('degrees');
			$arr['file_error'] = $file_error;
			$arr['video_error'] = $video_error;
			if( $this->input->post('id') != ''){
				$tutorid =  $this->input->post('id');
				$tutor_det = $this->Common_model->get_row('tutors', array('id' => $tutorid), '');
				$arr['profile_pic'] = $tutor_det->profile_pic;
				$arr['demo_video'] = $tutor_det->demo_video;
			}
			else{
				$arr['profile_pic'] = $arr['demo_video'] = '';
			}
			$arr['page_title'] = 'Add Tutor';
			$arr['main_menu'] = 'tutors';
			$arr['sub_menu1'] = 'add_tutor';
			$arr['msg'] = $this->session->flashdata('msg');
			$arr['action'] = $this->session->flashdata('action');	
			$this->load->view('admin/tutors/add_tutor',$arr);
		} 
		else {
			$id = $this->input->post('id');

			$save_data = array(
			'name_title' => $this->input->post('name_title'),
			'name' => $this->input->post('name'),
			'phone_number' =>$this->input->post('phone_number'),
			'email' => $this->input->post('email'),
			'profile' => $this->input->post('profile'),
			'address' => $this->input->post('address'),
			'degrees' => $this->input->post('degrees'),
			'added_by' =>$this->session->userdata('user_id') ,
			'added_on' =>date('Y-m-d H:i:s'),
			);
			if( $this->input->post('id') != ''){
				$id = $this->input->post('id');
				$tutor_det = $this->Common_model->get_row('tutors', array('id' => $id), '');

				$new_name = $id.'_'.rand();
				$file_config = array(
					'upload_path' => "uploads/tutors/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" 
				);
				$video_config = array(
					'upload_path' => "uploads/tutors/demo/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "30720000"
				);
				if ($_FILES['profile_pic']['name'] != '') {
					$save_data['profile_pic'] = $pic_name = $this->upload_file($file_config, 'uploads/tutors/', 'profile_pic', FALSE);
					$this->create_thumbs($pic_name);
					if ($tutor_det->profile_pic != '') {
						if(file_exists('uploads/tutors/'.$tutor_det->profile_pic))
							unlink('uploads/tutors/' . $tutor_det->profile_pic);
						if(file_exists('uploads/tutors/medium/'.$tutor_det->profile_pic))
							unlink('uploads/tutors/medium/' . $tutor_det->profile_pic);
						if(file_exists('uploads/tutors/small/'.$tutor_det->profile_pic))
							unlink('uploads/tutors/small/' . $tutor_det->profile_pic);
						if(file_exists('uploads/tutors/very_small/'.$tutor_det->profile_pic))
							unlink('uploads/tutors/very_small/' . $tutor_det->profile_pic);
					}
				} else
					$save_data['profile_pic'] = $tutor_det->profile_pic;

				if ($_FILES['demo_video']['name'] != '') {
					$save_data['demo_video'] = $this->upload_file($video_config, 'uploads/tutors/demo/', 'demo_video', FALSE);
					if ($tutor_det->demo_video != '') {
						unlink('uploads/tutors/demo/' . $tutor_det->demo_video);
					}
				} else
					$save_data['demo_video'] = $tutor_det->demo_video;

				$this->Common_model->update_with_array('tutors',array('id'=>$id),$save_data);
				$this->session->set_flashdata('msg','Details of tutor has been updated.');
				$this->session->set_flashdata('action', 'alert-success');
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
				'user_id' => $update_user,
				'table_name' => 'tutors',
				'table_id' => $id,
				'description' => 'Details of tutor id #'.$id.' updated',
				'operation' => 'Update',
				'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

			}
			else{
				$save_data['status'] = 'A';
				$id = $this->Common_model->insert('tutors',$save_data);
				$new_name = $id.'_'.rand();
				$file_config = array(
					'upload_path' => "uploads/tutors/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "2048000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				$video_config = array(
					'upload_path' => "uploads/tutors/demo/",
					'allowed_types' => "*",
					'overwrite' => TRUE,
					'file_name' => $new_name,
					'max_size' => "10240000" // Can be set to particular file size , here it is 2 MB(2048 Kb)
				);
				if ($_FILES['profile_pic']['name'] != '') {
					$file_name = $this->upload_file($file_config, 'uploads/tutors/', 'profile_pic', FALSE);
					$this->create_thumbs($file_name);
					$this->Common_model->update_with_array('tutors',array('id'=>$id),array('profile_pic' => $file_name));
				} 
				if ($_FILES['demo_video']['name'] != '') {
					$video_name = $this->upload_file($video_config, 'uploads/tutors/demo/', 'demo_video', FALSE);
					$this->Common_model->update_with_array('tutors',array('id'=>$id),array('demo_video' => $video_name));
				} 

				//save data to users table
				$users_data = array(
					'user_name' => $this->input->post('email'),
					'name' => $this->input->post('name'),
					'email' => $this->input->post('email'),
					'password' => sha1(PASS_SALT . $this->input->post('email')),
					'group_id' => '3',
					'email_verified' => '1',
					'status' => 'A'
				);

				$user_id = $this->Common_model->insert('users',$users_data);
				if($user_id > 0){
					//update students table with user id
					$this->Common_model->update_with_array('tutors',array('id'=>$id),array('user_id' => $user_id));
				}

				$this->session->set_flashdata('msg', 'Details of tutor has been inserted');
				$this->session->set_flashdata('action', 'alert-success');
				//user log
				$update_date = date('Y-m-d H:i:s');
				$update_user =$this->session->userdata('user_id');
				$user_name = $this->session->userdata('user_name');
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'tutors',
					'table_id' => $id,
					'description' => 'Details of tutor id #'.$id.' inserted',
					'operation' => 'Insert',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);
			}

			

			redirect('admin/tutors/list_tutors');
		}

	}

	//function to list tutors
	function list_tutors(){
		if (!in_array('tutors_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
	
		$arr['page_title'] = 'List Tutors';
		$arr['main_menu'] = 'tutors';
		$arr['sub_menu1'] = 'list_tutors';
		$arr['msg'] = $this->session->flashdata('msg');
		$arr['action'] = $this->session->flashdata('action');
		$arr['tutors']=$this->Common_model->get_all_rows('tutors');		
		$this->load->view('admin/tutors/list_tutors',$arr);
	}

	function upload_file($config = array(), $destination = '', $file_name = '', $resize = FALSE)
	{
		$this->upload->initialize($config);
		if ($this->upload->do_upload($file_name)) {
			$upload_data = $this->upload->data();
			$file_path = explode($destination, $upload_data['full_path']);
			$imagename = $filename = $file_path[1];
			if ($resize == FALSE){
				return $filename;
			}
			elseif ($resize == TRUE) {
				// settings for resize image

				$config['source_image'] = $upload_data['full_path'];
				$this->image_lib->clear();
				$this->image_lib->initialize($config);
				$this->image_lib->resize();
				if (file_exists($upload_data['full_path'])) {
					unlink($upload_data['full_path']);//after resize delete orginal image
				}
				$filename = explode('.', $filename);
				$image_thumb = $filename[0] . '_thumb.' . $filename[1];
				rename($destination . $image_thumb, $destination . $imagename);
				return $imagename;
			}
		}
		else{
			print_r($this->upload->display_errors());exit;
		}
	}

	function create_thumbs($file_name){
        // Image resizing config
        $config = array(
            // Medium Image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/tutors/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 260,
				'height'        => 290,
				'quality' 		=> '100',
                'new_image'     => './uploads/tutors/medium/'.$file_name
                ),
            //thumb image
            array(
                'image_library' => 'GD2',
                'source_image'  => './uploads/tutors/'.$file_name,
                'maintain_ratio'=> FALSE,
                'width'         => 62,
				'height'        => 63,
				'quality' 		=> '100',
                'new_image'     => './uploads/tutors/small/'.$file_name
				),
			//very small
			array(
				'image_library' => 'GD2',
				'source_image'  => './uploads/tutors/'.$file_name,
				'maintain_ratio'=> FALSE,
				'width'         => 45,
				'height'        => 45,
				'quality' 		=> '100',
				'new_image'     => './uploads/tutors/very_small/'.$file_name
			)
		);
           
 
        $this->load->library('image_lib', $config[0]);
        foreach ($config as $item){
            $this->image_lib->initialize($item);
            if(!$this->image_lib->resize()){
                return false;
            }
            $this->image_lib->clear();
        }
	}

	//function to view tutor details
	function view_tutor(){
		if (!in_array('tutors_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		$tutor_details ='';
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$tutor_details = $this->Common_model->get_row('tutors',array('id' =>$id));
			if($tutor_details)			
			{
				$arr['id'] = $tutor_details->id;
				$arr['name_title'] = $tutor_details->name_title;	
				$arr['name'] = $tutor_details->name;		
				$arr['address'] = $tutor_details->address;	
				$arr['phone_number'] = $tutor_details->phone_number;
				$arr['email'] = $tutor_details->email;
				$arr['profile'] = $tutor_details->profile;
				$arr['profile_pic'] = $tutor_details->profile_pic;
				$arr['degrees'] = $tutor_details->degrees;
				$arr['demo_video'] = $tutor_details->demo_video;
				$arr['course_assigned'] = $tutor_details->course_assigned;
			}

		}
		$arr['page_title'] = 'View Tutor';
		$arr['main_menu'] = 'tutors';
		$arr['sub_menu1'] = 'list_tutors';
		$this->load->view('admin/tutors/view_tutor',$arr);
	}

	//function to suspend tutor
	function suspend_tutor(){
		if (!in_array('tutors_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$tutor_details = $this->Common_model->get_row('tutors',array('id' =>$id));
			if($tutor_details)			
			{
				//check if any class has been scheduled for future
				$class_schedules = $this->Common_model->tutor_active_class_exist($id);
				if($class_schedules){
					$this->session->set_flashdata('msg', 'Active class schedules exist for this tutor. Re-schedule or remove the same before suspending the tutor.');
					$this->session->set_flashdata('action', 'alert-danger');
					redirect('admin/tutors/list_tutors');
				}
				else{
					$update_date = date('Y-m-d H:i:s');
					$update_user = $this->session->userdata('user_id');
					$data_to_save = array(
						'status' => 'I',
						'added_by' =>$this->session->userdata('user_id') ,
						'added_on' => $update_date
					);
					$this->Common_model->update_with_array('tutors',array('id'=>$id),$data_to_save);

					//update users table status
					$this->Common_model->update_with_array('users',array('id'=>$tutor_details->user_id),array('status' => 'I'));

					$this->session->set_flashdata('msg', 'Tutor has been suspended');
					$this->session->set_flashdata('action', 'alert-success');

					//user log
					$data_to_log = array(
						'user_id' => $update_user,
						'table_name' => 'tutors',
						'table_id' => $id,
						'description' => 'Tutor with id #'.$id.' has been suspended',
						'operation' => 'Suspended',
						'updated_date' => $update_date
					);
					$this->Common_model->set_user_log($data_to_log);

					redirect('admin/tutors/list_tutors');
				}
				
			}

		}
	}

	//function to activate tutor
	function activate_tutor(){
		if (!in_array('tutors_management', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
		
		$encoded_id = ($this->input->get('id')!='') ? urldecode($this->input->get('id')) : '';
		if($encoded_id !=''){
			$enId = base64_decode($encoded_id);
			$enIdSplit = explode('_',$enId);
			$id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
			if($id > 0)
				$tutor_details = $this->Common_model->get_row('tutors',array('id' =>$id));
			if($tutor_details)			
			{
				$update_date = date('Y-m-d H:i:s');
				$update_user = $this->session->userdata('user_id');
				$data_to_save = array(
					'status' => 'A',
					'added_by' =>$this->session->userdata('user_id') ,
					'added_on' => $update_date
				);
				$this->Common_model->update_with_array('tutors',array('id'=>$id),$data_to_save);

				//update users table status
				$this->Common_model->update_with_array('users',array('id'=>$tutor_details->user_id),array('status' => 'A'));

				$this->session->set_flashdata('msg', 'Tutor has been activated');
				$this->session->set_flashdata('action', 'alert-success');

				//user log
				$data_to_log = array(
					'user_id' => $update_user,
					'table_name' => 'tutors',
					'table_id' => $id,
					'description' => 'Tutor with id #'.$id.' has been activated',
					'operation' => 'Activated',
					'updated_date' => $update_date
				);
				$this->Common_model->set_user_log($data_to_log);

				redirect('admin/tutors/list_tutors');
			}

		}
	}

}


