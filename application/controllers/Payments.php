<?php
 if (!defined('BASEPATH'))
     exit('No direct script access allowed');
 class Payments extends CI_Controller
   {

	 var $permissions = array();
     public function __construct()
       {
         parent::__construct();
         $this->load->library('form_validation');
		    $this->load->library('permission');
        $this->load->model('Common_model');
        $this->load->model('Courses_model');
        date_default_timezone_set("Asia/Kolkata");
         if (!$this->session->userdata('is_ilama_admin_login'))
           {
             redirect('login');
           } //!$this->session->userdata('is_admin_login')
		    $groupID = ($this->session->userdata('group_id')) ? $this->session->userdata('group_id') : 0;
        // get permissions and show error if they don't have any permissions at a particular category or all
        if (!$this->permissions = $this->permission->get_category_permissions($groupID))
        {
            show_error('You do not have any permissions!');
        }
       }
    
    public function index()
    {
        if (!in_array('payments_section', $this->permissions))
        {
            show_error('You do not have access to this page!');
        }
        $arr['body_class'] = 'other-pages t-profile-01';
        $arr['main_menu'] = 'payments';
        $arr['page_title'] = 'Payments';
        $arr['msg'] =$this->session->flashdata('msg');
        $arr['action'] = $this->session->flashdata('action'); 
        $user_id = $this->session->userdata('user_id');
        $groupID = $this->session->userdata('group_id');
        $arr['group_id'] = $groupID;
        $arr['profile'] = '';

        if($groupID == '4'){

            $student_id = $this->Common_model->get_row('students',array('user_id' => $user_id),'id');
            $arr['id'] = $student_id;
            $arr['active_course'] = $this->Common_model->get_active_enrolled_course($student_id);
            $arr['profile'] = $this->Common_model->get_row('students',array('user_id' => $user_id),'');
        }
        else{

            $tutor_id = $this->Common_model->get_row('tutors',array('user_id' => $user_id),'id');
            $arr['id'] = $tutor_id;
            $tutor_details = $this->Common_model->get_row('tutors',array('id' =>$tutor_id));
            if($tutor_details)
              $arr['profile'] = $tutor_details;
            $course_assigned = $tutor_details->course_assigned;

            if($course_assigned != ''){
                  $active_courses = $this->Common_model->get_tutor_active_course($course_assigned,'get_id');
            }
            else{
                  $active_courses = '';
            }
            $arr['active_course'] =  $active_courses;
        }
        $this->load->view('payments', $arr);
    }

    public function confirm_payment()
    {
        $groupID = $this->session->userdata('group_id');
        if (!in_array('payments_section', $this->permissions) && $groupID != '4')
        {
            show_error('You do not have access to this page!');
        }
        $arr['body_class'] = 'other-pages t-profile-01';
        $arr['main_menu'] = 'payments';
        $arr['page_title'] = 'Payments';
        $arr['msg'] =$this->session->flashdata('msg');
        $arr['action'] = $this->session->flashdata('action'); 
        $user_id = $this->session->userdata('user_id');
        $arr['group_id'] = $groupID;
        $course_id = $fee_id = $term_id = '';

        //get values
        $encoded_cid = ($this->input->get('cid')!='') ? urldecode($this->input->get('cid')) : '';
        if($encoded_cid !=''){
          $encId = base64_decode($encoded_cid);
          $encIdSplit = explode('_',$encId);
          $course_id = isset($encIdSplit[0]) ? $encIdSplit[0] : '';
        }

        $encoded_fid = ($this->input->get('fid')!='') ? urldecode($this->input->get('fid')) : '';
        if($encoded_fid !=''){
          $enfId = base64_decode($encoded_fid);
          $enfIdSplit = explode('_',$enfId);
          $fee_id = isset($enfIdSplit[0]) ? $enfIdSplit[0] : '';
        }

        $encoded_tid = ($this->input->get('tid')!='') ? urldecode($this->input->get('tid')) : '';
        if($encoded_tid !=''){
          $entId = base64_decode($encoded_tid);
          $entIdSplit = explode('_',$entId);
          $term_id = isset($entIdSplit[0]) ? $entIdSplit[0] : '';
        }

        if($course_id != '' && $term_id != '' && $fee_id != ''){
            $student_details = $this->Common_model->get_row('students',array('user_id' => $this->session->userdata('user_id')),'');
            $enroll_id =  $this->Common_model->get_row('students_enrolled',array('student_id' => $student_details->id,'course_id' => $course_id),'id');

            $arr['course_id'] = $course_id;
            $arr['fee_id'] = $fee_id;
            $arr['term_id'] = $term_id;
            $arr['coursetitle'] = $this->Common_model->get_row('courses',array('id' => $course_id),'title');
            $arr['fee_amount'] = $this->Common_model->get_row('fee_structure',array('id' => $fee_id),'amount');
            // $arr['fee_amount'] = 1;
            $arr['student_name'] = $student_details->name;
            $arr['student_id'] = $student_details->id;
            $arr['student_email'] = $student_details->email;
            $arr['student_phone'] = $student_details->mobile;
            $arr['enroll_id'] = $enroll_id;
            $arr['return_url'] = base_url().'payments/callback';
            $arr['surl'] = base_url().'payments/success';
            $arr['furl'] = base_url().'payments/failure';
            $arr['currency_code'] = 'INR';
            $this->load->view('make_payment',$arr);
        }
    }

    // initialized cURL Request
    private function get_curl_handle($payment_id, $amount)  {
        $url = 'https://api.razorpay.com/v1/payments/'.$payment_id.'/capture';
        $key_id = RAZOR_KEY_ID;
        $key_secret = RAZOR_KEY_SECRET;
        $fields_string = "amount=$amount";
        //cURL Request
        $ch = curl_init();
        //set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_USERPWD, $key_id.':'.$key_secret);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields_string);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        // curl_setopt($ch, CURLOPT_CAINFO, dirname(__FILE__).'/ca-bundle.crt');
        return $ch;
    }   
     
    // callback method
    public function callback() {        
        if (!empty($this->input->post('razorpay_payment_id')) && !empty($this->input->post('merchant_order_id'))) {
            $razorpay_payment_id = $this->input->post('razorpay_payment_id');
            $merchant_order_id = $this->input->post('merchant_order_id');
            $currency_code = 'INR';
            $trans_amount = $this->input->post('merchant_amount');
            $amount = $this->input->post('merchant_total');
            $trans_datetime = date('Y-m-d H:i:s');
            $success = false;
            $error = '';

            try { 
              
                //insert into payment table
                $data_to_payment_table = array(
                  'enrollment_id' => $merchant_order_id,
                  'payment_mode' => 'Online',
                  'transaction_id' => $razorpay_payment_id,
                  'transaction_status' => '',
                  'transaction_amount' => $trans_amount,
                  'transaction_date' => $trans_datetime,
                  'updated_datetime' => $trans_datetime
                );
                $payment_table_id = $this->Common_model->insert('payments',$data_to_payment_table);
                $ch = $this->get_curl_handle($razorpay_payment_id, $amount);
                
                //execute post
                $result = curl_exec($ch);
                $http_status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if ($result === false) {
                    $success = false;
                    $error = 'Curl error: '.curl_error($ch);
                } else {
                    $response_array = json_decode($result, true);
                        //Check success response
                        if ($http_status === 200 and isset($response_array['error']) === false) {
                            $success = true;
                            $updatedate = date('Y-m-d H:i:s');
                            //update status
                            $this->Common_model->update_with_array('payments', array('id' => $payment_table_id),array('transaction_status' => 'Success','acknowledge' => 'A', 'updated_datetime' => $updatedate));

                            //update students_enrolled table
                            $enroll_details = $this->Common_model->get_row('students_enrolled',array('id' => $merchant_order_id),'');
                            
                            if($enroll_details->payment_types != ''){
                                $payment_types = $enroll_details->payment_types.',Online';
                            }
                            else{
                                $payment_types = 'Online';
                            }
                
                            if($enroll_details->payment_ids != ''){
                                $payment_ids = $enroll_details->payment_ids.','.$payment_table_id;
                            }
                            else{
                                $payment_ids = $payment_table_id;
                            }
                
                            if($enroll_details->payment_amount != ''){
                                $payment_amount = $enroll_details->payment_amount.','.$trans_amount;
                            }
                            else{
                                $payment_amount = $trans_amount;
                            }
                
                
                            if($enroll_details->payment_dates != ''){
                                $payment_dates = $enroll_details->payment_dates.','.date('Y-m-d',strtotime($updatedate));
                            }
                            else{
                                $payment_dates = date('Y-m-d',strtotime($updatedate));
                            }
                
                            $paid_fee = $enroll_details->paid_fee + $trans_amount;
                            $balance_fee = $enroll_details->balance_fee - $trans_amount;
                
                            $save_to_enroll = array(
                                'payment_types' => $payment_types,
                                'payment_ids' => $payment_ids,
                                'payment_amount' => $payment_amount,
                                'payment_dates' => $payment_dates,
                                'paid_fee' => $paid_fee,
                                'balance_fee' => $balance_fee,
                                'updated_datetime' => $updatedate
                            );
                
                            $this->Common_model->update_with_array('students_enrolled',array('id' => $merchant_order_id),$save_to_enroll);
                            
                        } else {
                            $success = false;
                            $this->Common_model->update_with_array('payments', array('id' => $payment_table_id),array('transaction_status' => 'Failed','acknowledge' => 'A','updated_datetime' => date('Y-m-d H:i:s')));
                            if (!empty($response_array['error']['code'])) {
                                $error = $response_array['error']['code'].':'.$response_array['error']['description'];
                            } else {
                                $error = 'RAZORPAY_ERROR:Invalid Response <br/>'.$result;
                            }
                        }
                }
                //close connection
                curl_close($ch);
            } catch (Exception $e) {
                $success = false;
                $error = 'OPENCART_ERROR:Request to Razorpay Failed';
            }
            if ($success === true) {
                $encoded_trans_id = urlencode(base64_encode($razorpay_payment_id.'|'.ENCRYPTION_KEY));
                if(!empty($this->session->userdata('ci_subscription_keys'))) {
                    $this->session->unset_userdata('ci_subscription_keys');
                 }
                redirect($this->input->post('merchant_surl_id').'/'.$encoded_trans_id);
 
            } else {
                $encoded_trans_id = urlencode(base64_encode($razorpay_payment_id.'|'.ENCRYPTION_KEY));
                redirect($this->input->post('merchant_furl_id').'/'.$encoded_trans_id);
            }
        } else {
            echo 'An error occured. Contact site administrator, please!';
        }
    } 
    public function success($enctrans_id) {
      if (!in_array('payments_section', $this->permissions))
      {
          show_error('You do not have access to this page!');
      }
      $arr['body_class'] = 'other-pages t-profile-01';
      $arr['main_menu'] = 'payments';
      $arr['page_title'] = 'Payment Success';
      $arr['msg'] =$this->session->flashdata('msg');
      $arr['action'] = $this->session->flashdata('action'); 
      $user_id = $this->session->userdata('user_id');
      $groupID = $this->session->userdata('group_id');
      $arr['group_id'] = $groupID;
      $encoded_id = $enctrans_id;
      $enId = base64_decode($encoded_id);
      $enIdSplit = explode('|',$enId);
      $id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
      $arr['trans_details'] = $this->Common_model->get_row('payments',array('transaction_id' => $id),'');
      $this->load->view('payment_success', $arr);
    }  
    public function failure($enctrans_id) {
      $arr['body_class'] = 'other-pages t-profile-01';
      $arr['main_menu'] = 'payments';
      $arr['page_title'] = 'Payment Failed';
      $arr['msg'] =$this->session->flashdata('msg');
      $arr['action'] = $this->session->flashdata('action'); 
      $user_id = $this->session->userdata('user_id');
      $groupID = $this->session->userdata('group_id');
      $arr['group_id'] = $groupID;
      $encoded_id = $enctrans_id;
      $enId = base64_decode($encoded_id);
      $enIdSplit = explode('|',$enId);
      $id = isset($enIdSplit[0]) ? $enIdSplit[0] : '';
      $arr['trans_details'] = $this->Common_model->get_row('payments',array('transaction_id' => $id),'');
      $this->load->view('payment_failed', $arr);
    } 
    

  }
?>