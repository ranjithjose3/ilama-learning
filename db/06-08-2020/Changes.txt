ALTER TABLE `forum_threads` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;

ALTER TABLE `forum_thread_comments` CHANGE `comment` `comment` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL;

ALTER TABLE `forum_thread_comments` ADD `image` VARCHAR(100) NOT NULL AFTER `comment`;