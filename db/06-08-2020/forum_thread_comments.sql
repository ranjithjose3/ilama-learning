-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2020 at 03:27 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `forum_thread_comments`
--

CREATE TABLE `forum_thread_comments` (
  `id` bigint(11) NOT NULL,
  `thread_id` bigint(11) NOT NULL,
  `parent_id` bigint(20) NOT NULL,
  `comment` text NOT NULL,
  `commented_on` datetime NOT NULL DEFAULT current_timestamp(),
  `commented_by` int(11) NOT NULL,
  `deleted` datetime NOT NULL,
  `status` varchar(2) NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forum_thread_comments`
--

INSERT INTO `forum_thread_comments` (`id`, `thread_id`, `parent_id`, `comment`, `commented_on`, `commented_by`, `deleted`, `status`, `updated_on`) VALUES
(1, 6, 0, 'The sources of energy we are using now is fossil fuel i.e., petrol and petroleum products, and coal which are exhaustible and non-renewable. The demand of energy is increasing due to increased population and better technology that has added many machines, appliances to add comfort to life style. Hence ,the. demand for energy is increasing day by day. To overcome this problem we are looking for alternate sources of energy.', '2020-08-06 17:33:17', 3, '0000-00-00 00:00:00', 'A', '2020-08-06 17:33:17'),
(5, 6, 0, 'Happy to know thread was useful...', '2020-08-06 18:53:16', 6, '0000-00-00 00:00:00', 'A', '2020-08-06 18:53:16'),
(6, 6, 0, 'Answer was satisfactory right?', '2020-08-06 18:55:52', 3, '0000-00-00 00:00:00', 'A', '2020-08-06 18:55:52');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forum_thread_comments`
--
ALTER TABLE `forum_thread_comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forum_thread_comments`
--
ALTER TABLE `forum_thread_comments`
  MODIFY `id` bigint(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
