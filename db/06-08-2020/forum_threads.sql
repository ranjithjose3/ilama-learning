-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 06, 2020 at 03:28 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `forum_threads`
--

CREATE TABLE `forum_threads` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `image` varchar(100) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT current_timestamp(),
  `created_by` int(11) NOT NULL,
  `deleted` datetime NOT NULL,
  `status` varchar(2) NOT NULL,
  `updated_on` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forum_threads`
--

INSERT INTO `forum_threads` (`id`, `course_id`, `title`, `description`, `image`, `created_on`, `created_by`, `deleted`, `status`, `updated_on`) VALUES
(1, 1, 'Why does a compass needle get deflected when brought near a bar magnet?', 'as fasfas fasfasf ', '', '2020-07-27 19:15:57', 3, '0000-00-00 00:00:00', 'A', '2020-08-05 14:42:15'),
(2, 1, 'A ray of light travelling in cur enters obliquely into water. Does the light ray bend towards the normal or away form the normal? Why?', ' Light is a form of energy that produces in us the sensation of sight.  Reflection of light is the phenomenon of bouncing back of light in the same medium on striking the surface of any object.\r\n\r\nThe two laws of reflection are:         \r\n(i) the incident ray, the reflected ray and the normal (at the point of incidence), all lie in the same plane.         \r\n(ii) the angle of reflection (r) is always equal to the angle of incidence\r\n\r\nRefraction of light is the phenomenon of change in the path of', '2_1107340070.jpg', '2020-07-27 20:37:57', 3, '0000-00-00 00:00:00', 'A', '2020-08-06 18:30:36'),
(3, 1, 'Name two safety measures commonly used in electric circuits and appliances.', 'SFDav wyevts ywefvyefyucvf wyfvesyfysefyes.', '', '2020-08-05 14:40:22', 3, '2020-08-05 22:48:33', 'I', '2020-08-05 22:48:33'),
(6, 1, ' Why are we looking at alternate sources of energy?', 'Please reply to my question. I have got a debate on the same.', '6_2103013164.jpg', '2020-08-05 21:43:50', 6, '0000-00-00 00:00:00', 'A', '2020-08-06 17:59:03');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forum_threads`
--
ALTER TABLE `forum_threads`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forum_threads`
--
ALTER TABLE `forum_threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
