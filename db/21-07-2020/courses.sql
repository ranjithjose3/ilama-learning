-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 21, 2020 at 11:59 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `display_from` date NOT NULL,
  `icon_name` varchar(100) NOT NULL,
  `course_pdf` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `tutors_assigned` varchar(255) NOT NULL,
  `students_enrolled` text NOT NULL,
  `fee` varchar(50) NOT NULL,
  `payment_structure` varchar(50) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `category_id`, `title`, `duration`, `start_date`, `end_date`, `display_from`, `icon_name`, `course_pdf`, `details`, `tutors_assigned`, `students_enrolled`, `fee`, `payment_structure`, `status`, `added_by`, `added_on`) VALUES
(1, 1, 'CBSE Physics ', '2 Months', '2020-08-05', '2020-09-28', '2020-07-30', '1_965363331.jpg', 'test.pdf', '<p style=\"text-align:justify\">This course has videos, articles, and practice covering light, the human eye, electricity, &amp; magnetism (following CBSE &amp; NCERT curriculum) The complete list of chapters and subtopics of Class 10 NCERT textbook is provided below. Students can visit each and every subtopic by clicking the links provided and can learn in detail about all the concepts covered in Class 10 Physics.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">The list of chapters and subtopics given below is prepared according to the latest syllabus of CBSE. The content provided in the respective sub-topic is designed by expert physics teachers so that students can learn the concept in-depth.</p>\r\n\r\n<p style=\"text-align:justify\">Physics is one of the most interesting subjects which is filled with numerous engaging concepts. The concepts taught in the 11th standard are the fundamentals of the subject. Students must learn these concepts in-depth to develop their skills in the subject and build a strong foundation.</p>', '1,2,3', '1', '5000', '3', 'A', 1, '2020-07-20 18:06:20'),
(2, 2, 'Computer Science', '45', '2020-08-01', '2020-12-30', '2020-07-16', '2_692901694.jpg', 'test.pdf', '<p style=\"text-align:justify\">Days are counted for deciding the destiny of the future course of students. It is sure that students appearing for Higher Secondary exam would be immersed in their final preparation to give out their best. To enable them achieve their aim, Higher Secondary Computer Science subject for second year(Plus Two) students in capsule form is provided here. The quick revision notes highlights the area of study to be focused, expected questions, core points to be remembered etc. It is preferred the students answer the best 9 out of 11 questions. The quick note is given as PDF file.</p>', '1,3', '2,3', '5000', '4', 'A', 1, '2020-07-14 23:43:52'),
(3, 3, 'Mathematics', '45', '2020-09-07', '2020-11-30', '2020-08-10', '3_1964419620.jpg', 'test.pdf', '<p>Mathematics for plus two students.</p>', '1', '', '2500', '1', 'A', 1, '2020-07-20 15:09:41'),
(4, 2, 'Chemistry', '45', '2020-07-30', '2020-12-28', '2020-07-18', '4_870732559.jpg', 'test.pdf', '<p>Chemistry for plus one students details goes here.</p>', '2', '', '4000', '2', 'A', 1, '2020-07-14 23:44:41'),
(5, 2, 'Test course 1', '30', '2020-08-09', '2020-11-30', '2020-07-27', '5_1804129866.jpg', 'test.pdf', '<p>Test dveyvfywe yvfywvefytvwy wefvywvyeftwvyf wgefgcwgvfg wgf cwfycwgeyfew dfcwvftwtyf&nbsp; fcw ctwsycw&nbsp; fvtwvfw w efgw fwgs gws .</p>', '5,1,3', '', '4500', '1', 'A', 1, '2020-07-18 18:21:29'),
(6, 3, 'English', '30', '2020-08-09', '2020-10-31', '2020-07-20', '6_2086554856.jpg', '6_480090686.pdf', '<p>ADwsdwudyuu efbvcudeyfcue efbewufcsufue fshefc vsvyvfyswvfeyu</p>', '4,5', '', '2000', '1', 'A', 1, '2020-07-18 17:23:36'),
(7, 1, 'Hindi', '45', '2020-09-01', '2020-12-31', '2020-08-15', '7_1540339034.jpg', '7_1540339034.pdf', '<p>Hindi bfeucufbc dusfuscfusbufbcs vsdbubusb vsdvbdubvudsbv dsvbdvsduv sudv svjbdsbvudsbuvbd vdvfudbuvbdubvd vdvgduvd yuvdyu dvhd vgdu</p>', '1,3', '', '3000', '2', 'A', 1, '2020-07-18 17:26:31'),
(8, 2, 'CBSE Chemistry', '5 Months', '2020-08-01', '2020-12-31', '2020-07-24', '8_90344294.jpg', '8_241273931.pdf', '<p>CBSE Chemistry for plus one students</p>', '6,5,1', '', '5000', '1', 'A', 1, '2020-07-20 18:18:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
