-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2020 at 07:36 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `students_enrolled`
--

CREATE TABLE `students_enrolled` (
  `id` bigint(20) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'R: Registered, A: Active, I: Suspended',
  `admitted_date` datetime DEFAULT NULL,
  `payment_types` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'Separated by commas',
  `payment_ids` longtext NOT NULL COMMENT 'Separated by commas',
  `payment_amount` longtext NOT NULL COMMENT 'Separated by commas',
  `payment_dates` longtext NOT NULL COMMENT 'Separated by commas',
  `paid_fee` int(11) NOT NULL,
  `balance_fee` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students_enrolled`
--

INSERT INTO `students_enrolled` (`id`, `student_id`, `course_id`, `status`, `admitted_date`, `payment_types`, `payment_ids`, `payment_amount`, `payment_dates`, `paid_fee`, `balance_fee`, `updated_datetime`) VALUES
(1, 1, 1, 'A', '2020-07-20 00:00:00', 'Online,DD', '1,6', '2500,2000', '2020-07-20,2020-07-22', 4500, 500, '2020-07-22 23:02:22'),
(2, 2, 2, 'I', '2020-07-22 22:41:35', 'Cheque', '7', '1500', '2020-07-22', 1500, 3500, '2020-07-22 23:05:22'),
(3, 3, 2, 'R', NULL, '', '', '', '', 0, 5000, '2020-07-22 03:27:12');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students_enrolled`
--
ALTER TABLE `students_enrolled`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students_enrolled`
--
ALTER TABLE `students_enrolled`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
