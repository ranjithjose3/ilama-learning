-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2020 at 09:53 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active, I: Inactive',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `added_by`, `added_on`) VALUES
(1, 'Tenth', 'A', 1, '2020-07-13 16:44:41'),
(2, 'Plus One', 'A', 1, '2020-07-12 17:05:17'),
(3, 'Plus Two', 'A', 1, '2020-07-12 17:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('bj9ur64uenr7ct4v9ttnilbf0mlphh4o', '::1', 1594839467, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343833393436363b),
('njilblvtgmtpnt0l63rq5cj15c7m3nol', '::1', 1594839804, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343833393830343b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('v0fj17an4udlv6c941cnremn2uarcui2', '::1', 1594840141, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834303134313b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('457f34te4e26dcv8rrgn6v28kvc0mlnl', '::1', 1594840647, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834303634373b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('2it0o9vpmqn936fo8rpq53492rhmts4l', '::1', 1594840978, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834303937383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('oiva2gc126u3ggijdt5g509qdlrdiv90', '::1', 1594841293, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834313239333b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('4gcbgqf31qrajei6phclkgcb1jrc2mlh', '::1', 1594841686, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834313638363b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b6d73677c733a33393a2244657461696c73206f66207065726d697373696f6e20686173206265656e20696e736572746564223b5f5f63695f766172737c613a323a7b733a333a226d7367223b733a333a226f6c64223b733a363a22616374696f6e223b733a333a226f6c64223b7d616374696f6e7c733a31333a22616c6572742d73756363657373223b),
('ont6stqqhg7ab6v4r45hbbqjpk5mjpid', '::1', 1594842163, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834323136333b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('dofrv2to6rufufp769q2tcj5v7fbbc9c', '::1', 1594842468, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834323436383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('r8d9gdv0a8ngsckmb8n2h276qc2ha1lv', '::1', 1594842653, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539343834323436383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b);

-- --------------------------------------------------------

--
-- Table structure for table `class_schedules`
--

CREATE TABLE `class_schedules` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `webinar_link` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `topic` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active, I: Inactive',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `class_schedules`
--

INSERT INTO `class_schedules` (`id`, `course_id`, `tutor_id`, `webinar_link`, `date`, `time`, `topic`, `message`, `status`, `added_by`, `added_on`) VALUES
(1, 2, 1, '#', '2020-08-01', '18:40:49', 'Topic 1', 'No message ', 'A', 1, '2020-07-15 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `display_from` date NOT NULL,
  `icon_name` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `tutors_assigned` varchar(255) NOT NULL,
  `students_enrolled` text NOT NULL,
  `fee` varchar(50) NOT NULL,
  `payment_structure` varchar(50) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `category_id`, `title`, `start_date`, `end_date`, `display_from`, `icon_name`, `details`, `tutors_assigned`, `students_enrolled`, `fee`, `payment_structure`, `status`, `added_by`, `added_on`) VALUES
(1, 1, 'CBSE Physics ', '2020-07-20', '2020-09-28', '2020-07-16', '1_1535512776.jpg', '<p style=\"text-align:justify\">This course has videos, articles, and practice covering light, the human eye, electricity, &amp; magnetism (following CBSE &amp; NCERT curriculum) The complete list of chapters and subtopics of Class 10 NCERT textbook is provided below. Students can visit each and every subtopic by clicking the links provided and can learn in detail about all the concepts covered in Class 10 Physics.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">The list of chapters and subtopics given below is prepared according to the latest syllabus of CBSE. The content provided in the respective sub-topic is designed by expert physics teachers so that students can learn the concept in-depth.</p>\r\n\r\n<p style=\"text-align:justify\">Physics is one of the most interesting subjects which is filled with numerous engaging concepts. The concepts taught in the 11th standard are the fundamentals of the subject. Students must learn these concepts in-depth to develop their skills in the subject and build a strong foundation.</p>', '1,2,3', '1,3', '5000', '3', 'I', 1, '2020-07-14 23:43:37'),
(2, 2, 'Computer Science', '2020-08-01', '2020-12-30', '2020-07-16', '2_692901694.jpg', '<p style=\"text-align:justify\">Days are counted for deciding the destiny of the future course of students. It is sure that students appearing for Higher Secondary exam would be immersed in their final preparation to give out their best. To enable them achieve their aim, Higher Secondary Computer Science subject for second year(Plus Two) students in capsule form is provided here. The quick revision notes highlights the area of study to be focused, expected questions, core points to be remembered etc. It is preferred the students answer the best 9 out of 11 questions. The quick note is given as PDF file.</p>', '1,3', '1,2,3', '5000', '4', 'A', 1, '2020-07-14 23:43:52'),
(3, 3, 'Mathematics', '2020-09-07', '2020-11-30', '2020-08-10', '3_1964419620.jpg', '<p>Mathematics for plus two students.</p>', '1', '', '2500', '1', 'A', 1, '2020-07-14 23:45:10'),
(4, 2, 'Chemistry', '2020-07-30', '2020-12-28', '2020-07-18', '4_870732559.jpg', '<p>Chemistry for plus one students details goes here.</p>', '2', '', '4000', '2', 'A', 1, '2020-07-14 23:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE `enrollments` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `total_amount` float NOT NULL,
  `enrolled_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure`
--

CREATE TABLE `fee_structure` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `due_date` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_structure`
--

INSERT INTO `fee_structure` (`id`, `course_id`, `amount`, `due_date`, `added_by`, `added_on`) VALUES
(16, 2, 1500, '2020-07-16', 1, '2020-07-14 23:43:52'),
(17, 2, 1500, '2020-09-09', 1, '2020-07-14 23:43:52'),
(18, 2, 1000, '2020-10-03', 1, '2020-07-14 23:43:52'),
(20, 1, 2500, '2020-07-15', 1, '2020-07-14 23:43:38'),
(21, 1, 2000, '2020-08-11', 1, '2020-07-14 23:43:38'),
(24, 2, 1000, '2020-12-12', 1, '2020-07-14 23:43:52'),
(30, 1, 500, '2020-08-20', 1, '2020-07-14 23:43:38'),
(31, 3, 2500, '2020-08-17', 1, '2020-07-14 23:45:10'),
(32, 4, 2000, '2020-07-25', 1, '2020-07-14 23:44:41'),
(33, 4, 2000, '2020-09-30', 1, '2020-07-14 23:44:41');

-- --------------------------------------------------------

--
-- Table structure for table `log_table`
--

CREATE TABLE `log_table` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `log` varchar(10000) NOT NULL,
  `description` varchar(10000) CHARACTER SET utf8 NOT NULL,
  `operation` varchar(10000) NOT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_table`
--

INSERT INTO `log_table` (`id`, `user_id`, `log`, `description`, `operation`, `updated_date`) VALUES
(1, 2, '', 'The user kau logged out successfully at 2020-03-30 20:39:59', 'Sign Out', '2020-03-30 20:39:59'),
(2, 1, '', 'The user admin logined successfully at 2020-04-27 12:30:53', 'Sign In', '2020-04-27 12:30:53'),
(3, 1, '', 'The user admin logged out successfully at 2020-04-27 12:34:13', 'Sign Out', '2020-04-27 12:34:13'),
(4, 1, '', 'The user admin logined successfully at 2020-04-27 12:34:22', 'Sign In', '2020-04-27 12:34:22'),
(5, 1, '', 'The user admin logined successfully at 2020-07-10 16:30:04', 'Sign In', '2020-07-10 16:30:04'),
(6, 1, '', 'The user admin logined successfully at 2020-07-12 14:47:28', 'Sign In', '2020-07-12 14:47:28'),
(7, 1, '', 'The user admin logged out successfully at 2020-07-12 15:02:45', 'Sign Out', '2020-07-12 15:02:45'),
(8, 1, '', 'The user admin logined successfully at 2020-07-12 15:12:17', 'Sign In', '2020-07-12 15:12:17'),
(9, 1, '', 'Details of category id #1 inserted', 'Insert', '2020-07-12 17:03:51'),
(10, 1, '', 'Details of category id #2 inserted', 'Insert', '2020-07-12 17:05:18'),
(11, 1, '', 'Details of category id #3 inserted', 'Insert', '2020-07-12 17:07:40'),
(12, 1, '', 'Details of category id #1 updated', 'Insert', '2020-07-12 17:30:28'),
(13, 1, '', 'Details of category id #1 updated', 'Insert', '2020-07-12 17:30:41'),
(14, 1, '', 'Details of category id #3 updated', 'Insert', '2020-07-12 17:30:50'),
(15, 1, '', 'Details of category id #3 updated', 'Insert', '2020-07-12 17:30:59'),
(16, 1, '', 'Details of course id #1 inserted', 'Insert', '2020-07-13 00:29:35'),
(17, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 00:40:31'),
(18, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 00:40:48'),
(19, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 00:41:04'),
(20, 1, '', 'The user admin logined successfully at 2020-07-13 02:00:25', 'Sign In', '2020-07-13 02:00:25'),
(21, 1, '', 'The user admin logged out successfully at 2020-07-13 02:03:06', 'Sign Out', '2020-07-13 02:03:06'),
(22, 1, '', 'The user admin logined successfully at 2020-07-13 16:41:48', 'Sign In', '2020-07-13 16:41:48'),
(23, 1, '', 'Details of category id #1 updated', 'Insert', '2020-07-13 16:44:36'),
(24, 1, '', 'Details of category id #1 updated', 'Insert', '2020-07-13 16:44:41'),
(25, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 17:26:51'),
(26, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 17:30:38'),
(27, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 17:33:10'),
(28, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 17:58:32'),
(29, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 18:14:31'),
(30, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-13 19:01:54'),
(31, 1, '', 'The user admin logined successfully at 2020-07-13 21:29:29', 'Sign In', '2020-07-13 21:29:29'),
(32, 1, '', 'The user admin logined successfully at 2020-07-13 22:58:10', 'Sign In', '2020-07-13 22:58:10'),
(33, 1, '', 'Details of course id #2 inserted', 'Insert', '2020-07-14 01:40:53'),
(34, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 01:43:31'),
(35, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 01:58:04'),
(36, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 02:15:02'),
(37, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 02:17:46'),
(38, 1, '', 'The user admin logined successfully at 2020-07-14 15:08:32', 'Sign In', '2020-07-14 15:08:32'),
(39, 1, '', 'The user admin logged out successfully at 2020-07-14 16:03:51', 'Sign Out', '2020-07-14 16:03:51'),
(40, 1, '', 'The user admin logined successfully at 2020-07-14 16:04:09', 'Sign In', '2020-07-14 16:04:09'),
(41, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:22:16'),
(42, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:22:28'),
(43, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:31:31'),
(44, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:31:38'),
(45, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:31:47'),
(46, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:32:46'),
(47, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:34:30'),
(48, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:35:03'),
(49, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:44:08'),
(50, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 16:44:20'),
(51, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 16:56:41'),
(52, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 16:57:01'),
(53, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 16:58:37'),
(54, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:00:01'),
(55, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:00:22'),
(56, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:02:07'),
(57, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:02:52'),
(58, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:03:05'),
(59, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:04:25'),
(60, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:05:49'),
(61, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:08:15'),
(62, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:08:40'),
(63, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 17:09:55'),
(64, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 17:13:09'),
(65, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 17:15:04'),
(66, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:15:54'),
(67, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:16:11'),
(68, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:17:22'),
(69, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:20:19'),
(70, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 17:20:38'),
(71, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 17:21:03'),
(72, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 17:21:52'),
(73, 1, '', 'The user admin logined successfully at 2020-07-14 19:09:46', 'Sign In', '2020-07-14 19:09:46'),
(74, 1, '', 'Details of course id #3 inserted', 'Insert', '2020-07-14 19:55:28'),
(75, 1, '', 'The user admin logined successfully at 2020-07-14 22:01:45', 'Sign In', '2020-07-14 22:01:45'),
(76, 1, '', 'Details of course id #4 inserted', 'Insert', '2020-07-14 23:32:35'),
(77, 1, '', 'Details of course id #3 updated', 'Update', '2020-07-14 23:34:30'),
(78, 1, '', 'Details of course id #3 updated', 'Update', '2020-07-14 23:37:20'),
(79, 1, '', 'Details of course id #3 updated', 'Update', '2020-07-14 23:43:19'),
(80, 1, '', 'Details of course id #1 updated', 'Update', '2020-07-14 23:43:38'),
(81, 1, '', 'Details of course id #2 updated', 'Update', '2020-07-14 23:43:52'),
(82, 1, '', 'Details of course id #3 updated', 'Update', '2020-07-14 23:44:08'),
(83, 1, '', 'Details of course id #4 updated', 'Update', '2020-07-14 23:44:41'),
(84, 1, '', 'Details of course id #3 updated', 'Update', '2020-07-14 23:45:10'),
(85, 1, '', 'The user admin logined successfully at 2020-07-15 01:57:34', 'Sign In', '2020-07-15 01:57:34'),
(86, 1, '', 'Details of tutor id #4 inserted', 'Insert', '2020-07-15 02:17:54'),
(87, 1, '', 'Details of tutor id #4 updated', 'Update', '2020-07-15 02:21:26'),
(88, 1, '', 'Details of tutor id #1 updated', 'Update', '2020-07-15 02:23:12'),
(89, 1, '', 'Details of tutor id #1 updated', 'Update', '2020-07-15 02:25:12'),
(90, 1, '', 'Details of tutor id #1 updated', 'Update', '2020-07-15 02:26:33'),
(91, 1, '', 'Details of tutor id #2 updated', 'Update', '2020-07-15 02:26:55'),
(92, 1, '', 'Details of tutor id #2 updated', 'Update', '2020-07-15 02:27:47'),
(93, 1, '', 'Details of tutor id #3 updated', 'Update', '2020-07-15 02:28:21'),
(94, 1, '', 'Details of tutor id #3 updated', 'Update', '2020-07-15 02:28:44'),
(95, 1, '', 'The user admin logined successfully at 2020-07-15 14:44:01', 'Sign In', '2020-07-15 14:44:01'),
(96, 1, '', 'The user admin logined successfully at 2020-07-15 16:22:00', 'Sign In', '2020-07-15 16:22:00'),
(97, 1, '', 'Details of tutor id #1 updated', 'Update', '2020-07-15 16:50:00'),
(98, 1, '', 'The user admin logined successfully at 2020-07-15 23:00:53', 'Sign In', '2020-07-15 23:00:53'),
(99, 1, '', 'The user admin logined successfully at 2020-07-16 00:27:51', 'Sign In', '2020-07-16 00:27:51'),
(100, 1, '', 'Details of tutor id #5 inserted', 'Insert', '2020-07-16 01:01:15');

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `enrollment_id` int(11) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `transation_id` varchar(50) NOT NULL,
  `transaction_status` varchar(20) NOT NULL,
  `transaction_amount` float NOT NULL,
  `transaction_date` datetime NOT NULL,
  `acknowledge` char(1) NOT NULL,
  `cheque_no` varchar(50) NOT NULL,
  `dd_no` varchar(20) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `permission` varchar(200) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission`, `key`, `category`) VALUES
(1, 'Access Dashboard', 'access_dashboard', 'General'),
(2, 'User Management', 'user_management', 'Users'),
(3, 'User Group Management', 'user_group_management', 'Users'),
(4, 'Permission Management', 'permission_management', 'General'),
(5, 'Categories Management', 'categories_management', 'Courses'),
(6, 'Courses Management', 'courses_management', 'Courses'),
(7, 'Tutors Management', 'tutors_management', 'Tutors'),
(8, 'Students Management', 'students_management', 'Students');

-- --------------------------------------------------------

--
-- Table structure for table `permission_map`
--

CREATE TABLE `permission_map` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `permission_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_map`
--

INSERT INTO `permission_map` (`id`, `group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(8, 1, 7),
(9, 1, 8);

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `parent_email` varchar(255) NOT NULL,
  `registered_on` datetime NOT NULL,
  `enrolled_courses` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A: Active, I: Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_id`, `name`, `mobile`, `email`, `address`, `profile_pic`, `parent_name`, `parent_email`, `registered_on`, `enrolled_courses`, `status`) VALUES
(1, 6, 'Student 1', '9496862347', 'student1@gmail.com', '', '', 'Parent name 1', 'parentemail1@gmail.com', '0000-00-00 00:00:00', '1,2', 'A'),
(2, 7, 'Student 2', '89563910234', 'student2@gmail.com', '', '', '', '', '0000-00-00 00:00:00', '2', 'A'),
(3, 8, 'Student 3', '8990956710', 'student3@gmail.com', '', '', '', '', '0000-00-00 00:00:00', '2,1', 'A'),
(4, 9, 'AKHILA B NAIR', '9087654321', 'nair.akhilab@gmail.com', 'Puthen veettil\r\nPynkulam\r\nThrissur', '4_1401506821.jpg', 'Balakrishnan', 'balan@gmail.com', '2020-07-16 00:39:01', '', 'A'),
(6, 11, 'STUDENT TEST', '8976543108', 'nair.akhilab1@gmail.com', 'House name\r\nPlace name\r\nDistrict', '6_179840792.jpg', 'Parent1', 'parent1@gmail.com', '2020-07-16 00:55:55', '', 'A'),
(7, 12, 'RAGI K G', '8901234567', 'ragigopal@gmail.com', 'Housedbwugy\r\nctydsacyes\r\nsfcsywecf', '7_1312616630.jpg', 'Gopalan', 'gopalan@gmail.com', '2020-07-16 00:58:13', '', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `tutors`
--

CREATE TABLE `tutors` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `degrees` varchar(255) NOT NULL,
  `profile` text NOT NULL,
  `demo_video` varchar(100) NOT NULL,
  `course_assigned` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A: Active, I: Inactive',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tutors`
--

INSERT INTO `tutors` (`id`, `user_id`, `name`, `address`, `phone_number`, `email`, `profile_pic`, `degrees`, `profile`, `demo_video`, `course_assigned`, `status`, `added_by`, `added_on`) VALUES
(1, 2, 'Tutor 1', 'Address 1\r\nAddress 2', '8089898989', 'tutor1@gmail.com', '1.jpg', 'MCA', '<p>Tutor 1 profile goes here.</p>', '1.mp4', '1,2,3', 'A', 1, '2020-07-15 18:34:24'),
(2, 3, 'Tutor 2', 'Address 2 main,address 2', '9090909090', 'tutor2@gmail.com', '2.jpg', 'B.Com', '<p>Tutor 2 profile is here</p>', '2.mp4', '1,4', 'A', 1, '2020-07-16 00:34:05'),
(3, 4, 'Tutor 3', 'Address 2 main,address 2', '7865123456', 'tutor3@gmail.com', '3.jpg', 'BA Electronics', '<p>Tutor 3 profile.&nbsp;Tutor 3 profile.&nbsp;Tutor 3 profile.&nbsp;Tutor 3 profile.Tutor 3 profile.&nbsp;Tutor 3 profile.</p>', '3_825287555.mp4', '1,2', 'I', 1, '2020-07-16 00:34:09'),
(4, 5, 'MARIAM JAMES', 'House name 1\r\nPlace name1\r\nDistrict\r\nState\r\n678934', '7890203846', 'mariamjames@gmail.com', '4_771756804.jpg', 'B.Tech in IT, MBA', '<p style=\"text-align:justify\">Goal-oriented customer service rep with 7+ years of experience. Seeking to use proven telesales skills to raise customer satisfaction at Triple-P Components. Received 98% favorable customer review scores at Gibbs-Atalah Electronics. Customer retention for repeat clients was 35% above facility average. At Fireberry communications, received Employee of the Month Award 4x.</p>', '4_771756804.mp4', '', 'A', 1, '2020-07-15 02:21:25'),
(5, 13, 'Ranjith', 'House name\r\nPlace\r\nDistrict\r\nState', '8907654321', 'ranjith@gmail.com', '5_875971273.jpg', 'MCA', '<p style=\"text-align:justify\">My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.</p>', '5_875971273.mp4', '', 'A', 1, '2020-07-16 01:01:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `email_verified` char(1) NOT NULL COMMENT '1: Verified, 0: Not Verified',
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `name`, `email`, `password`, `group_id`, `email_verified`, `status`) VALUES
(1, 'admin', 'Admin', 'admin@admin.com', '466d3c5626cb11cdb45e30134364dba1b1faa12a', 1, '1', 'A'),
(2, 'tutor1@gmail.com', 'Tutor 1', 'tutor1@gmail.com', 'aabed7182b7ae9c7e3106686c46ae079abb7d34e', 3, '1', 'A'),
(3, 'tutor2@gmail.com', 'Tutor 2', 'tutor2@gmail.com', 'dd67a7c588aecd2856b7351b0ae5e50fcbb11aae', 3, '1', 'A'),
(4, 'tutor3@gmail.com', 'Tutor 3', 'tutor3@gmail.com', '9f9fb5835e0d0f52212d5005dbbc587a44540e03', 3, '1', 'I'),
(5, 'mariamjames@gmail.com', 'MARIAM JAMES', 'mariamjames@gmail.com', '2306f2e4fe88ea97241d61b313b5e98c6adb5648', 3, '1', 'A'),
(6, 'student1@gmail.com', 'Student 1', 'student1@gmail.com', 'e426f6981c73a10318019cb98aed3f5dbd226dc6', 4, '1', 'A'),
(7, 'student2@gmail.com', 'Student 2', 'student2@gmail.com', 'b806f58993c4a150027e0c3073977b65cad0ff9b', 4, '1', 'A'),
(8, 'student3@gmail.com', 'Student 3', 'student3@gmail.com', 'b812d0a9e421b99326f8d39ff37ca9ab52a0f0fe', 4, '1', 'A'),
(9, 'nair.akhilab@gmail.com', 'AKHILA B NAIR', 'nair.akhilab@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '1', 'A'),
(11, 'nair.akhilab1@gmail.com', 'Student test', 'nair.akhilab1@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '0', 'A'),
(12, 'ragigopal@gmail.com', 'RAGI K G', 'ragigopal@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '1', 'A'),
(13, 'ranjith@gmail.com', 'Ranjith', 'ranjith@gmail.com', 'f00b9747f773fdd52f27d63ddf046ad77331445a', 3, '1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Developer', 1, 'A'),
(2, 'Admin', 2, 'A'),
(3, 'Tutor', 3, 'A'),
(4, 'Student', 4, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_schedules`
--
ALTER TABLE `class_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollments`
--
ALTER TABLE `enrollments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure`
--
ALTER TABLE `fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_table`
--
ALTER TABLE `log_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `permission_map`
--
ALTER TABLE `permission_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tutors`
--
ALTER TABLE `tutors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_schedules`
--
ALTER TABLE `class_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `enrollments`
--
ALTER TABLE `enrollments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_structure`
--
ALTER TABLE `fee_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT for table `log_table`
--
ALTER TABLE `log_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `permission_map`
--
ALTER TABLE `permission_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tutors`
--
ALTER TABLE `tutors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
