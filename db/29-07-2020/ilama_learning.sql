-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 30, 2020 at 01:35 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active, I: Inactive',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `status`, `added_by`, `added_on`) VALUES
(1, 'School Courses', 'A', 1, '2020-07-13 16:44:41'),
(2, 'HSC 1 Courses', 'A', 1, '2020-07-12 17:05:17'),
(3, 'HSC 2 Courses', 'A', 1, '2020-07-12 17:30:59');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('rp8p79b81qng47qfl0liv3rdo9q9e7lp', '::1', 1595964178, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936343137383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('j5vmp7jj31riu825r9rprmpmpgk3jsuv', '::1', 1595964622, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936343632323b),
('o7tpb6csf0kbuoe5hu6p5brig4jsmqj4', '::1', 1595964976, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936343937363b),
('ocrn1gjhjbs6lpfas1rb9j2gdroftavp', '::1', 1595965320, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936353332303b),
('p2peeqmesq4ufsm68o8cjadph3rovp7p', '::1', 1595965698, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936353639383b),
('cpagf41fsp09uqm1s8luonaq4k3dfn8g', '::1', 1595966038, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936363033383b),
('ooc4tdrt6h273r3f1p7anecshuf1qqea', '::1', 1595966342, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936363334323b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('84p316dakcdub6s9dgilud8sso1ta09j', '::1', 1595967123, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936373132333b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('6tia7u0qm297erick21bm0amvok4o7bh', '::1', 1595967510, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936373531303b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('ab4u08cojbeokdn8fj9lbc5s8brh524g', '::1', 1595968017, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353936373734343b),
('ihaud4qn9cbuvvfq2etai9figdu29bjs', '::1', 1595973185, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937333138353b),
('ijrcdljbqq5r4bhqen84gc6a5epuo69l', '::1', 1595974018, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937343031383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('a2ahl7re21gn2t4lb6als5n0pmmcfc9k', '::1', 1595974566, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937343536363b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('mjgl7cpmp641fmfm6qhj555grs6hrfl2', '::1', 1595975987, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937353938373b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('jg55pplqd5r9crbh11jnl50r89ub8msn', '::1', 1595976370, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937363337303b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('mpolhjjrk44q9akrqeu9lfoqav64e46d', '::1', 1595976940, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937363934303b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b6d73677c733a3131393a22596f752068617665207375636365737366756c6c79207265676973746572656420666f722074686520636f75727365205465737420636f7572736520312e20596f752077696c6c2062652061646d697474656420746f2074686520636f757273652061667465722041646d696e20417070726f76616c2e223b5f5f63695f766172737c613a323a7b733a333a226d7367223b733a333a226f6c64223b733a363a22616374696f6e223b733a333a226f6c64223b7d616374696f6e7c733a31333a22616c6572742d73756363657373223b),
('20ovpkhnroqa7mi57p77gmd5vdjf7lsv', '::1', 1595977372, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937373337323b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('11m3mufupnsipjk374h184di6okljgfh', '::1', 1595977692, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937373639323b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b6d73677c733a3132303a22596f752068617665207375636365737366756c6c79207265676973746572656420666f722074686520636f757273652043425345204368656d69737472792e20596f752077696c6c2062652061646d697474656420746f2074686520636f757273652061667465722041646d696e20417070726f76616c2e223b5f5f63695f766172737c613a323a7b733a333a226d7367223b733a333a226f6c64223b733a363a22616374696f6e223b733a333a226f6c64223b7d616374696f6e7c733a31333a22616c6572742d73756363657373223b),
('fv3mtqa5eh2eh9ifobo8f6csr716lcf0', '::1', 1595978026, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937383032363b),
('q86bbdbqa8unllqnptdijihsj49sjsgr', '::1', 1595978044, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539353937383032363b),
('k7g5c2ebqi2a6fhm2a81ag00uge5in94', '::1', 1596023146, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363032333134333b),
('h9bggfe8e4s090erh6ikkk1f27g4dee9', '::1', 1596039016, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363033393031363b),
('bh3tgt7chtf7krmhoohia2r8e2jvra58', '::1', 1596039016, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363033393031363b),
('15lbvtsblfi5cqr5u73nb1knglej2f4q', '::1', 1596045162, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034353136323b),
('b92int75dhhtimhuoprk8bu1oqqqm59m', '::1', 1596045471, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034353437313b),
('43cq14ecvn47h50ghrk17ak7g31ivi38', '::1', 1596045789, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034353738383b),
('9u5b9rd6avvgl3sig6fl04rnr2g5kocj', '::1', 1596046092, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034363039323b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('7s6a153qpmlng16j8mupo1dldm13h35k', '::1', 1596046625, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034363632353b),
('sgpe3rlktoge6021lm55pecs5as419kt', '::1', 1596046926, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034363932363b),
('k6j8glk5ko16v4g1j03k1k2nlsftbq0c', '::1', 1596048079, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034383037393b),
('l7kgf5vk0gt4h0quggans29qpa82ut48', '::1', 1596048391, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034383339313b),
('gq1oqt23m5u07p545g4p167fs0diblig', '::1', 1596048716, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034383731363b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('8b0uaoq0h9meepfhea4h5kf6ci0i6doa', '::1', 1596049474, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034393437343b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('3hnqp7efqi0803gii2f66p1gssgs0bmi', '::1', 1596049931, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363034393933313b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('nojjh1a62v20fo86fuj8un6hvs3tj01s', '::1', 1596050280, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035303238303b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('i4a4ee1d5dq3josbopod929t20m5k3t4', '::1', 1596050628, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035303632383b757365725f69647c733a313a2239223b757365725f6e616d657c733a32323a226e6169722e616b68696c616240676d61696c2e636f6d223b6e616d657c733a31333a22414b48494c412042204e414952223b67726f75705f69647c733a313a2234223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('jqupbr7k246e31tcf973hauf4bn1l13n', '::1', 1596051008, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035313030383b),
('ljvj4ibbbmh9b54ihbfaa48mh7qri94b', '::1', 1596051401, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035313430313b),
('f7vdmmcvp9lrkgv6e08l3e49jbhtgfdq', '::1', 1596051714, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035313731343b),
('7kc9sgkvs8f1d2ctjp1vu15oa1oejlem', '::1', 1596052047, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035323034373b),
('hc003v79v4a1kabobc1r1aspgsq3ua6a', '::1', 1596052476, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035323437363b),
('m4mqki3jdqdnuos3iv0ld1oc77qj1n80', '::1', 1596052848, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035323834383b),
('un3po0s3q52c5ed9nm3o558vv9o0tpos', '::1', 1596053169, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035333136393b),
('mhj7eobqli9cfs66v3hhgftrrd2od86m', '::1', 1596053475, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035333437353b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('tq8hg6ki9prrtcblqe6n50shpapaus2g', '::1', 1596054002, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035343030323b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('74sjfpqbsdm4gdo3gs7l3iagl53mljd9', '::1', 1596054339, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035343333393b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('mltct0h2eks61vl4515fprmg4iakmci2', '::1', 1596055230, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035353233303b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('7fod9a605ifbptgj40p51hai7facjpo9', '::1', 1596055611, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035353631313b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('cr2dd01jn7rvm82hroejggdmejvh2vjj', '::1', 1596055912, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035353931323b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('iagudook6ugmjd53djrep5ndrshrrh4n', '::1', 1596059477, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035393437373b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('rnr1ltdb7rf99q0huoccko2irek9fhav', '::1', 1596059790, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363035393739303b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('dm47a8qvalv1mivmnufr3shdrdpefndj', '::1', 1596060106, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036303130363b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('b87bs3fnmfhd0t7occ7pfmfqhf233tqe', '::1', 1596060407, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036303430373b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('a112m90n2e4sjtbvafe89ql6oogm5bjr', '::1', 1596060855, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036303835353b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('7fo6g66lui819ti1a2jm0rfiehdujmcd', '::1', 1596061188, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036313138383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('ted22lqh84393pa1vr9ma12osj86jqnh', '::1', 1596061683, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036313638333b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('mcql9si8q2pmou5pa2a152lmmip5o3hn', '::1', 1596061988, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036313938383b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('05ve9sug4ah0k58mqiep25brbaoft8tu', '::1', 1596062307, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036323330373b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('5gmli4kept9msualte9k4ek90r1qcvnu', '::1', 1596062637, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036323633373b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('2v5io5h6l8ati5hiqo7l7dd6irddii3j', '::1', 1596062942, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036323934323b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('fp7m8qq4tf6jl9gjbll5taq5sgmv5oa6', '::1', 1596063269, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036333236393b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('mu1boa8jrtlubnpjrhd8vsfkd0fjnvvn', '::1', 1596063594, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036333539343b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('t024l249d97kigp4dfccsh6pemriq5lq', '::1', 1596063904, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036333930343b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('li2p44kig4m9jcdn7rtg0p08p4afm08t', '::1', 1596064272, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036343237323b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('3hv6hver2btpoadqv6qvmvi8347t5pbv', '::1', 1596064691, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036343639313b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('m4pc97jaatqmur92liplcsp0on8be0i4', '::1', 1596065003, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036353030333b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('237ch4ons8725kkusvrgfm92h8dc3qaa', '::1', 1596065304, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036353330343b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b),
('qcu8tqelgehkpl8achcgltagd2mkn8ie', '::1', 1596065581, 0x5f5f63695f6c6173745f726567656e65726174657c693a313539363036353330343b757365725f69647c733a313a2231223b757365725f6e616d657c733a353a2261646d696e223b6e616d657c733a353a2241646d696e223b67726f75705f69647c733a313a2231223b69735f696c616d615f61646d696e5f6c6f67696e7c623a313b);

-- --------------------------------------------------------

--
-- Table structure for table `class_schedules`
--

CREATE TABLE `class_schedules` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `tutor_id` int(11) NOT NULL,
  `webinar_link` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `end_time` time NOT NULL,
  `topic` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `send_message_at` datetime DEFAULT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active, I: Inactive',
  `deleted_at` varchar(50) NOT NULL DEFAULT '0' COMMENT '0: If not deleted, datetime: when deleted',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `class_schedules`
--

INSERT INTO `class_schedules` (`id`, `course_id`, `tutor_id`, `webinar_link`, `date`, `time`, `end_time`, `topic`, `message`, `send_message_at`, `status`, `deleted_at`, `added_by`, `added_on`) VALUES
(1, 2, 1, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-08-01', '18:40:49', '19:25:49', 'Topic 1', '<p>No message</p> ', NULL, 'A', '2020-07-20 14:49:36', 1, '2020-07-20 14:49:36'),
(2, 2, 1, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-08-01', '15:00:00', '15:30:00', 'Linked List', '<p>This is the message for students regarding their class schedule.</p>', NULL, 'A', '0', 1, '2020-07-17 02:26:15'),
(3, 3, 1, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-08-07', '13:00:00', '14:00:00', 'Algebraic Equations', '<p>Mfytdtyefyd yvyevdywyvd dvwyevydfewfv sfeysfyevws wfvyesvfyesv sfveysvfyesy.</p>', NULL, 'A', '0', 1, '2020-07-20 15:09:27'),
(4, 2, 1, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-08-02', '15:00:00', '15:30:00', 'Linked List', '<p>This is the message for students regarding their class schedule.</p>', NULL, 'A', '0', 1, '2020-07-17 02:26:15'),
(5, 8, 6, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-04-01', '18:40:49', '19:25:49', 'Topic 1', '<p>No message</p> ', NULL, 'A', '0', 1, '2020-07-20 14:49:36'),
(6, 8, 6, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-04-05', '18:40:49', '19:25:49', 'Topic 2', '<p>No message</p> ', NULL, 'A', '0', 1, '2020-07-20 14:49:36'),
(7, 8, 6, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-04-06', '18:40:49', '19:25:49', 'Topic 2', '<p>No message</p> ', NULL, 'A', '0', 1, '2020-07-20 14:49:36'),
(8, 8, 5, 'https://www.youtube.com/watch?v=NybHckSEQBI', '2020-04-07', '18:40:49', '19:25:49', 'Topic 3', '<p>No message</p> ', NULL, 'A', '0', 1, '2020-07-20 14:49:36');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `duration` varchar(100) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `display_from` date NOT NULL,
  `icon_name` varchar(100) NOT NULL,
  `course_pdf` varchar(100) NOT NULL,
  `details` text NOT NULL,
  `tutors_assigned` varchar(255) NOT NULL,
  `students_enrolled` text NOT NULL,
  `fee` varchar(50) NOT NULL,
  `payment_structure` varchar(50) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'A: Active',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `category_id`, `title`, `duration`, `start_date`, `end_date`, `display_from`, `icon_name`, `course_pdf`, `details`, `tutors_assigned`, `students_enrolled`, `fee`, `payment_structure`, `status`, `added_by`, `added_on`) VALUES
(1, 1, 'CBSE Physics ', '5 Months', '2020-08-05', '2020-09-28', '2020-07-30', '1_965363331.jpg', 'test.pdf', '<p style=\"text-align:justify\">This course has videos, articles, and practice covering light, the human eye, electricity, &amp; magnetism (following CBSE &amp; NCERT curriculum) The complete list of chapters and subtopics of Class 10 NCERT textbook is provided below. Students can visit each and every subtopic by clicking the links provided and can learn in detail about all the concepts covered in Class 10 Physics.&nbsp;</p>\r\n\r\n<p style=\"text-align:justify\">The list of chapters and subtopics given below is prepared according to the latest syllabus of CBSE. The content provided in the respective sub-topic is designed by expert physics teachers so that students can learn the concept in-depth.</p>\r\n\r\n<p style=\"text-align:justify\">Physics is one of the most interesting subjects which is filled with numerous engaging concepts. The concepts taught in the 11th standard are the fundamentals of the subject. Students must learn these concepts in-depth to develop their skills in the subject and build a strong foundation.</p>', '1,2,3', '1', '5000', '3', 'A', 1, '2020-07-20 18:06:20'),
(2, 2, 'Computer Science', '5 Months', '2020-08-01', '2020-12-30', '2020-07-16', '2_692901694.jpg', 'test.pdf', '<p style=\"text-align:justify\">Days are counted for deciding the destiny of the future course of students. It is sure that students appearing for Higher Secondary exam would be immersed in their final preparation to give out their best. To enable them achieve their aim, Higher Secondary Computer Science subject for second year(Plus Two) students in capsule form is provided here. The quick revision notes highlights the area of study to be focused, expected questions, core points to be remembered etc. It is preferred the students answer the best 9 out of 11 questions. The quick note is given as PDF file.</p>', '1,3', '2', '5000', '4', 'A', 1, '2020-07-14 23:43:52'),
(3, 3, 'Mathematics', '5 Months', '2020-09-07', '2020-11-30', '2020-08-10', '3_1964419620.jpg', 'test.pdf', '<p>Mathematics for plus two students.</p>', '1', '', '2500', '1', 'A', 1, '2020-07-20 15:09:41'),
(4, 2, 'Chemistry', '5 Months', '2020-07-30', '2020-12-28', '2020-07-18', '4_870732559.jpg', 'test.pdf', '<p>Chemistry for plus one students details goes here.</p>', '2', '', '4000', '2', 'A', 1, '2020-07-14 23:44:41'),
(5, 2, 'Test course 1', '5 Months', '2020-08-09', '2020-11-30', '2020-07-26', '5_1804129866.jpg', 'test.pdf', '<p>Test dveyvfywe yvfywvefytvwy wefvywvyeftwvyf wgefgcwgvfg wgf cwfycwgeyfew dfcwvftwtyf&nbsp; fcw ctwsycw&nbsp; fvtwvfw w efgw fwgs gws .</p>', '5,1,3', '', '4500', '1', 'A', 1, '2020-07-18 18:21:29'),
(6, 3, 'English', '5 Months', '2020-08-09', '2020-10-31', '2020-07-20', '6_2086554856.jpg', '6_480090686.pdf', '<p>ADwsdwudyuu efbvcudeyfcue efbewufcsufue fshefc vsvyvfyswvfeyu</p>', '4,5', '', '2000', '1', 'A', 1, '2020-07-18 17:23:36'),
(7, 1, 'Hindi', '5 Months', '2020-09-01', '2020-12-31', '2020-07-15', '7_1540339034.jpg', '7_1540339034.pdf', '<p>Hindi bfeucufbc dusfuscfusbufbcs vsdbubusb vsdvbdubvudsbv dsvbdvsduv sudv svjbdsbvudsbuvbd vdvfudbuvbdubvd vdvgduvd yuvdyu dvhd vgdu</p>', '1,3', '', '3000', '2', 'A', 1, '2020-07-23 15:11:33'),
(8, 2, 'CBSE Chemistry', '5 Months', '2020-04-01', '2020-07-31', '2020-03-25', '8_90344294.jpg', '8_241273931.pdf', '<p>CBSE Chemistry for plus one students</p>', '6,5,1', '', '5000', '1', 'A', 1, '2020-07-20 18:18:55'),
(9, 1, 'CBSE English', '10 Months', '2020-09-01', '2021-06-30', '2020-07-30', '9_2058057901.jpg', '9_1183265616.pdf', '<p>Course details</p>', '4,7', '', '6000', '3', 'A', 1, '2020-07-30 01:45:49'),
(10, 1, 'CBSE Chemistry', '5 Months', '2020-08-15', '2020-12-21', '2020-07-30', '10_1640729843.jpg', '10_2147283074.pdf', '<p>Course Details</p>', '1,2', '', '5000', '2', 'A', 1, '2020-07-30 01:46:10'),
(11, 1, 'ICSE Mathematics', '5 Months', '2020-09-01', '2021-01-25', '2020-07-30', '11_639795957.jpg', '11_639795957.pdf', '<p>CXugf7ewyf7ye efbrgvyurtguvyr ufrbgvurbn</p>', '6,1', '', '5000', '1', 'A', 1, '2020-07-30 01:51:22'),
(12, 1, 'ICSE SCIENCE', '10 Months', '2020-08-10', '2021-05-17', '2020-07-30', '12_154230528.jpg', '12_154230528.pdf', '<p>Cevfyevfuycer ygfverugvuerbg ugnritgirgtuin</p>', '5', '', '5500', '3', 'A', 1, '2020-07-30 01:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `enrollments`
--

CREATE TABLE `enrollments` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `payment_type` varchar(10) NOT NULL,
  `payment_status` varchar(10) NOT NULL,
  `total_amount` float NOT NULL,
  `enrolled_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `fee_structure`
--

CREATE TABLE `fee_structure` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `due_date` date NOT NULL,
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `fee_structure`
--

INSERT INTO `fee_structure` (`id`, `course_id`, `amount`, `due_date`, `added_by`, `added_on`) VALUES
(16, 2, 1500, '2020-07-16', 1, '2020-07-14 23:43:52'),
(17, 2, 1500, '2020-09-09', 1, '2020-07-14 23:43:52'),
(18, 2, 1000, '2020-10-03', 1, '2020-07-14 23:43:52'),
(20, 1, 2500, '2020-07-21', 1, '2020-07-20 18:06:20'),
(21, 1, 2000, '2020-08-11', 1, '2020-07-20 18:06:21'),
(24, 2, 1000, '2020-12-12', 1, '2020-07-14 23:43:52'),
(30, 1, 500, '2020-08-20', 1, '2020-07-20 18:06:21'),
(31, 3, 2500, '2020-08-17', 1, '2020-07-14 23:45:10'),
(32, 4, 2000, '2020-07-25', 1, '2020-07-14 23:44:41'),
(33, 4, 2000, '2020-09-30', 1, '2020-07-14 23:44:41'),
(34, 5, 4500, '2020-08-04', 1, '2020-07-18 18:21:29'),
(35, 6, 2000, '2020-08-04', 1, '2020-07-18 17:23:37'),
(36, 7, 1500, '2020-08-24', 1, '2020-07-18 17:26:32'),
(37, 7, 1500, '2020-10-12', 1, '2020-07-18 17:26:32'),
(38, 8, 5000, '2020-07-31', 1, '2020-07-20 18:18:55'),
(39, 9, 2000, '2020-09-16', 1, '2020-07-30 01:45:49'),
(40, 9, 2000, '2020-11-16', 1, '2020-07-30 01:45:49'),
(41, 9, 2000, '2021-01-16', 1, '2020-07-30 01:45:49'),
(42, 10, 3500, '2020-08-26', 1, '2020-07-30 01:46:10'),
(43, 10, 1500, '2020-10-21', 1, '2020-07-30 01:46:10'),
(44, 11, 5000, '2020-09-25', 1, '2020-07-30 01:51:23'),
(45, 12, 1500, '2020-10-13', 1, '2020-07-30 01:53:42'),
(46, 12, 1500, '2020-12-17', 1, '2020-07-30 01:53:42'),
(47, 12, 2500, '2021-01-06', 1, '2020-07-30 01:53:42');

-- --------------------------------------------------------

--
-- Table structure for table `forum_threads`
--

CREATE TABLE `forum_threads` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `created_on` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` datetime NOT NULL DEFAULT current_timestamp(),
  `status` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `forum_threads`
--

INSERT INTO `forum_threads` (`id`, `course_id`, `title`, `description`, `created_on`, `deleted`, `status`) VALUES
(1, 1, 'asd asdas das f', 'as fasfas fasfasf ', '2020-07-27 19:15:57', '2020-07-27 19:15:57', 'A'),
(2, 1, 'abcd ', 'abcd', '2020-07-27 20:37:57', '2020-07-27 20:37:57', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `log_table`
--

CREATE TABLE `log_table` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `table_name` varchar(100) NOT NULL,
  `table_id` int(11) NOT NULL,
  `description` varchar(10000) CHARACTER SET utf8 NOT NULL,
  `operation` varchar(10000) NOT NULL,
  `info` varchar(400) DEFAULT NULL,
  `updated_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_table`
--

INSERT INTO `log_table` (`id`, `user_id`, `table_name`, `table_id`, `description`, `operation`, `info`, `updated_date`) VALUES
(1, 1, '', 0, 'The user admin logined successfully at 2020-07-20 17:37:04', 'Sign In', NULL, '2020-07-20 17:37:04'),
(2, 1, '', 0, 'Details of course id #1 updated', 'Update', NULL, '2020-07-20 17:54:16'),
(3, 1, '', 0, 'Details of course id #1 updated', 'Update', NULL, '2020-07-20 17:59:28'),
(4, 1, '', 0, 'Details of course id #1 updated', 'Update', NULL, '2020-07-20 18:03:15'),
(5, 1, '', 0, 'Details of course id #1 updated', 'Update', NULL, '2020-07-20 18:06:20'),
(6, 1, '', 0, 'Details of course id #8 inserted', 'Insert', NULL, '2020-07-20 18:10:53'),
(7, 1, '', 0, 'Details of course id #8 updated', 'Update', NULL, '2020-07-20 18:12:11'),
(8, 1, '', 0, 'Details of course id #8 updated', 'Update', NULL, '2020-07-20 18:18:55'),
(9, 1, '', 0, 'The user admin logined successfully at 2020-07-20 23:30:13', 'Sign In', NULL, '2020-07-20 23:30:13'),
(10, 1, '', 0, 'The user admin logined successfully at 2020-07-21 16:35:35', 'Sign In', NULL, '2020-07-21 16:35:35'),
(11, 1, '', 0, 'The user admin logined successfully at 2020-07-21 19:52:04', 'Sign In', NULL, '2020-07-21 19:52:04'),
(12, 1, '', 0, 'The user admin logined successfully at 2020-07-21 21:50:39', 'Sign In', NULL, '2020-07-21 21:50:39'),
(13, 1, '', 0, 'The user admin logined successfully at 2020-07-22 01:55:44', 'Sign In', NULL, '2020-07-22 01:55:44'),
(14, 1, '', 0, 'The user admin logged out successfully at 2020-07-22 03:43:01', 'Sign Out', NULL, '2020-07-22 03:43:01'),
(15, 1, '', 0, 'The user admin logined successfully at 2020-07-22 14:20:55', 'Sign In', NULL, '2020-07-22 14:20:55'),
(16, 1, '', 0, 'The user admin logined successfully at 2020-07-23 00:28:05', 'Sign In', NULL, '2020-07-23 00:28:05'),
(17, 1, '', 0, 'The user admin logined successfully at 2020-07-23 14:35:26', 'Sign In', NULL, '2020-07-23 14:35:26'),
(18, 1, '', 0, 'The user admin logined successfully at 2020-07-23 17:00:36', 'Sign In', NULL, '2020-07-23 17:00:36'),
(19, 1, '', 0, 'The user admin logined successfully at 2020-07-23 21:20:09', 'Sign In', NULL, '2020-07-23 21:20:09'),
(20, 1, '', 0, 'The user admin logined successfully at 2020-07-23 23:42:13', 'Sign In', NULL, '2020-07-23 23:42:13'),
(21, 1, '', 0, 'The user admin logged out successfully at 2020-07-24 03:16:05', 'Sign Out', NULL, '2020-07-24 03:16:05'),
(22, 1, '', 0, 'The user admin logined successfully at 2020-07-26 23:52:03', 'Sign In', NULL, '2020-07-26 23:52:03'),
(23, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-26 23:59:20', 'Sign In', NULL, '2020-07-26 23:59:20'),
(24, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-26 23:59:50', 'Sign Out', NULL, '2020-07-26 23:59:50'),
(25, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 00:07:15', 'Sign In', NULL, '2020-07-27 00:07:15'),
(26, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 00:10:42', 'Sign Out', NULL, '2020-07-27 00:10:42'),
(27, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 00:10:55', 'Sign In', NULL, '2020-07-27 00:10:55'),
(28, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 00:31:52', 'Sign Out', NULL, '2020-07-27 00:31:52'),
(29, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 00:34:18', 'Sign In', NULL, '2020-07-27 00:34:18'),
(30, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 00:44:04', 'Sign Out', NULL, '2020-07-27 00:44:04'),
(31, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 00:45:45', 'Sign In', NULL, '2020-07-27 00:45:45'),
(32, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 00:48:25', 'Sign Out', NULL, '2020-07-27 00:48:25'),
(33, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 00:48:35', 'Sign In', NULL, '2020-07-27 00:48:35'),
(34, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 00:49:23', 'Sign In', NULL, '2020-07-27 00:49:23'),
(35, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 01:29:51', 'Sign Out', NULL, '2020-07-27 01:29:51'),
(36, 13, '', 0, 'The user ranjith@gmail.com has successfully logged in at 2020-07-27 01:41:20', 'Sign In', NULL, '2020-07-27 01:41:20'),
(37, 13, '', 0, 'The user ranjith@gmail.com logged out successfully at 2020-07-27 01:45:04', 'Sign Out', NULL, '2020-07-27 01:45:04'),
(38, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 01:45:14', 'Sign In', NULL, '2020-07-27 01:45:14'),
(39, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 01:56:38', 'Sign Out', NULL, '2020-07-27 01:56:38'),
(40, 9, '', 0, 'The user nair.akhilab@gmail.com has successfully logged in at 2020-07-27 02:01:19', 'Sign In', NULL, '2020-07-27 02:01:19'),
(41, 9, '', 0, 'The user nair.akhilab@gmail.com logged out successfully at 2020-07-27 02:01:37', 'Sign Out', NULL, '2020-07-27 02:01:37'),
(42, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-27 03:21:38', 'Sign In', NULL, '2020-07-27 03:21:38'),
(43, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-27 03:24:07', 'Sign Out', NULL, '2020-07-27 03:24:07'),
(44, 1, '', 0, 'The user admin logined successfully at 2020-07-28 02:07:45', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-28 02:07:45'),
(45, 1, '', 0, 'Password of the user :  has been updated', 'Update', NULL, '2020-07-28 02:08:13'),
(46, 1, '', 0, 'The user admin logged out successfully at 2020-07-28 02:08:42', 'Sign Out', '::1~2.51.20.242~Dubai', '2020-07-28 02:08:42'),
(47, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-28 02:10:57', 'Sign In', NULL, '2020-07-28 02:10:57'),
(48, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-28 02:16:41', 'Sign Out', NULL, '2020-07-28 02:16:41'),
(49, 1, '', 0, 'The user admin logined successfully at 2020-07-29 00:32:29', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 00:32:29'),
(50, 1, '', 0, 'The user admin logged out successfully at 2020-07-29 00:54:58', 'Sign Out', '::1~2.51.20.242~Dubai', '2020-07-29 00:54:58'),
(51, 1, '', 0, 'The user admin logined successfully at 2020-07-29 01:24:04', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 01:24:04'),
(52, 6, '', 0, 'The user student1@gmail.com has successfully logged in at 2020-07-29 01:50:51', 'Sign In', NULL, '2020-07-29 01:50:51'),
(53, 6, '', 0, 'The user student1@gmail.com logged out successfully at 2020-07-29 01:51:13', 'Sign Out', NULL, '2020-07-29 01:51:13'),
(54, 1, '', 0, 'The user admin logined successfully at 2020-07-29 01:52:00', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 01:52:00'),
(55, 1, '', 0, 'Password of the user :  has been updated', 'Update', NULL, '2020-07-29 01:52:19'),
(56, 1, '', 0, 'The user admin logged out successfully at 2020-07-29 01:52:24', 'Sign Out', '::1~2.51.20.242~Dubai', '2020-07-29 01:52:24'),
(57, 1, '', 0, 'The user admin logined successfully at 2020-07-29 03:26:29', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 03:26:29'),
(58, 1, '', 0, 'Password of the user :  has been updated', 'Update', NULL, '2020-07-29 03:26:47'),
(59, 1, '', 0, 'The user admin logged out successfully at 2020-07-29 03:26:50', 'Sign Out', '::1~2.51.20.242~Dubai', '2020-07-29 03:26:50'),
(60, 1, '', 0, 'The user admin logined successfully at 2020-07-29 03:30:10', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 03:30:10'),
(61, 1, '', 0, 'The user admin logged out successfully at 2020-07-29 03:30:45', 'Sign Out', '::1~2.51.20.242~Dubai', '2020-07-29 03:30:45'),
(62, 1, '', 0, 'The user admin logined successfully at 2020-07-29 03:31:02', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 03:31:02'),
(63, 1, '', 0, 'Password of the user :  has been updated', 'Update', NULL, '2020-07-29 03:34:10'),
(64, 1, '', 0, 'Password of the user : AKHILA B NAIR has been updated', 'Update', NULL, '2020-07-29 03:38:40'),
(65, 9, '', 0, 'The user nair.akhilab@gmail.com has successfully logged in at 2020-07-29 03:38:46', 'Sign In', NULL, '2020-07-29 03:38:46'),
(66, 9, '', 0, 'The user nair.akhilab@gmail.com logged out successfully at 2020-07-29 03:46:06', 'Sign Out', NULL, '2020-07-29 03:46:06'),
(67, 9, '', 0, 'The user nair.akhilab@gmail.com has successfully logged in at 2020-07-29 03:46:26', 'Sign In', NULL, '2020-07-29 03:46:26'),
(68, 9, '', 0, 'The user nair.akhilab@gmail.com logged out successfully at 2020-07-29 03:46:31', 'Sign Out', NULL, '2020-07-29 03:46:31'),
(69, 9, '', 0, 'The user nair.akhilab@gmail.com has successfully logged in at 2020-07-29 03:48:14', 'Sign In', NULL, '2020-07-29 03:48:14'),
(70, 9, '', 0, 'The user nair.akhilab@gmail.com logged out successfully at 2020-07-29 03:48:45', 'Sign Out', NULL, '2020-07-29 03:48:45'),
(71, 9, '', 0, 'The user nair.akhilab@gmail.com has successfully logged in at 2020-07-29 03:49:04', 'Sign In', NULL, '2020-07-29 03:49:04'),
(73, 9, 'students_enrolled', 5, 'The student AKHILA B NAIR has registered for the course Chemistry', 'Registered', NULL, '2020-07-29 04:16:20'),
(74, 9, 'students_enrolled', 6, 'The student AKHILA B NAIR has registered for the course Test course 1', 'Registered', NULL, '2020-07-29 04:17:32'),
(75, 9, 'students_enrolled', 7, 'The student AKHILA B NAIR has registered for the course Chemistry', 'Registered', NULL, '2020-07-29 04:35:26'),
(76, 9, 'students_enrolled', 8, 'The student AKHILA B NAIR has registered for the course CBSE Chemistry', 'Registered', NULL, '2020-07-29 04:36:31'),
(77, 9, '', 0, 'The user nair.akhilab@gmail.com logged out successfully at 2020-07-29 04:38:16', 'Sign Out', NULL, '2020-07-29 04:38:16'),
(78, 1, '', 0, 'The user admin logined successfully at 2020-07-29 23:33:17', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-29 23:33:17'),
(79, 1, '', 0, 'Details of tutor id #2 updated', 'Update', NULL, '2020-07-29 23:36:37'),
(80, 1, '', 0, 'Details of user id #3 updated', 'Update', NULL, '2020-07-29 23:39:00'),
(81, 1, '', 0, 'Password of the user : Vinay Nandagopal has been updated', 'Update', NULL, '2020-07-29 23:39:13'),
(82, 1, '', 0, 'The user admin logged out successfully at 2020-07-29 23:39:18', 'Sign Out', '::1~2.51.20.242~Dubai', '2020-07-29 23:39:18'),
(83, 1, '', 0, 'The user admin logined successfully at 2020-07-30 00:18:09', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-30 00:18:09'),
(84, 1, '', 0, 'Details of tutor id #1 updated', 'Update', NULL, '2020-07-30 00:18:29'),
(85, 9, '', 0, 'The user nair.akhilab@gmail.com has successfully logged in at 2020-07-30 00:52:37', 'Sign In', NULL, '2020-07-30 00:52:37'),
(86, 9, '', 0, 'The user nair.akhilab@gmail.com logged out successfully at 2020-07-30 00:54:52', 'Sign Out', NULL, '2020-07-30 00:54:52'),
(87, 1, '', 0, 'The user admin logined successfully at 2020-07-30 01:38:07', 'Sign In', '::1~2.51.20.242~Dubai', '2020-07-30 01:38:07'),
(88, 1, '', 0, 'Details of course id #9 inserted', 'Insert', NULL, '2020-07-30 01:41:45'),
(89, 1, '', 0, 'Details of course id #10 inserted', 'Insert', NULL, '2020-07-30 01:43:31'),
(90, 1, '', 0, 'Details of course id #9 updated', 'Update', NULL, '2020-07-30 01:45:49'),
(91, 1, '', 0, 'Details of course id #10 updated', 'Update', NULL, '2020-07-30 01:46:10'),
(92, 1, '', 0, 'Details of course id #11 inserted', 'Insert', NULL, '2020-07-30 01:51:23'),
(93, 1, '', 0, 'Details of course id #12 inserted', 'Insert', NULL, '2020-07-30 01:53:42'),
(94, 1, '', 0, 'Details of tutor id #8 inserted', 'Insert', NULL, '2020-07-30 04:03:09');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `subject` varchar(500) NOT NULL,
  `content` text NOT NULL,
  `attachments` text DEFAULT NULL,
  `courses` varchar(500) NOT NULL,
  `recipients` text NOT NULL,
  `inserted_date` datetime NOT NULL,
  `send_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `enrollment_id` int(11) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `transaction_status` varchar(20) NOT NULL,
  `transaction_amount` float NOT NULL,
  `transaction_date` datetime NOT NULL,
  `acknowledge` char(1) NOT NULL,
  `cheque_no` varchar(50) NOT NULL,
  `dd_no` varchar(20) NOT NULL,
  `others` varchar(50) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `enrollment_id`, `payment_mode`, `transaction_id`, `transaction_status`, `transaction_amount`, `transaction_date`, `acknowledge`, `cheque_no`, `dd_no`, `others`, `bank_name`, `updated_datetime`) VALUES
(1, 1, 'Online', '', 'Success', 2500, '2020-07-20 06:18:13', 'A', '', '', '', '', '2020-07-20 06:19:09'),
(6, 1, 'DD', '', 'Success', 2000, '2020-07-22 03:27:12', 'A', '', 'SBT45FG34', '', '', '2020-07-22 03:27:12'),
(7, 2, 'Cheque', '', 'Success', 1500, '2020-07-22 19:23:30', 'A', 'FDRL1244F', '', '', '', '2020-07-22 19:23:30');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(11) NOT NULL,
  `permission` varchar(200) DEFAULT NULL,
  `key` varchar(100) DEFAULT NULL,
  `category` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `permission`, `key`, `category`) VALUES
(1, 'Access Dashboard', 'access_dashboard', 'General'),
(2, 'User Management', 'user_management', 'Users'),
(3, 'User Group Management', 'user_group_management', 'Users'),
(4, 'Permission Management', 'permission_management', 'General'),
(5, 'Categories Management', 'categories_management', 'Courses'),
(6, 'Courses Management', 'courses_management', 'Courses'),
(7, 'Tutors Management', 'tutors_management', 'Tutors'),
(8, 'Students Management', 'students_management', 'Students'),
(9, 'Class Schedules Management', 'class_schedules_management', 'Class Schedules'),
(10, 'Accounts Management', 'accounts_management', 'Accounts');

-- --------------------------------------------------------

--
-- Table structure for table `permission_map`
--

CREATE TABLE `permission_map` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL DEFAULT 0,
  `permission_id` int(11) NOT NULL DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permission_map`
--

INSERT INTO `permission_map` (`id`, `group_id`, `permission_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 1, 5),
(6, 1, 6),
(8, 1, 7),
(9, 1, 8),
(10, 1, 9),
(11, 1, 10),
(12, 3, 1),
(13, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `staff_payment`
--

CREATE TABLE `staff_payment` (
  `id` bigint(20) NOT NULL,
  `tutor_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `dates` longtext NOT NULL,
  `topic` longtext NOT NULL,
  `no_of_students` longtext NOT NULL,
  `tution_fees` longtext NOT NULL,
  `total` int(11) NOT NULL,
  `released_dates` longtext NOT NULL,
  `released_amounts` longtext NOT NULL,
  `total_released` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `added_by` bigint(20) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff_payment`
--

INSERT INTO `staff_payment` (`id`, `tutor_id`, `course_id`, `dates`, `topic`, `no_of_students`, `tution_fees`, `total`, `released_dates`, `released_amounts`, `total_released`, `balance`, `added_by`, `added_on`) VALUES
(2, 6, 8, '2020-04-01,2020-04-05,2020-04-06', 'Topic 1,Topic 2,Topic 2', '50,100,40', '1500,3000,1200', 5700, '2020-07-16,2020-07-21', '2500,500', 3000, 2700, 1, '2020-07-24 03:03:04'),
(3, 5, 8, '2020-04-06', 'Topic 3', '100', '3000', 3000, '', '', 0, 3000, 1, '2020-07-24 02:43:49');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `parent_mobile` varchar(20) NOT NULL,
  `parent_email` varchar(255) NOT NULL,
  `registered_on` datetime NOT NULL,
  `enrolled_courses` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A: Active, I: Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_id`, `name`, `mobile`, `email`, `address`, `profile_pic`, `parent_name`, `parent_mobile`, `parent_email`, `registered_on`, `enrolled_courses`, `status`) VALUES
(1, 6, 'Student 1', '9496862347', 'student1@gmail.com', '', '', 'Parent name 1', '', 'parentemail1@gmail.com', '0000-00-00 00:00:00', '1', 'A'),
(2, 7, 'Student 2', '89563910234', 'student2@gmail.com', '', '', '', '', '', '0000-00-00 00:00:00', '2', 'A'),
(3, 8, 'Student 3', '8990956710', 'student3@gmail.com', '', '', '', '', '', '0000-00-00 00:00:00', '', 'A'),
(4, 9, 'AKHILA B NAIR', '9087654321', 'nair.akhilab@gmail.com', 'Puthen veettil\r\nPynkulam\r\nThrissur', '4_1401506821.jpg', 'Balakrishnan', '', 'balan@gmail.com', '2020-07-16 00:39:01', '', 'A'),
(6, 11, 'STUDENT TEST', '8976543108', 'nair.akhilab1@gmail.com', 'House name\r\nPlace name\r\nDistrict', '6_179840792.jpg', 'Parent1', '', 'parent1@gmail.com', '2020-07-16 00:55:55', '', 'A'),
(7, 12, 'RAGI K G', '8901234567', 'ragigopal@gmail.com', 'Housedbwugy\r\nctydsacyes\r\nsfcsywecf', '7_1312616630.jpg', 'Gopalan', '', 'gopalan@gmail.com', '2020-07-16 00:58:13', '', 'A'),
(8, 14, 'TEST NAME 1', '8967543210', 'testname1@gmail.com', 'Address 1\r\nPlace 1\r\nDistrict 1\r\nState 1', '8_736888708.jpg', 'Parent test name 1', '', 'parenttestname1@gmail.com', '2020-07-16 17:07:57', '', 'A'),
(9, 15, 'TEST 2', '7890123456', 'test2@gmail.com', 'Test 2 address 1\r\nplace 1\r\ndistrict 1', '9_1789628662.jpg', 'test2parent', '', 'test2parent@gmail.com', '2020-07-16 17:25:41', '', 'A'),
(10, 18, 'AKHILA TEST', '8976543210', 'akhilatest@gmail.com', 'ydfyfgetsy\r\nfcsdytvyfc\r\nfcdvsygv', '10_362665171.jpg', 'parent test', '', 'parenttesst@gmail.com', '2020-07-20 21:56:18', '', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `students_enrolled`
--

CREATE TABLE `students_enrolled` (
  `id` bigint(20) NOT NULL,
  `student_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `status` char(1) NOT NULL COMMENT 'R: Registered, A: Active, I: Suspended',
  `admitted_date` datetime DEFAULT NULL,
  `payment_types` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT 'Separated by commas',
  `payment_ids` longtext NOT NULL COMMENT 'Separated by commas',
  `payment_amount` longtext NOT NULL COMMENT 'Separated by commas',
  `payment_dates` longtext NOT NULL COMMENT 'Separated by commas',
  `paid_fee` int(11) NOT NULL,
  `balance_fee` int(11) NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students_enrolled`
--

INSERT INTO `students_enrolled` (`id`, `student_id`, `course_id`, `status`, `admitted_date`, `payment_types`, `payment_ids`, `payment_amount`, `payment_dates`, `paid_fee`, `balance_fee`, `updated_datetime`) VALUES
(1, 1, 1, 'A', '2020-07-20 00:00:00', 'Online,DD', '1,6', '2500,2000', '2020-07-20,2020-07-22', 4500, 500, '2020-07-22 23:02:22'),
(2, 2, 2, 'I', '2020-07-22 22:41:35', 'Cheque', '7', '1500', '2020-07-22', 1500, 3500, '2020-07-22 23:05:22'),
(3, 3, 2, 'R', NULL, '', '', '', '', 0, 5000, '2020-07-22 03:27:12'),
(8, 4, 8, 'R', NULL, '', '', '', '', 0, 5000, '2020-07-29 04:36:31');

-- --------------------------------------------------------

--
-- Table structure for table `tutors`
--

CREATE TABLE `tutors` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name_title` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `degrees` varchar(255) NOT NULL,
  `profile` text NOT NULL,
  `demo_video` varchar(100) NOT NULL,
  `course_assigned` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A: Active, I: Inactive',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tutors`
--

INSERT INTO `tutors` (`id`, `user_id`, `name_title`, `name`, `address`, `phone_number`, `email`, `profile_pic`, `degrees`, `profile`, `demo_video`, `course_assigned`, `status`, `added_by`, `added_on`) VALUES
(1, 2, 'MR.', 'Tutor 1', 'Address 1\r\nAddress 2', '8089898989', 'tutor1@gmail.com', '1.jpg', 'MCA', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '1.mp4', '1,2,3,5,7,8,10,11', 'A', 1, '2020-07-30 00:18:29'),
(2, 3, 'MR.', 'Vinay Nandagopal', 'Address 2 main,address 2', '9090909090', 'tutor2@gmail.com', '2.jpg', 'B.Com', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '2.mp4', '1,4,10', 'A', 1, '2020-07-29 23:36:37'),
(3, 4, 'MR.', 'Tutor 3', 'Address 2 main,address 2', '7865123456', 'tutor3@gmail.com', '3.jpg', 'BA Electronics', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '3_825287555.mp4', '1,2,5,7', 'I', 1, '2020-07-16 00:34:09'),
(4, 5, 'MS.', 'MARIAM JAMES', 'House name 1\r\nPlace name1\r\nDistrict\r\nState\r\n678934', '7890203846', 'mariamjames@gmail.com', '4_771756804.jpg', 'B.Tech in IT, MBA', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '4_771756804.mp4', '6,9', 'A', 1, '2020-07-15 02:21:25'),
(5, 13, 'MR.', 'Ranjith', 'House name\r\nPlace\r\nDistrict\r\nState', '8907654321', 'ranjith@gmail.com', '5_875971273.jpg', 'MCA', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '5_875971273.mp4', '5,6,8,12', 'A', 1, '2020-07-16 01:01:14'),
(6, 16, 'MS.', 'AKHILA B NAIR', 'vytvgygvyugbu', '8906754321', 'akhila@gmail.com', '6_180296825.jpg', 'M.Tech', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '6_180296825.mp4', '8,11', 'A', 1, '2020-07-20 16:15:27'),
(7, 17, 'MR.', 'Tester tutor', 'fyubvurdbvu\r\nfbgfubf\r\ndfbfubhuj', '7890123456', 'testertutor@gmail.com', '7_1442292419.jpg', 'BCA', '<p><span style=\"font-size:18px\"><strong>Professional Summary</strong></span></p>\r\n\r\n<p style=\"text-align:justify\">Knowledgeable and experienced History Professor looking to obtain a position in which to impact students and improve the college experience.</p>\r\n\r\n<p><strong><span style=\"font-size:18px\">Core Qualifications</span></strong></p>\r\n\r\n<ul>\r\n	<li style=\"text-align:justify\">Extremely knowledgeable in all history including US and other regions of the world</li>\r\n	<li style=\"text-align:justify\">Great ethics to be exemplified in the classroom</li>\r\n	<li style=\"text-align:justify\">Understanding of different teaching strategies and techniques and how to implement them in the classroom</li>\r\n	<li style=\"text-align:justify\">Excellent organizational skills needed to maintain order and accurate records</li>\r\n	<li style=\"text-align:justify\">Familiar with college requirements and curriculum in each class</li>\r\n	<li style=\"text-align:justify\">Excellent at incorporating modern technology into material covered</li>\r\n</ul>\r\n\r\n<p><span style=\"font-size:18px\"><strong>Experience</strong></span></p>\r\n\r\n<ol>\r\n	<li>History Professor</li>\r\n</ol>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li>6/1/2003 &ndash; Present</li>\r\n	<li>University of Alabama, Hunstville, AL</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ul style=\"margin-left:40px\">\r\n	<li style=\"text-align: justify;\">Create course requirements for students to follow</li>\r\n	<li style=\"text-align: justify;\">Develop examinations and writing assignments to monitor students├&ograve; progress and understanding of material</li>\r\n	<li style=\"text-align: justify;\">Relate historical concepts to current events and situations to help students develop an appreciation for history</li>\r\n	<li style=\"text-align: justify;\">Collaborate with other History Professors to determine the best curriculum and program for the reputation of the college</li>\r\n	<li style=\"text-align: justify;\">Oversee college placement testing for the history program</li>\r\n</ul>', '7_1442292419.mp4', '9', 'A', 1, '2020-07-20 16:24:16'),
(8, 19, 'MS.', 'Amaya Wilson', 'Amaya Nivas\r\nPlace', '8976431096', 'amayawilson@gmail.com', '8_544784857.jpg', 'BA English', '<p>jdfuerfu</p>', '8_544784857.mp4', '', 'A', 1, '2020-07-30 04:03:08');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `group_id` int(11) NOT NULL,
  `email_verified` char(1) NOT NULL COMMENT '1: Verified, 0: Not Verified',
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `name`, `email`, `password`, `group_id`, `email_verified`, `status`) VALUES
(1, 'admin', 'Admin', 'admin@admin.com', '466d3c5626cb11cdb45e30134364dba1b1faa12a', 1, '1', 'A'),
(2, 'tutor1@gmail.com', 'Tutor 1', 'tutor1@gmail.com', 'aabed7182b7ae9c7e3106686c46ae079abb7d34e', 3, '1', 'A'),
(3, 'vinaynandagopal@gmail.com', 'Vinay Nandagopal', 'vinaynandagopal@gmail.com', 'f0ee3e076eb877870dce470043044db9dcd33645', 3, '1', 'A'),
(4, 'tutor3@gmail.com', 'Tutor 3', 'tutor3@gmail.com', '9f9fb5835e0d0f52212d5005dbbc587a44540e03', 3, '1', 'I'),
(5, 'mariamjames@gmail.com', 'MARIAM JAMES', 'mariamjames@gmail.com', '2306f2e4fe88ea97241d61b313b5e98c6adb5648', 3, '1', 'A'),
(6, 'student1@gmail.com', 'Student 1', 'student1@gmail.com', 'e426f6981c73a10318019cb98aed3f5dbd226dc6', 4, '1', 'A'),
(7, 'student2@gmail.com', 'Student 2', 'student2@gmail.com', 'b806f58993c4a150027e0c3073977b65cad0ff9b', 4, '1', 'A'),
(8, 'student3@gmail.com', 'Student 3', 'student3@gmail.com', 'b812d0a9e421b99326f8d39ff37ca9ab52a0f0fe', 4, '1', 'A'),
(9, 'nair.akhilab@gmail.com', 'AKHILA B NAIR', 'nair.akhilab@gmail.com', 'f0ee3e076eb877870dce470043044db9dcd33645', 4, '1', 'A'),
(11, 'nair.akhilab1@gmail.com', 'Student test', 'nair.akhilab1@gmail.com', 'e426f6981c73a10318019cb98aed3f5dbd226dc6', 4, '0', 'A'),
(12, 'ragigopal@gmail.com', 'RAGI K G', 'ragigopal@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '1', 'A'),
(13, 'ranjith@gmail.com', 'Ranjith', 'ranjith@gmail.com', 'f00b9747f773fdd52f27d63ddf046ad77331445a', 3, '1', 'A'),
(14, 'testname1@gmail.com', 'Test name 1', 'testname1@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '1', 'A'),
(15, 'test2@gmail.com', 'Test 2', 'test2@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '1', 'A'),
(16, 'akhila@gmail.com', 'AKHILA B NAIR', 'akhila@gmail.com', '395253eac65b8acf9856bfd0ca46cbe942201c27', 3, '1', 'A'),
(17, 'testertutor@gmail.com', 'Tester tutor', 'testertutor@gmail.com', '3cb37354affbff6a9e92a1e3315f628913213a84', 3, '1', 'A'),
(18, 'akhilatest@gmail.com', 'Akhila test', 'akhilatest@gmail.com', '387e02a5767d7b126c927c5c75d06a17997f0f0f', 4, '0', 'A'),
(19, 'amayawilson@gmail.com', 'Amaya Wilson', 'amayawilson@gmail.com', '0bed04c662661d74367e1c2326746d058d0bfa33', 3, '1', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `sort_order`, `status`) VALUES
(1, 'Developer', 1, 'A'),
(2, 'Admin', 2, 'A'),
(3, 'Tutor', 3, 'A'),
(4, 'Student', 4, 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `class_schedules`
--
ALTER TABLE `class_schedules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `enrollments`
--
ALTER TABLE `enrollments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fee_structure`
--
ALTER TABLE `fee_structure`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `forum_threads`
--
ALTER TABLE `forum_threads`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_table`
--
ALTER TABLE `log_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key` (`key`);

--
-- Indexes for table `permission_map`
--
ALTER TABLE `permission_map`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_payment`
--
ALTER TABLE `staff_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_enrolled`
--
ALTER TABLE `students_enrolled`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tutors`
--
ALTER TABLE `tutors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `class_schedules`
--
ALTER TABLE `class_schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `enrollments`
--
ALTER TABLE `enrollments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fee_structure`
--
ALTER TABLE `fee_structure`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT for table `forum_threads`
--
ALTER TABLE `forum_threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `log_table`
--
ALTER TABLE `log_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permission_map`
--
ALTER TABLE `permission_map`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `staff_payment`
--
ALTER TABLE `staff_payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `students_enrolled`
--
ALTER TABLE `students_enrolled`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tutors`
--
ALTER TABLE `tutors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
