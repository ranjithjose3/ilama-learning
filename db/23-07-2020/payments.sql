-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2020 at 11:35 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `id` int(11) NOT NULL,
  `enrollment_id` int(11) NOT NULL,
  `payment_mode` varchar(10) NOT NULL,
  `transaction_id` varchar(50) NOT NULL,
  `transaction_status` varchar(20) NOT NULL,
  `transaction_amount` float NOT NULL,
  `transaction_date` datetime NOT NULL,
  `acknowledge` char(1) NOT NULL,
  `cheque_no` varchar(50) NOT NULL,
  `dd_no` varchar(20) NOT NULL,
  `others` varchar(50) NOT NULL,
  `bank_name` varchar(255) NOT NULL,
  `updated_datetime` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `payments`
--

INSERT INTO `payments` (`id`, `enrollment_id`, `payment_mode`, `transaction_id`, `transaction_status`, `transaction_amount`, `transaction_date`, `acknowledge`, `cheque_no`, `dd_no`, `others`, `bank_name`, `updated_datetime`) VALUES
(1, 1, 'Online', '', 'Success', 2500, '2020-07-20 06:18:13', 'A', '', '', '', '', '2020-07-20 06:19:09'),
(6, 1, 'DD', '', 'Success', 2000, '2020-07-22 03:27:12', 'A', '', 'SBT45FG34', '', '', '2020-07-22 03:27:12'),
(7, 2, 'Cheque', '', 'Success', 1500, '2020-07-22 19:23:30', 'A', 'FDRL1244F', '', '', '', '2020-07-22 19:23:30');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
