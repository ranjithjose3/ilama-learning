-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2020 at 11:34 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `tutors`
--

CREATE TABLE `tutors` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name_title` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `phone_number` varchar(15) NOT NULL,
  `email` varchar(255) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `degrees` varchar(255) NOT NULL,
  `profile` text NOT NULL,
  `demo_video` varchar(100) NOT NULL,
  `course_assigned` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A: Active, I: Inactive',
  `added_by` int(11) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tutors`
--

INSERT INTO `tutors` (`id`, `user_id`, `name_title`, `name`, `address`, `phone_number`, `email`, `profile_pic`, `degrees`, `profile`, `demo_video`, `course_assigned`, `status`, `added_by`, `added_on`) VALUES
(1, 2, 'MR.', 'Tutor 1', 'Address 1\r\nAddress 2', '8089898989', 'tutor1@gmail.com', '1.jpg', 'MCA', '<p>Tutor 1 profile goes here.</p>', '1.mp4', '1,2,3,5,7,8', 'A', 1, '2020-07-15 18:34:24'),
(2, 3, 'MR.', 'Tutor 2', 'Address 2 main,address 2', '9090909090', 'tutor2@gmail.com', '2.jpg', 'B.Com', '<p>Tutor 2 profile is here</p>', '2.mp4', '1,4', 'A', 1, '2020-07-16 00:34:05'),
(3, 4, 'MR.', 'Tutor 3', 'Address 2 main,address 2', '7865123456', 'tutor3@gmail.com', '3.jpg', 'BA Electronics', '<p>Tutor 3 profile.&nbsp;Tutor 3 profile.&nbsp;Tutor 3 profile.&nbsp;Tutor 3 profile.Tutor 3 profile.&nbsp;Tutor 3 profile.</p>', '3_825287555.mp4', '1,2,5,7', 'I', 1, '2020-07-16 00:34:09'),
(4, 5, 'MS.', 'MARIAM JAMES', 'House name 1\r\nPlace name1\r\nDistrict\r\nState\r\n678934', '7890203846', 'mariamjames@gmail.com', '4_771756804.jpg', 'B.Tech in IT, MBA', '<p style=\"text-align:justify\">Goal-oriented customer service rep with 7+ years of experience. Seeking to use proven telesales skills to raise customer satisfaction at Triple-P Components. Received 98% favorable customer review scores at Gibbs-Atalah Electronics. Customer retention for repeat clients was 35% above facility average. At Fireberry communications, received Employee of the Month Award 4x.</p>', '4_771756804.mp4', '6', 'A', 1, '2020-07-15 02:21:25'),
(5, 13, 'MR.', 'Ranjith', 'House name\r\nPlace\r\nDistrict\r\nState', '8907654321', 'ranjith@gmail.com', '5_875971273.jpg', 'MCA', '<p style=\"text-align:justify\">My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.My profile comes here.</p>', '5_875971273.mp4', '5,6,8', 'A', 1, '2020-07-16 01:01:14'),
(6, 16, 'MS.', 'AKHILA B NAIR', 'vytvgygvyugbu', '8906754321', 'akhila@gmail.com', '6_180296825.jpg', 'M.Tech', '<p>ysyufgbcy</p>', '6_180296825.mp4', '8', 'A', 1, '2020-07-20 16:15:27'),
(7, 17, 'MR.', 'Tester tutor', 'fyubvurdbvu\r\nfbgfubf\r\ndfbfubhuj', '7890123456', 'testertutor@gmail.com', '7_1442292419.jpg', 'BCA', '<p>evwyfvysubgfvudh</p>', '', '', 'A', 1, '2020-07-20 16:24:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tutors`
--
ALTER TABLE `tutors`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tutors`
--
ALTER TABLE `tutors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
