-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2020 at 11:35 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mobile` varchar(12) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(500) NOT NULL,
  `profile_pic` varchar(100) NOT NULL,
  `parent_name` varchar(255) NOT NULL,
  `parent_email` varchar(255) NOT NULL,
  `registered_on` datetime NOT NULL,
  `enrolled_courses` varchar(255) NOT NULL,
  `status` char(1) NOT NULL DEFAULT 'A' COMMENT 'A: Active, I: Inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `user_id`, `name`, `mobile`, `email`, `address`, `profile_pic`, `parent_name`, `parent_email`, `registered_on`, `enrolled_courses`, `status`) VALUES
(1, 6, 'Student 1', '9496862347', 'student1@gmail.com', '', '', 'Parent name 1', 'parentemail1@gmail.com', '0000-00-00 00:00:00', '1', 'A'),
(2, 7, 'Student 2', '89563910234', 'student2@gmail.com', '', '', '', '', '0000-00-00 00:00:00', '2', 'A'),
(3, 8, 'Student 3', '8990956710', 'student3@gmail.com', '', '', '', '', '0000-00-00 00:00:00', '', 'A'),
(4, 9, 'AKHILA B NAIR', '9087654321', 'nair.akhilab@gmail.com', 'Puthen veettil\r\nPynkulam\r\nThrissur', '4_1401506821.jpg', 'Balakrishnan', 'balan@gmail.com', '2020-07-16 00:39:01', '', 'A'),
(6, 11, 'STUDENT TEST', '8976543108', 'nair.akhilab1@gmail.com', 'House name\r\nPlace name\r\nDistrict', '6_179840792.jpg', 'Parent1', 'parent1@gmail.com', '2020-07-16 00:55:55', '', 'A'),
(7, 12, 'RAGI K G', '8901234567', 'ragigopal@gmail.com', 'Housedbwugy\r\nctydsacyes\r\nsfcsywecf', '7_1312616630.jpg', 'Gopalan', 'gopalan@gmail.com', '2020-07-16 00:58:13', '', 'A'),
(8, 14, 'TEST NAME 1', '8967543210', 'testname1@gmail.com', 'Address 1\r\nPlace 1\r\nDistrict 1\r\nState 1', '8_736888708.jpg', 'Parent test name 1', 'parenttestname1@gmail.com', '2020-07-16 17:07:57', '', 'A'),
(9, 15, 'TEST 2', '7890123456', 'test2@gmail.com', 'Test 2 address 1\r\nplace 1\r\ndistrict 1', '9_1789628662.jpg', 'test2parent', 'test2parent@gmail.com', '2020-07-16 17:25:41', '', 'A'),
(10, 18, 'AKHILA TEST', '8976543210', 'akhilatest@gmail.com', 'ydfyfgetsy\r\nfcsdytvyfc\r\nfcdvsygv', '10_362665171.jpg', 'parent test', 'parenttesst@gmail.com', '2020-07-20 21:56:18', '', 'A');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
