-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 23, 2020 at 11:34 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ilama_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `staff_payment`
--

CREATE TABLE `staff_payment` (
  `id` bigint(20) NOT NULL,
  `tutor_id` bigint(20) NOT NULL,
  `course_id` bigint(20) NOT NULL,
  `dates` longtext NOT NULL,
  `topic` longtext NOT NULL,
  `no_of_students` longtext NOT NULL,
  `tution_fees` longtext NOT NULL,
  `total` int(11) NOT NULL,
  `released_dates` longtext NOT NULL,
  `released_amounts` longtext NOT NULL,
  `total_released` int(11) NOT NULL,
  `balance` int(11) NOT NULL,
  `added_by` bigint(20) NOT NULL,
  `added_on` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `staff_payment`
--

INSERT INTO `staff_payment` (`id`, `tutor_id`, `course_id`, `dates`, `topic`, `no_of_students`, `tution_fees`, `total`, `released_dates`, `released_amounts`, `total_released`, `balance`, `added_by`, `added_on`) VALUES
(2, 6, 8, '2020-04-01,2020-04-05,2020-04-06', 'Topic 1,Topic 2,Topic 2', '50,100,40', '1500,3000,1200', 5700, '2020-07-16,2020-07-21', '2500,500', 3000, 2700, 1, '2020-07-24 03:03:04'),
(3, 5, 8, '2020-04-06', 'Topic 3', '100', '3000', 3000, '', '', 0, 3000, 1, '2020-07-24 02:43:49');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `staff_payment`
--
ALTER TABLE `staff_payment`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `staff_payment`
--
ALTER TABLE `staff_payment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
